let myVue = new Vue({
    el:'#myVue',
    data:{
        mainList: {data:[]},
        show_spinner:false,
        EF: get_project, //Edit Form
        imagesArr: get_ProjectImages,
        btn_submit: false,
    },
    mounted(){
        // this.getResults();
        $('#my_form').validate();
    },//End mounted()
    methods:{
        Preview_image(e)
        {
           let from = e.currentTarget.getAttribute('data-from');
           if (e.target.files && e.target.files[0]) //Preview_image
           {
                 if(from == 'create') {
                     $('#Preview_image_create').attr('src',URL.createObjectURL(e.target.files[0]) );
                 }
                 else if(from == 'edit') {
                     $('#Preview_image_edit').attr('src',URL.createObjectURL(e.target.files[0]) );
                 }
                 else if(from == 'addImage') {
                     $(e.currentTarget).closest('tr').find('img').attr('src', URL.createObjectURL(e.target.files[0]) );
                 }
           }
        },
        addNewImage()
        {
            this.imagesArr.push({ image:'',id: Math.random() });
        },
        remove_Image(index){
          this.imagesArr.splice(index,1);
        },
        do_submit()
        {
           if( ('#my_form').valid() )
           {
             this.show_spinner = true;
             this.btn_submit = true;
           }
        }

    }//End methods
});
