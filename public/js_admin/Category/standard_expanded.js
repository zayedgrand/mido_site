let myVue = new Vue({
    el:'#myVue',
    data:{
        mainList: {data:[]},
        show_spinner:false,
        EF: get_project, //Edit Form
        catsArr: get_ProjectImages,
        btn_submit: false,
    },
    mounted(){
        // this.getResults();
        $('#my_form').validate();
    },//End mounted()
    methods:{
        Preview_image(e)
        {
           let from = e.currentTarget.getAttribute('data-from');
           if (e.target.files && e.target.files[0]) //Preview_image
           {
                 if(from == 'create') {
                     $('#Preview_image_create').attr('src',URL.createObjectURL(e.target.files[0]) );
                 }
                 else if(from == 'edit') {
                     $('#Preview_image_edit').attr('src',URL.createObjectURL(e.target.files[0]) );
                 }
                 else if(from == 'addImage') { console.log('addImage');
                     $(e.currentTarget).closest('tr').find('img').attr('src', URL.createObjectURL(e.target.files[0]) );
                 }
           }
        },
        Preview_cat_image(e,arr,lang)
        {
            let from = e.currentTarget.getAttribute('data-from');
            if (e.target.files && e.target.files[0]) //Preview_image
            {
               $(e.currentTarget).closest('.form-group').find('img').attr('src', URL.createObjectURL(e.target.files[0]) );
               if(lang == 'en'){
                 arr.banner_en = URL.createObjectURL(e.target.files[0]);
               }
               else if(lang == 'ar'){
                 arr.banner_ar = URL.createObjectURL(e.target.files[0]);
               }
            }
        },
        addNewImage()
        {
            this.catsArr.push({name_en:'',name_ar:'',cc:'cc_'+Math.random() });
        },
        remove_categoiry(arr,index)
        {
            if(!arr.id){
              this.catsArr.splice(index,1);
            }
            else
            {
                showDeleteMessage(arr.name_en,delete_cat_api+'/'+arr.id).then(()=>{
                    myVue.catsArr.splice(index,1);
                });
            }
        },
        do_submit()
        {
           if( ('#my_form').valid() )
           {
             this.show_spinner = true;
             this.btn_submit = true;
           }
        },
        add_Cat_to_home_page(arr)
        {
            $.get(cat_in_HomePage_api+'/'+arr.id ,(responce)=>{
                 new Noty({text: 'category added to the home page', layout: 'topRight', type: 'success',timeout: 2000  }).show();
            });
        },
        switchinHomePage(id)
        {
            $.get(subCat_in_HomePage_api+'/'+id ,(responce)=>{
              let find = myVue.mainList.data.find(obj => obj.id == id);
                 // find.in_home_page = responce.case;
                 if(responce.case){
                   new Noty({text: 'In home page', layout:'topRight',type:'success',timeout: 2000 }).show();
                 }
                 else {
                   new Noty({text: 'hide from home page' ,layout:'topRight',type:'error',timeout: 2000 }).show();
                 }
            });
        },
    }//End methods
});
