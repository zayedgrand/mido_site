let myVue = new Vue({
    el:'#myVue',
    data:{
        mainList: {data:[]},
        show_spinner:true,
        action_spinner:false,
        EF: {}, //Edit Form
        btn_submit: false,
        searchArr_1: set_searchArr_1,
        searchArr_2: set_searchArr_2,
        searchArr_3: set_searchArr_3,
    },
    mounted(){
        this.getResults();
        $('#create_form').validate();
        $('#edit_form').validate();
    },//End mounted()
    methods:{
         getResults(page = 1){
           this.mainList = {data:[]};
           this.show_spinner = true;

            $.post(get_list+'?page=' + page ,$('#search_form').serializeArray(),(Response)=>{
                myVue.mainList = Response;
                myVue.show_spinner = false;
                    $( ".row_sortable" ).sortable({
                          delay: 150,
                          stop: function() {
                              var selectedData = new Array();
                              $('.row_sortable>tr').each(function() {
                                  selectedData.push($(this).data("id"));
                              });
                              myVue.updateRowsPosition(selectedData);
                          }
                      });//End sortable()
            });//End $.post
         },
         updateRowsPosition(arrayList)
         {
            $.post(updateRowsPosition_api ,{postionArray:arrayList,_token:const_token},(Response)=>{
                if(Response.status == 'success'){
                  new Noty({text: ' Rearranged', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                }
                else {
                  new Noty({text: 'problem in Rearranged please try agine' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                }
            });//End $.post
         },
         DeleteMessage(id,index){
              showDeleteMessage(id,delete_api+'/'+id).then(()=>{
                  myVue.mainList.data.splice(index,1);
              });
         },
         showORhide(id){
             $.get(showORhide_api+'/'+id ,(responce)=>{
               let find = myVue.mainList.data.find(obj => obj.id == id);
                  find.status = responce.case;
                  find.is_contacted = responce.case;
                  if(responce.case){
                    new Noty({text: 'Content has been shown in the site', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                  }
                  else {
                    new Noty({text: 'Content now is hidden in the site' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                  }
             });
         },
         showORhide_no_paginate(id){
             $.get(showORhide_api+'/'+id ,(responce)=>{
               let find = myVue.mainList.find(obj => obj.id == id);
                  find.status = responce.case;
                  find.is_contacted = responce.case;
                  if(responce.case){
                    new Noty({text: 'Content has been shown in the site', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                  }
                  else {
                    new Noty({text: 'Content now is hidden in the site' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                  }
             });
         },
        diffforhumans(data)
        {
            moment.locale('en');
            if (data) {
              return  moment(data).fromNow();
            }
        }


    }//End methods
});
