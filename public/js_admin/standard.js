let myVue = new Vue({
    el:'#myVue',
    data:{
        mainList: {data:[]},
        show_spinner:true,
        action_spinner:false,
        EF: {}, //Edit Form
        btn_submit: false,
        searchArr_1: set_searchArr_1,
        searchArr_2: set_searchArr_2,
        searchArr_3: set_searchArr_3,
    },
    mounted(){
        this.getResults();
        $('#create_form').validate();
        $('#edit_form').validate();
    },//End mounted()
    methods:{
         getResults(page = 1){
           this.mainList = {data:[]};
           this.show_spinner = true;

            $.post(get_list+'?page=' + page ,$('#search_form').serializeArray(),(Response)=>{
                myVue.mainList = Response;
                myVue.show_spinner = false;
            });
         },
         DeleteMessage(id,index){
              showDeleteMessage(id,delete_api+'/'+id).then(()=>{
                  myVue.mainList.data.splice(index,1);
              });
         },
         showCreateModel(){
            $('#create_model').modal('show');
         },
         showEditModel(this_data){
              $('#edit_model').modal('show');
              $("#edit_model input[type=file]").val(''); 
              this.EF = this_data;
         },
         showORhide(id){
             $.get(showORhide_api+'/'+id ,(responce)=>{
               let find = myVue.mainList.data.find(obj => obj.id == id);
                  find.status = responce.case;
                  find.is_contacted = responce.case;
                  if(responce.case){
                    new Noty({text: 'Content has been shown in the site', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                  }
                  else {
                    new Noty({text: 'Content now is hidden in the site' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                  }
             });
         },
         ContatctedOrNot(id){
             $.get(ContatctedOrNot_api+'/'+id ,(responce)=>{
               let find = myVue.mainList.data.find(obj => obj.id == id);
                  find.status = responce.case;
                  find.is_contacted = responce.case;
                  if(responce.case){
                    new Noty({text: 'Client has been contacted', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                  }
                  else {
                    new Noty({text: 'Client is not contacted' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                  }
             });
         },
         create()
         {
               if( $('#create_form').valid() )
               {
                   this.btn_submit = true;
                   this.action_spinner = true;
                   let my_formData = new FormData($('#create_form')[0]);
                   // Attach file
                   if($('#create_image').length > 0){
                      my_formData.append('image', $('#create_image')[0].files[0]);  console.log('create_form');
                   }

                   $.ajax({
                       type:"post",
                       url: create_api,
                       data: my_formData,
                       processData: false,
                       contentType: false,
                       success : function(responce){
                           if(responce.status == 'success')
                                myVue.mainList.data.unshift(responce.data);
                           $('#create_model').modal('hide');
                           myVue.btn_submit = false;
                           myVue.action_spinner = false;
                           new Noty({ text: 'Content has created', layout: 'topRight', type: 'success',timeout: 1500  }).show();
                           $('#create_form').trigger("reset");
                           $('#create_form').find('img').prop('src','');
                       },
                   });//End ajax
               }
         },
         edit(reload_table=false)
         {
              if( $('#edit_form').valid() )
              {
                  this.btn_submit = true;
                  this.action_spinner = true;
                  let my_formData = new FormData($('#edit_form')[0]);
                  // Attach file
                  if($('#edit_image').length > 0){
                    my_formData.append('image', $('#edit_image')[0].files[0]);
                  }

                  $.ajax({
                      type:"post",
                      url: update_api,
                      data: my_formData,
                      processData: false,
                      contentType: false,
                      success : function(responce){
                          if(responce.status == 'success')
                          {
                               if(reload_table){
                                  myVue.getResults();
                               }
                               else
                               {
                                   let find = myVue.mainList.data.find(obj => obj.id == responce.data.id);
                                   find.image = responce.data.image;
                                   find = responce.data;
                               }
                          }
                          $('#edit_model').modal('hide');
                          myVue.btn_submit = false;
                          myVue.action_spinner = false;
                          new Noty({text: 'Content has edited', layout: 'topRight', type: 'success',timeout: 1500  }).show();
                      },
                  });//End ajax
              }
         },
         Preview_image(e)
        {
           let from = e.currentTarget.getAttribute('data-from');
           if (e.target.files && e.target.files[0]) //Preview_image
           {
                 if(from == 'create') {
                     $('#Preview_image_create').attr('src',URL.createObjectURL(e.target.files[0]) );
                 }
                 else if(from == 'edit') {
                     $('#Preview_image_edit').attr('src',URL.createObjectURL(e.target.files[0]) );
                 }
           }
        },
        diffforhumans(data)
        {
            moment.locale('en');
            if (data) {
              return  moment(data).fromNow();
            }
        }


    }//End methods
});
