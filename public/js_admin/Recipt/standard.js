let myVue = new Vue({
    el:'#myVue',
    data:{
        mainList: {data:[]},
        show_spinner:true,
        action_spinner:false,
        btn_submit: false,
        set_searchArr_1: set_searchArr_1,
        set_searchArr_2: set_searchArr_2
    },
    mounted(){
        this.getResults();
        $('#create_form').validate();
        $('#edit_form').validate();
    },//End mounted()
    methods:{
         getResults(page = 1){

           this.mainList = {data:[]};
           this.show_spinner = true;

            $.post(get_list+'?page=' + page ,$('#search_form').serializeArray(),(Response)=>{
                myVue.mainList = Response;
                myVue.show_spinner = false;
            });
         },
         DeleteMessage(id,index){
              showDeleteMessage(id,delete_api+'/'+id).then(()=>{
                  myVue.mainList.data.splice(index,1);
              });
         }, 
         switchPiad(id){
             $.get(switchPiad_api+'/'+id ,(responce)=>{
               let find = myVue.mainList.data.find(obj => obj.id == id);
                  find.is_piad = responce.case;
                  if(responce.case){
                    new Noty({text: 'Recipt is Paid', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                  }
                  else {
                    new Noty({text: 'Recipt is not Paid' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                  }
             });
          },
          changeDeliveryStatus(id,status)
          {
              $.get(`${changeDeliveryStatus_api}/${id}/${status}` ,(responce)=>{
                let find = myVue.mainList.data.find(obj => obj.id == id);
                   find.delivery_status = responce.case;
                   if(responce.case){
                     new Noty({text: 'Delivered status changed ', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                   }
                   else {
                     new Noty({text: 'Delivered status problem' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                   }
              });
          },
          diffforhumans(data)
          {
              moment.locale('en');
              if (data) {
                return  moment(data).fromNow();
              }
          }
    },//End methods
});
