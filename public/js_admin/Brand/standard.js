let myVue = new Vue({
    el:'#myVue',
    data:{
        mainList: {data:[]},
        show_spinner:true,
        action_spinner:false,
        EF: {}, //Edit Form
        btn_submit: false,
        categoiry_subCategory: get_categoiry_subCategory,
        show_page: false,
        Category_id: null,
        SubCategory_id: null,
    },
    mounted(){
        this.getResults();
        $('#create_form').validate();
        $('#edit_form').validate();

    },//End mounted()
    methods:{
         getResults(page = 1)
         {
            this.Category_id = $('#Category_id').val();
            this.SubCategory_id = $('#SubCategory_id').val();
            this.mainList = {data:[]};
            this.show_spinner = true;
            this.show_page = false;

            if( this.SubCategory_id )
            {
                this.show_page = true;
                // $.post(get_list+'?page=' + page ,$('#search_form').serializeArray(),(Response)=>{
                $.post(get_list+'?page=' + page ,{
                    Category_id: $('#search_form [name="Category_id"]').val(),
                    SubCategory_id: $('#search_form [name="SubCategory_id"]').val(),
                    search: $('#inp_search').val(),
                    _token: const_token,
                },(Response)=>{
                    myVue.mainList = Response;
                    myVue.show_spinner = false;
                });
            }
         },
         CategoryChanged()
         {
            $('#SubCategory_id').val('');
            this.getResults();
         },
         DeleteMessage(id,index){
              showDeleteMessage(id,delete_api+'/'+id).then(()=>{
                  myVue.mainList.data.splice(index,1);
              });
         },
         showCreateModel(){
            $('#create_model').modal('show');
         },
         showEditModel(this_data){
              $('#edit_model').modal('show');
              this.EF = this_data;
         },
         showORhide(id){
             $.get(showORhide_api+'/'+id ,(responce)=>{
               let find = myVue.mainList.data.find(obj => obj.id == id);
                  find.status = responce.case;
                  find.is_contacted = responce.case;
                  if(responce.case){
                    new Noty({text: 'Content has been shown in the site', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                  }
                  else {
                    new Noty({text: 'Content now is hidden in the site' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                  }
             });
         },
         create()
         {
               if( $('#create_form').valid() )
               {
                   this.btn_submit = true;
                   this.action_spinner = true;
                   let my_formData = new FormData($('#create_form')[0]);
                   // Attach file
                   if($('#create_image').length > 0){
                      my_formData.append('image', $('#create_image')[0].files[0]);  console.log('create_form');
                   }

                   $.ajax({
                       type:"post",
                       url: create_api,
                       data: my_formData,
                       processData: false,
                       contentType: false,
                       success : function(responce){
                           if(responce.status == 'success')
                                myVue.mainList.data.unshift(responce.data);
                           $('#create_model').modal('hide');
                           myVue.btn_submit = false;
                           myVue.action_spinner = false;
                           new Noty({ text: 'Content has created', layout: 'topRight', type: 'success',timeout: 1500  }).show();
                           $('#create_form').trigger("reset");
                           $('#create_form').find('img').prop('src','');
                       },
                   });//End ajax
               }
         },
         edit()
         {
              if( $('#edit_form').valid() )
              {
                  this.btn_submit = true;
                  this.action_spinner = true;
                  let my_formData = new FormData($('#edit_form')[0]);
                  // Attach file
                  if($('#edit_image').length > 0){
                    my_formData.append('image', $('#edit_image')[0].files[0]);
                  }

                  $.ajax({
                      type:"post",
                      url: update_api,
                      data: my_formData,
                      processData: false,
                      contentType: false,
                      success : function(responce){
                          if(responce.status == 'success')
                          {
                             let find = myVue.mainList.data.find(obj => obj.id == responce.data.id);
                             find.banner_en = responce.data.banner_en;
                             find.banner_ar = responce.data.banner_ar;
                             find = responce.data;
                          }
                          $('#edit_model').modal('hide');
                          myVue.btn_submit = false;
                          myVue.action_spinner = false;
                          new Noty({text: 'Content has edited', layout: 'topRight', type: 'success',timeout: 1500  }).show();
                      },
                  });//End ajax
              }
         },
         switchinHomePage(id)
         {
             $.get(switchinHomePage_api+'/'+id ,(responce)=>{
               let find = myVue.mainList.data.find(obj => obj.id == id);
                  find.in_home_page = responce.case;
                  if(responce.case){
                    new Noty({text: 'In home page', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                  }
                  else {
                    new Noty({text: 'hide from home page' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                  }
             });
         },
         Preview_image(e)
        {
           if (e.target.files && e.target.files[0]) //Preview_image
           {
             $(e.currentTarget).closest('.form-group').find('img').attr('src',URL.createObjectURL(e.target.files[0]) );
           }
        },
        diffforhumans(data)
        {
            moment.locale('en');
            if (data) {
              return  moment(data).fromNow();
            }
        },


    },//End methods
    computed:{
        subCategories()
        {
            var find = this.categoiry_subCategory.find(obj=>obj.value == this.Category_id);
            if(find){
              return find.subCategoiry;
            }
        }
    }//End computed
});
