let myVue = new Vue({
    el:'#myVue',
    data:{
        mainList: {data:[]},
        show_spinner:true,
        action_spinner:false,
        EF: {}, //Edit Form
        btn_submit: false,

        Categories_and_sub: get_Categories_and_sub,
        brand_id: null,
        Categoy_id: '',
        subCategory_id: '',
        subCategoires_list: [],
        Brand_list: []
    },
    mounted(){
        this.getResults();
        $('#create_form').validate();
        $('#edit_form').validate();
    },//End mounted()
    methods:{
         getResults(page = 1){                    console.log('g r');
           this.brand_id = $('#brand_id').val();

           this.mainList = {data:[]};
           this.show_spinner = true;

            $.post(get_list+'?page=' + page ,$('#search_form').serializeArray(),(Response)=>{ console.log('r ');console.log(Response);
                myVue.mainList = Response;
                myVue.show_spinner = false;
            });
         },
         DeleteMessage(id,index){
              showDeleteMessage(id,delete_api+'/'+id).then(()=>{
                  myVue.mainList.data.splice(index,1);
              });
         },

         showORhide(id){
             $.get(showORhide_api+'/'+id ,(responce)=>{
               let find = myVue.mainList.data.find(obj => obj.id == id);
                  find.status = responce.case;
                  find.is_contacted = responce.case;
                  if(responce.case){
                    new Noty({text: 'Content has been shown in the site', layout: 'topRight', type: 'success',timeout: 2000  }).show();
                  }
                  else {
                    new Noty({text: 'Content now is hidden in the site' , layout: 'topRight', type: 'error',timeout: 2000  }).show();
                  }
             });
          },
          diffforhumans(data)
          {
              moment.locale('en');
              if (data) {
                return  moment(data).fromNow();
              }
          },
          CategoryChanged()
          {
              var Categoy_id = $('#category_id').val();
              this.Brand_list = [];
              if(Categoy_id)
              {
                  this.subCategoires_list = this.Categories_and_sub.find(obj=>obj.value == Categoy_id).subCategory;
              }
              else {
                this.subCategoires_list = [];
              }
              this.getResults();
          },
          subCategoryChanged()
          {
              this.Brand_list = this.subCategoires_list.find(obj=>obj.value == this.EF.sub_category_id).brand;
              this.getResults();
          }
    },//End methods
    computed:{
         Categoires_list()
         {
            if(this.brand_id)
            {
                return this.brands_categoirs.find(obj=>obj.value == this.brand_id).Category;
            }
            else {
              return [];
            }
         }
    }//End computed
});
