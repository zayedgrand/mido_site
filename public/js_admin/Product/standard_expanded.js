let myVue = new Vue({
    el:'#myVue',
    data:{
        mainList: {data:[]},
        show_spinner:false,
        EF: get_project, //Edit Form
        imagesArr: get_ProjectImages,
        btn_submit: false,
        Categories_and_sub: get_Categories_and_sub,
        brand_id: null,
        Categoy_id: '',
        subCategory_id: '',
        subCategoires_list: [],
        Brand_list: []
    },
    mounted(){
        // this.getResults();
        $('#my_form').validate();

        if(this.EF.sub_category_id){
            this.CategoryChanged();
            setTimeout(function () {
              $('#Categoy_id').val(myVue.EF.sub_category_id);
            }, 150);

            setTimeout(function () {
              myVue.subCategoryChanged();
            }, 300);

            // $('#Categoy_id').val(this.EF.category_id);
        }

    },//End mounted()
    methods:{
        Preview_image(e)
        {
           let from = e.currentTarget.getAttribute('data-from');
           if (e.target.files && e.target.files[0]) //Preview_image
           {
                 if(from == 'create') {
                     $('#Preview_image_create').attr('src',URL.createObjectURL(e.target.files[0]) );
                 }
                 else if(from == 'edit') {
                     $('#Preview_image_edit').attr('src',URL.createObjectURL(e.target.files[0]) );
                 }
                 else if(from == 'addImage') { console.log('addImage');
                     $(e.currentTarget).closest('tr').find('img').attr('src', URL.createObjectURL(e.target.files[0]) );
                 }
           }
        },
        addNewImage()
        {
            this.imagesArr.push({image:'' ,id: Math.random() });
        },
        remove_Image(index){
          this.imagesArr.splice(index,1);
        },
        CategoryChanged()
        {
            var Categoy_id = $('#category_id').val();
            if(Categoy_id)
            {
                this.subCategoires_list = this.Categories_and_sub.find(obj=>obj.value == Categoy_id).subCategory;
            }
            else {
              this.subCategoires_list = [];
            }
        },
        subCategoryChanged()
        {
            this.Brand_list = this.subCategoires_list.find(obj=>obj.value == this.EF.sub_category_id).brand;
        }
    },//End methods
    computed:{

         new_price()
         {
            if(this.EF.old_price === "" || this.EF.discount_percentage === "")
            {
                return this.EF.old_price; 
            }
            else {
                let discount = this.EF.old_price * (this.EF.discount_percentage / 100);
                return this.EF.price = Math.round(this.EF.old_price - discount);
            }
         }
    }//end computed
});
