let js_localization_en = [
  { label:'Added to cart' , value:'Added to cart ' },
  { label:'Max number in Stock' , value:'You exceeded the maximum allowed quantity from this product' },
  { label:'Max in Stock' , value:'You exceeded the maximum allowed quantity from this product' },
  { label:'Added to cart' , value:'Added to cart ' },
  { label:'prouduct increased in the cart' , value:'Quantity adjusted' },
  { label:'Must login first' , value:'You need to Login first' },
  { label:'increased in cart' , value:'Increased in cart' },
  { label:'decreased in cart' , value:'Decreased in cart' },
  { label:'product has deleted from the cart' , value:'product is deleted from the cart' },
  { label:'new address is added' , value:'the new address is added' },
  { label:'order cancled' , value:'Your order has been canceled' },
  { label:'order already canceled' , value:'your order is being processed and cant be canceled' },
];

let js_localization_ar = [
  { label:'Added to cart' , value:'تمت الإضافة إلى العربة' },
  { label:'Max number in Stock' , value:'لقد تجاوزت الحد المتوفر من هذا المنتج' },
  { label:'Max in Stock' , value:'لقد تجاوزت الحد المتوفر من هذا المنتج' },
  { label:'Added to cart' , value:'تمت الإضافة إلى العربة' },
  { label:'prouduct increased in the cart' , value:'تم تعديل الكمية' },
  { label:'Must login first' , value:'يجب تسجيل الدخول اولا' },
  { label:'increased in cart' , value:'تم زيادة الكمية' },
  { label:'decreased in cart' , value:'تم انخفاض الكمية' },
  { label:'product has deleted from the cart' , value:'تم حذف المنتج' },
  { label:'order cancled' , value:' لقد تم إلغاء طلبك  ' },
  { label:'order already canceled' , value:' تم تجهيز طلبك ولا يمكن إلغاؤه‎ ' },
];






//------------------------function------------------------

function lacal(val){
    if(site_lang == 'ar')
    {
        var selectedOne = js_localization_ar.find(obj=>obj.label == val) ;
    }
    else
    {
        var selectedOne = js_localization_en.find(obj=>obj.label == val) ;
    }

   if(selectedOne){
     return selectedOne.value;
   }
   else {
      return 'local not found';
   }
}
