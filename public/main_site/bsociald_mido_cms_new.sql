/*
Navicat MySQL Data Transfer

Source Server         : bsocial-dev
Source Server Version : 50540
Source Host           : bsocial-dev.com:3306
Source Database       : bsociald_mido_cms

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2015-01-26 17:04:19
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `aboutus`
-- ----------------------------
DROP TABLE IF EXISTS `aboutus`;
CREATE TABLE `aboutus` (
  `AboutID` bigint(64) NOT NULL AUTO_INCREMENT,
  `ImageID` bigint(64) NOT NULL,
  `Active` bit(1) NOT NULL,
  PRIMARY KEY (`AboutID`),
  KEY `AboutImageID` (`ImageID`) USING BTREE,
  CONSTRAINT `aboutus_ibfk_1` FOREIGN KEY (`ImageID`) REFERENCES `images` (`ImageID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of aboutus
-- ----------------------------

-- ----------------------------
-- Table structure for `banners`
-- ----------------------------
DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `BannerID` bigint(64) NOT NULL AUTO_INCREMENT,
  `ImageID` bigint(64) NOT NULL,
  `Active` bit(1) NOT NULL,
  PRIMARY KEY (`BannerID`),
  KEY `BannerImageID` (`ImageID`),
  CONSTRAINT `BannerImageID` FOREIGN KEY (`ImageID`) REFERENCES `images` (`ImageID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banners
-- ----------------------------
INSERT INTO `banners` VALUES ('10', '214', '');
INSERT INTO `banners` VALUES ('12', '216', '\0');
INSERT INTO `banners` VALUES ('13', '217', '');
INSERT INTO `banners` VALUES ('14', '218', '');

-- ----------------------------
-- Table structure for `brandcategorys`
-- ----------------------------
DROP TABLE IF EXISTS `brandcategorys`;
CREATE TABLE `brandcategorys` (
  `BrandID` bigint(64) NOT NULL,
  `CategoryID` bigint(64) NOT NULL,
  PRIMARY KEY (`BrandID`,`CategoryID`),
  KEY `BGCategoryID` (`CategoryID`),
  CONSTRAINT `BGBrandID` FOREIGN KEY (`BrandID`) REFERENCES `brands` (`BrandID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `BGCategoryID` FOREIGN KEY (`CategoryID`) REFERENCES `categorys` (`CategoryID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of brandcategorys
-- ----------------------------

-- ----------------------------
-- Table structure for `brandproducts`
-- ----------------------------
DROP TABLE IF EXISTS `brandproducts`;
CREATE TABLE `brandproducts` (
  `BrandID` bigint(64) NOT NULL,
  `ProductID` bigint(64) NOT NULL,
  `CategoryID` bigint(64) NOT NULL,
  PRIMARY KEY (`BrandID`,`ProductID`,`CategoryID`),
  KEY `BPProductID` (`ProductID`),
  KEY `BPCategoryID` (`CategoryID`),
  CONSTRAINT `BPBrandID` FOREIGN KEY (`BrandID`) REFERENCES `brands` (`BrandID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `BPCategoryID` FOREIGN KEY (`CategoryID`) REFERENCES `categorys` (`CategoryID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `BPProductID` FOREIGN KEY (`ProductID`) REFERENCES `products` (`ProductID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of brandproducts
-- ----------------------------

-- ----------------------------
-- Table structure for `brands`
-- ----------------------------
DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands` (
  `BrandID` bigint(64) NOT NULL AUTO_INCREMENT,
  `BrandName` varchar(256) NOT NULL,
  `BrandDesc` text,
  `ThumbImageID` bigint(64) NOT NULL,
  `ImageID` bigint(64) NOT NULL,
  `OuterImageID` bigint(64) DEFAULT NULL,
  `TypeID` bigint(64) NOT NULL,
  `PageViews` int(11) DEFAULT '0',
  PRIMARY KEY (`BrandID`),
  KEY `BImageID` (`ImageID`),
  KEY `BTypeID` (`TypeID`),
  KEY `ThumbImageIDs` (`ThumbImageID`),
  KEY `OuterImageID` (`OuterImageID`),
  CONSTRAINT `BImageID` FOREIGN KEY (`ImageID`) REFERENCES `images` (`ImageID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `brands_ibfk_1` FOREIGN KEY (`OuterImageID`) REFERENCES `images` (`ImageID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `BTypeID` FOREIGN KEY (`TypeID`) REFERENCES `types` (`TypeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ThumbImageIDs` FOREIGN KEY (`ThumbImageID`) REFERENCES `images` (`ImageID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of brands
-- ----------------------------
INSERT INTO `brands` VALUES ('56', 'Illy café – Espresso & Instant coffee', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '152', '153', '154', '2', '1');
INSERT INTO `brands` VALUES ('57', 'Angus US Beef', 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.', '155', '156', '157', '1', '1');
INSERT INTO `brands` VALUES ('58', 'Badoit – Natural Sparkling water', 'Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.', '158', '159', '160', '2', '1');
INSERT INTO `brands` VALUES ('59', 'Bridor – Frozen Bakery', 'Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.\r\n', '161', '162', '163', '4', '1');
INSERT INTO `brands` VALUES ('60', 'Azuki bean', 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.', '164', '165', '166', '1', '1');
INSERT INTO `brands` VALUES ('61', 'Dammann – Tea', 'Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.', '167', '168', '169', '2', '1');
INSERT INTO `brands` VALUES ('62', 'EKRO - Dutch Veal', 'Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.', '170', '171', '172', '1', '1');
INSERT INTO `brands` VALUES ('63', 'Evian – Natural Mineral Water', 'Azuki bean', '173', '174', '175', '2', '1');
INSERT INTO `brands` VALUES ('64', 'DAWN', 'Chocolates and Covertures ', '176', '177', '178', '1', '1');
INSERT INTO `brands` VALUES ('65', 'Haagen-Dazs – Ice Cream', 'Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.', '179', '180', '181', '3', '1');
INSERT INTO `brands` VALUES ('66', 'Ireks – Bakery Mixes', 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.', '182', '183', '184', '4', '1');
INSERT INTO `brands` VALUES ('67', 'Isi – Professional Siphons', 'Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.', '185', '186', '187', '5', '1');
INSERT INTO `brands` VALUES ('68', 'La Marzocco – Professional Coffee machines', 'Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.', '188', '189', '190', '5', '1');
INSERT INTO `brands` VALUES ('69', 'La Pavoni – Professional Coffee machines', 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.', '191', '192', '193', '5', '1');
INSERT INTO `brands` VALUES ('70', 'Veggies sunt bona vobis', 'Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.', '194', '195', '196', '1', '1');
INSERT INTO `brands` VALUES ('71', 'Dandelion horseradish', 'Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.', '197', '198', '199', '1', '1');
INSERT INTO `brands` VALUES ('72', 'Richs’s – Non Dairy Whipping Cream', 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.', '200', '201', '202', '3', '1');
INSERT INTO `brands` VALUES ('73', 'Veggies sunt bona vobis', 'Komatsuna prairie turnip wattle seed artichoke mustard horseradish taro rutabaga ricebean carrot black-eyed pea turnip greens beetroot yarrow watercress kombu.', '203', '204', '205', '1', '1');
INSERT INTO `brands` VALUES ('74', 'Volvic – natural Mineral Water', 'Cabbage bamboo shoot broccoli rabe chickpea chard sea lettuce lettuce ricebean artichoke earthnut pea aubergine okra brussels sprout avocado tomato.', '206', '207', '208', '2', '1');
INSERT INTO `brands` VALUES ('75', 'Illy issimo – Cold coffee Cans', 'Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.', '209', '210', '211', '2', '1');

-- ----------------------------
-- Table structure for `careers`
-- ----------------------------
DROP TABLE IF EXISTS `careers`;
CREATE TABLE `careers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Message` text COLLATE utf8_unicode_ci,
  `attach` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of careers
-- ----------------------------

-- ----------------------------
-- Table structure for `categorys`
-- ----------------------------
DROP TABLE IF EXISTS `categorys`;
CREATE TABLE `categorys` (
  `CategoryID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(256) NOT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categorys
-- ----------------------------
INSERT INTO `categorys` VALUES ('16', 'illy');
INSERT INTO `categorys` VALUES ('17', 'category');

-- ----------------------------
-- Table structure for `gallery`
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `GalleryID` bigint(64) NOT NULL AUTO_INCREMENT,
  `GalleryTitle` varchar(256) NOT NULL,
  `GalleryDescription` text NOT NULL,
  `ImageID` bigint(64) NOT NULL,
  PRIMARY KEY (`GalleryID`),
  KEY `GImageID` (`ImageID`) USING BTREE,
  CONSTRAINT `gallery_copy_ibfk_1` FOREIGN KEY (`ImageID`) REFERENCES `images` (`ImageID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gallery
-- ----------------------------
INSERT INTO `gallery` VALUES ('1', 'TBS team @ IREKS Germany', 'MIDO company & IREKS invited the TBS bakery team for a training program to introduce new products and recipes to the Egyptian market.  IREKS is one of the biggest Bakery Mixes producers in the world.  ', '132');
INSERT INTO `gallery` VALUES ('2', 'Haagen-Dazs at CAFEX', 'Haagen-Dazs at CAFEX', '133');
INSERT INTO `gallery` VALUES ('3', 'illy', 'New images and POS material', '134');
INSERT INTO `gallery` VALUES ('4', 'BADOIT', 'Now in Egypt in Glass and PET', '135');
INSERT INTO `gallery` VALUES ('5', 'Haagen-Dazs ', 'New summer Campaign at On The Run', '136');
INSERT INTO `gallery` VALUES ('6', 'MIDO', 'New Stores', '137');
INSERT INTO `gallery` VALUES ('7', 'illy IPSO', 'New Ipso Professional machines.', '138');
INSERT INTO `gallery` VALUES ('8', 'University of Coffee in Egypt', 'New Courses & New prices ', '139');
INSERT INTO `gallery` VALUES ('9', 'EVIAN & BADOIT', 'Evian and Badoit sponsored the Barista competition at CAFEX', '140');
INSERT INTO `gallery` VALUES ('10', 'Barista Competiton', 'MIDO was one of the main organizers for the Barista competition at CAFEX', '141');
INSERT INTO `gallery` VALUES ('11', 'illy', 'illy\'s display at CAFEX', '142');
INSERT INTO `gallery` VALUES ('12', 'MIDO', 'New Stores', '143');

-- ----------------------------
-- Table structure for `images`
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `ImageID` bigint(64) NOT NULL AUTO_INCREMENT,
  `ImageURL` varchar(256) NOT NULL,
  PRIMARY KEY (`ImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES ('116', '2015_01_21_05_49_0554bf84210b5e1_banner.jpg');
INSERT INTO `images` VALUES ('117', '2015_01_21_05_49_2554bf84357cd87_banner.jpg');
INSERT INTO `images` VALUES ('118', '2015_01_21_05_49_5054bf844e7239b_banner.jpg');
INSERT INTO `images` VALUES ('119', '2015_01_21_05_50_0954bf8462001b1_banner.jpg');
INSERT INTO `images` VALUES ('120', '2015_01_21_05_50_2854bf84744c67c_banner.jpg');
INSERT INTO `images` VALUES ('121', '2015_01_26_06_41_1354c627d9e5a74_news.jpg');
INSERT INTO `images` VALUES ('122', '2015_01_26_06_41_5954c6280794ebf_news.jpg');
INSERT INTO `images` VALUES ('123', '2015_01_26_06_44_0654c62886ac457_news.jpg');
INSERT INTO `images` VALUES ('124', '2015_01_26_06_44_5854c628ba933ec_news.jpg');
INSERT INTO `images` VALUES ('125', '2015_01_26_06_45_3854c628e25ec9c_news.jpg');
INSERT INTO `images` VALUES ('126', '2015_01_26_06_46_0154c628f90326f_news.jpg');
INSERT INTO `images` VALUES ('127', '2015_01_26_06_46_3654c6291c8d19c_news.jpg');
INSERT INTO `images` VALUES ('128', '2015_01_21_06_07_4654bf888270013_news.jpg');
INSERT INTO `images` VALUES ('129', '2015_01_21_06_08_1054bf889aec33f_news.jpg');
INSERT INTO `images` VALUES ('130', '2015_01_21_06_09_3554bf88efc8aa1_news.jpg');
INSERT INTO `images` VALUES ('131', '2015_01_21_06_10_0354bf890bde588_news.jpg');
INSERT INTO `images` VALUES ('132', '2015_01_21_06_11_0754bf894bb0e4b_gallery.jpg');
INSERT INTO `images` VALUES ('133', '2015_01_21_06_11_3554bf89670823b_gallery.jpg');
INSERT INTO `images` VALUES ('134', '2015_01_21_06_11_5354bf89798f09a_gallery.jpg');
INSERT INTO `images` VALUES ('135', '2015_01_21_06_13_0054bf89bc93696_gallery.jpg');
INSERT INTO `images` VALUES ('136', '2015_01_21_06_13_2054bf89d001107_gallery.jpg');
INSERT INTO `images` VALUES ('137', '2015_01_21_06_13_4354bf89e77c460_gallery.jpg');
INSERT INTO `images` VALUES ('138', '2015_01_21_06_14_0154bf89f95b335_gallery.jpg');
INSERT INTO `images` VALUES ('139', '2015_01_21_06_14_2754bf8a132d7aa_gallery.jpg');
INSERT INTO `images` VALUES ('140', '2015_01_21_06_14_4754bf8a27143ae_gallery.jpg');
INSERT INTO `images` VALUES ('141', '2015_01_21_06_15_0754bf8a3bde861_gallery.jpg');
INSERT INTO `images` VALUES ('142', '2015_01_21_06_15_2954bf8a514a3c6_gallery.jpg');
INSERT INTO `images` VALUES ('143', '2015_01_21_06_17_5354bf8ae184dd5_gallery.jpg');
INSERT INTO `images` VALUES ('144', '2015_01_21_06_50_0454bf926c0ba2e_brand_thumb.jpg');
INSERT INTO `images` VALUES ('145', '2015_01_21_06_50_0454bf926c1a48d_brand.jpg');
INSERT INTO `images` VALUES ('146', '2015_01_21_06_50_0454bf926c1b432_outer_brand.jpg');
INSERT INTO `images` VALUES ('147', '2015_01_21_06_50_3654bf928c0fc45_prod.jpg');
INSERT INTO `images` VALUES ('148', '2015_01_21_07_44_3354bf9f313fdba_brand_thumb.jpg');
INSERT INTO `images` VALUES ('149', '2015_01_21_07_44_3354bf9f314c8d8_brand.jpg');
INSERT INTO `images` VALUES ('150', '2015_01_21_07_44_3354bf9f314e818_outer_brand.jpg');
INSERT INTO `images` VALUES ('151', '2015_01_21_07_45_3354bf9f6d953e0_prod.jpg');
INSERT INTO `images` VALUES ('152', '2015_01_21_10_00_5054bfbf22919d4_brand_thumb.jpg');
INSERT INTO `images` VALUES ('153', '2015_01_21_10_00_5054bfbf22a2373_brand.jpg');
INSERT INTO `images` VALUES ('154', '2015_01_21_10_00_5054bfbf22a3312_outer_brand.jpg');
INSERT INTO `images` VALUES ('155', '2015_01_21_10_02_4054bfbf90a00f7_brand_thumb.jpg');
INSERT INTO `images` VALUES ('156', '2015_01_21_10_21_3454bfc3fe196d4_brand.jpg');
INSERT INTO `images` VALUES ('157', '2015_01_21_10_21_2554bfc3f5e47ea_outer_brand.jpg');
INSERT INTO `images` VALUES ('158', '2015_01_21_10_22_4554bfc4453b216_brand_thumb.jpg');
INSERT INTO `images` VALUES ('159', '2015_01_21_10_22_4554bfc44546d9a_brand.jpg');
INSERT INTO `images` VALUES ('160', '2015_01_21_10_22_4554bfc44547d35_outer_brand.jpg');
INSERT INTO `images` VALUES ('161', '2015_01_21_10_24_2754bfc4abbadac_brand_thumb.jpg');
INSERT INTO `images` VALUES ('162', '2015_01_21_10_24_2754bfc4abc58b8_brand.jpg');
INSERT INTO `images` VALUES ('163', '2015_01_21_10_24_2754bfc4abc6856_outer_brand.jpg');
INSERT INTO `images` VALUES ('164', '2015_01_21_10_25_0854bfc4d41ce1f_brand_thumb.jpg');
INSERT INTO `images` VALUES ('165', '2015_01_21_10_25_0854bfc4d45b61a_brand.jpg');
INSERT INTO `images` VALUES ('166', '2015_01_21_10_25_0854bfc4d470de3_outer_brand.jpg');
INSERT INTO `images` VALUES ('167', '2015_01_21_10_25_5454bfc502482d0_brand_thumb.jpg');
INSERT INTO `images` VALUES ('168', '2015_01_21_10_25_5454bfc50252eaf_brand.jpg');
INSERT INTO `images` VALUES ('169', '2015_01_21_10_25_5454bfc50253e4c_outer_brand.jpg');
INSERT INTO `images` VALUES ('170', '2015_01_21_10_26_3854bfc52ec770b_brand_thumb.jpg');
INSERT INTO `images` VALUES ('171', '2015_01_21_10_26_3854bfc52ed616a_brand.jpg');
INSERT INTO `images` VALUES ('172', '2015_01_21_10_26_3854bfc52ed7121_outer_brand.jpg');
INSERT INTO `images` VALUES ('173', '2015_01_21_10_27_3454bfc566f3c79_brand_thumb.jpg');
INSERT INTO `images` VALUES ('174', '2015_01_21_10_27_3554bfc5670a614_brand.jpg');
INSERT INTO `images` VALUES ('175', '2015_01_21_10_27_3554bfc5670b5b8_outer_brand.jpg');
INSERT INTO `images` VALUES ('176', '2015_01_21_10_28_0654bfc58618454_brand_thumb.jpg');
INSERT INTO `images` VALUES ('177', '2015_01_21_10_28_0654bfc58624f77_brand.jpg');
INSERT INTO `images` VALUES ('178', '2015_01_21_10_28_0654bfc58625f1a_outer_brand.jpg');
INSERT INTO `images` VALUES ('179', '2015_01_21_10_29_0054bfc5bc19a00_brand_thumb.jpg');
INSERT INTO `images` VALUES ('180', '2015_01_21_10_29_0054bfc5bc23645_brand.jpg');
INSERT INTO `images` VALUES ('181', '2015_01_21_10_29_0054bfc5bc274c5_outer_brand.jpg');
INSERT INTO `images` VALUES ('182', '2015_01_21_10_29_4154bfc5e5bd00d_brand_thumb.jpg');
INSERT INTO `images` VALUES ('183', '2015_01_21_10_29_4154bfc5e5c7bf1_brand.jpg');
INSERT INTO `images` VALUES ('184', '2015_01_21_10_29_4154bfc5e5c8b8e_outer_brand.jpg');
INSERT INTO `images` VALUES ('185', '2015_01_21_10_30_2354bfc60fbf403_brand_thumb.jpg');
INSERT INTO `images` VALUES ('186', '2015_01_21_10_30_2354bfc60fcde64_brand.jpg');
INSERT INTO `images` VALUES ('187', '2015_01_21_10_30_2354bfc60fcee09_outer_brand.jpg');
INSERT INTO `images` VALUES ('188', '2015_01_21_10_30_5954bfc633a8110_brand_thumb.jpg');
INSERT INTO `images` VALUES ('189', '2015_01_21_10_30_5954bfc633b4c3e_brand.jpg');
INSERT INTO `images` VALUES ('190', '2015_01_21_10_30_5954bfc633b5bd3_outer_brand.jpg');
INSERT INTO `images` VALUES ('191', '2015_01_21_10_31_3454bfc656b9e3c_brand_thumb.jpg');
INSERT INTO `images` VALUES ('192', '2015_01_21_10_31_3454bfc656c845f_brand.jpg');
INSERT INTO `images` VALUES ('193', '2015_01_21_10_31_3454bfc656c983e_outer_brand.jpg');
INSERT INTO `images` VALUES ('194', '2015_01_21_10_32_4654bfc69ea2f4f_brand_thumb.jpg');
INSERT INTO `images` VALUES ('195', '2015_01_21_10_32_4654bfc69eafa6e_brand.jpg');
INSERT INTO `images` VALUES ('196', '2015_01_21_10_32_4654bfc69eb0a10_outer_brand.jpg');
INSERT INTO `images` VALUES ('197', '2015_01_21_10_33_2154bfc6c1baa41_brand_thumb.jpg');
INSERT INTO `images` VALUES ('198', '2015_01_21_10_33_2154bfc6c1c561f_brand.jpg');
INSERT INTO `images` VALUES ('199', '2015_01_21_10_33_2154bfc6c1c65c3_outer_brand.jpg');
INSERT INTO `images` VALUES ('200', '2015_01_21_10_34_0354bfc6eb9baf8_brand_thumb.jpg');
INSERT INTO `images` VALUES ('201', '2015_01_21_10_34_0354bfc6eba8615_brand.jpg');
INSERT INTO `images` VALUES ('202', '2015_01_21_10_34_0354bfc6eba95b0_outer_brand.jpg');
INSERT INTO `images` VALUES ('203', '2015_01_21_10_34_4354bfc71327c03_brand_thumb.jpg');
INSERT INTO `images` VALUES ('204', '2015_01_21_10_34_4354bfc71341395_brand.jpg');
INSERT INTO `images` VALUES ('205', '2015_01_21_10_34_4354bfc71343ae9_outer_brand.jpg');
INSERT INTO `images` VALUES ('206', '2015_01_21_10_35_2054bfc738b0bbe_brand_thumb.jpg');
INSERT INTO `images` VALUES ('207', '2015_01_21_10_35_2054bfc738bc736_brand.jpg');
INSERT INTO `images` VALUES ('208', '2015_01_21_10_35_2054bfc738bd6dc_outer_brand.jpg');
INSERT INTO `images` VALUES ('209', '2015_01_21_10_54_3154bfcbb76f8f9_brand_thumb.jpg');
INSERT INTO `images` VALUES ('210', '2015_01_21_10_54_3154bfcbb77e355_brand.jpg');
INSERT INTO `images` VALUES ('211', '2015_01_21_10_54_3154bfcbb780297_outer_brand.jpg');
INSERT INTO `images` VALUES ('212', '2015_01_22_04_23_4454c0c1a0ec9bb_banner.jpg');
INSERT INTO `images` VALUES ('213', '2015_01_22_04_24_3154c0c1cf44f6c_banner.jpg');
INSERT INTO `images` VALUES ('214', '2015_01_22_04_25_1254c0c1f82023d_banner.jpg');
INSERT INTO `images` VALUES ('215', '2015_01_22_04_28_1354c0c2ad15b0e_banner.jpg');
INSERT INTO `images` VALUES ('216', '2015_01_22_04_28_2854c0c2bca367c_banner.jpg');
INSERT INTO `images` VALUES ('217', '2015_01_22_04_29_2354c0c2f384899_banner.jpg');
INSERT INTO `images` VALUES ('218', '2015_01_22_04_29_4854c0c30ccc03e_banner.jpg');

-- ----------------------------
-- Table structure for `inquery`
-- ----------------------------
DROP TABLE IF EXISTS `inquery`;
CREATE TABLE `inquery` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regarding` text COLLATE utf8_unicode_ci,
  `Message` text COLLATE utf8_unicode_ci,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of inquery
-- ----------------------------

-- ----------------------------
-- Table structure for `inqueryparty`
-- ----------------------------
DROP TABLE IF EXISTS `inqueryparty`;
CREATE TABLE `inqueryparty` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deliveryLocation` text COLLATE utf8_unicode_ci,
  `eventDate` date DEFAULT NULL,
  `productsNeeded` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `orderComments` text COLLATE utf8_unicode_ci,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of inqueryparty
-- ----------------------------
INSERT INTO `inqueryparty` VALUES ('1', 'test', 'test last', '0101010', 'cairo', '0000-00-00', 'Premium Pre Sliced Smoked Salmon', '11', 'comment test', '2015-01-26 12:46:51');
INSERT INTO `inqueryparty` VALUES ('2', 'test', 'test last', '0101010', 'cairo', '0000-00-00', 'Premium Pre Sliced Smoked Salmon', '22', 'comment test', '2015-01-26 12:46:52');
INSERT INTO `inqueryparty` VALUES ('3', 'test first', 'test last', '01010101', 'location', '0000-00-00', 'Frozen Salmon', '0', '00', '2015-01-26 12:47:41');
INSERT INTO `inqueryparty` VALUES ('4', 'test first', 'test last', '01010101', 'location', '0000-00-00', 'Frozen Salmon', '123', '00', '2015-01-26 12:49:00');

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `NewsID` bigint(64) NOT NULL AUTO_INCREMENT,
  `NewsTitle` varchar(256) NOT NULL,
  `NewsDescription` text NOT NULL,
  `ImageID` bigint(64) NOT NULL,
  `Date` date NOT NULL,
  `PageViews` int(11) DEFAULT '0',
  PRIMARY KEY (`NewsID`),
  KEY `NImageID` (`ImageID`),
  CONSTRAINT `NImageID` FOREIGN KEY (`ImageID`) REFERENCES `images` (`ImageID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1', 'illy Instant Coffee ', 'MIDO is launching very soon illy\'s new instant coffee in 100 gm tins for the retail market. The product will be found in limited stores (Gourmet, Royal House, Soaady, Oscar, Sunny) during phase one and then will be distributed on a wide scale starting March 2015. ', '121', '2015-01-19', '5');
INSERT INTO `news` VALUES ('2', 'New Retail Fleet', 'MIDO company added 3 new distribution trucks to the retail distribution fleet.  Those trucks can carry dry and frozen products to satisfy our clients growing needs.  This is a part of several actions taken to expand the retail division and to reach more customers in the Traditional and Modern trade channels. ', '122', '2015-01-15', '2');
INSERT INTO `news` VALUES ('3', 'Outsanding 2014!', 'The Company just announced its sales results and a solid 29% growth was achieved.  Retail Division grew by 45%, Food Service Division grew by 22% and Production by more than 110%.  \r\n2015...here we come!', '123', '2015-01-14', '2');
INSERT INTO `news` VALUES ('4', 'Y5 Milk - Finally!', 'Y5 Milk, illy\'s latest multifunctional espresso and cappuccino machine is finally available.  MIDO started the sales of this beautifull machine during the gifts season and it is on high demand.  This machine can make an outstanding cappuccino using illy\'s IPSO capsules with just one button.  You can fins this machine on illy Egypt\'s Facebook store as well as On The Run outlets (25) and gourmet stores (4). ', '124', '2014-12-15', '4');
INSERT INTO `news` VALUES ('5', 'EVIAN - We Did It Again!', 'Anais Arrondeau, EVX country manager has awarded MIDO\'s sales team with a very nice bonus. The team successfully  achieved  2014 targets. ', '125', '2015-01-11', '3');
INSERT INTO `news` VALUES ('6', 'GULF FOOD 2015', 'Mr. Mohamed Fakhry, our Managing Director will be visiting the GULF FOOD in UAE from 8th till 12th of FEB.', '126', '2015-01-01', '1');
INSERT INTO `news` VALUES ('7', 'Contract Renewal - illy & On The Run', 'Congratulations to the Coffee team, they were able to renew illy\'s contract with Exxon Mobil\'s On The Run outlets for another 5 years.  ', '127', '2014-11-02', '1');

-- ----------------------------
-- Table structure for `pageviews`
-- ----------------------------
DROP TABLE IF EXISTS `pageviews`;
CREATE TABLE `pageviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(256) DEFAULT NULL,
  `page_views` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pageviews
-- ----------------------------
INSERT INTO `pageviews` VALUES ('1', 'Home page', '163');
INSERT INTO `pageviews` VALUES ('2', 'News', '30');
INSERT INTO `pageviews` VALUES ('3', 'Brands', '13');
INSERT INTO `pageviews` VALUES ('4', 'About us', '28');
INSERT INTO `pageviews` VALUES ('5', 'Orders', '20');
INSERT INTO `pageviews` VALUES ('6', 'University of coffee', '34');
INSERT INTO `pageviews` VALUES ('7', 'Gallery', '19');
INSERT INTO `pageviews` VALUES ('8', 'Careers', '12');

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ProductID` bigint(64) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(256) NOT NULL,
  `ProductTitle` varchar(256) NOT NULL,
  `FacebookPage` varchar(256) DEFAULT NULL,
  `TwitterPage` varchar(256) DEFAULT NULL,
  `PdfLink` varchar(256) DEFAULT NULL,
  `ImageID` bigint(64) NOT NULL,
  PRIMARY KEY (`ProductID`),
  KEY `PImageID` (`ImageID`),
  CONSTRAINT `PImageID` FOREIGN KEY (`ImageID`) REFERENCES `images` (`ImageID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('24', 'Product Name', 'Product Title', 'Facebook ', 'Twitter Page', 'haagen.png', '147');
INSERT INTO `products` VALUES ('25', '11222', '2555', '3555', '4', 'haagen.png', '151');

-- ----------------------------
-- Table structure for `subscription`
-- ----------------------------
DROP TABLE IF EXISTS `subscription`;
CREATE TABLE `subscription` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of subscription
-- ----------------------------
INSERT INTO `subscription` VALUES ('1', 'aaaa@aaa.com', '2015-01-22 11:37:48');
INSERT INTO `subscription` VALUES ('2', 'mfakhry@mido.com.eg', '2015-01-22 12:01:46');

-- ----------------------------
-- Table structure for `types`
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types` (
  `TypeID` bigint(64) NOT NULL AUTO_INCREMENT,
  `TypeName` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`TypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO `types` VALUES ('1', 'Food');
INSERT INTO `types` VALUES ('2', 'Beverages');
INSERT INTO `types` VALUES ('3', 'Pastry');
INSERT INTO `types` VALUES ('4', 'Bakery');
INSERT INTO `types` VALUES ('5', 'Equipment');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` bigint(32) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('2', 'admin', '9c76370848a20e0f21fe6b4acecbfe83');
INSERT INTO `users` VALUES ('3', '1234', '6562c5c1f33db6e05a082a88cddab5ea');
