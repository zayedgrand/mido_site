<?php
class News extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NewsTitle, NewsDescription, ImageID, Date', 'required'),
			array('NewsTitle', 'length', 'max'=>256),
			array('ImageID', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('NewsID, NewsTitle, NewsDescription, ImageID, Date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'image' => array(self::BELONGS_TO, 'Images', 'ImageID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NewsID' => 'News',
			'NewsTitle' => 'News Title',
			'NewsDescription' => 'News Description',
			'ImageID' => 'Image',
			'Date' => 'Date',
		);
	}
        
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('image');
		$criteria->compare('NewsID',$this->NewsID,true);
		$criteria->compare('NewsTitle',$this->NewsTitle,true);
		$criteria->compare('NewsDescription',$this->NewsDescription,true);
		$criteria->compare('ImageID',$this->ImageID,true);
		$criteria->compare('Date',$this->Date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
