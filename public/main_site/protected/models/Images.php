<?php

/**
 * This is the model class for table "images".
 *
 * The followings are the available columns in table 'images':
 * @property string $ImageID
 * @property string $ImageURL
 *
 * The followings are the available model relations:
 * @property Aboutus[] $aboutuses
 * @property Banners[] $banners
 * @property Brands[] $brands
 * @property Brands[] $brands1
 * @property Brands[] $brands2
 * @property Gallery[] $galleries
 * @property News[] $news
 * @property Products[] $products
 */
class Images extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'images';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ImageURL', 'required'),
			array('ImageURL', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ImageID, ImageURL', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aboutuses' => array(self::HAS_MANY, 'Aboutus', 'ImageID'),
			'banners' => array(self::HAS_MANY, 'Banners', 'ImageID'),
			'brands' => array(self::HAS_MANY, 'Brands', 'OuterImageID'),
			'brands1' => array(self::HAS_MANY, 'Brands', 'ImageID'),
			'brands2' => array(self::HAS_MANY, 'Brands', 'ThumbImageID'),
			'galleries' => array(self::HAS_MANY, 'Gallery', 'ImageID'),
			'news' => array(self::HAS_MANY, 'News', 'ImageID'),
			'products' => array(self::HAS_MANY, 'Products', 'ImageID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ImageID' => 'Image',
			'ImageURL' => 'Image Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ImageID',$this->ImageID,true);
		$criteria->compare('ImageURL',$this->ImageURL,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Images the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
