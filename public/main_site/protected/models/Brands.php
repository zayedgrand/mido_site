<?php
class Brands extends CActiveRecord
{
	public $search_name ;
	public function tableName()
	{
		return 'brands';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('search_name', 'length', 'max'=>256),
			array('BrandName, ThumbImageID, ImageID, TypeID', 'required'),
			array('BrandName', 'length', 'max'=>256),
			array('ThumbImageID, ImageID, OuterImageID, TypeID', 'length', 'max'=>64),
			array('BrandDesc', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('BrandID, BrandName, BrandDesc, ThumbImageID, ImageID, OuterImageID, TypeID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categorys' => array(self::MANY_MANY, 'Categorys', 'brandcategorys(BrandID, CategoryID)'),
			'brandproducts' => array(self::HAS_MANY, 'Brandproducts', 'BrandID'),
			'outerImage' => array(self::BELONGS_TO, 'Images', 'OuterImageID'),
			'image' => array(self::BELONGS_TO, 'Images', 'ImageID'),
			'type' => array(self::BELONGS_TO, 'Types', 'TypeID'),
			'thumbImage' => array(self::BELONGS_TO, 'Images', 'ThumbImageID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'BrandID' => 'Brand',
			'BrandName' => 'Brand Name',
			'BrandDesc' => 'Brand Desc',
			'ThumbImageID' => 'Thumb Image',
			'ImageID' => 'Image',
			'OuterImageID' => 'Outer Image',
			'TypeID' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('type','outerImage');
                $criteria->compare('type.TypeName',$this->search_name,true);
		$criteria->compare('BrandID',$this->BrandID,true);
		$criteria->compare('BrandName',$this->BrandName,true);
		$criteria->compare('BrandDesc',$this->BrandDesc,true);
		$criteria->compare('ThumbImageID',$this->ThumbImageID,true);
		$criteria->compare('ImageID',$this->ImageID,true);
		$criteria->compare('OuterImageID',$this->OuterImageID,true);
		$criteria->compare('TypeID',$this->TypeID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Brands the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
