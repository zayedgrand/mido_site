<?php
class Products extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ProductName, ProductTitle, ImageID', 'required'),
			array('ProductName, ProductTitle, FacebookPage, TwitterPage, PdfLink', 'length', 'max'=>256),
			array('ImageID', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ProductID, ProductName, ProductTitle, FacebookPage, TwitterPage, PdfLink, ImageID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'brandproducts' => array(self::HAS_MANY, 'Brandproducts', 'ProductID'),
			'image' => array(self::BELONGS_TO, 'Images', 'ImageID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ProductID' => 'Product',
			'ProductName' => 'Product Name',
			'ProductTitle' => 'Product Title',
			'FacebookPage' => 'Facebook Page',
			'TwitterPage' => 'Twitter Page',
			'PdfLink' => 'Pdf Link',
			'ImageID' => 'Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
                $criteria->with = array('image','brandproducts');
                $criteria->together = true;
                $criteria->condition = "brandproducts.BrandID = ".$_GET['brandID']." AND brandproducts.CategoryID = ".$_GET['catID'];
		$criteria->compare('ProductID',$this->ProductID,true);
		$criteria->compare('ProductName',$this->ProductName,true);
		$criteria->compare('ProductTitle',$this->ProductTitle,true);
		$criteria->compare('FacebookPage',$this->FacebookPage,true);
		$criteria->compare('TwitterPage',$this->TwitterPage,true);
		$criteria->compare('PdfLink',$this->PdfLink,true);
		$criteria->compare('ImageID',$this->ImageID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
