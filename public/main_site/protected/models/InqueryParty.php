<?php

/**
 * This is the model class for table "inqueryparty".
 *
 * The followings are the available columns in table 'inqueryparty':
 * @property integer $ID
 * @property string $firstName
 * @property string $lastName
 * @property string $mobile
 * @property string $deliveryLocation
 * @property string $eventDate
 * @property string $productsNeeded
 * @property integer $quantity
 * @property string $orderComments
 * @property string $timestamp
 */
class Inqueryparty extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inqueryparty';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('timestamp', 'required'),
			array('quantity', 'numerical', 'integerOnly'=>true),
			array('firstName, lastName, productsNeeded', 'length', 'max'=>256),
			array('mobile', 'length', 'max'=>50),
			array('deliveryLocation, eventDate, orderComments', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, firstName, lastName, mobile, deliveryLocation, eventDate, productsNeeded, quantity, orderComments, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'firstName' => 'First Name',
			'lastName' => 'Last Name',
			'mobile' => 'Mobile',
			'deliveryLocation' => 'Delivery Location',
			'eventDate' => 'Event Date',
			'productsNeeded' => 'Products Needed',
			'quantity' => 'Quantity',
			'orderComments' => 'Order Comments',
			'timestamp' => 'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('deliveryLocation',$this->deliveryLocation,true);
		$criteria->compare('eventDate',$this->eventDate,true);
		$criteria->compare('productsNeeded',$this->productsNeeded,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('orderComments',$this->orderComments,true);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inqueryparty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
