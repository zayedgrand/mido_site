<?php
class UserIdentity extends CUserIdentity
{
    public function authenticate()
    {
        // check if login details exists in database
        $record=Users::model()->findByAttributes(array('user_name'=>$this->username)); 
        if($record===null)
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        else if($record->user_password!==md5($this->password))
        { 
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else
        {
            $this->setState('registUserID', $record->user_id);
            $this->setState('registUserName', $record->user_name);
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
}