<?php

class AdminCareersController extends Controller
{
    public $layout='//layouts/cpanel';

    public function actionIndex()
    {

        if(Yii::app()->user->isGuest) {
            $this->redirect(array('site/login')); 
        }

            $model=new Careers('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Careers']))
                    $model->attributes=$_GET['Careers'];

            $this->render('index',array(
                    'model'=>$model,
            ));
    }
    
    public function actionExport()
    {
        $exportInquery = Careers::model()->findAll() ;
        $this->renderPartial('export',array('exportInquery'=>$exportInquery));
    }
    
    public function actionDownload($id)
    {
        $prod = Careers::model()->findAll('ID = ' . $id);
        
        $path=Yii::app()->baseURL.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'career_attachs'.DIRECTORY_SEPARATOR.$prod[0]->attach;
        return Yii::app()->getRequest()->sendFile($prod[0]->attach,@file_get_contents($path));
    }
}
