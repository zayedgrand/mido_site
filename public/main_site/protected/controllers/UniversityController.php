<?php
class UniversityController extends Controller
{
    public function actionIndex()
    {
        $pageviews = PageViews::model()->findAll('page_name = "University of coffee"');
        if(count($pageviews) != 0)
        {
            $update = "UPDATE pageviews SET page_views=page_views + 1 WHERE page_name='University of coffee'";
            $command_update_dl = Yii::app()->db->createCommand($update)->execute();
        }
        else
        {
            $insert = "INSERT INTO pageviews (page_name,page_views) VALUES ('University of coffee',1)";
            $commandinserttype = Yii::app()->db->createCommand($insert)->execute();
        }
        
        $brandSlider = Brands::model()->with('outerImage')->findAll();
        $this->render('index',array('brandSlider'=>$brandSlider));
    }
}