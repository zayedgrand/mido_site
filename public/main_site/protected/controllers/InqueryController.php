<?php
class InqueryController extends Controller
{
    public function actionIndex()
    {
        $pageviews = PageViews::model()->findAll('page_name = "Orders"');
        if(count($pageviews) != 0)
        {
            $update = "UPDATE pageviews SET page_views=page_views + 1 WHERE page_name='Orders'";
            $command_update_dl = Yii::app()->db->createCommand($update)->execute();
        }
        else
        {
            $insert = "INSERT INTO pageviews (page_name,page_views) VALUES ('Orders',1)";
            $commandinserttype = Yii::app()->db->createCommand($insert)->execute();
        }
        
        $brandSlider = Brands::model()->with('outerImage')->findAll();
        $this->render('index',array('brandSlider'=>$brandSlider));
    }
    
    public function actionGetOrders()
    {
        $salmonfirstname = strip_tags($_POST['salmonfirstname']);
        $salmonlastname = strip_tags($_POST['salmonlastname']);
        $salmonmobile = strip_tags($_POST['salmonmobile']);
        $salmonlocation = strip_tags($_POST['salmonlocation']);
        $salmondate = strip_tags($_POST['salmondate']);
        $salmonproducts = $_POST['salmonproducts'];
        $salmonquantity = $_POST['salmonquantity'];
        $salmoncomment = strip_tags($_POST['salmoncomment']);
        
        $sqlInsert = "INSERT INTO inqueryparty (firstName, lastName,mobile,deliveryLocation,eventDate,productsNeeded,quantity,orderComments)
        VALUES ('".addslashes($salmonfirstname)."','".addslashes($salmonlastname)."','".addslashes($salmonmobile)."','".addslashes($salmonlocation)."','".addslashes($salmondate)."','".addslashes($salmonproducts)."','".addslashes($salmonquantity)."','".addslashes($salmoncomment)."')";
        $commandinsertques = Yii::app()->db->createCommand($sqlInsert)->execute();
		
		$subject = 'New Order';
		$message = 	'First Name : '.$salmonfirstname.'<br>Last Name : '.$salmonlastname.
					'<br>Mobile : '.$salmonmobile.'<br>Delivery Location : '.$salmonlocation.
					'<br>Event Date : '.$salmondate.'<br>Products Needed : '.$salmonproducts.
					'<br>Quantity : '.$salmonquantity.'<br>Order Comments : '.$salmoncomment;
		$mail=Yii::app()->Smtpmail;
		$mail->SetFrom('bsocialeg@gmail.com', 'Mido Website');
		$mail->Subject    = $subject;
		$mail->MsgHTML($message);
		$mail->AddAddress('mfakhry@mido.com.eg', "Mido Mail");
		if(!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		}else {
			echo "Done";
		}
    }
    
    public function actionGetInquiry()
    {
        $firstname = strip_tags($_POST['firstname']);
        $lastname = strip_tags($_POST['lastname']);
        $phone = strip_tags($_POST['phone']);
        $email = strip_tags($_POST['email']);
        $regarding = strip_tags($_POST['regarding']);
        $message = strip_tags($_POST['message']);
		
        $sqlInsert = "INSERT INTO inquery (firstName, lastName, phone, email, regarding,Message)
        VALUES ('".addslashes($firstname)."','".addslashes($lastname)."','".addslashes($phone)."','".addslashes($email)."','".addslashes($regarding)."','".addslashes($message)."')";
        $commandinsertques = Yii::app()->db->createCommand($sqlInsert)->execute();
		
		$subject = 'Inquery';
		$message = 	'First Name : '.$firstname.'<br>Last Name : '.$lastname.'<br>Phone : '.$phone.'<br>Email : '.$email.'<br>Regarding : '.$regarding.'<br>Message : '.$message;
		$mail=Yii::app()->Smtpmail;
		$mail->SetFrom('bsocialeg@gmail.com', 'Mido Website');
		$mail->Subject    = $subject;
		$mail->MsgHTML($message);
		$mail->AddAddress('mfakhry@mido.com.eg', "Mido Mail");
		if(!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		}else {
			echo "Done";
		}
    }
}