<?php
Yii::import('ext.ImageEditor.ImageEditor');
class AdminOurTeamController extends Controller
{
	public $layout='//layouts/cpanel';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionCreate()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('cpanel/login'));
            }

            $model=new OurTeam;

            if(isset($_POST['OurTeam']))
            {
                $file = CUploadedFile::getInstanceByName('image');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_OurTeam.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/our_team/".$ts_time1);
                }
                else
                {
                    $ts_time1 = '';
                }

                $model->attributes=$_POST['OurTeam'];
                $model->image = $ts_time1;

                if($model->save())
                    $this->redirect(array('index'));
            }

            $this->render('create',array(
                    'model'=>$model,
            ));
	}

	public function actionUpdate($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('cpanel/login'));
            }

            $model=$this->loadModel($id);
            // $model2=Images::model()->findByPk($model->image);

            if(isset($_POST['OurTeam']))
            {
                $model->attributes = $_POST['OurTeam'];

                $file = CUploadedFile::getInstanceByName('image');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_OurTeam.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/our_team/".$ts_time1);
                    // $model2->ImageURL = $ts_time1;
										$model->image = $ts_time1;
                }
                // $model2->save();

                if($model->save())
                    $this->redirect(array('update','id'=>$model->id));
            }

            $this->render('update',array(
                    'model'=>$model,
            ));
	}

	public function actionDelete($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('cpanel/login'));
            }

            $this->loadModel($id)->delete();

            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('cpanel/login'));
            }

            $model=new OurTeam('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OurTeam']))
			$model->attributes=$_GET['OurTeam'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=OurTeam::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='OurTeam-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
