<?php
class NewsController extends Controller
{
    public function actionIndex()
    {
        $pageviews = PageViews::model()->findAll('page_name = "News"');
        if(count($pageviews) != 0)
        {
            $update = "UPDATE pageviews SET page_views=page_views + 1 WHERE page_name='News'";
            $command_update_dl = Yii::app()->db->createCommand($update)->execute();
        }
        else
        {
            $insert = "INSERT INTO pageviews (page_name,page_views) VALUES ('News',1)";
            $commandinserttype = Yii::app()->db->createCommand($insert)->execute();
        }
        
        $allNews = News::model()->with('image')->findAll(array('order'=>'t.Date DESC'));
        $brandSlider = Brands::model()->with('outerImage')->findAll();
        $this->render('index',array('brandSlider'=>$brandSlider,'allNews'=>$allNews));
    }
    
    public function actionNews($nid)
    {
        $update = "UPDATE news SET PageViews=PageViews + 1 WHERE NewsID='$nid'";
        $command_update_dl = Yii::app()->db->createCommand($update)->execute();

        $news = News::model()->with('image')->findAll(array('condition'=>'NewsID = '.$nid));
        $allNews = News::model()->with('image')->findAll(array('order'=>'t.Date DESC'));
        $brandSlider = Brands::model()->with('outerImage')->findAll();
        $this->render('news',array('brandSlider'=>$brandSlider,'news'=>$news,'allNews'=>$allNews));
    }
}