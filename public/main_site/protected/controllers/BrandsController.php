<?php
class BrandsController extends Controller
{
    public function actionIndex()
    {
        $pageviews = PageViews::model()->findAll('page_name = "Brands"');
        if(count($pageviews) != 0)
        {
            $update = "UPDATE pageviews SET page_views=page_views + 1 WHERE page_name='Brands'";
            $command_update_dl = Yii::app()->db->createCommand($update)->execute();
        }
        else
        {
            $insert = "INSERT INTO pageviews (page_name,page_views) VALUES ('Brands',1)";
            $commandinserttype = Yii::app()->db->createCommand($insert)->execute();
        }
        
        $brandTypes = Types::model()->with()->findAll();
        $brands = Brands::model()->with('image','thumbImage','type')->findAll();
        $this->render('index',array('brands'=>$brands,'brandTypes'=>$brandTypes));
    }
    
    public function actionBrand($bid)
    {
        $update = "UPDATE brands SET PageViews=PageViews + 1 WHERE BrandID='$bid'";
        $command_update_dl = Yii::app()->db->createCommand($update)->execute();
        
        $brandSlider = Brands::model()->with('image')->findAll();
        $brands = Brands::model()->with('outerImage')->findAll('t.BrandID = ' . $bid);
        $brandCategories = Brandcategorys::model()->findAll('t.BrandID = ' . $bid);
        
        $this->render('brand',array('brands'=>$brands,'bid'=>$bid,'brandCategories'=>$brandCategories,'brandSlider'=>$brandSlider));
    }
    
    public function actionDownload($id)
    {
        $prod = Products::model()->findAll('ProductID = ' . $id);
        
        $path=Yii::app()->baseURL.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'brands_pdf'.DIRECTORY_SEPARATOR.$prod[0]->PdfLink;
        return Yii::app()->getRequest()->sendFile($prod[0]->PdfLink,@file_get_contents($path));
    }
}