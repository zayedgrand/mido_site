<?php

class AdminSubscriptionController extends Controller
{
    public $layout='//layouts/cpanel';
    
    public function actionIndex()
    {
        if(Yii::app()->user->isGuest) {
            $this->redirect(array('site/login')); 
        }
        
        $model=new Subscription('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Subscription']))
                $model->attributes=$_GET['Subscription'];

        $this->render('index',array(
                'model'=>$model,
        ));
    }

    public function actionExport()
    {
        if(Yii::app()->user->isGuest) {
            $this->redirect(array('site/login')); 
        }
        
        $exportSubsc = Subscription::model()->findAll() ;
        $this->renderPartial('export',array('exportSubsc'=>$exportSubsc));
    }
}
