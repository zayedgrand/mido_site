<?php
Yii::import('ext.ImageEditor.ImageEditor');
class AdminBrandProductsController extends Controller
{
	public $layout='//layouts/cpanel';
        
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}
        
	public function actionCreate($catID,$brandID)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }

            $model=new Products;
            $model2=new Images;
            $model3=new Brandproducts;
            
            
            if(isset($_POST['Products']))
            {
                $file = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_prod.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/products/".$ts_time1);
                    $image = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/products/".$ts_time1);
                    $image->resize("215", '215') ;
                    $image->saveToFile(Yii::app()->basePath."/../uploads/products/".$ts_time1, "jpg", "100");
                }
                else
                {
                    $ts_time1 = '';
                }
                
                $model2->ImageURL = $ts_time1;
                $model2->save() ;
                
                $insert_image = Yii::app()->db->getLastInsertID();
                
                $file2 = CUploadedFile::getInstanceByName('PdfLink');
                if(!empty($file2))
                {
                    $ts_time2 = $file2->name ;
                    $file2->saveAs(Yii::app()->basePath."/../uploads/brands_pdf/".$ts_time2);
                }
                else
                {
                    $ts_time2 = '';
                }
                
                $model->attributes=$_POST['Products'];
                $model->ImageID = $insert_image;
                $model->PdfLink = $ts_time2;
                $model->save();
                
                $prodID = Yii::app()->db->getLastInsertID();
                
                $model3->BrandID = $brandID;
                $model3->ProductID = $prodID;
                $model3->CategoryID = $catID;
                
                if($model3->save())
                    $this->redirect(array('index?catID='.$catID.'&brandID='.$brandID));
            }

            $this->render('create',array(
                    'model'=>$model,
            ));
	}
        
	public function actionUpdate($catID,$brandID,$id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }

            $model=$this->loadModel($id);
            $model2=Images::model()->findByPk($model->ImageID);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Products']))
            {
                $model->attributes=$_POST['Products'];
                
                $file2 = CUploadedFile::getInstanceByName('PdfLink');
                if(!empty($file2))
                {
                    $ts_time2 = $file2->name ;
                    $file2->saveAs(Yii::app()->basePath."/../uploads/brands_pdf/".$ts_time2);
                    $model->PdfLink = $ts_time2;
                }
                
                $file = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_prod.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/products/".$ts_time1);
                    $image = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/products/".$ts_time1);
                    $image->resize("215", '215') ;
                    $image->saveToFile(Yii::app()->basePath."/../uploads/products/".$ts_time1, "jpg", "100");
                    $model2->ImageURL = $ts_time1;
                }
                $model2->save();
                
                if($model->save())
                    $this->redirect(array('update?catID='.$catID.'&brandID='.$brandID.'&id='.$id));
            }

            $this->render('update',array(
                    'model'=>$model,'image'=>$model2->ImageURL
            ));
	}
        
	public function actionDelete($catID,$brandID,$id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $delete = "DELETE FROM brandproducts WHERE BrandID='".$brandID."' AND ProductID='".$id."' AND CategoryID='".$catID."'";
            $confirmSubmit = Yii::app()->db->createCommand($delete)->execute();
//            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if($confirmSubmit == true)
                $this->redirect(array('adminBrandProducts/index?catID='.$catID.'&brandID='.$brandID));
	}
        
	public function actionIndex($catID,$brandID)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }

            $model=new Products('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Products']))
                $model->attributes=$_GET['Products'];

            $this->render('index',array(
                    'model'=>$model,'catID'=>$catID,'brandID'=>$brandID
            ));
	}
        
	public function loadModel($id)
	{
		$model=Products::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	protected function performAjaxValidation($model)
	{
            if(isset($_POST['ajax']) && $_POST['ajax']==='products-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
	}
}
