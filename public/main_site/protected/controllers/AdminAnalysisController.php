<?php
class AdminAnalysisController extends Controller
{
	public $layout='//layouts/cpanel';
        
	public function actionIndex()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $pageViews = PageViews::model()->findAll();
            $orders  = count(InqueryParty::model()->findAll());
            $inquiry = count(Inquery::model()->findAll());
            $careers = count(Careers::model()->findAll());
            $subscriptions = count(Subscription::model()->findAll());
            
            $this->render('index',array(
                'pageViews'=>$pageViews
                ,'orders'=>$orders
                ,'inquiry'=>$inquiry
                ,'careers'=>$careers
                ,'subscriptions'=>$subscriptions));
	}
}