<?php
class AboutController extends Controller
{
    public function actionIndex()
    {
        $pageviews = PageViews::model()->findAll('page_name = "About us"');
        if(count($pageviews) != 0)
        {
            $update = "UPDATE pageviews SET page_views=page_views + 1 WHERE page_name='About us'";
            $command_update_dl = Yii::app()->db->createCommand($update)->execute();
        }
        else
        {
            $insert = "INSERT INTO pageviews (page_name,page_views) VALUES ('About us',1)";
            $commandinserttype = Yii::app()->db->createCommand($insert)->execute();
        }

        $brandSlider = Brands::model()->with('outerImage')->findAll();
        $OurTeam = OurTeam::model() ->findAll();
        $this->render('index',array('brandSlider'=>$brandSlider,'OurTeam'=>$OurTeam));
    }
}
