<?php

class AdminInqueryController extends Controller
{
    public $layout='//layouts/cpanel';

    public function actionIndex()
    {
        if(Yii::app()->user->isGuest) {
            $this->redirect(array('site/login')); 
        }

            $model=new Inquery('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Inquery']))
                    $model->attributes=$_GET['Inquery'];

            $model2=new InqueryParty('search');
            $model2->unsetAttributes();  // clear any default values
            if(isset($_GET['InqueryParty']))
                    $model2->attributes=$_GET['InqueryParty'];

            $this->render('index',array(
                    'model'=>$model,'model2'=>$model2,
            ));
    }

    public function actionExport()
    {
        $exportInquery = Inquery::model()->findAll() ;
        $this->renderPartial('export',array('exportInquery'=>$exportInquery));
    }

    public function actionExportParty()
    {
        $exportInquery = InqueryParty::model()->findAll() ;
        $this->renderPartial('exportParty',array('exportInquery'=>$exportInquery));
    }
}
