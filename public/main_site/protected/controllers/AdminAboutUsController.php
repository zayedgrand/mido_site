<?php
Yii::import('ext.ImageEditor.ImageEditor');
class AdminAboutUsController extends Controller
{
	public $layout='//layouts/cpanel';
        
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
        
	public function actionCreate()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
                $model=new Aboutus;
                $model2=new Images;
                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if(isset($_POST['Aboutus']))
                {
                    $file = CUploadedFile::getInstanceByName('ImageURL');
                    if(!empty($file))
                    {
                        $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_banner.jpg";
                        $file->saveAs(Yii::app()->basePath."/../uploads/about_slider/".$ts_time1);
                        $image = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/about_slider/".$ts_time1);
                        $image->resize("1400", '547') ;
                        $image->saveToFile(Yii::app()->basePath."/../uploads/about_slider/".$ts_time1, "jpg", "100");
                    }
                    else
                    {
                        $ts_time1 = '';
                    }

                    $model2->ImageURL = $ts_time1;
                    $model2->save() ;

                    $insert_id = Yii::app()->db->getLastInsertID();
                        $model->attributes=$_POST['Aboutus'];
                        $model->ImageID = $insert_id;
                        if($model->save())
                                $this->redirect(array('index'));
		}

		$this->render('create',array(
                'model'=>$model,'model2'=>$model2
            ));
	}
        
	public function actionUpdate($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $model=$this->loadModel($id);
            $model2=Images::model()->findByPk($model->ImageID);
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Aboutus']))
            {
                $model->attributes=$_POST['Aboutus'];

                $file = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_banner.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/about_slider/".$ts_time1);
                    $image = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/about_slider/".$ts_time1);
                    $image->resize("1400", '547') ;
                    $image->saveToFile(Yii::app()->basePath."/../uploads/about_slider/".$ts_time1, "jpg", "100");
                    $model2->ImageURL = $ts_time1;
                }
                $model2->save();

                if($model->save())
                        $this->redirect(array('update','id'=>$model->AboutID));
            }

            $this->render('update',array(
                'model'=>$model,
            ));
	}
        
	public function actionDelete($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
	public function actionIndex()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $model=new Aboutus('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Aboutus']))
                $model->attributes=$_GET['Aboutus'];

            $this->render('index',array(
                'model'=>$model,
            ));
	}
        
	public function loadModel($id)
	{
            $model=Aboutus::model()->findByPk($id);
            if($model===null)
                throw new CHttpException(404,'The requested page does not exist.');
            return $model;
	}
        
	protected function performAjaxValidation($model)
	{
            if(isset($_POST['ajax']) && $_POST['ajax']==='aboutus-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
	}
}
