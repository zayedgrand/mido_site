<?php
Yii::import('ext.ImageEditor.ImageEditor');
class AdminHomeController extends Controller
{
	public $layout='//layouts/cpanel';

        
	public function filters()
	{
            return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete', // we only allow deletion via POST request
            );
	}
        
	public function actionCreate()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $model=new Banners;
            $model2=new Images;
            // Uncomment the following line if AJAX validation is needed
             //$this->performAjaxValidation($model);

            if(isset($_POST['Banners']))
            {
                $file = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_banner.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/home_slider/".$ts_time1);
                    $image = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/home_slider/".$ts_time1);
                    $image->resize("1400", '547') ;
                    $image->saveToFile(Yii::app()->basePath."/../uploads/home_slider/".$ts_time1, "jpg", "100");
                }
                else
                {
                    $ts_time1 = '';
                }

                $model2->ImageURL = $ts_time1;
                $model2->save() ;

                $insert_id = Yii::app()->db->getLastInsertID();

                $model->attributes=$_POST['Banners'];
                $model->ImageID = $insert_id;
                if($model->save())
                    $this->redirect(array('index'));
            }

            $this->render('create',array(
                'model'=>$model,'model2'=>$model2
            ));
	}
        
	public function actionUpdate($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $model=$this->loadModel($id);
            $model2=Images::model()->findByPk($model->ImageID);
            
            if(isset($_POST['Banners']))
            {
                $model->attributes = $_POST['Banners'];
                
                $file = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_banner.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/home_slider/".$ts_time1);
                    $image = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/home_slider/".$ts_time1);
                    $image->resize("1400", '547') ;
                    $image->saveToFile(Yii::app()->basePath."/../uploads/home_slider/".$ts_time1, "jpg", "100");
                    $model2->ImageURL = $ts_time1;
                }
                $model2->save();
                
                if($model->save())
                    $this->redirect(array('update','id'=>$model->BannerID));
            }

            $this->render('update',array(
                'model'=>$model,
            ));
	}
        
	public function actionDelete($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $this->loadModel($id)->delete();
            
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
	public function actionIndex()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $model=new Banners('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Banners']))
                    $model->attributes=$_GET['Banners'];

            $this->render('index',array(
                    'model'=>$model,
            ));
	}
        
	public function loadModel($id)
	{
            $model=Banners::model()->findByPk($id);
            if($model===null)
                throw new CHttpException(404,'The requested page does not exist.');
            return $model;
	}
        
	protected function performAjaxValidation($model)
	{
            if(isset($_POST['ajax']) && $_POST['ajax']==='banners-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
	}
}
