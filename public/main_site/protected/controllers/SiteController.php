<?php
class SiteController extends Controller
{
    public $layout = '//layouts/cpanel';
        public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
        
        public function actionIndex()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $this->render('index');
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionLogin()
	{
		$model=new LoginForm;
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
                
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
                        
			if($model->validate() && $model->login())
                            $this->redirect(array('adminHome/index'));
		}
                
		$this->render('login',array('model'=>$model));
	}
        
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('adminHome/index'));
	}
        
        public function loadModel($id) {
        if(!$this->example) $this->example = Example::model()->findByPk($id);
        if ($this->example === NULL)
            throw new CHttpException(404, 'The requested page does not exist.');
 
        return $this->example;
    }
}