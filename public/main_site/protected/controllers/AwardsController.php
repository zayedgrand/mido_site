<?php
class AwardsController extends Controller
{
    public function actionIndex()
    {
        $pageviews = PageViews::model()->findAll('page_name = "Gallery"');
        if(count($pageviews) != 0)
        {
            $update = "UPDATE pageviews SET page_views=page_views + 1 WHERE page_name='Gallery'";
            $command_update_dl = Yii::app()->db->createCommand($update)->execute();
        }
        else
        {
            $insert = "INSERT INTO pageviews (page_name,page_views) VALUES ('Gallery',1)";
            $commandinserttype = Yii::app()->db->createCommand($insert)->execute();
        }
        
        $brandSlider = Brands::model()->with('outerImage')->findAll();
        $allGallery = Gallery::model()->with('image')->findAll(array('order'=>'t.GalleryID DESC'));
        $this->render('index',array('allGallery'=>$allGallery,'brandSlider'=>$brandSlider));
    }
}