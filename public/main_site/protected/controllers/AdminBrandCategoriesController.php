<?php
Yii::import('ext.ImageEditor.ImageEditor');
class AdminBrandCategoriesController extends Controller
{
	public $layout='//layouts/cpanel';
        
	public function filters()
	{
		return array(
			'accessControl', 
//			'postOnly + delete',
		);
	}
        
        public function actionIndex($id)
        {
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }

            $dataProvider=new CActiveDataProvider('Brandcategorys', array (
            'criteria' => array(
                'condition'=>'BrandID = ' . $id
            ),
            'pagination' => array('pageSize' => 5),
             ));

            $this->render('index',array('dataProvider'=>$dataProvider,'brandID'=>$id));
        }
        
        public function actionCreate($brandID)
        {
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $model = new Brandcategorys;
            $model2 = new Categorys;
            
            if(isset($_POST['CatName']))
            {
                $model2->CategoryName = $_POST['CatName'];
                $model2->save() ;
                
                $insertCatID = Yii::app()->db->getLastInsertID();

                $model->BrandID = $brandID;
                $model->CategoryID = $insertCatID;
                if($model->save())
                    $this->redirect(array('adminBrandCategories/index/id/'.$brandID));
            }
            $this->render('create',array('brandID'=>$brandID));
        }
        
        public function actionUpdate($brandID,$catID)
        {
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $model = Brandcategorys::model()->findAll('BrandID = '.$brandID.' AND CategoryID='.$catID);
            $category = Categorys::model()->findAll('CategoryID = '.$model[0]->CategoryID);
            
            if(isset($_POST['CatName']))
            {
                $update = "UPDATE categorys SET CategoryName='".addslashes($_POST['CatName'])."' WHERE CategoryID='".$model[0]->CategoryID."'";
                $confirmSubmit = Yii::app()->db->createCommand($update)->execute();
                
                if($confirmSubmit == true)
                    $this->redirect(array('adminBrandCategories/update?catID='.$catID.'&brandID='.$brandID));
            }
            
            $this->render('update',array('model'=>$model,'CategoryID'=>$category[0]->CategoryName));
        }
        
        public function actionDelete($brandID,$catID)
        {
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $delete = "DELETE FROM brandcategorys WHERE CategoryID='".$catID."' AND BrandID='".$brandID."'";
            $confirmSubmit = Yii::app()->db->createCommand($delete)->execute();

            if($confirmSubmit == true)
                $this->redirect(array('adminBrandCategories/index/id/'.$brandID));
        }
        
	public function loadModel($id)
	{
		$model=Brands::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='brands-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
