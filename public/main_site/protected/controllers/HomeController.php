<?php
class HomeController extends Controller
{
    public function actionIndex()
    {
        $pageviews = PageViews::model()->findAll('page_name = "Home page"');
        if(count($pageviews) != 0)
        {
            $update = "UPDATE pageviews SET page_views=page_views + 1 WHERE page_name='Home page'";
            $command_update_dl = Yii::app()->db->createCommand($update)->execute();
        }
        else
        {
            $insert = "INSERT INTO pageviews (page_name,page_views) VALUES ('Home page',1)";
            $commandinserttype = Yii::app()->db->createCommand($insert)->execute();
        }
        
        $latestNews = News::model()->with('image')->findAll(array('order'=>'t.Date DESC','limit'=>'4'));
        $brandSlider = Brands::model()->with('outerImage')->findAll();
        $images = Banners::model()->with('image')->findAll(array('condition'=>'Active = 1','order'=>'t.BannerID DESC'));
        $this->render('index',array('images'=>$images,'brandSlider'=>$brandSlider,'latestNews'=>$latestNews));
    }
    
    public function actionSubscription()
    {
        $newsletter = strip_tags($_POST['newsletter']);
        
        $emailExist = Subscription::model()->findAll('email = "' . $newsletter.'"');
        if(count($emailExist) == 0)
        {
            $sqlInsert = "INSERT INTO subscription (email) VALUES ('".addslashes($newsletter)."')";
            $commandinsertques = Yii::app()->db->createCommand($sqlInsert)->execute();
        }
        
        echo "Done";
    }
}