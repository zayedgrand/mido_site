<?php
Yii::import('ext.ImageEditor.ImageEditor');
class AdminGalleryController extends Controller
{
	public $layout='//layouts/cpanel';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
        
	public function actionCreate()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('cpanel/login')); 
            }
            
            $model=new Gallery;
            $model2=new Images;

            if(isset($_POST['Gallery']))
            {
                $file = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_gallery.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/gallery/".$ts_time1);
                }
                else
                {
                    $ts_time1 = '';
                }

                $model2->ImageURL = $ts_time1;
                $model2->save() ;

                $insert_id = Yii::app()->db->getLastInsertID();

                $model->attributes=$_POST['Gallery'];
                $model->ImageID = $insert_id;
                
                if($model->save())
                    $this->redirect(array('index'));
            }

            $this->render('create',array(
                    'model'=>$model,
            ));
	}
        
	public function actionUpdate($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('cpanel/login')); 
            }
            
            $model=$this->loadModel($id);
            $model2=Images::model()->findByPk($model->ImageID);

            if(isset($_POST['Gallery']))
            {
                $model->attributes = $_POST['Gallery'];
                
                $file = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_gallery.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/gallery/".$ts_time1);
                    $model2->ImageURL = $ts_time1;
                }
                $model2->save();
                
                if($model->save())
                    $this->redirect(array('update','id'=>$model->GalleryID));
            }

            $this->render('update',array(
                    'model'=>$model,
            ));
	}
        
	public function actionDelete($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('cpanel/login')); 
            }
            
            $this->loadModel($id)->delete();

            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
	public function actionIndex()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('cpanel/login')); 
            }
            
            $model=new Gallery('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Gallery']))
			$model->attributes=$_GET['Gallery'];

		$this->render('index',array(
			'model'=>$model,
		));
	}
        
	public function loadModel($id)
	{
		$model=Gallery::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gallery-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
