<?php
Yii::import('ext.ImageEditor.ImageEditor');
class AdminBrandsController extends Controller
{
	public $layout='//layouts/cpanel';
        
	public function filters()
	{
		return array(
			'accessControl', 
			'postOnly + delete',
		);
	}
        
	public function actionView($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
	public function actionCreate()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
		$model=new Brands;
                
		if(isset($_POST['Brands']))
		{
                    $file = CUploadedFile::getInstanceByName('ThumbImageURL');
                    if(!empty($file))
                    {
                        $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_brand_thumb.jpg";
                        $file->saveAs(Yii::app()->basePath."/../uploads/brands/".$ts_time1);
                        $image = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/brands/".$ts_time1);
                        $image->resize("386", '383') ;
                        $image->saveToFile(Yii::app()->basePath."/../uploads/brands/".$ts_time1, "jpg", "100");
                    }
                    else
                    {
                        $ts_time1 = '';
                    }
                    
                    $model2 = new Images;
                    $model2->ImageURL = $ts_time1;
                    $model2->save() ;
                    
                    $insertThumbID = Yii::app()->db->getLastInsertID();
                    
                    $file2 = CUploadedFile::getInstanceByName('ImageURL');
                    if(!empty($file2))
                    {
                        $ts_time2 = date("Y_m_d_H_i_s").uniqid()."_brand.jpg";
                        $file2->saveAs(Yii::app()->basePath."/../uploads/brands/".$ts_time2);
                    }
                    else
                    {
                        $ts_time2 = '';
                    }
                    
                    $model3 = new Images;
                    $model3->ImageURL = $ts_time2;
                    $model3->save() ;
                    
                    $insertImageID = Yii::app()->db->getLastInsertID();
                    
                    $file3 = CUploadedFile::getInstanceByName('OuterImageURL');
                    if(!empty($file3))
                    {
                        $ts_time3 = date("Y_m_d_H_i_s").uniqid()."_outer_brand.jpg";
                        $file3->saveAs(Yii::app()->basePath."/../uploads/brands/".$ts_time3);
                        $image1 = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/brands/".$ts_time3);
                        $image1->resize("125", '125') ;
                        $image1->saveToFile(Yii::app()->basePath."/../uploads/brands/".$ts_time3, "jpg", "100");
                    }
                    else
                    {
                        $ts_time3 = '';
                    }
                    
                    $model4 = new Images;
                    $model4->ImageURL = $ts_time3;
                    $model4->save() ;
                    
                    $insertOuterImageID = Yii::app()->db->getLastInsertID();
                    
                    $model->attributes=$_POST['Brands'];
                    $model->ThumbImageID = $insertThumbID;
                    $model->ImageID = $insertImageID;
                    $model->OuterImageID = $insertOuterImageID;
                    $model->TypeID = $_POST['TypeID'];
//                    $model->attributes=$_POST['Brands'];
                    if($model->save())
                        $this->redirect(array('index'));
		}

		$this->render('create',array(
                    'model'=>$model,
		));
	}
        
	public function actionUpdate($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $model=$this->loadModel($id);
            $modelThumb=Images::model()->findByPk($model->ThumbImageID);
            $modelImage=Images::model()->findByPk($model->ImageID);
            $modelouter=Images::model()->findByPk($model->OuterImageID);

            if(isset($_POST['Brands']))
            {
                $file = CUploadedFile::getInstanceByName('ThumbImageURL');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_brand_thumb.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/brands/".$ts_time1);
                    $image = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/brands/".$ts_time1);
                    $image->resize("386", '383') ;
                    $image->saveToFile(Yii::app()->basePath."/../uploads/brands/".$ts_time1, "jpg", "100");
                    $modelThumb->ImageURL = $ts_time1;
                }
                
                $modelThumb->save();
                
                $file2 = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file2))
                {
                    $ts_time2 = date("Y_m_d_H_i_s").uniqid()."_brand.jpg";
                    $file2->saveAs(Yii::app()->basePath."/../uploads/brands/".$ts_time2);
                    $modelImage->ImageURL = $ts_time2;
                }
                
                $modelImage->save();
                
                $file3 = CUploadedFile::getInstanceByName('OuterImageURL');
                if(!empty($file3))
                {
                    $ts_time3 = date("Y_m_d_H_i_s").uniqid()."_outer_brand.jpg";
                    $file3->saveAs(Yii::app()->basePath."/../uploads/brands/".$ts_time3);
                    $image1 = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/brands/".$ts_time3);
                    $image1->resize("125", '125') ;
                    $image1->saveToFile(Yii::app()->basePath."/../uploads/brands/".$ts_time3, "jpg", "100");
                    $modelouter->ImageURL = $ts_time3;
                }
                
                $modelouter->save();
                
                $model->attributes=$_POST['Brands'];
                $model->TypeID=$_POST['TypeID'];
                
                if($model->save())
                    $this->redirect(array('update','id'=>$model->BrandID));
            }

            $this->render('update',array(
                    'model'=>$model,'thumb'=>$modelThumb->ImageURL,'image'=>$modelImage->ImageURL,
                    'outer'=>$modelouter->ImageURL,
            ));
	}
        
	public function actionDelete($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
	public function actionIndex()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
		$model=new Brands('search');
		$model->unsetAttributes();
		if(isset($_GET['Brands']))
			$model->attributes=$_GET['Brands'];

		$this->render('index',array(
			'model'=>$model,
		));
	}
        
	public function loadModel($id)
	{
		$model=Brands::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='brands-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
