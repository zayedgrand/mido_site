<?php
class CareersController extends Controller
{
    public function actionIndex()
    {
        $pageviews = PageViews::model()->findAll('page_name = "Careers"');
        if(count($pageviews) != 0)
        {
            $update = "UPDATE pageviews SET page_views=page_views + 1 WHERE page_name='Careers'";
            $command_update_dl = Yii::app()->db->createCommand($update)->execute();
        }
        else
        {
            $insert = "INSERT INTO pageviews (page_name,page_views) VALUES ('Careers',1)";
            $commandinserttype = Yii::app()->db->createCommand($insert)->execute();
        }
        
        $brandSlider = Brands::model()->with('outerImage')->findAll();
        $this->render('index',array('brandSlider'=>$brandSlider));
    }
    
    public function actionGetCareers()
    {
        $firstname = strip_tags($_POST['careersfirstname']);
        $lastname = strip_tags($_POST['careerslastname']);
        $textarea = strip_tags($_POST['careerstextarea']);
        
        $file = CUploadedFile::getInstanceByName('careerscv');
        if(!empty($file))
        {
            $explode_extention = explode('.',$file->name);
            $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_careers." . end($explode_extention);
            $file->saveAs(Yii::app()->basePath."/../uploads/career_attachs/".$ts_time1);
        }
        else
        {
            $ts_time1 = '';
        }

        $sqlInsert = "INSERT INTO careers (firstName, lastName, Message, attach)
        VALUES ('".addslashes($firstname)."','".addslashes($lastname)."','".addslashes($textarea)."','".$ts_time1."')";
        $commandinsertques = Yii::app()->db->createCommand($sqlInsert)->execute();
        echo "Done";
    }
}