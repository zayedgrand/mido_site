<?php
Yii::import('ext.ImageEditor.ImageEditor');
class AdminNewsController extends Controller
{
	public $layout='//layouts/cpanel';
        
	public function filters()
	{
            return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete', // we only allow deletion via POST request
            );
	}
        
	public function actionCreate()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }
            
            $model=new News;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['News']))
            {
                $file2 = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file2))
                {
                    $ts_time2 = date("Y_m_d_H_i_s").uniqid()."_news.jpg";
                    $file2->saveAs(Yii::app()->basePath."/../uploads/news/".$ts_time2);
                    $image1 = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/news/".$ts_time2);
                    $image1->resize("400", '294') ;
                    $image1->saveToFile(Yii::app()->basePath."/../uploads/news/".$ts_time2, "jpg", "100");
                }
                else
                {
                    $ts_time2 = '';
                }

                $model3 = new Images;
                $model3->ImageURL = $ts_time2;
                $model3->save() ;

                $insertImageID = Yii::app()->db->getLastInsertID();

                $model->attributes=$_POST['News'];
                $model->ImageID = $insertImageID;
                if($model->save())
                    $this->redirect(array('index'));
            }

            $this->render('create',array(
                'model'=>$model,
            ));
	}
        
	public function actionUpdate($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }

            $model=$this->loadModel($id);
            $modelImage=Images::model()->findByPk($model->ImageID);
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['News']))
            {
                $model->attributes=$_POST['News'];
                
                $file = CUploadedFile::getInstanceByName('ImageURL');
                if(!empty($file))
                {
                    $ts_time1 = date("Y_m_d_H_i_s").uniqid()."_news.jpg";
                    $file->saveAs(Yii::app()->basePath."/../uploads/news/".$ts_time1);
                    $image = ImageEditor::createFromFile(Yii::app()->basePath."/../uploads/news/".$ts_time1);
                    $image->resize("400", '294') ;
                    $image->saveToFile(Yii::app()->basePath."/../uploads/news/".$ts_time1, "jpg", "100");
                    $modelImage->ImageURL = $ts_time1;
                }
                
                $modelImage->save();
                
                if($model->save())
                    $this->redirect(array('update','id'=>$id));
            }

            $this->render('update',array(
                    'model'=>$model,
                    'image'=>$modelImage->ImageURL,
            ));
	}
        
	public function actionDelete($id)
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }

            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
	public function actionIndex()
	{
            if(Yii::app()->user->isGuest) {
                $this->redirect(array('site/login')); 
            }

            $model=new News('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['News']))
                $model->attributes=$_GET['News'];

            $this->render('index',array(
                    'model'=>$model,
            ));
	}
        
	public function loadModel($id)
	{
            $model=News::model()->findByPk($id);
            if($model===null)
                throw new CHttpException(404,'The requested page does not exist.');
            return $model;
	}
        
	protected function performAjaxValidation($model)
	{
            if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
	}
}
