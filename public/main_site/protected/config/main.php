<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
//      'defaultController' => 'comingsoon', 
	'name'=>'Mido',
        'defaultController' => 'home', 
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
            'application.models.*',
            'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'4556',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('41.131.26.157','::1'),
		),
                'ycm'=>array(
                    'username'=>'1234',
                    'password'=>'4567',
                    'registerModels'=>array(
                        //'application.models.Blog', // one model
                        'application.models.*', // all models in folder
                    ),
                    'uploadCreate'=>true, // create upload folder automatically
                    'redactorUpload'=>true, // enable Redactor image upload
                ),
	),

	// application components
	'components'=>array(
		'request'=>array(
            'enableCookieValidation'=>true,
			//'enableCsrfValidation'=>true,
        ),
		'Smtpmail'=>array(
                'class'=>'application.extensions.smtpmail.PHPMailer',
                'Host'=>"ssl://smtp.gmail.com",
                'Username'=>'bsocialeg@gmail.com',
                'Password'=>'b$0c1@l3g',
                'Mailer'=>'smtp',
                'Port'=>465,
                'SMTPAuth'=>true, 
            ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
				'http://<user:\w+>.example.com/<lang:\w+>/profile' => 'user/profile',
				'<controller:\w+>/<action:\w+>/title/<title:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=midocom_website_cms',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'QZ67JK',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'clientScript'=>array(
		  'packages'=>array(
			'jquery'=>array(
			  'baseUrl'=>'//ajax.googleapis.com/ajax/libs/jquery/1.10.2/',
			  'js'=>array('jquery.min.js'),
			  'coreScriptPosition'=>CClientScript::POS_HEAD
			),
			'jquery.ui'=>array(
			  'baseUrl'=>'//ajax.googleapis.com/ajax/libs/jqueryui/1.8/',
			  'js'=>array('jquery-ui.min.js'),
			  'depends'=>array('jquery'),
			  'coreScriptPosition'=>CClientScript::POS_BEGIN
			)
		  ),
		),
	),
);
//include('testtt.php');
