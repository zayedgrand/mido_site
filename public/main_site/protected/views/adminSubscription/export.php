<?php
$data[] = array ('ID', 'Email','Timestamp');

foreach($exportSubsc as $exportSubscfe)
{
    $data[] = array (
        $exportSubscfe->ID,
        $exportSubscfe->email,
        $exportSubscfe->timestamp
        );
}
Yii::import('application.extensions.phpexcel.JPhpExcel');
$xls = new JPhpExcel('UTF-8', false, 'mido_Subscriptions');
$xls->addArray($data);
$xls->generateXML('mido_Subscriptions');
?>