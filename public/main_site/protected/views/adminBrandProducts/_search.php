<?php
/* @var $this AdminBrandProducts2Controller */
/* @var $model Products */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ProductID'); ?>
		<?php echo $form->textField($model,'ProductID',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProductName'); ?>
		<?php echo $form->textField($model,'ProductName',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ProductTitle'); ?>
		<?php echo $form->textField($model,'ProductTitle',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FacebookPage'); ?>
		<?php echo $form->textField($model,'FacebookPage',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TwitterPage'); ?>
		<?php echo $form->textField($model,'TwitterPage',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'PdfLink'); ?>
		<?php echo $form->textField($model,'PdfLink',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ImageID'); ?>
		<?php echo $form->textField($model,'ImageID',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->