<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#products-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

echo CHtml::Link('<i class="icon-plus-sign"></i> Create Products',array('/adminBrandProducts/create?catID='.$catID.'&brandID='.$brandID),array('style' => 'color: rgb(242, 242, 242);
background-color: rgb(6, 148, 203);margin-top: 17px;
padding: 2px 11px;
border-radius: 5px;
float: left;'));

$this->widget('zii.widgets.grid.CGridView', array(
    'pager'=>array('header'=>''),
    'id'=>'products-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        array(
              'type' => 'raw',
              'value' => 'CHtml::image(Yii::app()->baseUrl . "/uploads/products/" . $data->image->ImageURL,$data->image->ImageURL,array("width"=>100))',
            'header'=> 'Image'
       ),
        'ProductName',
        'ProductTitle',
        array(
            'class'=>'CButtonColumn',
            'header' => 'Actions',
            'template'=>'{update}{delete}',//{view}{delete}
            'buttons'=>array
            (
                'update' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/adminBrandProducts/update?catID=$_GET[catID]&brandID=$_GET[brandID]&id=$data->ProductID")',
                ),
                'delete' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/adminBrandProducts/delete?catID=$_GET[catID]&brandID=$_GET[brandID]&id=$data->ProductID")',
                    'click'=>'function(){return confirm("Are You sure");}',
                ),
            ),
        ),
    ),
)); ?>
