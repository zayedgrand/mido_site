<?php
/* @var $this AdminBrandProducts2Controller */
/* @var $model Products */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'products-form',
	'enableAjaxValidation'=>false,
     'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>
    
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'ProductName'); ?>
		<?php echo $form->textField($model,'ProductName',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
		<?php echo $form->error($model,'ProductName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ProductTitle'); ?>
		<?php echo $form->textField($model,'ProductTitle',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
		<?php echo $form->error($model,'ProductTitle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FacebookPage'); ?>
		<?php echo $form->textField($model,'FacebookPage',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
		<?php echo $form->error($model,'FacebookPage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TwitterPage'); ?>
		<?php echo $form->textField($model,'TwitterPage',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
		<?php echo $form->error($model,'TwitterPage'); ?>
	</div>

	<div class="row">
            <?php echo CHtml::label('PdfLink','PDF Link'); ?>
            <?php echo CHtml::fileField('PdfLink'); ?>
	</div>

        <?php
	if(ISSET($image))
        {
            echo '<img src="'.Yii::app()->baseUrl.'/uploads/products/'.$image.'" width="200px">';
        }
        ?>
	<div class="row">
            <?php echo CHtml::label('ImageURL','Image'); ?>
            <?php echo CHtml::fileField('ImageURL'); ?>
	</div>

	<div class="row buttons">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->