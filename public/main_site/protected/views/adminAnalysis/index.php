<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'PAGE VIEWS'
        },
        subtitle: {
//            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. of views'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{point.y:.1f}'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                $countPages = 0 ;
                $countPagesQuery = count($pageViews);
                foreach($pageViews as $pageViewsfe)
                {
                    $countPages ++ ;
                    echo "['".$pageViewsfe->page_name."', ".$pageViewsfe->page_views."]";
                    if($countPagesQuery > $countPages)
                    {
                        echo ",";
                    }
                }
                ?>
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                x: 4,
                y: 10,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
        }]
    });
    
        $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'ORDERS - SUBSCRIPTIONS - INQUIRY - CAREERS'
        },
        subtitle: {
//            text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No. of Senders'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{point.y:.1f}'
        },
        series: [{
            name: 'Population',
            data: [
                <?php
                echo "['Orders', ".$orders."],";
                echo "['Subscriptions', ".$subscriptions."],";
                echo "['Inquery', ".$inquiry."],";
                echo "['Careers', ".$careers."]";
                ?>
            ],
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                x: 4,
                y: 10,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
        }]
    });
});
</script>

<div id="container" style="min-width: 500px; height: 400px; margin: 0 auto"></div><br/>
<hr style="border: 3px;
height: 0px;
border-bottom-color: #0694CB;
border-bottom-style: inset;
width: 80%;
margin: 28px auto;"/>
<div id="container2" style="min-width: 500px; height: 400px; margin: 0 auto"></div><br/>

<script src="<?php echo Yii::app()->baseUrl ;?>/highchart/js/highcharts.js"></script>
<script src="<?php echo Yii::app()->baseUrl ;?>/highchart/js/modules/exporting.js"></script>