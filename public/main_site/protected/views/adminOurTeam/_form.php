<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'OurTeam-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model,'position',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
		<?php echo $form->error($model,'position'); ?>
	</div>

<br>

				<?php echo $form->errorSummary($model);
				      if(ISSET($image))
				      {
				          echo '<img src="'.Yii::app()->baseUrl.'/uploads/our_team/'.$image.'" width="400px">';
				      }
	      ?>
	<div class="row">
        <?php echo CHtml::label('image  289px* 289px','image'); ?> <br>
				<?php echo CHtml::fileField('image'); ?>
	</div>


					<br><br>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
