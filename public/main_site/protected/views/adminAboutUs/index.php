<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ba-bbq.min.js"></script>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#aboutus-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

echo CHtml::Link('<i class="icon-plus-sign"></i> Create Banner',array('/adminAboutUs/create'),array('style' => 'color: rgb(242, 242, 242);
background-color: rgb(6, 148, 203);margin-top: 17px;
padding: 2px 11px;
border-radius: 5px;
float: left;'));

$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'aboutus-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
            array(
		  'type' => 'raw',
		  'value' => 'CHtml::image(Yii::app()->baseUrl . "/uploads/about_slider/" . $data->image->ImageURL,$data->image->ImageURL,array("width"=>400))',
		'header'=> 'Image'
	   ),
            'Active',
            array(
			'class'=>'CButtonColumn',
			'header' => 'Actions',
			'template'=>'{update}{delete}',//{view}{delete}
		),
    ),
)); ?>
