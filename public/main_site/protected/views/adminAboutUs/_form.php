<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'aboutus-form',
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation'=>false
)); ?>
    
	<?php echo $form->errorSummary($model); 
        
        if(ISSET($image))
        {
            echo '<img src="'.Yii::app()->baseUrl.'/uploads/about_slider/'.$image.'" width="400px">';
        }
        ?>
	<div class="row">
                <?php echo CHtml::label('ImageURL','Image'); ?>
		<?php echo CHtml::fileField('ImageURL'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Active'); ?>
		<?php echo $form->checkBox($model,'Active'); ?>
		<?php echo $form->error($model,'Active'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->