<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $this->pageTitle; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl ;?>/images/favicon2.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo Yii::app()->baseUrl ;?>/images/favicon2.ico" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300' rel='stylesheet' type='text/css'>
    <?php
    Yii::app()->clientScript->scriptMap=array(
        'jquery.min.js'=>false,
        'jquery.ba-bbq.js'=>false,
        'jquery.yii.js'=>false
    );
    ?>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link id="bootstrap-style" href="<?php echo Yii::app()->request->baseUrl; ?>/css_bootstrap/bootstrap.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css_bootstrap/bootstrap-responsive.css" rel="stylesheet">
    <link id="base-style" href="<?php echo Yii::app()->request->baseUrl; ?>/css_bootstrap/style.css" rel="stylesheet">
    <link id="base-style-responsive" href="<?php echo Yii::app()->request->baseUrl; ?>/css_bootstrap/style-responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <link id="ie-style" href="<?php echo Yii::app()->request->baseUrl; ?>/css_bootstrap/ie.css" rel="stylesheet">
    <![endif]-->

    <!--[if IE 9]>
            <link id="ie9style" href="<?php echo Yii::app()->request->baseUrl; ?>/css_bootstrap/ie9.css" rel="stylesheet">
    <![endif]-->

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery-1.9.1.min.js"></script>

<style>
    .navbar-inverse{
            display:none !important;
    }

   a{
	font: 16px 'Source Sans Pro', sans-serif;
	color: #E5E5E5;

	text-transform: uppercase;
	border-bottom: 1px solid rgba(195, 195, 195, 0.32);
	letter-spacing: 1px;
	line-height: 10px;
    }
   .breadcrumb2 li{
        padding-left: 0;
        font: 16px 'Source Sans Pro', sans-serif;
        color: #E5E5E5;

        text-transform: capitalize;
        display: block;
        letter-spacing: 1px;
        line-height: 10px;
    }

	.ahref:hover{
		text-decoration:none;
		color: #fff;
	}
	.subahref:hover{
		text-decoration:none;
		color: #fff;
	}
	.main-left-nav
	{
		width: 21%;
		position: absolute;
		background: #0694CB;
		box-shadow: 1px 0px 5px #000;
		min-height: 100%;
	}
	.hactive{
		text-shadow: 0px 0px 1px;
		color: #FFF;
	}
	.hactive:hover{
		cursor:default;
		color:#FFF !important;
		text-shadow: 0px 0px 1px;
	}
	.ahead{
		font-family: 'Oxygen', sans-serif;
		font-weight: 300;
		font-size: 18px;
		margin: 0;
		text-indent: 18px;
		border-bottom: 1px solid rgba(195, 195, 195, 0.32);
		color: #4E4E4E;
	}
	.sublast{
	border-bottom: 1px solid rgba(195, 195, 195, 0.32);
	}
	#contentss{
		padding-left: 20px;
	}
	.otitle{
		font-family: 'Oxygen', sans-serif;
		font-weight: 300;
		text-align: left;
		font-family: 'Source Sans Pro', sans-serif;
		text-shadow: 0px 0px 0px;
		font-size: 22px;
		margin-top: 25px;
	}
	.expanded-table{
		font-family: 'Source Sans Pro', sans-serif;
	}
	.expanded-table th,.expanded-table td{
		font-family: 'Source Sans Pro', sans-serif;
		font-size: 17px;
		padding-top: 17px;
	}
	#yw0{
		padding:0 !important;
	}
	.mainco{
		padding-left: 0;
		margin: 0;
		position: relative;
		top: 0;
		float: left;
		width: 78.2%;
		margin-left: 21%;
	}
		.breadcrumb2 {
			list-style: none;
			overflow: hidden;
			font: 18px 'Source Sans Pro', sans-serif;
			margin: 0;
			margin-top: 32px;
		}
		.breadcrumb2 li {
			float: left;
		}
		.breadcrumb2 li a {
			color: white;
			text-decoration: none;
			padding: 10px 0 10px 55px;
			background: brown;                   /* fallback color */
			background: #0694CB;
			position: relative;
			display: block;
			float: left;
			text-transform:capitalize;
		}
		.breadcrumb2 li a:after {
			content: " ";
			display: block;
			width: 0;
			height: 0;
			border-top: 50px solid transparent;           /* Go big on the size, and let overflow hide */
			border-bottom: 50px solid transparent;
			border-left: 30px solid #0694CB;
			position: absolute;
			top: 50%;
			margin-top: -50px;
			left: 100%;
			z-index: 2;
		}
		.breadcrumb2 li a:before {
			content: " ";
			display: block;
			width: 0;
			height: 0;
			border-top: 50px solid transparent;           /* Go big on the size, and let overflow hide */
			border-bottom: 50px solid transparent;
			border-left: 30px solid white;
			position: absolute;
			top: 50%;
			margin-top: -50px;
			margin-left: 1px;
			left: 100%;
			z-index: 1;
		}
		.breadcrumb2 li:first-child a {
			padding-left: 10px;
			box-shadow: inset 2px 0px 5px -2px #000;
		}
		.breadcrumb2 li:nth-child(2) a       { background:        #11ADEA; }
		.breadcrumb2 li:nth-child(2) a:after { border-left-color: #11ADEA; }
		.breadcrumb2 li:nth-child(3) a       { background:        #1DBBF8; }
		.breadcrumb2 li:nth-child(3) a:after { border-left-color: #1DBBF8; }
		.breadcrumb2 li:nth-child(4) a       { background:        #4DCDFF; }
		.breadcrumb2 li:nth-child(4) a:after { border-left-color: #4DCDFF; }
		.breadcrumb2 li:nth-child(5) a       { background:        #7DD9FD; }
		.breadcrumb2 li:nth-child(5) a:after { border-left-color: #7DD9FD; }
		.breadcrumb2 li:last-child a {
			background: white !important;
			color: black;
			padding-left: 40px;
			pointer-events: none;
 			cursor: default;
		}
		.breadcrumb2 li:last-child a:after { border: 0; }
		.breadcrumb2 li a:hover { background: #077AA7; }
		.breadcrumb2 li a:hover:after { border-left-color: #077AA7 !important; }
		.right{
			text-align:right;
		}
</style>
</head>
<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner navbar-up">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
                            <?php echo CHtml::Link('<img src="'.Yii::app()->baseUrl.'/images/logo-cpanel.png" style="width: 78%;">', array('/adminHome/index'), array('class'=>'brand'));?>
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav tablet-mob-style">
                                <?php
                                if(Yii::app()->user->name != "Guest")
                                {
                                ?>
                                    <ul class="nav pull-right">
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> <?php echo Yii::app()->user->name ;?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li>
 									<!--<span>Account Settings</span>-->
									<?php echo CHtml::Link('<i class="icon-signout" style="color:white"></i> Logout', array('/site/logout'));?>
								</li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
                                <?php
                                }
                                ?>

				</div>
				<!-- end: Header Menu -->

			</div>
		</div>
	</div>
	<!-- start: Header -->
		<div class="container-fluid-full">
		<div class="row-fluid">
		<?php
			if(Yii::app()->user->name != "Guest")
			{
			?>
			<div id="sidebar-left" class="span2 sidebar-left-menu">
                            <div class="nav-collapse sidebar-nav">
                                <ul class="nav nav-tabs nav-stacked main-menu">
                                    <li <?php if(Yii::app()->controller->id == "adminHome") { echo "class='active'"; } ?>><?php echo CHtml::Link('<i class="icon-tasks"></i><span class="hidden-tablet menu-text"> Home page</span>', array('/adminHome/index'));?></li>
                                    <!--<li <?php // if(Yii::app()->controller->id == "adminAboutUs") { echo "class='active'"; } ?>><?php // echo CHtml::Link('<i class="icon-tasks"></i><span class="hidden-tablet menu-text"> About us</span>', array('/adminAboutUs/index'));?></li>-->
                                    <li <?php if(Yii::app()->controller->id == "adminBrands") { echo "class='active'"; } ?>><?php echo CHtml::Link('<i class="icon-tasks"></i><span class="hidden-tablet menu-text"> Brands</span>', array('/adminBrands/index'));?></li>
                                    <li <?php if(Yii::app()->controller->id == "adminNews") { echo "class='active'"; } ?>><?php echo CHtml::Link('<i class="icon-picture"></i><span class="hidden-tablet menu-text"> News</span>', array('/adminNews/index'));?></li>
                                    <li <?php if(Yii::app()->controller->id == "adminSubscription") { echo "class='active'"; } ?>><?php echo CHtml::Link('<i class="icon-picture"></i><span class="hidden-tablet menu-text"> Subscriptions</span>', array('/adminSubscription/index'));?></li>
                                    <li <?php if(Yii::app()->controller->id == "adminInquery") { echo "class='active'"; } ?>><?php echo CHtml::Link('<i class="icon-folder-open"></i><span class="hidden-tablet menu-text"> Inquiry</span>', array('/adminInquery/index'));?></li>
                                    <li <?php if(Yii::app()->controller->id == "adminGallery") { echo "class='active'"; } ?>><?php echo CHtml::Link('<i class="icon-envelope"></i><span class="hidden-tablet menu-text"> Gallery</span>', array('/adminGallery/index'));?></li>
                                    <li <?php if(Yii::app()->controller->id == "adminOurTeam") { echo "class='active'"; } ?>><?php echo CHtml::Link('<i class="icon-envelope"></i><span class="hidden-tablet menu-text"> Our Team</span>', array('/adminOurTeam/index'));?></li>
                                    <li <?php if(Yii::app()->controller->id == "adminCareers") { echo "class='active'"; } ?>><?php echo CHtml::Link('<i class="icon-envelope"></i><span class="hidden-tablet menu-text"> Careers</span>', array('/adminCareers/index'));?></li>
                                    <li <?php if(Yii::app()->controller->id == "adminAnalysis") { echo "class='active'"; } ?>><?php echo CHtml::Link('<i class="icon-envelope"></i><span class="hidden-tablet menu-text"> Analysis</span>', array('/adminAnalysis/index'));?></li>
                                </ul>
                            </div>
			</div>
			<?php
			}
			else
			{
			?>
			<style>
				.row-fluid .span10{
					width:100%;
					background-color: #F2F2F2;
				}

				#content{
					background: #F2F2F2;
				}

				.navbar .btn-navbar{
				display:none;
				}
			</style>
			<?php
			}
			?>
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			<div id="page-content" class="span10">
                        <?php
                        if(Yii::app()->controller->id == "adminHome") { $bcc = "Home page"; }
//                        if(Yii::app()->controller->id == "adminAboutUs") { $bcc = "About us"; }
                        if(Yii::app()->controller->id == "adminBrands") { $bcc = "Brands"; }
                        if(Yii::app()->controller->id == "adminBrandCategories") { $bcc = "Brands"; $bc = "Categories"; }
                        if(Yii::app()->controller->id == "adminBrandProducts") { $bcc = "Brands"; $aa = "Categories"; $b = "Products"; }
                        if(Yii::app()->controller->id == "adminNews") { $bcc = "News"; }
                        if(Yii::app()->controller->id == "adminInquery") { $bcc = "Inquiry"; }
                        if(Yii::app()->controller->id == "adminSubscription") { $bcc = "Subscriptions"; }
                        if(Yii::app()->controller->id == "adminGallery") { $bcc = "Gallery"; }
                        if(Yii::app()->controller->id == "adminCareers") { $bcc = "Careers"; }
                        if(Yii::app()->controller->id == "adminAnalysis") { $bcc = "Analysis"; }
                        ?>
                        <ul class="breadcrumb2">
                            <?php
                            if(ISSET($b))
                            {
                                echo '<li>'.CHtml::Link($bcc,array('adminBrands/index')).'</li>';
                                echo '<li>'.CHtml::Link($aa,array('adminBrandCategories/index/id/'.$_GET['catID'])).'</li>';
                                echo '<li>'.CHtml::Link($b,array('adminBrandProducts/index?catID='.$_GET['catID'].'&brandID='.$_GET['brandID'])).'</li>';
                            }
                            elseif(ISSET($bc))
                            {
                                echo '<li>'.CHtml::Link($bcc,array('adminBrands/index')).'</li>';
                                echo '<li>'.CHtml::Link($bc,array('adminBrandCategories/index/id/'.$_GET['id'])).'</li>';
                            }
                            elseif(ISSET($bcc))
                            {
                                echo '<li>'.CHtml::Link($bcc,array(Yii::app()->controller->id.'/index')).'</li>';
                            }



                            if(strpos(Yii::app()->controller->action->id, 'add') !== false)
                            {
                                echo '<li>'.CHtml::Link('Add '.$bcc,array(Yii::app()->controller->id.'/'.Yii::app()->controller->action->id)).'</li>';
                            }
                            elseif(strpos(Yii::app()->controller->action->id, 'edit') !== false)
                            {
                                echo '<li>'.CHtml::Link('Edit '.$bcc,array(Yii::app()->controller->id.'/'.Yii::app()->controller->action->id)).'</li>';
                            }
                            else
                            {
                                echo '<li>'.CHtml::Link('Manage Page',array(Yii::app()->controller->id.'/'.Yii::app()->controller->action->id)).'</li>';
                            }
                            ?>
                        </ul>
                            <div class="span-19">
                            <div id="content">
                                    <?php echo $content; ?>
                            </div><!-- content -->
                    </div>
                            <?php
                                    $this->beginWidget('zii.widgets.CPortlet', array(
                                            'title'=>'Operations',
                                    ));
                                    $this->widget('zii.widgets.CMenu', array(
                                            'items'=>$this->menu,
                                            'htmlOptions'=>array('class'=>'operations'),
                                    ));
                                    $this->endWidget();
                            ?>
                    </div>

		</div>
		</div>

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>

	<div class="clearfix"></div>

	<footer>

		<p>
			<span style="text-align:center;float:right">&copy; 2015 <a style="color: white;" href="http://www.bsocial-eg.com/" alt=" Powered_by_BSocial"> Powered by BSocial</a></span>

		</p>

	</footer>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery-migrate-1.0.0.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery-ui-1.10.0.custom.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.ui.touch-punch.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/modernizr.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.cookie.js"></script>
        <script src='<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/fullcalendar.min.js'></script>
        <script src='<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.dataTables.min.js'></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/excanvas.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.flot.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.flot.pie.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.flot.stack.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.flot.resize.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.chosen.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.uniform.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.cleditor.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.noty.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.elfinder.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.raty.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.iphone.toggle.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.uploadify-3.1.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.gritter.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.imagesloaded.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.masonry.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.knob.modified.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/jquery.sparkline.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/counter.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/retina.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js_bootstrap/custom.js"></script>
</body>
</html>
