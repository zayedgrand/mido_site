<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Mido - Food &amp; Beverage Supplier Since 1977 </title>
    <meta name="Description" content="MIDO Company was established in 1977 with the prime objective to be the leading Food & Beverage distributor and solution provider to the Food Service in Egypt.">
    <meta name="Keywords" content="  Food, Beverage, patisserie,  mido,official distributor, distributor, supply,supply chain,brand developer, illy order, Smoked Salmon , Middle East, illy, egypt, cairo, evian, Haagen-Dazs, badoit, certified angus beef, bridor, cunill, dammann, ekro, ireks, isi, la marzocco, la pavoni, midamar, ravifruit, volvic, issimo, delifrance">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl ;?>/images/favicon2.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo Yii::app()->baseUrl ;?>/images/favicon2.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->baseUrl ;?>/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/menu_sideslide.css" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

    <link href="<?php echo Yii::app()->baseUrl ;?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl ;?>/css/owl.theme.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <style>
        footer > *:nth-child(1) {
            padding: 25px 0;
            background-color: #000;
        }
        footer .container {
            width: 90%;
            margin: 0 auto;
            background: #000;
        }

        footer .container .flex {
            -webkit-display: flex;
            -moz-display: flex;
            -ms-display: flex;
            -o-display: flex;
            display: flex;
        }
		.flex-wrap {
			-webkit-flex-wrap:wrap;
			-moz-flex-wrap:wrap;
			-ms-flex-wrap:wrap;
			-o-flex-wrap:wrap;
			flex-wrap:wrap;
		}
        footer .flex > * {
            width: 100%;
        }
        .white-color {
            color: #fff;
        }
        footer h1 {
            padding-bottom: 0!important;
            text-transform: uppercase;
            margin-bottom: 10px;
            font-size: 1.17em;
        }
        .flex-asian {
            -webkit-flex-direction: column;
            -moz-flex-direction: column;
            -ms-flex-direction: column;
            -o-flex-direction: column;
            flex-direction: column;
        }
        .flex-wrap {
            -webkit-flex-wrap: wrap;
            -moz-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
            -o-flex-wrap: wrap;
            flex-wrap: wrap;
        }
        .flex-middle {
    -webkit-align-items: center;
    -moz-align-items: center;
    -ms-align-items: center;
    -o-align-items: center;
    align-items: center;
}
.flex-center {
    -webkit-justify-content: center;
    -moz-justify-content: center;
    -ms-justify-content: center;
    -o-justify-content: center;
    justify-content: center;
}
        footer ul {
            height: 200px;
            list-style:none;
            padding-left:0
        }
        footer li {
            width: auto!important;
            margin: 10px 0;
            text-transform: uppercase;
        }
        footer a {
            color: #8d8d8d;
            font-size: 15px;
            display: inline-block;
            text-decoration: none;
        }
        footer > *:nth-child(2) {
            padding: 15px 0px;
            font-size: 13px;
            background-color: #000;
            color: #fff;
            text-align: center;
        }
        footer .social-media a {
    background-color: #ebebeb;
    width: 23px;
    height: 22px;
    border-radius: 5px;
    margin: 0 5px;
    color: #000;
}

@media (max-width: 850px){
footer .container > .flex {
    display: block;
}
footer ul {
    height: auto;
}
}
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script src="<?php echo Yii::app()->baseUrl ;?>/js/owl.carousel.min.js"></script>
				    <script type="text/javascript">
    var a = Math.ceil(Math.random() * 10);
    var b = Math.ceil(Math.random() * 10);
    var c = a + b
    function DrawBotBoot()
    {
        document.write("What is "+ a + " + " + b +"? ");
        document.write("<input id='BotBootInput' class='form-row-input' required type='text' maxlength='2' size='2'/>");
    }

    function ValidBotBoot(){
        var d = document.getElementById('BotBootInput').value;
        if (d == c) {
				return true;
		$('.captcha-msg').hide();
			}else{
			$('.captcha-msg').html('wrong captcha');return false;


			}


    }
	$(document).ready(function() {

	});
    </script>
  </head>
  <body>
  <h1 style="display:none">Mido website</h1>
    <!-- toggle menu-->
    <div class="menu-wrap">
        <button class="close-button" id="close-button">Close Menu</button>
        <nav class="menu">
            <div class="icon-list">
                <?php
                echo CHtml::Link('<span>HOME</span>', array('./'));
                echo CHtml::Link('<span>ABOUT US</span>', array('./About'));
                echo CHtml::Link('<span>NEWS</span>', array('./News')); ?>
				<a href="http://www.mido.com.eg"><span>SHOP</span></a>
				<?php
                echo CHtml::Link('<span>UNIVERSITY OF COFFEE</span>', array('./University'));
                echo CHtml::Link('<span>GALLERY</span>', array('./Gallery'));
                echo CHtml::Link('<span>AWARDS</span>', array('./awards'));
                echo CHtml::Link('<span>CAREERS</span>', array('./Careers'));
		echo CHtml::Link('<span>Portfolio</span>', array('/uploads/mido-portfolio.pdf'));
                ?>

                <div class="menu-info-block">
                    <p>202 2266 8412</p>
                    <p>202 2268 5157</p>
                    <span>support@mido.com.eg</span>
                </div>

                <div class="menu-info-block">
                    <p>
                        8 fathy talaat St.<br>
                        Masaken Shiraton<br>
                        Heliopoles - Cairo<br>
                        Egypt
                    </p>
                </div>

                <div class="menu-info-block">
                    <a><i class="fa fa-facebook"></i></a>
                    <a><i class="fa fa-twitter"></i></a>
                    <a><i class="fa fa-google-plus"></i></a>
                    <a><i class="fa fa-rss"></i></a>
                </div>
            </div>
        </nav>
    </div>
    <!-- Toggle menu button-->
    <button class="menu-button" id="open-button"><span class="aftoggle">Menu</span></button>
    <!-- end of toggle menu-->
    <!--Main Container -->
    <div class="content-wrap">
        <div class="content">
        <!--Header -->
            <header>
                <a class="hdr-logo" href="<?php echo Yii::app()->baseUrl ;?>"><img alt="Mido" src="<?php echo Yii::app()->baseUrl ;?>/images/logo.png"/></a>
            </header>
			<div class="fixed-inquiry" data-toggle="modal" data-target=".smoked-salmon-popup" >
				<a class="fixed-inquiry-opened">Inquiry</a>
				<a class="fixed-inquiry-link">?</a>
			</div>
            <!--End Header -->
            <?php $this->beginContent('//layouts/main'); ?>
            <div id="content">
                <?php echo $content; ?>
            </div>
            <?php $this->endContent(); ?>


            <!--Footer html general block-->
            <div class="quote-block">
                <h2>
                    We are focused
                    <br>
                    and we believe in our dreams
                </h2>
            </div>
            <?php
            if(Yii::app()->controller->id == "home")
            {
                ?>
                <!--newsLetter-->
                <div class="footer-newsletter">
                        <h3>
                            Subscribe to our newsletter and win prizes from our online store
                        </h3>
                        <div class="newsletter-form-block">
                            <form name="newsletter" class="inquiry-form" method="POST" action="<?php echo Yii::app()->controller->createUrl('home/subscription'); ?>">
                                <input type="email" name="newsletter" class="newsletter-input" placeholder="Your email" />
                                <input type="submit" class="btn absolute-box-button" value="Subscribe" />
                            </form>
                        </div>
                </div>
                <!--end newsLetter-->
                <?php
            }
            ?>


            <!--footer social icons-->
            <footer>
  <div class="black-bg">
    <div class="container">
      <div class="flex">
        <div>
          <h1 class="white-color">sitemap</h1>
          <ul class="flex flex-asian flex-wrap">
            <li><a href="./About">about us</a></li>
            <li><a href="./News">news</a></li>
            <li><a href="http://www.mido.com.eg/">shop</a></li>
            <li><a href="./University/index">our courses</a></li>
            <li><a href="./Gallery">Gallery</a></li>
            <li><a href="./awards">awards</a></li>
            <li><a href="./Careers">career</a></li>
            <li><a href="./uploads/mido-portfolio.pdf">portfolio</a></li>
            <li><a href="{{url('FAQ')}}">FAQs</a></li>
          </ul>
        </div>
        <div>
          <h1 class="white-color">contact us</h1>
          <ul class="flex flex-asian flex-wrap">
            <li><a href="#" class="tel">+202 2266 8412</a></li>
            <li><a href="#" class="tel">+202 2266 8416</a></li>
            <li><a class="lowercase" href="mailto:{{$site_settings['our_email']}}">support@mido.com.eg</a></li>
            <li><a href="#" class="lowercase">8 fathy talaat st., sheraton buildings, heliopolis cairo, egypt</a></li>
          </ul>
        </div>
        <div>
          <h1 class="white-color upper">Follow us on</h1>
          <ul class="flex flex-wrap social-media">
            <li><a href="{{$site_settings['instagram_link']}}" class="flex flex-center flex-middle text-center" target="_blank"><i class="fab fa-instagram"></i></a></li>
            <li><a href="{{$site_settings['facebook_link']}}" class="flex flex-center flex-middle text-center" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="{{$site_settings['linkedin_link']}}" class="flex flex-center flex-middle text-center" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="text-center">
    <p>All Rights Reserved Mido © 2019 Designed And Developed By <span class="cap">vhorus</span></p>
  </div>
</footer>

            <!--end  social icons-->

            <!-- end footer-->
            </div>
        </div>
    		<div class="modal fade smoked-salmon-popup" tabindex="-1" role="dialog"  aria-hidden="true">
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">
                                <h3 class="title-head  marginedtop marginedbottom">INQUIRY</h3>
                                <div class="salamon-inquiry-form-inner">
                                    <form class="inquiry-form" name="inquiry-form" action="<?php echo Yii::app()->controller->createUrl('inquery/getinquiry'); ?>"  method="POST">
                                            <div class="form-row">
                                                <label>First Name</label>
                                                <input class="form-row-input" type="text" name="firstname" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                                <label>Last Name</label>
                                                <input class="form-row-input" type="text" name="lastname" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                            	<label>Phone #</label>
                                            	<input class="form-row-input"  type="text" pattern="\d+" name="phone" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                            	<label>Email</label>
                                            	<input class="form-row-input" type="text" name="email" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                                <label>Regarding</label>
                                                <input class="form-row-input" type="text" name="regarding" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                                <label>Are you human?</label>

                            <script type="text/javascript">DrawBotBoot()</script>

												<span class="captcha-msg" style="  position: relative;top: -9px;color: #F00;font-weight: 900;"></span>
                                            </div>

                                            <div class="form-row">
                                                <label>Message</label>
                                                <textarea class="form-row-textarea" name="message" required></textarea>
                                            </div>
                                            <p class="right-aligned marginedtop mobcenter">
                                                <input type="submit" class="inquiry-form-submit btn absolute-box-button"  value="Submit" />
                                            </p>
                                    </form>

                                </div>
			    </div>


			  </div>
			</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo Yii::app()->baseUrl ;?>/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ;?>/js/classie.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ;?>/js/main.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ;?>/js/jquery.form.js"></script>
    <script>
    $(document).ready(function() {
          //general script for brands
         $("#owl-demo2").owlCarousel({
            items : 9,
            lazyLoad : true,
	    loop: true,
	    autoplay: true,
	    autoplayTimeout: 1000,
	    autoplayHoverPause: true,
            navigation : true,
            pagination:true,
	    slideSpeed: 300,
	    paginationSpeed: 400,
            itemsTablet: [600,5],
            itemsMobile : [479,3],
            navigationText :[" <i class='fa fa-angle-left'></i> "," <i class='fa fa-angle-right'></i> "]
        });

         $("#owl-demo3").owlCarousel({
            items : 4,
            lazyLoad : true,
      	    loop: true,
      	    autoplay: true,
      	    autoplayTimeout: 1000,
      	    autoplayHoverPause: true,
                // navigation : true,
                pagination:true,
      	    slideSpeed: 300,
      	    paginationSpeed: 400,
            itemsTablet: [600,5],
            itemsMobile : [479,3],
            navigationText :[" <i class='fa fa-angle-left'></i> "," <i class='fa fa-angle-right'></i> "]
        });

					$('.inquiry-form-submit').click(function(){

							$(".inquiry-form").ajaxForm({
										beforeSubmit:ValidBotBoot

										,
										  beforeSend: function()
										{

												$("#progress").show();
												//clear everything
												$("#bar").width('0%');
												$("#message").html("");
												$("#percent").html("0%");

										},
										uploadProgress: function(event, position, total, percentComplete)
										{
												$("#bar").width(percentComplete+'%');
												$("#percent").html(percentComplete+'%');

										},
										success: function()
										{
												$("#bar").width('100%');
												$("#percent").html('100%');
												$('.inquiry-form .form-row').css('opacity','0.5');
												$('.inquiry-form-submit').val('Sent');
												$('.inquiry-form-submit').attr('disabled','disabled');
											  //  alert('done');
										},

										error: function()
										{

												//$("#message").html("<font color='red'> ERROR: unable to upload files</font>");

										}

								 });
						});
    });
    </script>
  </body>
</html>
