<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ba-bbq.min.js"></script>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#careers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

echo CHtml::Link('<i class="icon-download-alt"></i> EXPORT',array('/adminCareers/export'),array('style' => 'color: rgb(242, 242, 242);
background-color: rgb(6, 148, 203);margin-top: 17px;
padding: 2px 11px;
border-radius: 5px;
float: left;'));

$this->widget('zii.widgets.grid.CGridView', array(
        'pager'=>array('header'=>''),
	'id'=>'careers-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'firstName',
		'lastName',
		'Message',
                array(
                    'class'=>'CLinkColumn',
                    'label'=>'Download',
                    'urlExpression'=>'array("adminCareers/download/id/".$data->ID)',
                    'header'=>'Download Attachment'
		),
		'timestamp'
	),
)); ?>
