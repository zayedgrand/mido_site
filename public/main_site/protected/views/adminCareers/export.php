<?php
$data[] = array ('First name', 'Last name','Attachment', 'Message','Timestamp');

foreach($exportInquery as $exportInqueryfe)
{
    $url = $this->createAbsoluteUrl('uploads/career_attachs/'.$exportInqueryfe->attach);
    $data[] = array (
        $exportInqueryfe->firstName,
        $exportInqueryfe->lastName,
        $url,
        $exportInqueryfe->Message,
        $exportInqueryfe->timestamp
        );
}
Yii::import('application.extensions.phpexcel.JPhpExcel');
$xls = new JPhpExcel('UTF-8', false, 'mido_Careers');
$xls->addArray($data);
$xls->generateXML('mido_Careers');
?>