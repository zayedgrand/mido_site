<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-gallery.jpg" alt="mido awards" class="img-responsive" />
</div>
<h3 class="title-head marginedtop ">Awards</h3>
<div class="gallery-container col-md-11">
    <div id="blog-landing">

        <article class="white-panel"> 
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/doc00703020130915173355_001d.jpg" alt="ALT">
                    <h2 style="text-align:center;"><span>The Country with biggest growth</span></h2>
                    <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>-->
        </article>
        <article class="white-panel"> 
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/doc00703020130915173355_002d.jpg" alt="ALT">
                    <h2 style="text-align:center;"><span>Mido international trading S.A.E</span></h2>
                 <!--   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>-->
        </article>
        <article class="white-panel"> 
                    <img src="<?php echo Yii::app()->baseUrl; ?>/images/10.jpg" alt="ALT">
                    <h2 style="text-align:center;"><span>Mido international trading S.A.E</span></h2>
             <!--       <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>-->
        </article>
    </div>
</div>

<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" src="'.Yii::app()->baseUrl.'/images/pixel.jpg"  data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->outerImage->ImageURL.'" alt="Mido Clients">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/pinterest_grid.js"></script> 
<script>
$(document).ready(function() {
        //script for gallery 
    $('#blog-landing').pinterest_grid({
        no_columns: 2,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 700
    });
});
</script>