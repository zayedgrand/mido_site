<!--Home banners slider-->
<div class="home-slider-container">
    <div id="owl-demo" class="owl-carousel">
        <?php
        foreach($images as $imagesfe)
        {
            echo '<div class="item"><img src="'.Yii::app()->baseUrl.'/images/pixel.jpg" data-src="'.Yii::app()->baseUrl.'/uploads/home_slider/'.$imagesfe->image->ImageURL.'" alt="Mido Food Beverage" class="lazyOwl"></div>';
        }
        ?>
    </div>
</div>
<!--end banners slider-->

<!--Home features-->
<div class="home-products-block col-md-11 block">
        <div class="home-product-item col-md-4 " >
            <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/home-prod1.jpg" alt="Food &amp; Beverage brands" class="img-responsive" />
            <div class="home-product-label">
				<h2>Brand Development</h2>
<!--
                    <span>MIDO has pioneered in presenting new products in the market and excelled in the distribution and particularly in after sales services.</span>
-->

            </div>
            <p class="home-product-label-outer">MIDO has pioneered in presenting new products in the market and excelled in the distribution and particularly in after sales services.</p>

        </div>

        <div class="home-product-item col-md-4">
            <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/home-prod2.jpg" alt="Smoked Salmon" class="img-responsive"/>
            <div class="home-product-label">
				<h2>Production</h2>
<!--
                    <span>The only Smoked Salmon Facility in North Africa & the Middle East that is HACCP certified.</span>
-->

            </div>
            <p class="home-product-label-outer">The only Smoked Salmon Facility in North Africa & the Middle East that is HACCP certified.</p>

        </div>

        <div class="home-product-item col-md-4 "  >
            <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/home-prod3.jpg" alt="Supply Chain" class="img-responsive"/>
            <div class="home-product-label">
                <h2>Supply Chain</h2>
<!--
                    <span>MIDO is your professional distributor for all Food, Pastry, and Bakery needs.</span>
-->
            </div>
            <p class="home-product-label-outer">MIDO is your professional distributor for all Food, Pastry, and Bakery needs.</p>

        </div>
        <div class="clear"></div>
</div>
<!--end features-->

<!--quote-->
<div class="home-quote-holder" style="margin-top: 21px;">
                <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/qoute.jpg" alt="mido" class="img-responsive"/>
</div>
<!--end quote-->


<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11 ssdsd">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div title="Mido Brands" class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" src="'.Yii::app()->baseUrl.'/images/pixel.jpg" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->outerImage->ImageURL.'" alt="Mido brand">
                      </div>';
            }
            ?>

        </div>
    </div>
</div>
<!-- end brands carousal-->

<!-- university block-->
<div class="home-university-block">
        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/university.jpg" alt="Mido university" class="img-responsive" />
        <div class="university-text">
                <h3>
                        The first
                        <span class="mid-text">university of coffee</span>
                        <span class="bottom-text">in the middle east</span>
                </h3>
                <p class="txt-right">
                    <?php
                    echo CHtml::Link('KNOW MORE',array('university/index'), array('class'=>'btn absolute-box-button'));
                    ?>
                </p>
        </div>

</div>
<!--end university block-->

<!--home news block-->
<div class="home-news-block">
    <h3 class="title-head">LATEST NEWS</h3>
    <div class="home-news-block-carousel">
        <div id="owl-demo3" class="owl-carousel owl-theme">
            <?php
            foreach($latestNews as $latestNewsfe)
            {
                $rest = substr($latestNewsfe->NewsDescription, 0, 100);
                $myDateTime = DateTime::createFromFormat('Y-m-d', $latestNewsfe->Date);
                $formattednewsdate = $myDateTime->format('d-m-Y');
                echo '<div class="item">
                          <div class="news-left col-md-4">
                              <img src="'.Yii::app()->baseUrl.'/uploads/news/'.$latestNewsfe->image->ImageURL.'" alt="'.$latestNewsfe->NewsTitle.'" class="img-responsive"/>
                          </div>
                          <div class="news-right  col-md-8">
                              <h4><strong>'.$latestNewsfe->NewsTitle.'</strong></h4>
                              <span>'.$formattednewsdate.'</span>
                              <em style="display:none;">new</em>
                              <u style="display:none;">new</u>
                              <p>'.$rest.'</p>';
                                echo CHtml::Link('Read more',array('news/news/nid/'.$latestNewsfe->NewsID), array('class'=>'btn absolute-box-button'));
                          echo '</div>
                    </div>';
            }
            ?>
       </div>
    </div>
</div>

 
<!--end news block-->
<script>
    $(document).ready(function() {
    	//top banners
        $("#owl-demo").owlCarousel({
           singleItem:true,
	loop: true,
	autoPlay: true,
			lazyLoad : true,
			navigation : true,
            pagination:false,
            navigationText :[" <i class='fa fa-angle-left'></i> "," <i class='fa fa-angle-right'></i> "],
            slideSpeed : 300,
            paginationSpeed : 400

        });

        //brands
        $("#owl-demo2").owlCarousel({
           items: 9,
        loop: true,
        autoPlay: true,
                        lazyLoad : true,
                        navigation : true,
            pagination:false,
            navigationText :[" <i class='fa fa-angle-left'></i> "," <i class='fa fa-angle-right'></i> "],
            slideSpeed : 300,
            paginationSpeed : 400

        });

        // news
        var owl = $("#owl-demo3");

        owl.owlCarousel({
            items : 3,
            itemsDesktop : [1000,3],
            itemsDesktopSmall : [900,2],
            itemsTablet: [600,1],
            itemsMobile : 1
        });
    });
    </script>
