<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'banners-form',
	'enableAjaxValidation'=>false,
        'stateful'=>true, 
      'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>
	<?php echo $form->errorSummary($model); 
        if(ISSET($image))
        {
            echo '<img src="'.Yii::app()->baseUrl.'/uploads/home_slider/'.$image.'" width="400px">';
        }
        ?>
	<div class="row">
                <?php echo CHtml::label('ImageURL','Image'); ?>
		<?php echo CHtml::fileField('ImageURL'); ?>
                <br/>Recommended size : 1400px * 547px
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Active'); ?>
                <?php echo $form->checkBox($model,'Active'); ?>
		<?php echo $form->error($model,'Active'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->