<div class="inner-pages-banner">
        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-news.jpg" alt="products shelfs" class="img-responsive" />
</div>

<div class="news-inner-block">
    <h3 class="title-head">LATEST NEWS</h3>
                <?php
                foreach($allNews as $allNewsfe)
                {
                    $rest = substr($allNewsfe->NewsDescription, 0, 100);
                    $myDateTime = DateTime::createFromFormat('Y-m-d', $allNewsfe->Date);
                    $formattednewsdate = $myDateTime->format('d-m-Y');
                    echo '<div class="main-news-section col-md-6 col-sm-6">
                    <div class="news-left col-md-5">
                            <img src="'.Yii::app()->baseUrl.'/uploads/news/'.$allNewsfe->image->ImageURL.'" alt="'.$allNewsfe->NewsTitle.'" class="img-responsive"/>
                    </div>
                    <div class="news-right  col-md-7">
                            <h4><b>'.$allNewsfe->NewsTitle.'</b></h4>
                            <span>'.$formattednewsdate.'</span>
                            <p>'.$rest.'</p>';
                    echo CHtml::Link('Read more',array('news/news/nid/'.$allNewsfe->NewsID), array('class'=>'btn absolute-box-button'));
                    echo '</div>
                    <div class="clear"></div>
                    </div>';
                }
                ?>
            <div class="clear"></div>
</div>

<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" src="'.Yii::app()->baseUrl.'/images/pixel.jpg" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->outerImage->ImageURL.'" alt="Mido Clients">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59892009-1', 'auto');
  ga('send', 'pageview');

</script>
