<div class="inner-pages-banner">
        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-news.jpg" class="img-responsive" />
</div>

<div class="news-inner-block">
    <h3 class="title-head">LATEST NEWS</h3>
    <div class="news-left col-md-5">
        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/news/<?php echo $news[0]->image->ImageURL ; ?>" class="img-responsive"/>
    </div>
    <div class="news-right  col-md-7">
        <?php $myDateTime = DateTime::createFromFormat('Y-m-d', $news[0]->Date);
                $formattednewsdate = $myDateTime->format('d-m-Y');
        ?>
        <h4><b><?php echo $news[0]->NewsTitle ; ?></b></h4>
        <span><?php echo $formattednewsdate ; ?></span>
        <p><?php echo $news[0]->NewsDescription ; ?></p>
    </div>
    <div class="clear"></div>
</div>

<div class="news-archive-block">
    <div class="col-md-12">
        <h3 class="seprator-title">NEWS ARCHIVE</h3>
        <div class="news-archive-inner">
            <ul>
                <?php
                foreach($allNews as $allNewsfe)
                {
                    $amyDateTime = DateTime::createFromFormat('Y-m-d', $allNewsfe->Date);
                    $aformattednewsdate = $amyDateTime->format('d-m-Y');
                    echo '<li class="col-md-4 col-xs-12">';
                    echo CHtml::Link($allNewsfe->NewsTitle.'<span>('.$aformattednewsdate.')</span>',array('news/news/nid/'.$allNewsfe->NewsID), '');
                    echo '</li>';
                }
                ?>
            </ul>
        </div>
    </div>
    <div class="clear"></div>
</div>

<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->outerImage->ImageURL.'" alt="Lazy Owl Image">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59892009-1', 'auto');
  ga('send', 'pageview');

</script>