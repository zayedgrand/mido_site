<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gallery-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>
    
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'GalleryTitle'); ?>
		<?php echo $form->textField($model,'GalleryTitle',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
		<?php echo $form->error($model,'GalleryTitle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'GalleryDescription'); ?>
		<?php echo $form->textArea($model,'GalleryDescription',array('size'=>60,'maxlength'=>256,'style'=>'width: 400px;height: 297px;')); ?>
		<?php echo $form->error($model,'GalleryDescription'); ?>
	</div>

	<?php echo $form->errorSummary($model); 
        if(ISSET($image))
        {
            echo '<img src="'.Yii::app()->baseUrl.'/uploads/gallery/'.$image.'" width="400px">';
        }
        ?>
	<div class="row">
                <?php echo CHtml::label('ImageURL','Image'); ?>
		<?php echo CHtml::fileField('ImageURL'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->