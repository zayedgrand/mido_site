<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-gallery.jpg" class="img-responsive" />
</div>
<h3 class="title-head marginedtop ">Gallery</h3>
<div class="gallery-container col-md-11">
    <section id="blog-landing">
        <?php
        foreach($allGallery as $allGalleryfe)
        {
            echo '<article class="white-panel"> 
                    <img src="'.Yii::app()->baseUrl.'/uploads/gallery/'.$allGalleryfe->image->ImageURL.'" alt="ALT">
                    <h1><a href="#">'.$allGalleryfe->GalleryTitle.'</a></h1>
                    <p>'.$allGalleryfe->GalleryDescription.'</p>
                  </article>';
        }
        ?>
    </section>
</div>

<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->image->ImageURL.'" alt="Lazy Owl Image">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/pinterest_grid.js"></script> 
<script>
$(document).ready(function() {
        //script for gallery 
    $('#blog-landing').pinterest_grid({
        no_columns: 4,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 700
    });
});
</script>