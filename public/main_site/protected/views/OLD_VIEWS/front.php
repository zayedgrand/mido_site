<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Mido - Food &amp; Beverage Supplier Since 1977 </title>
    <meta name="Description" content="MIDO Company was established in 1977 with the prime objective to be the leading Food & Beverage distributor and solution provider to the Food Service in Egypt.">
    <meta name="Keywords" content="  Food, Beverage, patisserie,  mido,official distributor, distributor, supply,supply chain,brand developer, illy order, Smoked Salmon , Middle East, illy, egypt, cairo, evian, Haagen-Dazs, badoit, certified angus beef, bridor, cunill, dammann, ekro, ireks, isi, la marzocco, la pavoni, midamar, ravifruit, volvic, issimo, delifrance">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl ;?>/images/favicon2.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo Yii::app()->baseUrl ;?>/images/favicon2.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->baseUrl ;?>/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/menu_sideslide.css" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

    <link href="<?php echo Yii::app()->baseUrl ;?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl ;?>/css/owl.theme.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script src="<?php echo Yii::app()->baseUrl ;?>/js/owl.carousel.min.js"></script>
				    <script type="text/javascript">
    var a = Math.ceil(Math.random() * 10);
    var b = Math.ceil(Math.random() * 10);
    var c = a + b
    function DrawBotBoot()
    {
        document.write("What is "+ a + " + " + b +"? ");
        document.write("<input id='BotBootInput' class='form-row-input' required type='text' maxlength='2' size='2'/>");
    }

    function ValidBotBoot(){
        var d = document.getElementById('BotBootInput').value;
        if (d == c) {
				return true;
		$('.captcha-msg').hide();
			}else{
			$('.captcha-msg').html('wrong captcha');return false;


			}


    }
	$(document).ready(function() {

	});
    </script>
  </head>
  <body>
  <h1 style="display:none">Mido website</h1>
    <!-- toggle menu-->
    <div class="menu-wrap">
        <button class="close-button" id="close-button">Close Menu</button>
        <nav class="menu">
            <div class="icon-list">
                <?php
                echo CHtml::Link('<span>HOME</span>', array('/Home/index'));
                echo CHtml::Link('<span>ABOUT US</span>', array('/About/index'));
                echo CHtml::Link('<span>NEWS</span>', array('/News/index'));
                echo CHtml::Link('<span>ORDERS</span>', array('/Inquery/index'));
                echo CHtml::Link('<span>UNIVERSITY OF COFFEE</span>', array('/University/index'));
                echo CHtml::Link('<span>GALLERY</span>', array('/Gallery/index'));
                echo CHtml::Link('<span>AWARDS</span>', array('/awards/index'));
                echo CHtml::Link('<span>CAREERS</span>', array('/Careers/index'));
		echo CHtml::Link('<span>Portfolio</span>', array('/uploads/mido-portfolio.pdf'));
                ?>

                <div class="menu-info-block">
                    <p>202 2266 8412</p>
                    <p>202 2268 5157</p>
                    <span>support@mido.com.eg</span>
                </div>

                <div class="menu-info-block">
                    <p>
                        8 fathy talaat St.<br>
                        Masaken Shiraton<br>
                        Heliopoles - Cairo<br>
                        Egypt
                    </p>
                </div>

                <div class="menu-info-block">
                    <a><i class="fa fa-facebook"></i></a>
                    <a><i class="fa fa-twitter"></i></a>
                    <a><i class="fa fa-google-plus"></i></a>
                    <a><i class="fa fa-rss"></i></a>
                </div>
            </div>
        </nav>
    </div>
    <!-- Toggle menu button-->
    <button class="menu-button" id="open-button"><span class="aftoggle">Menu</span></button>
    <!-- end of toggle menu-->
    <!--Main Container -->
    <div class="content-wrap">
        <div class="content">
        <!--Header -->
            <header>
                <a class="hdr-logo" href="<?php echo Yii::app()->baseUrl ;?>"><img alt="Mido" src="<?php echo Yii::app()->baseUrl ;?>/images/logo.png"/></a>
            </header>
			<div class="fixed-inquiry" data-toggle="modal" data-target=".smoked-salmon-popup" >
				<a class="fixed-inquiry-opened">Inquiry</a>
				<a class="fixed-inquiry-link">?</a>
			</div>
            <!--End Header -->
            <?php $this->beginContent('//layouts/main'); ?>
            <div id="content">
                <?php echo $content; ?>
            </div>
            <?php $this->endContent(); ?>


            <!--Footer html general block-->
            <div class="quote-block">
                <h2>
                    We are focused
                    <br>
                    and we believe in our dreams
                </h2>
            </div>
            <?php
            if(Yii::app()->controller->id == "home")
            {
                ?>
                <!--newsLetter-->
                <div class="footer-newsletter">
                        <h3>
                            Subscribe to our newsletter and win prizes from our online store
                        </h3>
                        <div class="newsletter-form-block">
                            <form name="newsletter" class="inquiry-form" method="POST" action="<?php echo Yii::app()->controller->createUrl('home/subscription'); ?>">
                                <input type="email" name="newsletter" class="newsletter-input" placeholder="Your email" />
                                <input type="submit" class="btn absolute-box-button" value="Subscribe" />
                            </form>
                        </div>
                </div>
                <!--end newsLetter-->
                <?php
            }
            ?>


            <!--footer social icons-->
            <footer class="footer">
                <h3>FOLLOW US ON</h3>
                <div class="footer-social-icons">
                    <a class="footer-social-icons-link"><i class="fa fa-facebook"></i></a>
                    <a class="footer-social-icons-link"><i class="fa fa-twitter"></i></a>
                    <a class="footer-social-icons-link"><i class="fa fa-google-plus"></i></a>
                </div>
            </footer>
            <!--end  social icons-->
            <div class="footer-copy">
                <p>All Rights Reserved Mido © 2015 Designed And Developed By <a target="_blank" href="http://www.vhorus.com/">Vhorus</a>.</p>
            </div>
            <!-- end footer-->
            </div>
        </div>
    		<div class="modal fade smoked-salmon-popup" tabindex="-1" role="dialog"  aria-hidden="true">
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">
                                <h3 class="title-head  marginedtop marginedbottom">INQUIRY</h3>
                                <div class="salamon-inquiry-form-inner">
                                    <form class="inquiry-form" name="inquiry-form" action="<?php echo Yii::app()->controller->createUrl('inquery/getinquiry'); ?>"  method="POST">
                                            <div class="form-row">
                                                <label>First Name</label>
                                                <input class="form-row-input" type="text" name="firstname" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                                <label>Last Name</label>
                                                <input class="form-row-input" type="text" name="lastname" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                            	<label>Phone #</label>
                                            	<input class="form-row-input"  type="text" pattern="\d+" name="phone" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                            	<label>Email</label>
                                            	<input class="form-row-input" type="text" name="email" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                                <label>Regarding</label>
                                                <input class="form-row-input" type="text" name="regarding" required placeholder="" />
                                            </div>
                                            <div class="form-row">
                                                <label>Are you human?</label>

                            <script type="text/javascript">DrawBotBoot()</script>

												<span class="captcha-msg" style="  position: relative;top: -9px;color: #F00;font-weight: 900;"></span>
                                            </div>

                                            <div class="form-row">
                                                <label>Message</label>
                                                <textarea class="form-row-textarea" name="message" required></textarea>
                                            </div>
                                            <p class="right-aligned marginedtop mobcenter">
                                                <input type="submit" class="inquiry-form-submit btn absolute-box-button"  value="Submit" />
                                            </p>
                                    </form>

                                </div>
			    </div>


			  </div>
			</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo Yii::app()->baseUrl ;?>/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ;?>/js/classie.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ;?>/js/main.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ;?>/js/jquery.form.js"></script>
    <script>
    $(document).ready(function() {
          //general script for brands
         $("#owl-demo2").owlCarousel({
            items : 9,
            lazyLoad : true,
	    loop: true,
	    autoplay: true,
	    autoplayTimeout: 1000,
	    autoplayHoverPause: true,
            navigation : true,
            pagination:true,
	    slideSpeed: 300,
	    paginationSpeed: 400,
            itemsTablet: [600,5],
            itemsMobile : [479,3],
            navigationText :[" <i class='fa fa-angle-left'></i> "," <i class='fa fa-angle-right'></i> "]
        });

         $("#owl-demo3").owlCarousel({
            items : 4,
            lazyLoad : true,
      	    loop: true,
      	    autoplay: true,
      	    autoplayTimeout: 1000,
      	    autoplayHoverPause: true,
                // navigation : true,
                pagination:true,
      	    slideSpeed: 300,
      	    paginationSpeed: 400,
            itemsTablet: [600,5],
            itemsMobile : [479,3],
            navigationText :[" <i class='fa fa-angle-left'></i> "," <i class='fa fa-angle-right'></i> "]
        });

					$('.inquiry-form-submit').click(function(){

							$(".inquiry-form").ajaxForm({
										beforeSubmit:ValidBotBoot

										,
										  beforeSend: function()
										{

												$("#progress").show();
												//clear everything
												$("#bar").width('0%');
												$("#message").html("");
												$("#percent").html("0%");

										},
										uploadProgress: function(event, position, total, percentComplete)
										{
												$("#bar").width(percentComplete+'%');
												$("#percent").html(percentComplete+'%');

										},
										success: function()
										{
												$("#bar").width('100%');
												$("#percent").html('100%');
												$('.inquiry-form .form-row').css('opacity','0.5');
												$('.inquiry-form-submit').val('Sent');
												$('.inquiry-form-submit').attr('disabled','disabled');
											  //  alert('done');
										},

										error: function()
										{

												//$("#message").html("<font color='red'> ERROR: unable to upload files</font>");

										}

								 });
						});
    });
    </script>
  </body>
</html>
