<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-inquiry.jpg" class="img-responsive" />
</div>
<h3 class="title-head marginedtop marginedbottom">INQUIRY FORM</h3>
<div class="inquiry-form-block">

    <div class="form-gallery col-md-5">
            <div class="form-gallery-top col-md-12">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/inquiry-form1.jpg" class="img-responsive"/>

            </div>
            <div class="form-gallery-left col-md-6 col-xs-6">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/inquiry-form2.jpg" class="img-responsive"/>
                    <div class="inner-inquiry-box">
                            <a>
                                    <img src="<?php echo Yii::app()->baseUrl ;?>/images/illy-logo.jpg" />
                                    FACEBOOK STORE

                            </a>
                    </div>
            </div>
            <div class="form-gallery-right col-md-6 col-xs-6">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/inquiry-form3.jpg" class="img-responsive"/>

                    <div class="inner-inquiry-box">
                            <p>
                                    Order smoked salmon for weddings and parties ...NOW
                            </p>
                            <p class="right-aligned"><a class="btn absolute-box-button" data-toggle="modal" data-target=".smoked-salmon-popup">CLICK HERE</a></p>
                    </div>
            </div>
            <div class="clear"></div>
    </div>
    <div class="inquiry-form-inner col-md-7">
            <form class="inquiry-form" name="inquiry-form" action="inquiries.php" method="POST">
                    <div class="form-row">
                            <label>First Name</label>
                            <input class="form-row-input" type="text" name="firstname" required placeholder="First Name" />
                    </div>
                    <div class="form-row">
                            <label>Last Name</label>
                            <input class="form-row-input" type="text" name="lastname" required placeholder="Last Name" />
                    </div>
                    <div class="form-row">
                            <label>Regarding</label>
                            <input class="form-row-input" type="text" name="firstname" required placeholder="First Name" />
                    </div>

                    <div class="form-row">
                            <label>Message</label>
                            <textarea class="form-row-textarea" name="inquirytextarea" required></textarea>
                    </div>
                    <p class="right-aligned marginedtop mobcenter">
                            <input type="submit" class="inquiry-form-submit btn absolute-box-button" value="Submit" />
                    </p>
            </form>
    </div>
    <div class="clear"></div>					
</div>


<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->image->ImageURL.'" alt="Lazy Owl Image">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->

<div class="modal fade smoked-salmon-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <h3 class="long-title-head  marginedtop marginedbottom">SMOKED SALMON ORDERS FOR WEDDINGS AND PARTIES</h3>
                  <div class="salamon-inquiry-form-inner">
                                  <form class="inquiry-form" name="inquiry-form" action="inquiries.php" method="POST">
                                          <div class="form-row">
                                                  <label>First Name</label>
                                                  <input class="form-row-input" type="text" name="salmonfirstname" required placeholder="First Name" />
                                          </div>
                                          <div class="form-row">
                                                  <label>Last Name</label>
                                                  <input class="form-row-input" type="text" name="salmonlastname" required placeholder="Last Name" />
                                          </div>
                                          <div class="form-row">
                                                  <label>Mobile</label>
                                                  <input class="form-row-input" type="text" name="salmonmobile" required placeholder="First Name" />
                                          </div>
                                          <div class="form-row">
                                                  <label>Delivery Location</label>
                                                  <input class="form-row-input" type="text" name="salmonlocation" required placeholder="First Name" />
                                          </div>

                                          <div class="form-row">
                                                  <label>Event date</label>
                                                  <input class="form-row-input" type="text" name="salmondate" required placeholder="First Name" />
                                          </div>
                                          <div class="form-row">
                                                  <label>Products needed</label>
                                                  <select class="form-row-select" name="salmondate" required >
                                                          <option>Salmon</option>
                                                  </select> 
                                          </div>

                                          <div class="form-row">
                                                  <label>Order Comments</label>
                                                  <textarea class="form-row-textarea" name="salmontextarea" required></textarea>
                                          </div>
                                          <p class="center-aligned marginedtop mobcenter">
                                                  <input type="submit" class="inquiry-form-submit btn absolute-box-button" value="Submit" />
                                          </p>
                                  </form>
                          </div>
      </div>


    </div>
  </div>	

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ;?>/js/jquery.form.js"></script>
	<script>
    $(document).ready(function() {
     // script for inquiry page
      $('.inquiry-form-submit').click(function(){
		$(".inquiry-form").ajaxForm({
					 
				  beforeSend: function() 
				{
					$("#progress").show();
					//clear everything
					$("#bar").width('0%');
					$("#message").html("");
					$("#percent").html("0%");
				},
				uploadProgress: function(event, position, total, percentComplete) 
				{
					$("#bar").width(percentComplete+'%');
					$("#percent").html(percentComplete+'%');

				
				},
				success: function() 
				{
					$("#bar").width('100%');
					$("#percent").html('100%');
					alert('done');
				
							

		
				},
				
				error: function()
				{

					//$("#message").html("<font color='red'> ERROR: unable to upload files</font>");

				}
						 
			 });
		});
              
 
    });
    </script>