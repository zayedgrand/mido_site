<div class="inner-pages-banner">
        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-news.jpg" class="img-responsive" />
</div>
<style>
.main-news-section{
margin-bottom: 30px;
margin-top: 15px;
position:relative;
min-height: 185px;
}
</style>
<div class="news-inner-block">
    <h3 class="title-head">LATEST NEWS</h3>
                <?php
                foreach($allNews as $allNewsfe)
                {
                    $rest = substr($allNewsfe->NewsDescription, 0, 100);
                    echo '<div class="main-news-section col-md-6 col-sm-6">
                    <div class="news-left col-md-5">
                            <img src="'.Yii::app()->baseUrl.'/uploads/news/'.$allNewsfe->image->ImageURL.'" class="img-responsive"/>
                    </div>
                    <div class="news-right  col-md-7">
                            <h4>'.$allNewsfe->NewsTitle.'</h4>
                            <span>'.$allNewsfe->Date.'</span>
                            <p>'.$rest.'</p>';
                    echo CHtml::Link('Read more',array('news/news/nid/'.$allNewsfe->NewsID), array('class'=>'btn absolute-box-button'));
                    echo '</div>
                    <div class="clear"></div>
                    </div>';
                }
                ?>
            <div class="clear"></div>
</div>

<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->image->ImageURL.'" alt="Lazy Owl Image">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->