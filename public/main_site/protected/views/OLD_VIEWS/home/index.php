<!--Home banners slider-->
<div class="home-slider-container">
    <div id="owl-demo" class="owl-carousel">
        <?php
        foreach($images as $imagesfe)
        {
            echo '<div class="item"><img src="'.Yii::app()->baseUrl.'/uploads/'.$imagesfe->image->ImageURL.'" alt="Mido"></div>';
        }
        ?>
    </div>
</div>
<!--end banners slider-->

<!--Home features-->
<div class="home-products-block col-md-11 block">
        <div class="home-product-item col-md-4 " >
            <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/home-prod1.jpg" class="img-responsive" />
            <div class="home-product-label">
                <h2>Brand Development
                    <p>Evian spring is more than 8000 years old, located at the heart of the French Alps.  Evian ground water is fed by rain and melted snow.</p>	
                </h2>
            </div>	
            <p class="home-product-label-outer">Evian spring is more than 8000 years old, located at the heart of the French Alps.  Evian ground water is fed by rain and melted snow.</p>	
        </div>
    
        <div class="home-product-item col-md-4">
            <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/home-prod2.jpg" class="img-responsive"/>
            <div class="home-product-label">
                <h2>Production
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>			
                </h2>
            </div>
            <p class="home-product-label-outer">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>	
        </div>
    
        <div class="home-product-item col-md-4 "  >
            <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/home-prod3.jpg" class="img-responsive"/>
            <div class="home-product-label">
                <h2>Supply Chain
                    <p>IREKS is a partner of the bakeries, known worldwide for first-class baking ingredients and creative product ideas.</p>	
                </h2>
            </div>
            <p class="home-product-label-outer">IREKS is a partner of the bakeries, known worldwide for first-class baking ingredients and creative product ideas.</p>	

        </div>
        <div class="clear"></div>
</div>
<!--end features-->

<!--quote-->
<div class="home-quote-holder">
                <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/qoute.jpg" class="img-responsive"/>
</div>
<!--end quote-->


<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->image->ImageURL.'" alt="Lazy Owl Image">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->

<!-- university block-->
<div class="home-university-block">
        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/university.jpg" class="img-responsive" />
        <div class="university-text">
                <h2>
                        The first
                        <span class="mid-text">university of coffee</span>
                        <span class="bottom-text">in the middle east</span>
                </h2>
                <p class="txt-right"><a class="btn absolute-box-button">KNOW MORE</a></p>
        </div>	

</div>
<!--end university block-->

<!--home news block-->
<div class="home-news-block">
    <h3 class="title-head">LATEST NEWS</h3>
    <div class="home-news-block-carousel">
        <div id="owl-demo3" class="owl-carousel owl-theme">
            <?php
            foreach($latestNews as $latestNewsfe)
            {
                $rest = substr($latestNewsfe->NewsDescription, 0, 100);
                echo '<div class="item">
                          <div class="news-left col-md-4">
                              <img src="'.Yii::app()->baseUrl.'/uploads/news/'.$latestNewsfe->image->ImageURL.'" class="img-responsive"/>
                          </div>
                          <div class="news-right  col-md-8">
                              <h4>'.$latestNewsfe->NewsTitle.'</h4>
                              <span>'.$latestNewsfe->Date.'</span>
                              <p>'.$rest.'</p>';
                                echo CHtml::Link('Read more',array('news/news/nid/'.$latestNewsfe->NewsID), array('class'=>'btn absolute-box-button'));
                          echo '</div>
                    </div>';
            }
            ?>
       </div>
    </div>
</div>

<!--end news block-->
<script>
    $(document).ready(function() {
    	//top banners
        $("#owl-demo").owlCarousel({
            navigation : true,
            pagination:false,
            navigationText :[" <i class='fa fa-angle-left'></i> "," <i class='fa fa-angle-right'></i> "],
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true
        });
        
        // news 
        var owl = $("#owl-demo3");

        owl.owlCarousel({
            items : 2, 
            itemsDesktop : [1000,2],
            itemsDesktopSmall : [900,2], 
            itemsTablet: [600,1], 
            itemsMobile : 1 
        });
    });
    </script>
