<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-careers.jpg" class="img-responsive" />
</div>
<h3 class="title-head marginedtop marginedbottom">CAREERS</h3>
<div class="careers-form-block">

    <div class="careers-form-inner col-md-6">
            <p class="careers-form-text">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
            </p>
            <form class="careers-form" name="inquiry-form" action="getcareers.php" method="POST">
                    <div class="form-row">
                            <label>First Name</label>
                            <input class="form-row-input" type="text" name="careersfirstname" required placeholder="First Name" />
                    </div>
                    <div class="form-row">
                            <label>Last Name</label>
                            <input class="form-row-input" type="text" name="careerslastname" required placeholder="Last Name" />
                    </div>

                    <div class="form-row">
                            <label>Message</label>
                            <textarea class="form-row-textarea" name="careerstextarea" required></textarea>
                    </div>
                    <p class="right-aligned marginedtop mobcenter">
                            <input type="submit" class="careers-form-submit btn absolute-box-button" value="Send" />
                    </p>
            </form>
    </div>
    <div class="clear"></div>					
</div>


<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->image->ImageURL.'" alt="Lazy Owl Image">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->

<!--script for careers-->
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ;?>/js/jquery.form.js"></script>
<script>
$(document).ready(function() {
//script for careers
$('.careers-form-submit').click(function(){
        $(".careers-form").ajaxForm({

                    beforeSend: function() 
                  {
                          $("#progress").show();
                          //clear everything
                          $("#bar").width('0%');
                          $("#message").html("");
                          $("#percent").html("0%");
                  },
                  uploadProgress: function(event, position, total, percentComplete) 
                  {
                          $("#bar").width(percentComplete+'%');
                          $("#percent").html(percentComplete+'%');


                  },
                  success: function() 
                  {
                          $("#bar").width('100%');
                          $("#percent").html('100%');
                          alert('done');




                  },

                  error: function()
                  {

                          //$("#message").html("<font color='red'> ERROR: unable to upload files</font>");

                  }

           });
    });


});
</script>