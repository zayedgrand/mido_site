<!-- timeline plugin-->
<link href="<?php echo Yii::app()->baseUrl ;?>/css/tooltipster.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/themes/tooltipster-light.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/themes/tooltipster-noir.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/themes/tooltipster-punk.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/themes/tooltipster-shadow.css" />	
<!--end -->
<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-about.jpg" class="img-responsive" />
</div>
<h3 class="title-head marginedtop marginedbottom">ABOUT US</h3>
<div class="aboutus-block">
    <p class="aboutus-text">
        MIDO Company was established in 1977 with the prime objective to be the leading Food & Beverage distributor and solution provider to the Food Service and Away from Home market in Egypt.  In 1985, MIDO established the first Smoked Salmon facility in the Middle East, currently MIDO’s Smoked Salmon is considered to be the most prestigious with the highest quality.
    </p>
    <h3>We are much more than just distributors!</h3>
</div>

<div class="aboutus-timeline-block">
        <h2 class="aboutus-boxed-head">MIDO's Achievement Timeline</h2>

        <div class="aboutus-timeline-inner">
            <div id="owl-demo-timeline" class="owl-carousel">
                <a class="item aboutus-timeline-milestone ttleft" title="Establishment … Food & Beverage Importing & Distribution" ><span>1977</span></a>
                <a class="item aboutus-timeline-milestone ttleft" title="Smoked Salmon production through  a JV with UK Co."><span>1985</span></a>
                <a class="item aboutus-timeline-milestone ttleft" title="MIDO’s smoked salmon new facility in operation"><span>1990</span></a>
                <a class="item aboutus-timeline-milestone ttcenter" title="Sharm elsheikh branch – unique location & presence" ><span>1991</span></a>
                <a class="item aboutus-timeline-milestone ttright" title="Supply chain services to international accounts"><span>1993</span></a>
                <a class="item aboutus-timeline-milestone ttright" title="New strategy (F&B Brands/Non hotels)"><span>2001</span></a>
                <a class="item aboutus-timeline-milestone ttright" title="Management migration to 2nd generation"><span>2013</span></a>
            </div>
        </div>
</div>

<div class="onestopshop-block">
        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/onestopshop.png" class="img-responsive" />
</div>

<div class="family-block">
        <h2 class="family-block-title">
                We are one big <span>family</span>
        </h2>
</div>

<div class="facilites-block">
        <h3 class="title-head">FACILITIES</h3>
        <div class="facilites-inner">
                <div class="facilites-item  col-xs-12 col-sm-6 col-md-3 ">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/headoffice.jpg" />
                        <p>Head office & Sales<br>
                        in Heliopolis
                        </p>
                </div>
                <div class="facilites-item col-xs-12 col-sm-6 col-md-3">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/coldstores.jpg" />
                        <p>Cold Stores<br>
                        1000 tons
                        </p>
                </div>
                <div class="facilites-item col-xs-12 col-sm-6 col-md-3">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/drystores.jpg" />
                        <p>Dry Stores<br>
                        250 Pallets
                        </p>
                </div>
                <div class="facilites-item col-xs-12 col-sm-6 col-md-3">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/smokedsalmon.jpg" />
                        <p>Smoked Salmon Production<br>
                        Capacity 350 Tons
                        </p>
                </div>
                <div class="facilites-item col-xs-12 col-sm-6 col-md-3">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/trucks.jpg" />
                        <p>Refrigerated Trucks <br>
                        15
                        </p>
                </div>
                <div class="facilites-item col-xs-12 col-sm-6 col-md-3">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/employee.jpg" />
                        <p>Employee <br>
                        140 Employees
                        </p>
                </div>
                <div class="facilites-item col-xs-12 col-sm-6 col-md-3">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/coldstores2.jpg" />
                        <p>Cold Stores (Under construction)<br>
                        3000 Tons
                        </p>
                </div>
                <div class="facilites-item col-xs-12 col-sm-6 col-md-3">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/universityicon.jpg" />
                        <p>University Of Coffee<br>
                        16 Seats
                        </p>
                </div>
                <div class="clear"></div>
        </div>
</div>


<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->image->ImageURL.'" alt="Lazy Owl Image">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ;?>/js/jquery.tooltipster.min.js"></script>
<script>
$(document).ready(function() {
    //timeline script
    $('.ttleft').tooltipster({
            touchDevices: true,
            animation:"swing",
            position:"top-left",
            minWidth:"300",
            interactiveTolerance:"50",
            offsetY:"15",
            theme: "tooltipster-pink",
            arrowColor:"#E10915"

    });

    $('.ttcenter').tooltipster({
            touchDevices: true,
            animation:"swing",
            position:"top",
            minWidth:"300",
            interactiveTolerance:"50",
            offsetY:"15",
            theme: "tooltipster-pink",
            arrowColor:"#E10915"

    });

    $('.ttright').tooltipster({
            touchDevices: true,
            animation:"swing",
            position:"top-right",
            minWidth:"300",
            interactiveTolerance:"50",
            offsetY:"15",
            theme: "tooltipster-pink",
            arrowColor:"#E10915"

    });
    var owl = $("#owl-demo-timeline");

    owl.owlCarousel({
            pagination:false,
        items : 7, //10 items above 1000px browser width
        itemsDesktop : [1000,7], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,3], // betweem 900px and 601px
        itemsTablet: [600,3], //2 items between 600 and 0
        itemsMobile :  [479,1] // itemsMobile disabled - inherit from itemsTablet option
    });
});
</script>