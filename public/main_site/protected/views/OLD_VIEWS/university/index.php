<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-university.jpg" class="img-responsive" />
</div>
<h3 class="title-head marginedtop marginedbottom">
            <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/universitytitle.jpg" width="230" />
    </h3>
<div class="aboutus-block">
    <p class="aboutus-text">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
    </p>
</div>
<div class="university-courses-block">
    <h3 class="title-head marginedbottom">Our Courses</h3>
    <div class="university-courses-inner col-md-12">
            <div class="university-courses-item col-xs-6">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/course1.jpg" class="img-responsive" />
                    <div class="university-courses-overlay">
                            <p class="course-num">Course 1</p>
                            <h3>One day practical</h3>
                            <ul class="course-elements">
                                    <li>  The basics of espresso, Cappuccino & Latte </li>
                                    <li>  About Agriculture</li>
                                    <li>  About Roasting</li>
                                    <li>  Troubleshooting</li>
                                    <li>   Practical Training</li>

                            </ul>
                            <p class="course-price">
                                    <span class="course-price-title">Price</span>
                                    <span  class="course-price-value">350 L.E</span>
                            </p>
                    </div>
            </div>
            <div class="university-courses-item col-xs-6">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/course2.jpg" class="img-responsive" />
                    <div class="university-courses-overlay">
                            <p class="course-num">Course 2</p>
                            <h3>Coffee Expert</h3>
                            <ul class="course-elements">

                                    <li> Coffee History</li>
                                    <li>  Green Coffee</li>
                                    <li>  Coffee Transformation</li>
                                    <li>  Science and health</li>
                                    <li>  Coffee benefits</li>
                                    <li>  Statistics</li>
                                    <li>  Decaffeination and instant coffee process</li>
                                    <li>  Espresso – The mix of science and art</li>
                                    <li>  Coffee machines</li>
                                    <li>  Quality control</li>
                                    <li>  Trouble shooting</li>
                                    <li>  Practical training</li>

                            </ul>
                            <p class="course-price">
                                    <span class="course-price-title">Price</span>
                                    <span  class="course-price-value">950 L.E</span>
                            </p>
                    </div>
            </div>
            <div class="university-courses-item col-xs-6">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/course3.jpg" class="img-responsive" />
                    <div class="university-courses-overlay">
                            <p class="course-num">Course 3</p>
                            <h3>Coffee machines world</h3>
                            <ul class="course-elements">

                                    <li>   Espresso machines and technologies</li>
                                    <li>   Coffee grinders</li>
                                    <li>   Super automatic coffee machines</li>
                                    <li>   IPSO systems</li>
                                    <li>   MITACA systems</li>
                                    <li>   Preventative maintenance programs</li>
                                    <li>   Troubleshooting</li>

                            </ul>
                            <p class="course-price">
                                    <span class="course-price-title">Price</span>
                                    <span  class="course-price-value">250 L.E</span>
                            </p>
                    </div>
            </div>
            <div class="university-courses-item col-xs-6">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/course4.jpg" class="img-responsive" />
                    <div class="university-courses-overlay">
                            <p class="course-num">Course 4</p>
                            <h3>Latte Art</h3>
                            <ul class="course-elements">

                                    <li>  Cuppuccino and Lattee Steps</li>
                                    <li>  Patterns</li>
                                    <li>  Innovation drinks</li>
                                    <li>  Menu preparation</li>

                            </ul>
                            <p class="course-price">
                                    <span class="course-price-title">Price</span>
                                    <span  class="course-price-value">750 L.E</span>
                            </p>
                    </div>
            </div>
            <div class="university-courses-item col-xs-6">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/course5.jpg" class="img-responsive" />
                    <div class="university-courses-overlay">
                            <p class="course-num">Course 5</p>
                            <h3>Maestro Barista</h3>
                            <ul class="course-elements">

                                    <li>   Full Coffee expert course</li>
                                    <li>   Full coffee machines maintenance</li>
                                    <li>   Full latte art course</li>


                            </ul>
                            <p class="course-price">
                                    <span class="course-price-title">Price</span>
                                    <span  class="course-price-value">1950 L.E</span>
                            </p>
                    </div>
            </div>
            <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>	


<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->image->ImageURL.'" alt="Lazy Owl Image">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->

<script src="<?php echo Yii::app()->baseUrl ;?>/js/jquery.slimscroll.min.js"></script>
<script>
    $(document).ready(function() {
      //script for university page
	 $('.course-elements').slimscroll({
	  height: '155px',
	   alwaysVisible: true,
	   color:'#fff',
	   size:'4px'
	});
    });
</script>