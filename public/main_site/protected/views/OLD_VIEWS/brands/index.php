<script src="<?php echo Yii::app()->baseUrl ;?>/js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/component2.css" />
<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-brands.jpg" class="img-responsive" />
</div>
<h3 class="title-head marginedtop marginedbottom">
    BRANDS
</h3>
<div class="aboutus-block">
    <p class="aboutus-text">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
    </p>
</div>
<div class="container2">	
    <!-- Codrops top bar -->
    <div class="brands-filters-block">
        <span>Filter By:</span>
        <?php
        echo CHtml::Link('All', '', array('class'=>'btn absolute-box-button filter','data-filter'=>'all'));
        foreach($brandTypes as $brandTypesfe)
        {
            echo CHtml::Link($brandTypesfe->TypeName, '', array('class'=>'btn absolute-box-button filter','data-filter'=>'.'.$brandTypesfe->TypeName));
        }
        ?>
    </div>
    <div class="main">
        <ul id="og-grid" class="og-grid">
            <?php
            foreach($brands as $brandsfe)
            {
                ?>
                <li class="mix <?php echo $brandsfe->type->TypeName ; ?>">
                    <a href="<?php echo Yii::app()->createUrl('brands/brand/bid/'.$brandsfe->BrandID) ; ?>" data-largesrc="<?php echo Yii::app()->baseUrl . '/uploads/' . $brandsfe->thumbImage->ImageURL ;?>" data-title="<?php echo $brandsfe->BrandName ;?>" data-description="<?php echo $brandsfe->BrandDesc ;?>">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/brands/<?php echo $brandsfe->image->ImageURL ;?>" alt="<?php echo $brandsfe->BrandName ;?>"/>
                    </a>
                    <span class="middle-span"></span>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
</div><!-- /container -->

<!--script for grid -->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/grid.js"></script>
<!--script for filters -->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/jquery.mixitup.js"></script>
<script>
    //script for grid 
    $(function() {
            Grid.init();
    });
    //script for filters	
    $(function(){

            // Instantiate MixItUp:

            $('.og-grid').mixItUp();

    });
</script>