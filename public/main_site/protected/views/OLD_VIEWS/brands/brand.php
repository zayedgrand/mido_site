<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/menu_sideslide.css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<link href="<?php echo Yii::app()->baseUrl ;?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo Yii::app()->baseUrl ;?>/css/owl.theme.css" rel="stylesheet">
<!-- stylesheet for tabs-->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/easy-responsive-tabs.css " />
<!-- include modernizer in brands-->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/modernizr.custom.js"></script>

<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-illy.jpg" class="img-responsive" />
</div>
<h3 class="title-head marginedtop marginedbottom">
            Products
    </h3>
<div class="brand-brief-container col-md-12">	
    <div class="brand-brief">
            <p class="brand-top-image"><img src="<?php echo Yii::app()->baseUrl.'/uploads/brands/'.$brands[0]->image->ImageURL ; ?>" /></p>
            <p class="brand-brief-text">
                 <?php echo $brands[0]->BrandDesc ; ?>
            </p>
    </div>
</div>
<div class="clear"></div>
<div class="brand-products-items-container">
<!--Horizontal Tab-->
<div id="parentHorizontalTab">
    <ul class="resp-tabs-list hor_1">
        <?php
        foreach($brandCategories as $brandCategoriesfe1)
        {
            $catName = Categorys::model()->findAll('t.CategoryID = ' . $brandCategoriesfe1->CategoryID);
            echo '<li>'.$catName[0]->CategoryName.'</li>';
        }
        ?>
        
    </ul>
<div class="resp-tabs-container hor_1">
    <?php
    foreach($brandCategories as $brandCat)
    {
        echo '<div><div class="brand-products-items-inner col-md-12">';
        $catName = Products::model()->with('brandproducts','image')->findAll('brandproducts.BrandID = ' . $bid . ' AND brandproducts.CategoryID = ' . $brandCat->CategoryID);
        foreach($catName as $catNamefe)
        {
            echo '<div class="brand-products-item col-md-3">
                        <img class="brand-products-img" src="'.Yii::app()->baseUrl.'/uploads/products/'.$catNamefe->image->ImageURL.'" />
                        <p class="brand-products-title">
                                '.$catNamefe->ProductName.'
                        </p>
                        <p class="brand-products-titlesub">'.$catNamefe->ProductTitle.'</p>
                        <div class="brand-products-share">
                                <a class="share-product"><i class="fa fa-facebook"></i></a>
                                <a class="share-product"><i class="fa fa-twitter"></i></a>
                                <a class="dwn-pdf"><i class="fa fa-file-pdf-o"></i></a>
                        </div>
                </div>';
        }
        echo '</div><div class="clear"></div></div>';
    }
    ?>
</div>
</div>
</div>


<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->image->ImageURL.'" alt="Lazy Owl Image">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->

<!-- script for tabs-->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/easyResponsiveTabs.js"></script>

    <script>
$(document).ready(function() {
//Horizontal Tab
    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
});

</script>