<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ba-bbq.min.js"></script>
<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
});

$('#myTab a[href="#profile"]').tab('show') // Select tab by name
$('#myTab a:first').tab('show') // Select first tab
$('#myTab a:last').tab('show') // Select last tab
$('#myTab li:eq(2) a').tab('show') // Select third tab (0-indexed)
</script>
<style> .nav-tabs > li > a:hover, .nav-tabs > li > a{ color:#000; }</style>

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">inquiry</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Orders</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
        <?php
        Yii::app()->clientScript->registerScript('search', "
        $('.search-button').click(function(){
                $('.search-form').toggle();
                return false;
        });
        $('.search-form form').submit(function(){
                $('#inquery-grid').yiiGridView('update', {
                        data: $(this).serialize()
                });
                return false;
        });
        ");

        echo CHtml::Link('<i class="icon-download-alt"></i> EXPORT',array('/adminInquery/export'),array('style' => 'color: rgb(242, 242, 242);
        background-color: rgb(6, 148, 203);margin-top: 17px;
        padding: 2px 11px;
        border-radius: 5px;
        float: left;'));

        $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'inquery-grid',
                'dataProvider'=>$model->search(),
                'filter'=>$model,
                'columns'=>array(
                        'firstName',
                        'lastName',
                        'phone',
                        'email',
                        'regarding',
                        'Message',
                        'timestamp',
                ),
        )); ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
    <?php
        Yii::app()->clientScript->registerScript('search', "
        $('.search-button').click(function(){
                $('.search-form').toggle();
                return false;
        });
        $('.search-form form').submit(function(){
                $('#inquery-grid').yiiGridView('update', {
                        data: $(this).serialize()
                });
                return false;
        });
        ");

        echo CHtml::Link('<i class="icon-download-alt"></i> EXPORT',array('/adminInquery/exportParty'),array('style' => 'color: rgb(242, 242, 242);
        background-color: rgb(6, 148, 203);margin-top: 17px;
        padding: 2px 11px;
        border-radius: 5px;
        float: left;'));

        $this->widget('zii.widgets.grid.CGridView', array(
                'pager'=>array('header'=>''),
                'id'=>'inquery-grid',
                'dataProvider'=>$model2->search(),
                'filter'=>$model2,
                'columns'=>array(
                        'firstName',
                        'lastName',
                        'mobile',
                        'deliveryLocation',
                        'eventDate',
                        'productsNeeded',
                        'quantity',
                        'orderComments',
                        'timestamp'
                ),
        )); ?>
    </div>
  </div>

</div>

