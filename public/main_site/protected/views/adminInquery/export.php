<?php
$data[] = array ('First name', 'Last name', 'Regarding','Message','Timestamp');

foreach($exportInquery as $exportInqueryfe)
{
    $data[] = array (
        $exportInqueryfe->firstName,
        $exportInqueryfe->lastName,
        $exportInqueryfe->regarding,
        $exportInqueryfe->Message,
        $exportInqueryfe->timestamp
        );
}
Yii::import('application.extensions.phpexcel.JPhpExcel');
$xls = new JPhpExcel('UTF-8', false, 'mido_Inquery');
$xls->addArray($data);
$xls->generateXML('mido_Inquery');
?>