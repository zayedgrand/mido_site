<?php
$data[] = array ('First name', 'Last name', 'Mobile','Delivery location','Event date','Products needed','Order comments','Timestamp');

foreach($exportInquery as $exportInqueryfe)
{
    $data[] = array (
        $exportInqueryfe->firstName,
        $exportInqueryfe->lastName,
        $exportInqueryfe->mobile,
        $exportInqueryfe->deliveryLocation,
        $exportInqueryfe->eventDate,
        $exportInqueryfe->productsNeeded,
        $exportInqueryfe->orderComments,
        $exportInqueryfe->timestamp,
    );
}
Yii::import('application.extensions.phpexcel.JPhpExcel');
$xls = new JPhpExcel('UTF-8', false, 'mido_party_Inquery');
$xls->addArray($data);
$xls->generateXML('mido_party_Inquery');
?>