<?php
/* @var $this AdminBrandsController */
/* @var $model Brands */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'brands-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>
    
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
            <?php echo $form->labelEx($model,'BrandName'); ?>
            <?php echo $form->textField($model,'BrandName',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
            <?php echo $form->error($model,'BrandName'); ?>
	</div>
        
	<div class="row">
            <?php echo $form->labelEx($model,'BrandDesc'); ?>
            <?php echo $form->textArea($model,'BrandDesc',array('rows'=>6, 'cols'=>50,'style'=>'width: 400px;height: 297px;')); ?>
            <?php echo $form->error($model,'BrandDesc'); ?>
	</div>
    
        <?php
        if(ISSET($thumb))
        {
            echo '<img src="'.Yii::app()->baseUrl.'/uploads/brands/'.$thumb.'" width="200px">';
        }
        ?>
	<div class="row">
            <?php echo CHtml::label('ThumbImageURL','Thumb Image'); ?>
            <?php echo CHtml::fileField('ThumbImageURL'); ?>
            <br/>Recommended size : 386px * 383px
	</div>
    
        <?php
        if(ISSET($image))
        {
            echo '<img src="'.Yii::app()->baseUrl.'/uploads/brands/'.$image.'" width="200px">';
        }
        ?>
	<div class="row">
            <?php echo CHtml::label('ImageURL','Image'); ?>
            <?php echo CHtml::fileField('ImageURL'); ?>
            <br/>Max. size : 105px * 105px
	</div>
    
        <?php
        if(ISSET($outer))
        {
            echo '<img src="'.Yii::app()->baseUrl.'/uploads/brands/'.$outer.'" width="200px">';
        }
        ?>
	<div class="row">
            <?php echo CHtml::label('OuterImageURL','Image'); ?>
            <?php echo CHtml::fileField('OuterImageURL'); ?>
            <br/>Recommended size : 125px * 125px
	</div>
	<div class="row">
            <?php echo CHtml::label('Type','TypeID');
            $list = CHtml::listData(Types::model()->findAll(), 'TypeID', 'TypeName');
            echo CHtml::dropDownList('TypeID',$model->TypeID, $list, array('empty' => 'Select Type'));
//          echo $form->textField($model,'TypeID',array('size'=>60,'maxlength'=>64)); 
            ?>
	</div>
        
	<div class="row buttons">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->