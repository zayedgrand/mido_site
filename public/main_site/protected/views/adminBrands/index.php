<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ba-bbq.min.js"></script>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#brands-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

echo CHtml::Link('<i class="icon-plus-sign"></i> Create Brand',array('/adminBrands/create'),array('style' => 'color: rgb(242, 242, 242);
background-color: rgb(6, 148, 203);margin-top: 17px;
padding: 2px 11px;
border-radius: 5px;
float: left;'));

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'brands-grid',
        'pager'=>array('header'=>''),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            array(
                  'type' => 'raw',
                  'value' => 'CHtml::image(Yii::app()->baseUrl . "/uploads/brands/" . $data->outerImage->ImageURL,$data->outerImage->ImageURL,array("width"=>150))',
                'header'=> 'Image'
           ),
            'BrandName',
//            'BrandDesc',
            array(
                 'name' => 'search_name',
                 'header' => 'Type',
                 'value' => '$data->type->TypeName',
                   ),
            array(
			'class'=>'CLinkColumn',
			'label'=>'Categories',
			'urlExpression'=>'array("adminBrandCategories/index/id/".$data->BrandID)',
			'header'=>'View Categories'
		),
            array(
			'class'=>'CButtonColumn',
			'header' => 'Actions',
			'template'=>'{update}{delete}',//{view}{delete}
		),
	),
)); ?>
