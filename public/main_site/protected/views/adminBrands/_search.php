<?php
/* @var $this AdminBrandsController */
/* @var $model Brands */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'BrandID'); ?>
		<?php echo $form->textField($model,'BrandID',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BrandName'); ?>
		<?php echo $form->textField($model,'BrandName',array('size'=>60,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BrandDesc'); ?>
		<?php echo $form->textArea($model,'BrandDesc',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ThumbImageID'); ?>
		<?php echo $form->textField($model,'ThumbImageID',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ImageID'); ?>
		<?php echo $form->textField($model,'ImageID',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TypeID'); ?>
		<?php echo $form->textField($model,'TypeID',array('size'=>60,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'page_views'); ?>
		<?php echo $form->textField($model,'page_views'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->