<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>
    
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'NewsTitle',array('style'=>'width: 146px;')); ?>
		<?php echo $form->textField($model,'NewsTitle',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
		<?php echo $form->error($model,'NewsTitle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NewsDescription',array('style'=>'width: 146px;')); ?>
		<?php echo $form->textArea($model,'NewsDescription',array('rows'=>6, 'cols'=>50,'style'=>'width: 400px;height: 297px;','class'=>'')); ?><!--,'id'=>'textarea2','class'=>'cleditor'-->
		<?php echo $form->error($model,'NewsDescription'); ?>
	</div>

	<?php
	if(ISSET($image))
        {
            echo '<img src="'.Yii::app()->baseUrl.'/uploads/news/'.$image.'" width="200px">';
        }
        ?>
	<div class="row">
                <?php echo CHtml::label('ImageURL','Image',array('style'=>'width: 146px;')); ?>
		<?php echo CHtml::fileField('ImageURL'); ?>
                <br/>Recommended size : 400px * 294px
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Date',array('style'=>'width: 146px;')); ?>
		<?php echo $form->textField($model,'Date',array('class'=>'input-xlarge datepicker','id'=>'date01')); ?>
		<?php echo $form->error($model,'Date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->