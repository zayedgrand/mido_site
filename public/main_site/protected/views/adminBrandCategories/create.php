<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'brands-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>

    <div class="row">
        <?php echo CHtml::label('Category Name','CatName'); ?>
        <?php echo CHtml::textField('CatName','',array('size'=>60,'maxlength'=>256,'style'=>'width:400px;')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Add Category'); ?>
    </div>

<?php $this->endWidget(); ?>

</div>