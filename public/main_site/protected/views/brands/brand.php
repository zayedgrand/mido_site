<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/menu_sideslide.css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<link href="<?php echo Yii::app()->baseUrl ;?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo Yii::app()->baseUrl ;?>/css/owl.theme.css" rel="stylesheet">
<!-- stylesheet for tabs-->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/easy-responsive-tabs.css " />
<!-- include modernizer in brands-->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/modernizr.custom.js"></script>

<meta name="twitter:widgets:csp" content="on">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=613308222081625&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-illy.jpg" class="img-responsive" />
</div>
<h3 class="title-head marginedtop marginedbottom">
            Products
    </h3>
<div class="brand-brief-container col-md-12">	
    <div class="brand-brief">
            <p class="brand-top-image"><img src="<?php echo Yii::app()->baseUrl.'/uploads/brands/'.$brands[0]->image->ImageURL ; ?>" /></p>
            <p class="brand-brief-text">
                 <?php echo $brands[0]->BrandDesc ; ?>
            </p>
    </div>
</div>
<div class="clear"></div>
<div class="brand-products-items-container">
<!--Horizontal Tab-->
<div id="parentHorizontalTab">
    <ul class="resp-tabs-list hor_1">
        <?php
        foreach($brandCategories as $brandCategoriesfe1)
        {
            $catName = Categorys::model()->findAll('t.CategoryID = ' . $brandCategoriesfe1->CategoryID);
            echo '<li>'.$catName[0]->CategoryName.'</li>';
        }
        ?>
        
    </ul>
<div class="resp-tabs-container hor_1">
    <?php
    foreach($brandCategories as $brandCat)
    {
        echo '<div><div class="brand-products-items-inner col-md-12">';
        $catName = Products::model()->with('brandproducts','image')->findAll('brandproducts.BrandID = ' . $bid . ' AND brandproducts.CategoryID = ' . $brandCat->CategoryID);
        foreach($catName as $catNamefe)
        {
            echo '<div class="brand-products-item col-md-3">
                        <img class="brand-products-img" src="'.Yii::app()->baseUrl.'/uploads/products/'.$catNamefe->image->ImageURL.'" />
                        <p class="brand-products-title">
                                '.$catNamefe->ProductName.'
                        </p>
                        <p class="brand-products-titlesub">'.$catNamefe->ProductTitle.'</p>
                        <div class="brand-products-share">';
                        ?>
                        <div class="fb-share-button fa fa-facebook" data-href="<?php echo Yii::app()->createAbsoluteUrl("brands/brand/bid/" . $catNamefe->ProductID) ; ?>" data-layout="link"></div>
                        <!--<a href="https://www.facebook.com/sharer/sharer.php?u=<?php // echo Yii::app()->createAbsoluteUrl("movies/movie/title/" . $nowplayingfe->movie->title) ; ?>" target="_blank">-->
<!--                        <a class="share-product" id="gal-share" href="javascript:fbShare('https://www.facebook.com/sharer.php?s=100','<?php // echo str_replace("'",'', $catNamefe->ProductName); ?>','<?php // echo str_replace("'",'', $catNamefe->ProductTitle); ?>','https://bsocial-dev.com/Beta/ELC/Facebook/PreciousMoments/Tab/images/image')" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>-->
                        <a class="share-product twitter-share-button" href="http://twitter.com/share" data-text="<?php echo str_replace("'",'', $catNamefe->ProductName); ?>" data-url="<?php echo str_replace("'",'', $catNamefe->ProductTitle); ?>" data-via="Mido website"><i class="fa fa-twitter"></i></a>
                        <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                        <?php
                        echo CHtml::Link("<i class='fa fa-file-pdf-o'></i>", 
                        array('download','id'=>$catNamefe->ProductID), 
                        array('target'=>'_blank','class'=>'dwn-pdf'));
                        echo '</div>
                </div>';
        }
        echo '</div><div class="clear"></div></div>';
    }
    ?>
</div>
</div>
</div>


<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item sdf">
                        <span class="mid-span"></span>
                        <img class="lazyOwl txa" src="'.Yii::app()->baseUrl.'/images/pixel.jpg" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->outerImage->ImageURL.'" alt="Mido brand">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->

<!-- script for tabs-->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/easyResponsiveTabs.js"></script>

    <script>
$(document).ready(function() {
//Horizontal Tab
    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
});

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59892009-1', 'auto');
  ga('send', 'pageview');

</script>