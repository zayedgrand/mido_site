<script src="<?php echo Yii::app()->baseUrl ;?>/js/modernizr.custom.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ;?>/css/component2.css" />
<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-brands.jpg" class="img-responsive" />
</div>
<h3 class="title-head marginedtop marginedbottom">
    BRANDS
</h3>
<div class="aboutus-block">
    <p class="aboutus-text">
        Since 1977, MIDO has been in the distribution business primarily to the Food Service channel as well as Retail.
        <br/>  
        The Company only represents the best brands in the world in every category offering very high quality and impeccable after sales services.
        <br/>
        One order + One delivery + One invoice = BIG Support
        <br/>
        We approach our clients through our different divisions; Food, Beverage, Pastry, Bakery and kitchen Equipment.
    </p>
</div>
<div class="container2">	
    <!-- Codrops top bar -->
    <div class="brands-filters-block">
        <span>Filter By:</span>
        <?php
        echo CHtml::Link('All', '', array('class'=>'btn absolute-box-button filter','data-filter'=>'all'));
        foreach($brandTypes as $brandTypesfe)
        {
            echo CHtml::Link($brandTypesfe->TypeName, '', array('class'=>'btn absolute-box-button filter','data-filter'=>'.'.$brandTypesfe->TypeName));
        }        
        ?>

    </div>
    <div style="text-align:center;">
        <select class="brands-filter-select">
            <option class="btn absolute-box-button filter" value="all">All</option>
            <?php
            foreach($brandTypes as $brandTypesfe)
            {
            ?>
            <option class="btn absolute-box-button filter" value="<?php echo ".".$brandTypesfe->TypeName; ?>"><?php echo $brandTypesfe->TypeName; ?></option>	
            <?php
            }
            ?>
        </select>
    </div>
    <div class="main">
        <ul id="og-grid" class="og-grid">
            <?php
            foreach($brands as $brandsfe)
            {
                $rep_text = str_replace('"', "'", $brandsfe->BrandDesc) ;
                ?>
                <li class="mix <?php echo $brandsfe->type->TypeName ; ?>">
                    <a href="<?php echo Yii::app()->createUrl('brands/brand/bid/'.$brandsfe->BrandID) ; ?>" data-largesrc="<?php echo Yii::app()->baseUrl . '/uploads/brands/' . $brandsfe->thumbImage->ImageURL ;?>" data-title="<?php echo $brandsfe->BrandName ;?>" data-description="<?php echo $rep_text ;?>">
                        <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/brands/<?php echo $brandsfe->image->ImageURL ;?>" alt="<?php echo $brandsfe->BrandName ;?>"/>
                    </a>
                    <span class="middle-span"></span>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
</div><!-- /container -->

<!--script for grid -->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/grid.js"></script>
<!--script for filters -->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/jquery.mixitup.js"></script>
<script>
    $(document).ready(function() {
			$('.brands-filter-select').on('change', function() { 
				//var valo = $(this).val();
			   $('.og-grid').mixItUp('filter',this.value);
			});	
			});	
    //script for grid 
    $(function() {
            Grid.init();
    });
    //script for filters	
    $(function(){

            // Instantiate MixItUp:

            $('.og-grid').mixItUp();

    });
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59892009-1', 'auto');
  ga('send', 'pageview');

</script>