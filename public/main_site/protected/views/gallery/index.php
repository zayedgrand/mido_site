<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-gallery.jpg" alt="mido gallery" class="img-responsive" />
</div>
<h3 class="title-head marginedtop ">Gallery</h3>
<div class="gallery-container col-md-11">
    <div id="blog-landing">
        <?php
        foreach($allGallery as $allGalleryfe)
        {
            echo '<article class="white-panel"> 
                    <img src="'.Yii::app()->baseUrl.'/uploads/gallery/'.$allGalleryfe->image->ImageURL.'" alt="mido website">
                    <h2><span>'.$allGalleryfe->GalleryTitle.'</span></h2>
                    <p>'.$allGalleryfe->GalleryDescription.'</p>
                  </article>';
        }
        ?>
    </div>
</div>

<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" src="'.Yii::app()->baseUrl.'/images/pixel.jpg"  data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->outerImage->ImageURL.'" alt="mido Clients">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->
<script src="<?php echo Yii::app()->baseUrl ;?>/js/pinterest_grid.js"></script> 
<script>
$(document).ready(function() {
        //script for gallery 
    $('#blog-landing').pinterest_grid({
        no_columns: 4,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 700
    });
});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59892009-1', 'auto');
  ga('send', 'pageview');

</script>