<link href="<?php echo Yii::app()->baseUrl ;?>/css/datepkr.css" property="stylesheet" rel="stylesheet">
<div class="inner-pages-banner">
    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/banner-inquiry.jpg" alt="mido" class="img-responsive" />
</div>
	<script type="text/javascript">
    var a = Math.ceil(Math.random() * 10);
    var b = Math.ceil(Math.random() * 10);       
    var c = a + b
    function DrawBotBoot()
    {
        document.write("What is "+ a + " + " + b +"? ");
        document.write("<input id='BotBootInput' class='form-row-input' required type='text' maxlength='2' size='2'/>");
    }    
	
    function ValidBotBoot(){
        var d = document.getElementById('BotBootInput').value;
        if (d == c) {
			$('.captcha-msg').hide();
				return true;
	
			}else{
			$('.captcha-msg').html('wrong captcha');return false;
				
			
			}

        	
    }
    </script>
<h3 class="title-head marginedtop marginedbottom">Orders</h3>
<div class="inquiry-form-block">

    <!--<div class="form-gallery col-md-5">
            <div class="form-gallery-top col-md-12">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/inquiry-form1.jpg" class="img-responsive"/>

            </div>
            <div class="form-gallery-left col-md-6 col-xs-6">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/inquiry-form2.jpg" class="img-responsive"/>
                    <div class="inner-inquiry-box">
                            <a>
                                    <img src="<?php echo Yii::app()->baseUrl ;?>/images/illy-logo.jpg" />
                                    FACEBOOK STORE

                            </a>
                    </div>
            </div>
            <div class="form-gallery-right col-md-6 col-xs-6">
                    <img src="<?php echo Yii::app()->baseUrl ;?>/uploads/inquiry-form3.jpg" class="img-responsive"/>

                    <div class="inner-inquiry-box">
                            <p>
                                    Order smoked salmon for weddings and parties ...NOW
                            </p>
                            <p class="right-aligned"><a class="btn absolute-box-button" data-toggle="modal" data-target=".smoked-salmon-popup">CLICK HERE</a></p>
                    </div>
            </div>
            <div class="clear"></div>
    </div>-->
						<div class="inquiry-form-inner col-md-7">
							
							<form class="order-form" name="inquiry-form" action="<?php echo Yii::app()->controller->createUrl('getorders'); ?>" method="POST">
								<div class="form-row">
									<label>First Name</label>
									<input class="form-row-input" type="text" name="salmonfirstname" required placeholder="" />
								</div>
								<div class="form-row">
									<label>Last Name</label>
									<input class="form-row-input" type="text" name="salmonlastname" required placeholder="" />
								</div>
								<div class="form-row">
									<label>Mobile</label>
									<input class="form-row-input" type="text" pattern="[0-9]{11,}" name="salmonmobile" required placeholder="" />
								</div>
								<div class="form-row">
									<label>Delivery Location</label>
									<input class="form-row-input" type="text" name="salmonlocation" required placeholder="" />
								</div>
								
								<div class="form-row">
									<label>Event Date</label>
									<input class="form-row-input" type="text" name="salmondate" id="salmondate" required />
								</div>
								<div class="form-row">
									<label>Products Needed</label>
									<select class="form-row-select" name="salmonproducts" required >
										<option value="">Choose a product</option>
										<option value="Frozen Salmon">Frozen Salmon</option>
										<option value="Premium Pre Sliced Smoked Salmon">Premium Pre Sliced Smoked Salmon</option>
										<option value="Premium Smoked Salmon whole">Premium Smoked Salmon whole</option>
										<option value="Salmon Burgers">Salmon Burgers</option>
										<option value="Salmon Fillets">Salmon Fillets</option>
										<option value="Salmon Steaks">Salmon Steaks</option>
										<option value="Salmon Loins">Salmon Loins</option>
										<option value="Salmon Stripes">Salmon Stripes</option>
									</select> 
								</div>
								
								<div class="form-row">
									<label>Quantity</label>
									<input class="form-row-input" type="text" name="salmonquantity" id="salmonquantity" required />
								</div>
								<div class="form-row">
									<label>Order Comments</label>
									<textarea class="form-row-textarea" name="salmoncomment" required></textarea>
								</div>
								                                   <div class="form-row">
                                                <label>Are you human?</label>

<script type="text/javascript">DrawBotBoot()</script>
                                       
												<span class="captcha-msg" style="  position: relative;top: -9px;color: #F00;font-weight: 900;"></span>
                                            </div>
								<p class="center-aligned marginedtop mobcenter">
									<input type="submit" class="order-form-submit btn absolute-box-button" value="Submit" />
								</p>
							</form>
						
						</div>
						 <div class="clear"></div>					
					</div>
					<div class="illy-store-block">
						<a class="illy-store-block-btn" href="https://www.facebook.com/illyEgypt/app_651053318281253" target="_blank">
							<img src="<?php echo Yii::app()->baseUrl ;?>/images/facbook-store-btn.png" alt="mido facebook store"/>
						</a>
					</div>

<!-- general brands carousal-->
<div class="home-brands-block">
    <h3 class="title-head">Our Brands</h3>
    <div class="home-brands-block-carousel col-md-11">
        <div id="owl-demo2" class="owl-carousel">
            <?php
            foreach($brandSlider as $brandSliderfe)
            {
                echo '<div class="item">
                        <span class="mid-span"></span>
                        <img class="lazyOwl" src="'.Yii::app()->baseUrl.'/images/pixel.jpg" data-src="'.Yii::app()->baseUrl.'/uploads/brands/'.$brandSliderfe->image->ImageURL.'" alt="Mido clients">
                      </div>';
            }
            ?>
            
        </div>
    </div>
</div>
<!-- end brands carousal-->

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ;?>/js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ;?>/js/zebra_datepicker.js"></script>
<script>
$(document).ready(function() {

    $('#salmondate').Zebra_DatePicker();
    $('#salmondate').attr('readonly',false);
 
 // script for inquiry page
  $('.order-form-submit').click(function(){
            $(".order-form").ajaxForm({
beforeSubmit:ValidBotBoot
									
										,
                              beforeSend: function() 
                            {
                                    $("#progress").show();
                                    //clear everything
                                    $("#bar").width('0%');
                                    $("#message").html("");
                                    $("#percent").html("0%");
                            },
                            uploadProgress: function(event, position, total, percentComplete) 
                            {
                                    $("#bar").width(percentComplete+'%');
                                    $("#percent").html(percentComplete+'%');


                            },
                            success: function() 
                            {
                                    $("#bar").width('100%');
                                    $("#percent").html('100%');
                                    //alert('done');
									$('.order-form .form-row').css('opacity','0.5');
									$('.order-form-submit').val('Sent');
									$('.order-form-submit').attr('disabled','disabled');



                            },

                            error: function()
                            {

                                    //$("#message").html("<font color='red'> ERROR: unable to upload files</font>");

                            }

                     });
                        

			});


});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59892009-1', 'auto');
  ga('send', 'pageview');

</script>