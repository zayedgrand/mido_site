<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

	'profile' => 'Profile',
	'my_profile' => 'My Profile',
	'wishlist' => 'Wishlist',
	'my_wishlist' => 'My Wishlist',
	'logout' => 'Logout',
	'search' => 'Search',
	'categories' => 'Categories',
	'brands' => 'Brands',
	'cart' => 'Cart',
	'view_all' => 'View All',
	'pound' => 'EGP',
	'add_to_cart' => 'Add to Cart',
	'in_cart' => 'In Cart',
	'follow_us_on' => 'Follow us on',
	'contact_us' => 'Contact Us',
	'sitemap' => 'Sitemap',
	'about_us' => 'About Us',
	'news' => 'News',
	'shop' => 'Shop',
	'our_courses' => 'Our Courses',
	'gallery' => 'Gallery',
	'awards' => 'Awards',
	'career' => 'Career',
	'portfolio' => 'Portfolio',
	'FAQs' => 'FAQs',
	'Free_delivery_in_Cairo' => 'Free Delivery in Cairo within 48 Hours',
	'Minimum_basket' => 'Minimum Basket is',
	'copyright' => 'All Rights Reserved Mido © 2019 Designed And Developed By',
	'register' => 'Register',
	'login' => 'Login',
	'add_to_wishlist' => 'Add to Wishlist',
	'in_stock' => 'In Stock',
	'description' => 'Description',
	'in_wishlist' => 'In Wishlist',
	'forget_password' => 'Forget Password?',
	'new_customer' => 'New Customer?',
	'create_new_account' => 'Create New Account',
	'name' => 'Name',
	'password' => 'Password',
	'confirm_password' => 'Confirm Password',
	'birth_date' => 'Birthdate',
	'male' => 'Male',
	'female' => 'Female',
	'mobile' => 'Mobile',
	'email' => 'E-mail',
	'address' => 'Address',
	'choose_city' => 'Choose District',

	'city' => 'District',
	'new_password' => 'New Password',
	'confirm_new_password' => 'Confirm New Password',
	'add_new_address' => 'Add New Address',
	'my_order_history' => 'My Order History',
	'save_profile' => 'Save Profile',
	'my_order_history' => 'My Order History',
	'orders_history' => 'Orders History',
	'description' => 'Description',
	'description' => 'Description',
	'description' => 'Description',

	'Out of stock' => 'Out of stock',
	'History' => 'History',

	'my_cart' => 'My Cart',
	'price' => 'Price',
	'qty' => 'Qty',
	'payment' => 'Payment',
	'credit_card' => 'Credit Card',
	'cash_on_delivery' => 'Cash on Delivery',
	'promo_code' => 'Promo Code',
	'submit_promo_code' => 'Submit Promo Code',
	'subtotal' => 'Subtotal',
	'total' => 'Total',
	'maximize_message_1' => 'Maximize your basket to ', // Maximize your basket to 1499 LE to get a Free product to taste of your choice
	'maximize_message_2' => ' LE to get a free product to taste from your choice', // Maximize your basket to 1499 LE to get a Free product to taste of your choice
	'discount' => 'Discount',
	'minimum_message' => 'Minimum Amount 999 EGP',

	'my_profile' => 'My Profile',
	'orders_history' => 'Orders History',
	'order_place_on' => 'Order Placed On',
	'Street name' => 'Street ',
	'Building no' => 'Building ',
	'Apartment no' => 'Apartment ',
	'My Profile' => 'My Profile'
];
