<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Log out' => 'Log out',
    'Are you sure you want to log out?' => 'Are you sure you want to log out?',
    'Press No if youwant to continue work. Press Yes to logout current user' => 'Press No if youwant to continue work. Press Yes to logout current user',

    'No' => 'No',
    'Yes' => 'Yes',
    'total' => 'total',
    'promo discount percentage' => 'promo discount percentage',
    'in cart' => 'in cart',
    'add to cart' => 'add to cart',
    'Out Of Stock' => 'Out Of Stock',
    'view all' => 'view all',
    'L.E' => 'L.E',

    'register' => 'register',
    'name' => 'name',
    'password' => 'password',
    'confirm password' => 'confirm password',
    'gender' => 'gender',
    'male' => 'male',
    'female' => 'female',
    'mobile' => 'mobile',
    'E-Mail' => 'E-Mail',
    'address' => 'address',
    'city' => 'city',
    'login' => 'login',
    'Email' => 'Email',
    'Submit' => 'Submit',
    'forgot password?' => 'forgot password?',
    'your cart' => 'your cart',
    'Quantity' => 'Quantity',
    'order subtotal' => 'Order Subtotal',
    'checkout' => 'checkout',
    'logout' => 'logout',
    'the email or password is wrong' => 'the email or password is wrong',
    'price' => 'price',
    'related products' => 'related products',
    'brands' => 'brands',
    'Thank you for purchasing from us' => 'Thank you for purchasing from us',

    //---- new
    'profile has updated' => 'profile has updated',
    'EGP' => 'EGP',
    'processing' => 'processing',
    'shipping' => 'shipping',
    'delivered' => 'delivered',
    'canceled' => 'canceled',
    'yes' => 'yes',
    'no' => 'no',
    'Is piad' => 'Is paid',
    'Address' => 'Address',
    'Order Details' => 'Order Details',
    'status' => 'Status',
    'All' => 'All',
    'my order history' => 'my order history',
    'All Dates' => 'All Dates',
    'past 3 months' => 'past 3 months',
    'Order Placed On' => 'Order Placed On',
    'Order ID' => 'Order ID',
    'Sub Total price' => 'Sub Total price',
    'Discount' => 'Discount',
    'Final price' => 'Final price',
    'Order Details' => 'Order Details',
    'single price' => 'single price',
    'Qty' => 'Qty',
    'Total Price' => 'Total Price',
    'order' => 'order',
    'details' => 'details',
    'Promo code is added' => 'Promo code added successfully',
    'Hello' => 'Hello',
    'Related Products' => 'Related Products',
    'tax' => 'tax',
    'Forget password Email has send to your mail' => 'Forget password Email has send to your mail',
    'wrong email or password' => 'wrong email or password',
    // 'Qty' => 'Qty',
    'Minimum Amount' => 'Minimum Amount',
    'Congratulation' => 'Congratulation',
    'you_can_test_1' => 'You can taste for free * If the total basket exceeds',
    'you_can_test_2' => 'LE , you are eligible to choose 1 item for free.',
    'Add new address' => 'Add new address',
    'submit address' => 'Submit address',
    'no result for search' => 'No result for search',
    'please add Street name/Building number/Apartment number' => 'Please add Street name/Building number/Apartment number',
    'cancle order' => 'Cancel Order',
    'cant cancle order' => 'Cancel Order',
    'cancele order' => 'Cancel order' ,
    'order canceled' => 'Order canceled' ,
    'cant cancele order' => 'cancel order' ,
    'off' => 'off'
];
