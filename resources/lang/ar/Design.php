<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Log out' => 'Log out',
    'Are you sure you want to log out?' => 'Are you sure you want to log out?',
    'Press No if youwant to continue work. Press Yes to logout current user' => 'Press No if youwant to continue work. Press Yes to logout current user',

	'profile' => 'الملف الشخصي',
	'wishlist' => 'القوائم المفضلة',
	'my_wishlist' => 'قائمة المفضلات',
	'logout' => 'تسجيل الخروج',
	'hello' => 'الملف الشخصي',
	'search' => 'ما الذي تبحث عنه؟',
	'categories' => 'الفئات',
	'brands' => '',
	'cart' => 'العربة',
	'view_all' => 'أظهر الكل',
	'pound' => 'جنيه',
	'add_to_cart' => 'أضف للعربة',
	'in_cart' => 'في العربة',
	'follow_us_on' => 'تابعنا',
	'contact_us' => 'تواصل معنا',
	'sitemap' => 'خريطة الموقع',
	'about_us' => 'من نحن',
	'news' => 'الأخبار',
	'shop' => 'التسوق',
	'our_courses' => 'الدورات التدريبية',
	'gallery' => 'معرض الصور',
	'awards' => 'الجوائز',
	'career' => 'الوظائف',
	'portfolio' => 'الأعمال السابقة',
	'FAQs' => 'الأسئلة الشائعة',
	'Free_delivery_in_Cairo' => 'خدمة التوصيل مجانا داخل القاهرة خلال 48 ساعة ',
	'Minimum_basket' => 'الحد الأدنى للعربة ',
	'copyright' => 'جميع الحقوق محفوظة Mido &copy; 2019 By',
	'register' => 'التسجيل',
	'login' => 'تسجيل الدخول',
	'add_to_wishlist' => 'أضف لقائمة المفضلات',
	'in_stock' => 'متوفر',
	'description' => 'التفاصيل',
	'in_wishlist' => 'في قائمة المفضلات ',
	'forget_password' => 'هل نسيت كلمة المرور؟',
	'new_customer' => 'مستخدم جديد؟',
	'create_new_account' => 'أضف حساب جديد',
	'name' => 'الإسم',
	'password' => 'كلمة المرور',
	'confirm_password' => 'كد كلمة المرور',
	'birth_date' => 'تاريخ الميلاد',
	'male' => 'ذكر',
	'female' => 'أنثى',
	'mobile' => 'الموبايل',
	'email' => 'البريد الإلكتروني',
	'address' => 'العنوان',
	'choose_city' => 'اختيار المنطقة',

	'city' => 'المنطقة',
	'new_password' => 'كلمة المرور الجديدة',
	'confirm_new_password' => 'تأكيد كلمة المرور الجديدة',
	'add_new_address' => 'أضف عنوان جديد',
	'my_order_history' => 'التفاصيل',
	'save_profile' => 'حفظ',
	'my_order_history' => 'المشتريات السابقة',
	'description' => 'تفاصيل المنتج',

	'Out of stock' => 'إنتهى من المخزن',
	'History' => 'تاريخ',

	'my_cart' => 'عربتي',
	'price' => 'السعر',
	'qty' => 'الكمية',
	'payment' => 'طريقة الدفع',
	'credit_card' => 'بطاقة الإئتمان',
	'cash_on_delivery' => 'نقدي',
	'promo_code' => 'رمز ترويجي',
	'submit_promo_code' => 'إدخال الرمز الترويجي',
	'subtotal' => 'المجموع',
	'total' => 'السعر الكلي',
	'maximize_message_1' => 'تابع الشراء لتصل لمبلغ ', // Maximize your basket to 1499 LE to get a Free product to taste of your choice
	'maximize_message_2' => ' لتحصل على هدية مجانية من إختيارك', // Maximize your basket to 1499 LE to get a Free product to taste of your choice
	'discount' => 'الخصم',
	'minimum_message' => 'الحد الأدني 999 جنيه',

	'my_profile' => 'الملف الشخصي',
	'orders_history' => 'المشتريات السابقة',
	'order_place_on' => 'تاريخ الطلب',
  'Street name' => 'اسم الشارع',
	'Building no' => 'رقم المبنى',
	'Apartment no' => 'رقم الشقة',
  'My Profile' => 'بياناتي',

	//All Rights Reserved Mido © 2019 Designed And Developed By vhorus
];
