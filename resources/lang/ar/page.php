<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Log out' => 'Log out',
    'Are you sure you want to log out?' => 'Are you sure you want to log out?',
    'Press No if youwant to continue work. Press Yes to logout current user' => 'Press No if youwant to continue work. Press Yes to logout current user',
    'Yes' => 'Yes',
    'No' => 'No',

    'total' => 'المجموع',
    'promo discount percentage' => 'نسبة الخصم',
    'in cart' => 'في العربة',
    'add to cart' => 'اضف اللي العربة',
    'Out Of Stock' => 'غير متوفر',
    'view all' => 'عرض الكل',
    'L.E' => 'جنية',
    'register' => 'حساب جديد',
    'name' => 'الاسم',
    'password' => 'كلمة السر',
    'confirm password' => 'تأكيد كلمة المرور',
    'gender' => 'النوع',
    'male' => 'ذكر',
    'female' => 'انثة',
    'mobile' => 'الهاتف',
    'E-Mail' => 'البريد',
    'address' => 'العنوان',
    'city' => 'المدينة',
    'login' => 'تسجيل الدخول',
    'Email' => 'البريد',
    'Submit' => 'ارسال',
    'forgot password?' => 'نسيت كلمة المرور؟',

    'your cart' => 'عربتك',
    'Quantity' => 'الكمية',
    'order subtotal' => 'المجموع',
    'checkout' => 'الدفع',
    'logout' => 'تسجيل الخروج',
    'the email or password is wrong' => ' البريد او كلمة المرور خطأ ',
    'price' => 'السعر',
    'related products' => ' المنتجات المتعلقة ',
    'brands' => ' العلامات التجارية ',
    'Thank you for purchasing from us' => 'شكرا لشرائك من ميدو',

    //---- new
    'profile has updated' => 'تم تحديث الملف الشخصي',
    'EGP' => 'جنيه',
    'processing' => 'قيد التشغيل',
    'shipping' => 'الشحن',
    'delivered' => 'تم التوصيل',
    'canceled' => 'ألغيت',
    'yes' => 'نعم',
    'no' => 'لا',
    'Is piad' => 'مدفوع',
    'Address' => 'عنوان',
    'Order Details' => 'تفاصيل الطلب',
    'status' => 'الحالة',
    'All' => 'الكل',
    'my order history' => 'تاريخ طلبي',
    'All Dates' => 'كل التواريخ',
    'past 3 months' => '3 أشهر الماضية',
    'Order Placed On' => 'Order Placed On',
    'Order ID' => 'رقم الطلب',
    'Sub Total price' => 'السعر الإجمالي الفرعي',
    'Discount' => 'الخصم',
    'Final price' => 'السعر النهائي',
    'Order Details' => 'تفاصيل الطلب',
    'single price' => 'سعر الوحدة',
    'Qty' => 'الكمية',
    'Total Price' => 'السعر الكلي',
    'order' => 'الطلب',
    'details' => 'التفاصيل',
    'Qty' => 'الكمية',
    'Hello' => 'مرحبا',
    'Related Products' => 'منتجات ذات صله',
    'tax' => 'ضريبة' ,
    'Forget password Email has send to your mail' => 'نسيت كلمة المرور تم إرسال البريد الإلكتروني إلى البريد الخاص بك' ,
    'wrong email or password' => 'البريد الإلكتروني أو كلمة المرور خاطئة',
    'Minimum Amount' => 'اقل مبلغ',
    'Congratulation' => 'تهانينا! يمكنك التذوق مجانا‎',
    'you_can_test_1' => '* إذا تجاوز إجمالي العربة',
    'you_can_test_2' => 'جنيه ، فيمكنك اختيار منتج واحد مجانًا',
    'Add new address' => 'إضافة عنوان جديد',
    'submit address' => 'إضافة العنوان',
    'no result for search' => ' لا يوجد نتائج للبحث ',
    'please add Street name/Building number/Apartment number' => 'يرجى إضافة اسم الشارع / رقم المبنى / رقم الشقة',
    'cancle order' => '',
    'cant cancle order' => 'cant cancle order now',
    'cancele order' => 'الغاء الطلب' ,
    'order canceled' => 'تم الغاء الطلب' ,
    'cant cancele order' => 'الغاء الطلب' ,
    'off' => 'خصم' ,
];
