@extends('Design.layout.blank')
@section('content')

		<article id="product-page" class="main-page grid grid-1">
			<section>
				<div class="container">
					<div class="banner-container">
						<img src="https://placeit-assets.s3-accelerate.amazonaws.com/landing-pages/make-a-twitch-banner2/Purple-Twitch-Banner-1024x324.png"/>
					</div>
					<div class="grid grid-9 grid-1-5-5">
						<div class="thumbnails">
							<div class="thumbnail-container active product-background flex flex-center flex-middle active">
								<img width="75" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/>
							</div>
							<div class="thumbnail-container product-background flex flex-center flex-middle">
								<img width="75" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/>
							</div>
							<div class="thumbnail-container product-background flex flex-center flex-middle">
								<img width="75" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/>
							</div>
							<div class="thumbnail-container product-background flex flex-center flex-middle">
								<img width="75" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/>
							</div>
							<div class="thumbnail-container product-background flex flex-center flex-middle">
								<img width="75" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/>
							</div>
							<div class="thumbnail-container product-background flex flex-center flex-middle">
								<img width="75" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/>
							</div>
							<div class="thumbnail-container product-background flex flex-center flex-middle">
								<img width="75" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/>
							</div>
						</div>
						<div class="product-img-container flex flex-center flex-middle product-background">
							<img width="400" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/>
						</div>
						<div class="flex flex-middle product-details">
							<div class="w-100 h-100">
								<div class="flex flex-wrap full-w-children h-100">
									<div>
										<h1><span>Glass range</span></h1>
										<div class="size">75cL</div>
									</div>
									<div>
										<div class="price upper">200 EGP</div>
										<div class="status cap">in stock</div>
									</div>
									<div class="flex flex-middle">
										<select>
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
											<option>6</option>
										</select>
										<button class="add-to-cart cap main-btn flex flex-between upper"><span>add to cart</span><i class="fas fa-shopping-cart"></i></button>
									</div>
									<div class="flex flex-middle">
										<i class="far fa-heart"></i>
										<span class="cap">add to wishlist</span>
									</div>
									<div class="description">
										<span class="desc-title cap">description</span>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<hr/>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		});

		$('.thumbnails img').on('click',function(){
			$('.thumbnails img').parent('.thumbnail-container').removeClass('active');
			$(this).parent('.thumbnail-container').addClass('active');
			var img = $(this).attr('src');
			$('.product-img-container').children('img').attr('src',img)
		});
	})
</script>

@endsection
