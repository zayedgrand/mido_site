@extends('Design.layout.blank')
@section('content')
		<style>
			header, footer {display:none}
			.main-page {padding-top:0}
			form p {    text-align: center;
    line-height: 27px;
    margin: 17px 0;}
		.back-to {margin-top:8px}
		</style>
		<article id="login-page" class="login-page grid grid-1">
			<div class="flex flex-center">
				<div>
					<img class="logo" width="250" class="w-100 text-center" src="{{asset('site_assets/images/mido_logo.png')}}"/>
				</div>
			</div>
			<div class="form-container flex flex-center">
				<form>
					<h1 class="flex flex-center cap">reset password</h1>
					<div class="field-container w-100">
						<input type="password" placeholder="New Password" class="w-100"/>
					</div>
					<div class="field-container w-100">
						<input type="password" placeholder="Confirm New Password" class="w-100"/>
					</div>
					<div><button type="submit" class="cap main-btn w-100">Send</button></div>
					<div class="flex flex-right"><a href="#" class="back-to">Back to Sign In</a></div>
				</form>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	})
</script>

@endsection
