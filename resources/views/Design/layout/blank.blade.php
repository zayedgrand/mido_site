<!DOCTYPE html>
<html>
	<head>
		<title>Ecommerce</title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<link rel="stylesheet" href="{{asset('site_assets/css/sm.css')}}"/>
		<link href="https://fonts.googleapis.com/css?family=Baloo" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<style>
			@font-face {
			  font-family: AvenirNext;
			  src: url({{asset('site_assets/fonts/AvenirNextLTPro-Regular.ttf')}});
			}
		</style>
		<link rel="stylesheet" href="{{asset('site_assets/css/style.css')}}?ver=1.6"/>
	</head>
	<body class="{{($lang=='ar')?'direction-rtl':'direction-ltr'}}">
		<div class="menu-wrap">
			<button class="close-button" id="close-button">Close Menu</button>
			<nav class="menu">
				<div class="icon-list">
					<a href="/Home/index"><span>HOME</span></a><a href="/About/index"><span>ABOUT US</span></a><a href="/News/index"><span>NEWS</span></a><a href="/Inquery/index"><span>ORDERS</span></a><a href="/University/index"><span>UNIVERSITY OF COFFEE</span></a><a href="/Gallery/index"><span>GALLERY</span></a><a href="/awards/index"><span>AWARDS</span></a><a href="/Careers/index"><span>CAREERS</span></a><a href="/uploads/mido-portfolio.pdf"><span>Portfolio</span></a>
					<div class="menu-info-block">
						<p>202 2266 8412</p>
						<p>202 2268 5157</p>
						<span>support@mido.com.eg</span>
					</div>

					<div class="menu-info-block">
						<p>
							8 fathy talaat St.<br>
							Masaken Shiraton<br>
							Heliopoles - Cairo<br>
							Egypt
						</p>
					</div>

					<div class="menu-info-block flex">
						<a><i class="fab fa-facebook-f"></i></a>
						<a><i class="fab fa-twitter"></i></a>
						<a><i class="fab fa-google-plus-g"></i></a>
						<a><i class="fa fa-rss"></i></a>
					</div>
				</div>
			</nav>
		</div>
		<header>
			<div class="black-bg grey-color">
				<div class="container">
					<div class="flex flex-between flex-middle">
						<span class="filler"></span>
						<div id="search-form-container" class="flex flex-middle">
							<form id="search-form">
								<input type="text" placeholder="Search"/>
								<button type="submit"><i class="fas fa-search"></i></button>
							</form>
							<div class="dropdown-hover">
								<a href="" class="dropdown-hover-head">AR</a>
							</div>
							<span class="sep grey-bg"></span>
							<div class="dropdown-hover">
								<span class="dropdown-hover-head">Hello Log in</head>
								<div class="dropdown-hover-body">
									<ul class="dropdown-hover-body-content">
										<li><a href="#">choose 1</a></li>
										<li><a href="#">choose 2</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="social-list-container">
							<ul class="flex social-list">
								<li><a href="#" class="flex flex-center flex-middle"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#" class="flex flex-center flex-middle"><i class="fab fa-instagram"></i></a></li>
								<li><a href="#" class="flex flex-center flex-middle"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="grey-bg">
				<div class="flex flex-between black-color">
					<div class="container">
						<div class="flex flex-between">
							<div class="flex flex-middle">
								<div class="logos-container logo-space">
									<a href="#"><img width="134" class="logo" class="w-100 text-center" src="{{asset('site_assets/images/mido_logo_main.png')}}"/></a>
								</div>
								<div><i class="fas fa-bars"></i></div>
								<ul class="flex bold">
									<li>
										<div class="dropdown-hover">
											<span class="dropdown-hover-head cap">categories</span>
											<div class="dropdown-hover-body">
												<div class="container">
													<ul class="flex flex-wrap">
														<li class="logo-space"></li>
														<li class="a-list">
															<div class="dropdown-hover">
																<span class="dropdown-hover-head cap">choose 1</span>
																<div class="dropdown-hover-body">
																		<div class="container">
																			<ul class="flex flex-wrap">
																				<li class="logo-space"></li>
																				<li><a href="#">choose 1</a></li>
																				<li><a href="#">choose 2</a></li>
																				<li><a href="#">choose 3</a></li>
																				<li><a href="#">choose 4</a></li>
																				<li><a href="#">choose 5</a></li>
																				<li><a href="#">choose 6</a></li>
																			</ul>
																		</div>
																</div>
															</div>
														</li>
														<li class="a-list">
															<div class="dropdown-hover">
																<span class="dropdown-hover-head cap">choose 1</span>
																<div class="dropdown-hover-body">
																		<div class="container">
																			<ul class="flex flex-wrap">
																				<li class="logo-space"></li>
																				<li><a href="#">choose 1</a></li>
																				<li><a href="#">choose 2</a></li>
																				<li><a href="#">choose 3</a></li>
																				<li><a href="#">choose 4</a></li>
																				<li><a href="#">choose 5</a></li>
																				<li><a href="#">choose 6</a></li>
																			</ul>
																		</div>
																</div>
															</div>
														</li>
														<li class="a-list">
															<div class="dropdown-hover">
																<span class="dropdown-hover-head cap">choose 1</span>
																<div class="dropdown-hover-body">
																		<div class="container">
																			<ul class="flex flex-wrap">
																				<li class="logo-space"></li>
																				<li><a href="#">choose 1</a></li>
																				<li><a href="#">choose 2</a></li>
																				<li><a href="#">choose 3</a></li>
																				<li><a href="#">choose 4</a></li>
																				<li><a href="#">choose 5</a></li>
																				<li><a href="#">choose 6</a></li>
																			</ul>
																		</div>
																</div>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>

									</li>
									<li>
										<div class="dropdown-hover">
											<span class="dropdown-hover-head cap">brands</span>
											<div class="dropdown-hover-body">
												<div class="container">
													<ul class="flex flex-wrap">
														<li class="logo-space"></li>
														<li><a href="#">choose 1</a></li>
														<li><a href="#">choose 2</a></li>
													</ul>
												</div>
											</div>
										</div>

									</li>
								</ul>
							</div>
							<a href="#" class="flex flex-middle">
								<span class="cart-icon-wrapper">
									<span class="count">20</span>
									<img class="cart-icon" width="30" class="w-100 text-center" src="{{asset('site_assets/images/cart.png')}}"/>
								</span>
								<span class="bold cart-text">cart</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</header>

		@yield('content')
		<footer>
			<div class="black-bg">
				<div class="container">
					<div class="flex">
						<div>
							<h1 class="white-color">sitemap</h1>
							<ul class="flex flex-asian flex-wrap">
								<li><a href="#">about us</a></li>
								<li><a href="#">news</a></li>
								<li><a href="#">shop</a></li>
								<li><a href="#">our courses</a></li>
								<li><a href="#">gallary</a></li>
								<li><a href="#">awards</a></li>
								<li><a href="#">careers</a></li>
								<li><a href="#">portfolio</a></li>
								<li><a href="#">FAQs</a></li>
							</ul>
						</div>
						<div>
							<h1 class="white-color">contact us</h1>
							<ul class="flex flex-asian flex-wrap">
								<li><a href="#">(+202) 2266 8412</a></li>
								<li><a href="#">(+202) 2268 5157</a></li>
								<li><a href="#">support@mido.com.eg</a></li>
								<li><a href="#">Adderss</a></li>
							</ul>
						</div>
						<div>
							<h1 class="white-color cap">follow us on</h1>
							<ul class="flex flex-wrap social-media">
								<li><a href="#" class="flex flex-center flex-middle text-center"><i class="fab fa-instagram"></i></a></li>
								<li><a href="#" class="flex flex-center flex-middle text-center"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#" class="flex flex-center flex-middle text-center"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="text-center">
				All Rights Reserved Mido &copy; 2019 Designed And Developed By <span>vhorus</span>
			</div>
		</footer>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>

		<script>
		$(function(){
			$('.fa-bars').on('click',function(){
				$('body').addClass('show-menu')
			});
			$('.menu-wrap .close-button').on('click',function(){
				$('body').removeClass('show-menu')

			});

			$('#product-page .fa-heart').on('click',function(){
				$(this).toggleClass('far fas');
			})
		});

		</script>
      @yield('script')
	</body>
</html>
