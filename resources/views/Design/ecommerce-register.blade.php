@extends('Design.layout.blank')
@section('content')
		<style>
			header, footer {display:none}
			.main-page {padding-top:0}
		</style>
		<article id="register-page" class="main-page grid grid-1">
			<div class="flex flex-center">
				<div>
					<img class="logo" class="w-100 text-center" src="{{asset('site_assets/images/mido_logo.png')}}"/>
				</div>
			</div>
			<div class="form-container flex flex-center">
				<form>
					<h1 class="flex flex-center cap">registeration</h1>
					<div class="field-container w-100">
						<input type="text" placeholder="Name" class="w-100"/>
					</div>
					<div class="field-container w-100">
						<input type="password" placeholder="Password" class="w-100"/>
					</div>
					<div class="field-container w-100">
						<input type="password" placeholder="Confirm Password" class="w-100"/>
					</div>
					<div class="field-container w-100">
						<input type="date" placeholder="DOB" class="w-100"/>
					</div>
					<div class="field-container w-100 flex flex-middle gender-field">
						<label>Male</label>
						<input type="radio" name="gender" value="male"/>
						<label>Female</label>
						<input type="radio" name="gender" value="male"/>
					</div>
					<div class="field-container w-100">
						<input type="tel" placeholder="Mobile" class="w-100"/>
					</div>
					<div class="field-container w-100">
						<input type="email" placeholder="E-mail" class="w-100"/>
					</div>
					<div class="field-container w-100">
						<textarea col="50" class="w-100" placeholder="Address"></textarea>
					</div>
					<div class="field-container w-100">
						<input type="text" placeholder="City" class="w-100"/>
					</div>
					<div><button type="submit" class="cap main-btn w-100">Sign Up</button></div>
				</form>
			</div>
			<div class="flex flex-center flex-middle auth-footer">
				all rights reserved Mido &copy; 2019 Designed and Developed by <span>Vhorus</span>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	})
</script>

@endsection
