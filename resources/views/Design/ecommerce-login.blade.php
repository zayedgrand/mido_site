@extends('Design.layout.blank')
@section('content')
		<style>
			header, footer {display:none}
			.main-page {padding-top:0}
		</style>
		<article id="login-page" class="login-page grid grid-1">
			<div class="flex flex-center">
				<div>
					<img class="logo" width="250" class="w-100 text-center" src="{{asset('site_assets/images/mido_logo.png')}}"/>
				</div>
			</div>
			<div class="form-container flex flex-center">
				<form>
					<h1 class="flex flex-center cap">login</h1>
					<div class="field-container w-100">
						<input type="text" placeholder="Username" class="w-100"/>
						<i class="fas fa-user-alt"></i>
					</div>
					<div class="field-container w-100">
						<input type="password" placeholder="Password" class="w-100"/>
						<i class="fas fa-lock"></i>
					</div>
					<div><button type="submit" class="cap main-btn w-100">login</button></div>
					<div class="flex flex-right"><a href="#" class="forget-password">forget password?</a></div>
					<div class="flex flex-center"><span class="cap new-customer">new customer?</span></div>
					
					<div><button type="submit" class="cap second-btn w-100">create your account</button></div>
				</form>
			</div>
			<div class="flex flex-center flex-middle auth-footer">
				all rights reserved Mido &copy; 2019 Designed and Developed by <span>Vhorus</span>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	})
</script>

@endsection
