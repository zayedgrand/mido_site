@extends('Design.layout.blank')
@section('content')

		<article id="history-inner-page" class="main-page grid grid-1">
			<div class="container">
				<div class="flex flex-between wishlist-border">
					<h1 class="cap">order #5 details</h1>
				</div>
				<div class="wishlist-product flex flex-between flex-middle flex-wrap wishlist-border">
					<div>
						<div class="date">
							<span class="bold">Order Placed On:</span>
							<span>29 December 2018</span>
						</div>
						<div class="order_id">
							<span class="bold">Order ID:</span>
							<span>#40567890000</span>
						</div>
						<div class="total_before">
							<span class="bold">Total:</span>
							<span>#40567890000</span>
						</div>
						<div class="discount">
							<span class="bold">Discount:</span>
							<span>25%</span>
						</div>
						<div class="total_after">
							<span class="bold">Total:</span>
							<span>#40567890000</span>
						</div>
						<div class="status">
							<span class="bold">Status:</span>
							<span>Canceled</span>
						</div>
						<div class="address">
							<span class="bold">Address:</span>
							<span>عمارة يعقوبيان</span>
						</div>
					</div>
				</div>
				<div class="wishlist-product flex flex-between flex-middle flex-wrap wishlist-border">
					<div class="grid grid-2 w-100 history-grid">
						<div class="grid grid-2">
							<div>
								<img class="w-100 product-background" width="30" class="w-100 text-center" src="{{asset('site_assets/images/dummy-product.png')}}"/>
							</div>
							<div class="flex flex-middle">
								<div>
									<div class="history-title">
										<span>Blueberry Bonne Maman</span>
									</div>
									<div class="history-details">
										<span>370 gr</span>
									</div>
									<div class="history-qty">
										<span class="bold">Qty:</span>
										<span>2</span>
									</div>
									<div class="history-price">
										<span class="bold">Price:</span>
										<span>200 EGP</span>
									</div>
								</div>
							</div>
						</div>
						<div class="grid grid-2">
							<div>
								<img class="w-100 product-background" width="30" class="w-100 text-center" src="{{asset('site_assets/images/dummy-product.png')}}"/>
							</div>
							<div class="flex flex-middle">
								<div>
									<div class="history-title">
										<span>Blueberry Bonne Maman</span>
									</div>
									<div class="history-details">
										<span>370 gr</span>
									</div>
									<div class="history-qty">
										<span class="bold">Qty:</span>
										<span>2</span>
									</div>
									<div class="history-price">
										<span class="bold">Price:</span>
										<span>200 EGP</span>
									</div>
								</div>
							</div>
						</div>
						<div class="grid grid-2">
							<div>
								<img class="w-100 product-background" width="30" class="w-100 text-center" src="{{asset('site_assets/images/dummy-product.png')}}"/>
							</div>
							<div class="flex flex-middle">
								<div>
									<div class="history-title">
										<span>Blueberry Bonne Maman</span>
									</div>
									<div class="history-details">
										<span>370 gr</span>
									</div>
									<div class="history-qty">
										<span class="bold">Qty:</span>
										<span>2</span>
									</div>
									<div class="history-price">
										<span class="bold">Price:</span>
										<span>200 EGP</span>
									</div>
								</div>
							</div>
						</div>
						<div class="grid grid-2">
							<div>
								<img class="w-100 product-background" width="30" class="w-100 text-center" src="{{asset('site_assets/images/dummy-product.png')}}"/>
							</div>
							<div class="flex flex-middle">
								<div>
									<div class="history-title">
										<span>Blueberry Bonne Maman</span>
									</div>
									<div class="history-details">
										<span>370 gr</span>
									</div>
									<div class="history-qty">
										<span class="bold">Qty:</span>
										<span>2</span>
									</div>
									<div class="history-price">
										<span class="bold">Price:</span>
										<span>200 EGP</span>
									</div>
								</div>
							</div>
						</div>
						<div class="grid grid-2">
							<div>
								<img class="w-100 product-background" width="30" class="w-100 text-center" src="{{asset('site_assets/images/dummy-product.png')}}"/>
							</div>
							<div class="flex flex-middle">
								<div>
									<div class="history-title">
										<span>Blueberry Bonne Maman</span>
									</div>
									<div class="history-details">
										<span>370 gr</span>
									</div>
									<div class="history-qty">
										<span class="bold">Qty:</span>
										<span>2</span>
									</div>
									<div class="history-price">
										<span class="bold">Price:</span>
										<span>200 EGP</span>
									</div>
								</div>
							</div>
						</div>
						<div class="grid grid-2">
							<div>
								<img class="w-100 product-background" width="30" class="w-100 text-center" src="{{asset('site_assets/images/dummy-product.png')}}"/>
							</div>
							<div class="flex flex-middle">
								<div>
									<div class="history-title">
										<span>Blueberry Bonne Maman</span>
									</div>
									<div class="history-details">
										<span>370 gr</span>
									</div>
									<div class="history-qty">
										<span class="bold">Qty:</span>
										<span>2</span>
									</div>
									<div class="history-price">
										<span class="bold">Price:</span>
										<span>200 EGP</span>
									</div>
								</div>
							</div>
						</div>
						<div class="grid grid-2">
							<div>
								<img class="w-100 product-background" width="30" class="w-100 text-center" src="{{asset('site_assets/images/dummy-product.png')}}"/>
							</div>
							<div class="flex flex-middle">
								<div>
									<div class="history-title">
										<span>Blueberry Bonne Maman</span>
									</div>
									<div class="history-details">
										<span>370 gr</span>
									</div>
									<div class="history-qty">
										<span class="bold">Qty:</span>
										<span>2</span>
									</div>
									<div class="history-price">
										<span class="bold">Price:</span>
										<span>200 EGP</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	})
</script>

@endsection
