@extends('Design.layout.blank')
@section('content')

		<article id="history-page" class="main-page grid grid-1">
			<div class="container">
				<div class="wishlist-border">
					<form id="search_form" class="flex flex-between flex-middle" action="" method="post">
							{{csrf_field()}}
							<h1 class="cap">my order history</h1>
							<div class="flex">
									<div class="dropdown-hover">
										<span class="dropdown-hover-head flex flex-between flex-middle main-btn"><span>Title</span></span>
										<div class="dropdown-hover-body">
											<ul class="dropdown-hover-body-content">
												<li class="active">choose 1</li>
												<li>choose 2</li>
											</ul>
										</div>
									</div>
									<div class="dropdown-hover">
										<span class="dropdown-hover-head flex flex-between flex-middle main-btn"><span>Title</span></span>
										<div class="dropdown-hover-body">
											<ul class="dropdown-hover-body-content">
												<li class="active">choose 1</li>
												<li>choose 2</li>
											</ul>
										</div>
									</div>
							</div>
					</form>
				</div>
				<div class="wishlist-product flex flex-between flex-middle flex-wrap wishlist-border">
					<div>
						<div class="date">
							<span class="bold">Order Placed On:</span>
							<span>29 December 2018</span>
						</div>
						<div class="order_id">
							<span class="bold">Order ID:</span>
							<span>#40567890000</span>
						</div>
						<div class="total_before">
							<span class="bold">Total:</span>
							<span>#40567890000</span>
						</div>
						<div class="discount">
							<span class="bold">Discount:</span>
							<span>25%</span>
						</div>
						<div class="total_after">
							<span class="bold">Total:</span>
							<span>#40567890000</span>
						</div>
						<div class="status">
							<span class="bold">Status:</span>
							<span>Canceled</span>
						</div>
						<div class="address">
							<span class="bold">Address:</span>
							<span>عمارة يعقوبيان</span>
						</div>
					</div>
					<div>
						<a href="#" class="flex flex-between main-btn cap"><span>Order Details</span></a>
					</div>
				</div>
				<div class="wishlist-product flex flex-between flex-middle flex-wrap wishlist-border">
					<div>
						<div class="date">
							<span class="bold">Order Placed On:</span>
							<span>29 December 2018</span>
						</div>
						<div class="order_id">
							<span class="bold">Order ID:</span>
							<span>#40567890000</span>
						</div>
						<div class="total_before">
							<span class="bold">Total:</span>
							<span>#40567890000</span>
						</div>
						<div class="discount">
							<span class="bold">Discount:</span>
							<span>25%</span>
						</div>
						<div class="total_after">
							<span class="bold">Total:</span>
							<span>#40567890000</span>
						</div>
						<div class="status">
							<span class="bold">Status:</span>
							<span>Canceled</span>
						</div>
						<div class="address">
							<span class="bold">Address:</span>
							<span>عمارة يعقوبيان</span>
						</div>
					</div>
					<div>
						<a href="#" class="flex flex-between main-btn cap"><span>Order Details</span></a>
					</div>
				</div>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	})
</script>

@endsection
