@extends('Design.layout.blank')
@section('content')

		<article id="profile-page" class="main-page grid grid-1">
			<div class="container">
				<div class="">
					<div class="profile-info">
						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>Name:</label>
							</div>
							<input type="text" value="Serag Mounir Farid"/>
							<div class="flex flex-middle flex-right">
								<span class="edit cap">edit</span>
							</div>
						</div>
						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>Username:</label>
							</div>
							<input type="text" value="seragmounir7"/>
							<div class="flex flex-middle flex-right">
								<span class="edit cap">edit</span>
							</div>
						</div>
						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>Password:</label>
							</div>
							<input type="password" value="********"/>
							<div class="flex flex-middle flex-right">
								<span class="edit cap">edit</span>
							</div>
						</div>
						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>E-mail:</label>
							</div>
							<input type="email" value="seragmounir7@gmail.com"/>
							<div class="flex flex-middle flex-right">
								<span class="edit cap">edit</span>
							</div>
						</div>
						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>Birthday:</label>
							</div>
							<input type="text" value="Serag Mounir Farid"/>
							<div class="flex flex-middle flex-right">
								<span class="edit cap">edit</span>
							</div>
						</div>
						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>Phone:</label>
							</div>
							<input type="text" value="01234567890"/>
							<div class="flex flex-middle flex-right">
								<span class="edit cap">edit</span>
							</div>
						</div>
						<div class="grid grid-20 grid-1-20-1 address-grid field">
							<div class="flex">
								<label>Address:</label>
							</div>
							<ul class="address">
								<li class="field">abc acbdfasfji sfdiafjsdif dsfhasdf;, cairo, Egypt</li>
								<li class="field">abc acbdfasfji sfdiafjsdif dsfhasdf;, cairo, Egypt</li>
							</ul>
							<div class="flex flex-middle flex-right flex-wrap">
								<div class="edit cap">edit</div>
								<div class="edit cap">+Add</div>
							</div>
						</div>
						<div class="flex flex-between profile-router">
							<div>
								<a class="main-btn cap">my wishlist</a>
								<a class="main-btn cap">my order history</a>
							</div>
							<div>
								<a class="main-btn cap">save profile</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	})
</script>

@endsection
