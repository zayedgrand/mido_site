@extends('Design.layout.blank')
@section('content')
		<style>
			.grid-3 {grid-column-gap: 35px;grid-row-gap: 20px;}
			.files {margin:20px 0}
			.files > * {border: 1px solid #e6e6e6;}
			.files b {padding:10px}
			.files img {padding-left:5px}
		</style>
		<article id="download-page" class="main-page grid grid-1">
			<div class="container">
				<img style="width:100%;display:block" src="{{asset('site_assets/images/download.png')}}"/>
				<section>
					<div class="grid grid-3 files">
						<div class="flex flex-between flex-middle">
							<b>Catalogue 1</b>
							<div>
								<a href="{{asset('site_assets/download/pdf.pdf')}}" download>
									<img style="width:50px;display:block" src="{{asset('site_assets/images/pdf.png')}}"/>
								</a>
							</div>
						</div>
						<div class="flex flex-between flex-middle">
							<b>Catalogue 1</b>
							<div>
								<a href="{{asset('site_assets/images/download.png')}}" download>
									<img style="width:50px;display:block" src="{{asset('site_assets/images/pdf.png')}}"/>
								</a>
							</div>
						</div>
						<div class="flex flex-between flex-middle">
							<b>Catalogue 1</b>
							<div>
								<a href="{{asset('site_assets/images/download.png')}}" download>
									<img style="width:50px;display:block" src="{{asset('site_assets/images/pdf.png')}}"/>
								</a>
							</div>
						</div>
						<div class="flex flex-between flex-middle">
							<b>Catalogue 1</b>
							<div>
								<a href="{{asset('site_assets/images/download.png')}}" download>
									<img style="width:50px;display:block" src="{{asset('site_assets/images/pdf.png')}}"/>
								</a>
							</div>
						</div>
					</div>
				</section>
			</div>
		<article>

@endsection
