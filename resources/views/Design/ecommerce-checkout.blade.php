@extends('Design.layout.blank')
@section('content')
		<style>
		</style>
		<article id="checkout-page" class="main-page grid grid-1">
			<div class="container">
				<div class="checkout-row grid grid-7 grid-1-2-1-1-1-1 title-container">
					<span>1- My Cart</span>
					<span></span>
					<span>price</span>
					<span>Qty</span>
					<span>Total</span>
					<span></span>
				</div>
				
				<!--products in cart-->
				<div class="checkout-row grid grid-7 grid-1-2-1-1-1-1 product-container">
					<div class="flex flex-middle">
						<div class="product-background"><img width="100" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
					</div>
					<div class="flex flex-middle">
						<div>
							<div class="product-name">Glass range</div>
							<div class="product-size">75cl</div>
						</div>
					</div>
					<span class="product-price flex flex-middle">200 EFP</span>
					<span class="product-qty flex flex-middle">
						
										<select>
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
											<option>6</option>
										</select>
					</span>
					<span class="product-total flex flex-middle">200 EFP</span>
					<span class="product-delete flex flex-middle"><img class="delete-icon" width="30" class="w-100 text-center" src="{{asset('site_assets/images/error22.svg')}}"/></span>
				</div>
				<div class="checkout-row grid grid-7 grid-1-2-1-1-1-1 product-container">
					<div class="flex flex-middle">
						<div class="product-background"><img width="100" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
					</div>
					<div class="flex flex-middle">
						<div>
							<div class="product-name">Glass range</div>
							<div class="product-size">75cl</div>
						</div>
					</div>
					<span class="product-price flex flex-middle">200 EFP</span>
					<span class="product-qty flex flex-middle">
						
										<select>
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
											<option>6</option>
										</select>
					</span>
					<span class="product-total flex flex-middle">200 EFP</span>
					<span class="product-delete flex flex-middle"><img class="delete-icon" width="30" class="w-100 text-center" src="{{asset('site_assets/images/error22.svg')}}"/></span>
				</div>
				
				<!--products in cart-->
				<form>
					<div class="checkout-row payment-container">
						<span>2- payment</span>
						<div>
							<input type="radio" name="payment" checked="checked"/>
							<label>Cash on Delivery</label>
							<input type="radio" name="payment"/>
							<label>Credit Card</label>
						</div>
					</div>
					<div class="checkout-row address-container">
						<span>2- Address</span>
						<div>
							<input type="radio" name="address" checked="checked"/>
							<label>Address 1</label>
						</div>
						<div>
							<input type="radio" name="address"/>
							<label>Address 2</label>
						</div>
					</div>
					<div class="note">
						<p class="flex flex-middle"><span>Congratulation!</span> You can taste for free</p>
					</div>
					<div class="grid grid-5 products-grid">
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							
							
							
							
						</div>
					<div class="flex flex-right total">
						<div class="total-table w-20">
							<div>
								<div class="flex flex-between">
									<span class="cap">subtotal</span>
									<span>400 EGP</span>
								</div>
								<div class="flex flex-between">
									<span class="cap">shipping</span>
									<span>50 EGP</span>
								</div>
							</div>
							<hr/>
							<div>
								<div class="flex flex-between">
									<span class="cap bold">total</span>
									<span class="bold">450 EGP</span>
								</div>
							</div>
							<button class="main-btn w-100 upper" type="submit">checkout</button>
						</div>
					</div>
				</form>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			if($(this).hasClass('active')){
				$(this).next('.dropdown-click-body').slideUp();
				$(this).removeClass('active');
				//---------------------------------------
				$(this).find('i').toggleClass('fa-minus fa-plus');
			}else{
				$('.dropdown-click-head').removeClass('active');
				$('.dropdown-click-body').slideUp();
				$(this).addClass('active');
				$(this).next('.dropdown-click-body').slideDown();
				//---------------------------------------
				$('.dropdown-click-head').find('.fa-minus').toggleClass('fa-minus fa-plus');
				$(this).find('i').toggleClass('fa-minus fa-plus');
			}
			
			var faqs_height = $('#FAQs > div').innerHeight();
		})
	})
</script>

@endsection
