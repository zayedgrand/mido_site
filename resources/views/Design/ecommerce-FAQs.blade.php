@extends('Design.layout.blank')
@section('content')
		<style>
			.dropdown-click 		{margin-bottom:50px}
			.dropdown-click-head 	{border:1px solid #ededed;padding:10px}
			.dropdown-click-body	{padding:10px}
			
			#FAQs > div				{
										height:470px
										
									}
			.fa-plus , .fa-minus 	{color:#de1222}
			.dropdown-click:nth-child(1),
			.dropdown-click:nth-child(2),
			.dropdown-click:nth-child(3),
			.dropdown-click:nth-child(4),
			.dropdown-click:nth-child(5)	{padding-right:20px}
			
			
			.dropdown-click:nth-child(6),
			.dropdown-click:nth-child(7),
			.dropdown-click:nth-child(8),
			.dropdown-click:nth-child(9),
			.dropdown-click:nth-child(10)	{padding-left:20px}
		</style>
		<article id="FAQ-page" class="main-page grid grid-1">
			<div class="container">
				<div class="banner-container">
					<img src="https://placeit-assets.s3-accelerate.amazonaws.com/landing-pages/make-a-twitch-banner2/Purple-Twitch-Banner-1024x324.png"/>
				</div>
				<section id="FAQs">
					<div class="flex flex-wrap flex-asian">
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
						<div class="dropdown-click w-50">
							<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-plus"></i></span>
							<div class="dropdown-click-body">
								<p>this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer this is answer </p>
							</div>
						</div>
					</div>
				</section>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			if($(this).hasClass('active')){
				$(this).next('.dropdown-click-body').slideUp();
				$(this).removeClass('active');
				//---------------------------------------
				$(this).find('i').toggleClass('fa-minus fa-plus');
			}else{
				$('.dropdown-click-head').removeClass('active');
				$('.dropdown-click-body').slideUp();
				$(this).addClass('active');
				$(this).next('.dropdown-click-body').slideDown();
				//---------------------------------------
				$('.dropdown-click-head').find('.fa-minus').toggleClass('fa-minus fa-plus');
				$(this).find('i').toggleClass('fa-minus fa-plus');
				//---------------------------------------
			}
		})
	})
</script>

@endsection
