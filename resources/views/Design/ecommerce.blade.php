@extends('Design.layout.blank')
@section('content')


		<article id="home-page" class="main-page grid grid-1">
			<section>
				<div class="container">
					<div class="banner-container">
						<img src="https://placeit-assets.s3-accelerate.amazonaws.com/landing-pages/make-a-twitch-banner2/Purple-Twitch-Banner-1024x324.png"/>
					</div>
					<div class="products-grid-container">
						<div class="flex flex-right">
							<a class="main-btn upper">view all</a>
						</div>
						
						
						
						
						
						
						<div class="grid grid-5 products-grid">
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							
							
							
							
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container">
					<div class="banner-container">
						<img src="https://placeit-assets.s3-accelerate.amazonaws.com/landing-pages/make-a-twitch-banner2/Purple-Twitch-Banner-1024x324.png"/>
					</div>
					<div class="products-grid-container">
						<div class="flex flex-right">
							<a class="main-btn upper">view all</a>
						</div>
						
						
						
						
						
						
						<div class="grid grid-5 products-grid">
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							
							
							
							
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container">
					<div class="banner-container">
						<img src="https://placeit-assets.s3-accelerate.amazonaws.com/landing-pages/make-a-twitch-banner2/Purple-Twitch-Banner-1024x324.png"/>
					</div>
					<div class="products-grid-container">
						<div class="flex flex-right">
							<a class="main-btn upper">view all</a>
						</div>
						
						
						
						
						
						
						<div class="grid grid-5 products-grid">
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							
							
							
							
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container">
					<div class="banner-container">
						<img src="https://placeit-assets.s3-accelerate.amazonaws.com/landing-pages/make-a-twitch-banner2/Purple-Twitch-Banner-1024x324.png"/>
					</div>
					<div class="products-grid-container">
						<div class="flex flex-right">
							<a class="main-btn upper">view all</a>
						</div>
						
						
						
						
						
						
						<div class="grid grid-5 products-grid">
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							
							
							
							
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container">
					<div class="banner-container">
						<img src="https://placeit-assets.s3-accelerate.amazonaws.com/landing-pages/make-a-twitch-banner2/Purple-Twitch-Banner-1024x324.png"/>
					</div>
					<div class="products-grid-container">
						<div class="flex flex-right">
							<a class="main-btn upper">view all</a>
						</div>
						
						
						
						
						
						
						<div class="grid grid-5 products-grid">
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="flex flex-center product">
								<div>
									<a href="#">
										<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
										<div class="text-center details">details</div>
										<div class="text-center weight">weight</div>
										<div class="text-center price">price</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
											<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
										</div>
									</div>
								</div>
							</div>
							
							
							
							
						</div>
					</div>
				</div>
			</section>

		<article>

@endsection


@section('script')
<!--script-->

@endsection
