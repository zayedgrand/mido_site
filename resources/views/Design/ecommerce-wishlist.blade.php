@extends('Design.layout.blank')
@section('content')

		<article id="wishlist-page" class="main-page grid grid-1">
			<div class="container">
				<h1 class="wishlist-border cap">my wishlist</h1>
				<div class="wishlist-product flex flex-between flex-middle flex-wrap wishlist-border">
					<div class="flex flex-middle">
							<div class="img-wrapper product-background flex flex-center flex-middle">
								<img width="200" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png">
							</div>
							<div>
								<div class="title">illy instabt coffee</div>
								<div class="desc">illy instant coffee 100g</div>
								<div class="price upper">200 egp</div>
							</div>
					</div>
					<div>
						<button class="flex flex-between main-btn add-to-cart cap"><span>add to cart</span><i class="fas fa-shopping-cart"></i></button>
					</div>
				</div>
				<div class="wishlist-product flex flex-between flex-middle flex-wrap wishlist-border">
					<div class="flex flex-middle">
							<div class="img-wrapper product-background flex flex-center flex-middle">
								<img width="200" src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png">
							</div>
							<div>
								<div class="title">illy instabt coffee</div>
								<div class="desc">illy instant coffee 100g</div>
								<div class="price upper">200 egp</div>
							</div>
					</div>
					<div>
						<button class="flex flex-between main-btn add-to-cart cap"><span>add to cart</span><i class="fas fa-shopping-cart"></i></button>
					</div>
				</div>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	})
</script>

@endsection
