@extends('Design.layout.blank')
@section('content')
		<article id="brand-page" class="main-page grid grid-1">
			<section>
				<div class="container">
					<div class="banner-container">
						<img src="https://placeit-assets.s3-accelerate.amazonaws.com/landing-pages/make-a-twitch-banner2/Purple-Twitch-Banner-1024x324.png"/>
					</div>
					<div class="grid grid-6 grid-1-rest brand-grid">
						<aside>
							<div class="dropdown-click">
								<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-chevron-down"></i></span>
								<div class="dropdown-click-body">
									<ul class="dropdown-click-body-content">
										<li class="active">choose 1</li>
										<li>choose 2</li>
									</ul>
								</div>
							</div>
							<div class="dropdown-click">
								<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-chevron-down"></i></span>
								<div class="dropdown-click-body">
									<ul class="dropdown-click-body-content">
										<li class="active">choose 1</li>
										<li>choose 2</li>
									</ul>
								</div>
							</div>
							<div class="dropdown-click">
								<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-chevron-down"></i></span>
								<div class="dropdown-click-body">
									<ul class="dropdown-click-body-content">
										<li class="active">choose 1</li>
										<li>
										
											<div class="dropdown-click">
												<span class="dropdown-click-head flex flex-between flex-middle"><span>Title</span><i class="fas fa-chevron-down"></i></span>
												<div class="dropdown-click-body">
													<ul class="dropdown-click-body-content">
														<li class="active">choose 1</li>
														<li>choose 2</li>
													</ul>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</aside>
						<div class="products-grid-container">
							<div class="grid grid-3 products-grid">
								<div class="flex flex-center product">
									<div>
										<a href="#">
											<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
											<div class="text-center details">Glass Range</div>
											<div class="text-center weight">75CL</div>
											<div class="text-center price">200 EGP</div>
										</a>
										<span class="seperator"></span>
										<div class="product-footer">
											<div class="flex flex-center">
												<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
											</div>
										</div>
									</div>
								</div>
								<div class="flex flex-center product">
									<div>
										<a href="#">
											<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
											<div class="text-center details">details</div>
											<div class="text-center weight">weight</div>
											<div class="text-center price">price</div>
										</a>
										<span class="seperator"></span>
										<div class="product-footer">
											<div class="flex flex-center">
												<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
											</div>
										</div>
									</div>
								</div>
								<div class="flex flex-center product">
									<div>
										<a href="#">
											<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
											<div class="text-center details">details</div>
											<div class="text-center weight">weight</div>
											<div class="text-center price">price</div>
										</a>
										<span class="seperator"></span>
										<div class="product-footer">
											<div class="flex flex-center">
												<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
											</div>
										</div>
									</div>
								</div>
								<div class="flex flex-center product">
									<div>
										<a href="#">
											<div><img src="https://support.apple.com/library/content/dam/edam/applecare/images/en_US/homepod/watch-product-lockup-callout.png"/></div>
											<div class="text-center details">details</div>
											<div class="text-center weight">weight</div>
											<div class="text-center price">price</div>
										</a>
										<span class="seperator"></span>
										<div class="product-footer">
											<div class="flex flex-center">
												<button class="upper add-to-cart main-btn flex flex-between flexmiddle"><span>Add to cart</span><i class="fas fa-shopping-cart"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					
					</div>
				</div>
			</section>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	})
</script>

@endsection
