
<!-- START PAGE SIDEBAR -->
<div class="page-sidebar page-sidebar-fixed scroll">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        {{-- <li class="xn-logo">
            {{-- <a href="index.html">ATLANT</a> --}}
            {{-- <a href="#" style="position: relative; top: 2px; background: url({{asset('images/assets/logo-for-admin.png')}}) top center no-repeat white;"> Mido </a> --}}
          {{--  <a href="#" style="position: relative; top: 2px; background: url({{asset('')}}) top center no-repeat white;">
              <p>ss</p>
              <img src="{{asset('images/assets/logo-for-admin.png')}}"/>
            </a>
            <a href="#" class="x-navigation-control"></a>
        </li> --}}


        <li class="xn-profile">
              <a href="#" class="profile-mini">
                  <img src="{{asset('atlant/assets/images/users/avatar.jpg')}}" style="width: 100px;height: 100px;"/>
              </a>
              {{-- <div class="profile">


                  {{-- <div class="profile-data">
                     @if (auth('Admin')->check())
                       <div class="profile-data-name"> {{auth('Admin')->user()->name}} </div>
                       <div class="profile-data-title">{{auth('Admin')->user()->email}}</div>
                     @elseif (auth('Company')->check())
                        @php( $CompanyMembership = \App\Functions::company_membership_info() )
                        @isset ($CompanyMembership->see_auctions)
                            <p id="MembershipType" class="{{\App\Functions::get_membership_class_name($CompanyMembership)}}">
                              {{$CompanyMembership->membership_name}} -
                              <span> {{\App\Functions::get_membership_reming_dayes($CompanyMembership)}} @lang('page.remaining day') </span>
                            </p>
                        @else
                            @php( $CompanyMembership = null )
                        @endisset


                       <div class="profile-data-name"> {{auth('Company')->user()->name_en}} </div>
                       <div class="profile-data-title">{{auth('Company')->user()->email}}</div>
                     @endif

                  </div> --}}
                {{--   <div class="profile-controls">
                      <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                      <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                  </div>
              </div> --}}
        </li>
        @if (!isset($active))
          @php($active = '')
        @endif

        <li class="xn-title" style="padding-right: 21px;">
          {{-- <img src="{{asset('images/assets/logo-for-admin.png')}}" style="display: table; margin: 0 auto;"/> --}}
          <img src="{{asset('site_assets/images/mido_logo_main.png')}}" style="display: table; margin: 0 auto;width:120px"/>
        </li>

          @permission('DashBoard')
          <li class="{{($active == 'DashBoard')?'active':''}}" >
              <a href="{{url('Admin/DashBoard')}}" >  <span class="fa fa-dashboard mydir"></span>  <span class="xn-text">DashBoard </span> </a>
          </li>
          @endpermission




          <!--.....................if Admin..........................-->

          {{-- @permission('DashBoard') --}}

          {{-- <li class="{{($active == 'DashBoard')?'active':''}}" >
              <a href="{{url('DashBoard')}}" > <span class="fa fa-home mydir"></span> <span class="xn-text"> DashBoard </span>   </a>
          </li> --}}

          @permission('Members')
          <li class="xn-title"> Users </li>

          <li class="{{($active == 'Members')?'active':''}}" >
              <a href="{{url('Admin/Members')}}" >  <span class="fa fa-users mydir"></span>  <span class="xn-text"> Users </span> </a>
          </li>
          @endpermission

          {{-- <li class="{{($active == 'ContactUs')?'active':''}}" >
              <a href="{{url('Admin/ContactUs')}}" >  <span class="fa fa-user mydir"></span>  <span class="xn-text">Contact Us </span> </a>
          </li> --}}

          <li class="xn-title"> Control </li>

          @permission('City_main')
          <li class="{{($active == 'City')?'active':''}}" >
              <a href="{{url('Admin/City')}}" >  <span class="fa fa-location-arrow mydir"></span>  <span class="xn-text"> Districts </span> </a>
          </li>
          @endpermission
          @permission('PopularQuestion_main')
          <li class="{{($active == 'PopularQuestion')?'active':''}}" >
              <a href="{{url('Admin/PopularQuestion')}}" >  <span class="fa fa fa-question-circle mydir"></span>  <span class="xn-text"> FAQs </span> </a>
          </li>
          @endpermission
          @permission('PdfDownloads_main')
          <li class="{{($active == 'PdfDownloads')?'active':''}}" >
              <a href="{{url('Admin/PdfDownloads')}}" >  <span class="fa fa fa-download mydir"></span>  <span class="xn-text"> Downloads </span> </a>
          </li>
          @endpermission

          @permission('PromoCodes_main')
          <li class="{{($active == 'PromoCodes')?'active':''}}" >
              <a href="{{url('Admin/PromoCodes')}}" >  <span class="fa fa-money mydir"></span>  <span class="xn-text"> Promo Codes </span> </a>
          </li>
          @endpermission

          {{-- @permission('Brand_and_categoiry_main')
          <li class="{{($active == 'Brand')?'active':''}}" >
              <a href="{{url('Admin/Brand')}}" >  <span class="fa fa-briefcase mydir"></span>  <span class="xn-text"> Brands & Categories </span> </a>
          </li>
          @endpermission --}}

          @permission('Category_and_subCategory_main')
          <li class="{{($active == 'Category')?'active':''}}" >
              <a href="{{url('Admin/Category')}}" >  <span class="fa fa-briefcase mydir"></span>  <span class="xn-text"> Categories & Subs </span> </a>
          </li>
          @endpermission

          @permission('Brand_main')
          <li class="{{($active == 'Brand')?'active':''}}" >
              <a href="{{url('Admin/Brand')}}" >  <span class="fa fa-briefcase mydir"></span>  <span class="xn-text"> Brands </span> </a>
          </li>
          @endpermission

          @permission('Homepage_main')
          <li class="{{($active == 'HomePage')?'active':''}}" >
              <a href="{{url('Admin/HomePage')}}" >  <span class="fa fa-home mydir"></span>  <span class="xn-text"> Home Page </span> </a>
          </li>
          @endpermission

          @permission('Product_main')
          <li class="{{($active == 'Product')?'active':''}}" >
              <a href="{{url('Admin/Product')}}" >  <span class="fa fa-shopping-cart mydir"></span>  <span class="xn-text"> Products </span> </a>
          </li>
          @endpermission

          @permission('FreeTestList_main')
          <li class="{{($active == 'FreeTestList')?'active':''}}" >
              <a href="{{url('Admin/FreeTestList')}}" >  <span class="fa fa-shopping-cart mydir"></span>  <span class="xn-text"> Free Taste   </span> </a>
          </li>
          @endpermission

          @permission('Recipt_main')
          <li class="{{($active == 'Recipt')?'active':''}}" >
              <a href="{{url('Admin/Recipt')}}" >  <span class="fa fa-file-text-o mydir"></span>  <span class="xn-text"> Invoices </span> </a>
          </li>
          @endpermission

          @permission('PagesBanner_main')
          <li class="{{($active == 'PagesBanner')?'active':''}}" >
              <a href="{{url('Admin/PagesBanner')}}" >  <span class="fa fa-image mydir"></span>  <span class="xn-text"> Pages Banners </span> </a>
          </li>
          @endpermission

          {{--
          <li class="xn-openable {{($active == 'AboutUs'||$active == 'Partner'||$active == 'Facility')?'active':''}}">
              <a href="#"> <i class="fa fa-certificate mydir"></i> <span class="xn-text">About Us</span>    </a>
              <ul>
                  <li class="{{($active == 'AboutUs')?'active':''}}" >
                      <a href="{{url('Admin/AboutUs')}}" >
                         <i class="fa fa-certificate mydir"></i>  <span class="xn-text"> About Us</span>
                       </a>
                  </li>
                  <li class="{{($active == 'Partner')?'active':''}}" >
                      <a href="{{url('Admin/Partner')}}" >
                         <i class="fa fa-certificate mydir"></i>  <span class="xn-text"> Partners</span>
                      </a>
                  </li>
                  <li class="{{($active == 'Facility')?'active':''}}" >
                      <a href="{{url('Admin/Facility')}}" >
                         <i class="fa fa-certificate mydir"></i> <span class="xn-text"> Facilities</span>
                      </a>
                  </li>
              </ul>
          </li><!--End xn-openable -->
          --}}


          <li class="{{($active == 'Setting')?'active':''}}" >
              <a href="{{url('Admin/Setting')}}" >  <span class="fa fa-info-circle mydir"></span>  <span class="xn-text"> Info & Content </span> </a>
          </li>






          @permission('mainSuperAdmin')

          <li class="xn-title"> Admin </li>

          <li class="{{($active == 'Admin')?'active':''}}" >
              <a href="{{url('Admin/Admins')}}" >  <span class="fa fa-gavel mydir"></span>  <span class="xn-text"> Admin </span> </a>
          </li>
          <li class="{{($active == 'Role')?'active':''}}" >
              <a href="{{url('Admin/Role')}}" >  <span class="fa fa-gavel mydir"></span>  <span class="xn-text"> Role </span> </a>
          </li>

           {{-- <li class="xn-openable {{($active == 'Admin'||$active == 'Role')?'active':''}}">
             <a href="#"><span class="xn-text">  <i class="fa fa-gavel mydir"></i> Admins</span>    </a>
             <ul>
                 <li class="{{($active == 'Admin')?'active':''}}" >
                     <a href="{{url('Admin/Admins')}}" >
                       <i class="fa fa-gavel mydir"></i> <span class="xn-text"> Admins </span>
                     </a>
                 </li>
                 <li class="{{($active == 'Role')?'active':''}}" >
                     <a href="{{url('Admin/Role')}}" >
                       <i class="fa fa-gavel mydir"></i> <span class="xn-text"> Roles </span>
                     </a>
                 </li>
             </ul>
         </li> --}}
         @endpermission


            {{-- <li class="xn-openable {{($active == 'Country'||$active == 'Governorate'||$active == 'Area')?'active':''}}">
              <a href="#"><span class="xn-text">@lang('page.Location')</span> <i class="fa fa-location-arrow mydir"></i>    </a>
              <ul>

                    <li class="{{($active == 'Country')?'active':''}}" >
                        <a href="{{url('Country')}}" >  <span class="xn-text">
                          @lang('page.Country')</span> <i class="fa fa-location-arrow mydir"></i>  </a>
                    </li>



                    <li class="{{($active == 'Governorate')?'active':''}}" >
                        <a href="{{url('Governorate')}}"><span class="xn-text">
                          @lang('page.Governorate') </span> <i class="fa fa-location-arrow mydir"></i> </a>
                    </li>

                    <li class="{{($active == 'Area')?'active':''}}" >
                        <a href="{{url('Area')}}"><span class="xn-text">
                          @lang('page.Area') </span> <i class="fa fa-location-arrow mydir"></i> </a>
                    </li>

              </ul>
          </li> --}}
 </ul>
<!--===================================================End care=========================================================-->

<!--===================================================End NotificationNotification=========================================================-->
     <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->
