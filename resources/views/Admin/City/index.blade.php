

@component('components.panel_default_with_blank')
    @slot('active') City @endslot
    {{-- @slot('page_title') City  @endslot --}}
    @slot('panel_title') Districts @endslot
    @slot('the_permission') City_main @endslot

    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">
          @permission('City_create')
          <button class="btn btn-primary btn-rounded" v-on:click="showCreateModel()">
                 <i class="fa fa-plus mydir"></i> Create
          </button>
          <br>
          @endpermission

          <div class="col-md-6 pull-right">
            {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent'=>'getResults()']) !!}
                <input type="text" name="search" class="form-control mydirection" placeholder=" search"  >
            {!! Form::close() !!}
          </div>
          <br> <br>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Name Ar  </th>
                    <th> Name En  </th>
                    <th> Visibility </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      <td> <p v-text="list.name_en"></p>  </td>
                      <td> <p v-text="list.name_ar"></p>  </td>
                      <td>
                         <span class="badge badge-success"  v-if="list.status"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>
                      <td>
                          @permission('City_status')
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.status ,'btn-danger':!list.status }"  v-on:click="showORhide(list.id)">
                               <i class="fa fa-eye" v-if="list.status"></i>
                               <i class="fa fa-eye-slash" v-else ></i>
                           </button>
                           @endpermission
                           @permission('City_edit')
                           <button class="btn btn-primary btn-rounded" v-on:click="showEditModel(list)" >
                              <i class="fa fa-pencil"></i>
                           </button>
                           @endpermission
                           @permission('City_delete')
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                           @endpermission
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->

            {{-- ------------------- create ------------------------ --}}
                @permission('City_create')
                @component('components.modal')
                    @slot('id')
                      create_model
                    @endslot
                    @slot('header')
                         Add new
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>"mydirection",'id'=>'create_form' ,'v-on:submit.prevent'=>'create()' ]) !!}
                    @endslot
                    @slot('body')

                        {!! Form::label('name_en','Name En') !!}
                        {!! Form::text('name_en',null,['class'=>'form-control','required' ]) !!}

                        {!! Form::label('name_ar','Name Ar') !!}
                        {!! Form::text('name_ar',null,['class'=>'form-control','required' ]) !!}

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" >  Add new </button>
                    @endslot
                @endcomponent
                @endpermission
            {{-- ------------------- edit ------------------------ --}}
                @permission('City_edit')
                @component('components.modal')
                    @slot('id')
                      edit_model
                    @endslot
                    @slot('header')
                          edit
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>" ",'id'=>'edit_form' ,'v-on:submit.prevent'=>'edit()'   ]) !!}
                    @endslot
                    @slot('body')

                      {!! Form::hidden('id',null,['id'=>'edit_id','v-model'=>'EF.id']) !!}

                            <div class="form-group">
                                {!! Form::label('name_en','Name En') !!}
                                {!! Form::text('name_en',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_en']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('name_ar','Name Ar') !!}
                                {!! Form::text('name_ar',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_ar']) !!}
                            </div>

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" > Update </button>
                    @endslot
                @endcomponent
                @endpermission

       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/City/delete`;
            var get_list =   `${admin_url}/City/list`;
            var showORhide_api = `${admin_url}/City/showORhide`;
            var create_api = `${admin_url}/City/create`;
            var update_api = `${admin_url}/City/update`;
        </script>
        <script src="{{asset('js_admin/standard.js')}}"> </script>
    @endslot

@endcomponent
