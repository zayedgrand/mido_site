

@component('components.panel_default_with_blank')
    @slot('active') Product @endslot
    @slot('panel_title') Product Edit @endslot
    @slot('the_permission') Product_edit @endslot
    @slot('breadcrumb')
        <ul class="breadcrumb">
          <li><a href="{{url('Admin/Product')}}">Products</a></li>
          <li class="active">Edit</li>
       </ul>
     @endslot


    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">

          {!! Form::open([ 'method'=>'PATCH','url'=>['Admin/Product',$Product->id], 'class'=>" ",'id'=>'my_form' ,'v-on:submit'=>'do_submit()' ,'files'=>true  ]) !!}

             <table class="table mydir">
                  <tr>
                     <th>
                         <p> Category  : </p>
                         <v-select :options="Categories_and_sub" :name="'category_id'"  v-on:s_change="CategoryChanged()" :f_item="'All'" :s_id="'category_id'" :value="EF.category_id" :required='true' ></v-select>
                         <br><br>
                         <p> Sub Category  : </p>
                         {{-- <v-select :options="subCategoires_list" :name="'category_id'"  :f_item="'All'"></v-select> --}}
                         <select class="form-control" name="sub_category_id" id="Categoy_id" v-model="EF.sub_category_id" v-on:change="subCategoryChanged()" required>
                            <option value=""> choose </option>
                            <option v-for="(l) in subCategoires_list" v-text="l.label" :value="l.value" >   </option>
                         </select>
                         <br>
                         <p> Brand  : </p>
                         {{-- <v-select :options="subCategoires_list" :name="'category_id'"  :f_item="'All'"></v-select> --}}
                         <select class="form-control" name="brand_id" id="Brand_id" required v-model="EF.brand_id" >
                            <option value=""> choose </option>
                            <option v-for="(l) in Brand_list" v-text="l.label" :value="l.value" >   </option>
                         </select>
                     </th>
                     <th>

                     </th>
                  </tr>
                  <tr>
                     <th>
                        <p> Name En: </p>
                        {!! Form::text('name_en',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_en','required']) !!}
                     </th>
                     <th>
                        <p> Name Ar: </p>
                        {!! Form::text('name_ar',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_ar','required']) !!}
                     </th>
                  </tr>
                  <tr>
                     <th>
                        <p> Short Description En: </p>
                        {!! Form::text('short_description_en',null,['class'=>'form-control','v-model'=>'EF.short_description_en' ,'required']) !!}
                     </th>
                     <th>
                        <p> Short Description Ar: </p>
                        {!! Form::text('short_description_ar',null,['class'=>'form-control','v-model'=>'EF.short_description_ar' ,'required']) !!}
                     </th>
                  </tr>
                  <tr>
                     <th>
                        <p> Description En: </p>
                        {!! Form::textarea('description_en',null,['class'=>'form-control','required' ,'rows'=>'10','v-model'=>'EF.description_en','required']) !!}
                     </th>
                     <th>
                        <p> Description Ar: </p>
                        {!! Form::textarea('description_ar',null,['class'=>'form-control','required' ,'rows'=>'10','v-model'=>'EF.description_ar','required']) !!}
                     </th>
                  </tr>
                  <tr>
                     <th width="50%">

                       <div class="row">
                            <div class="col-md-4">
                                <p> Main Price (EGP): </p>
                                {!! Form::number('old_price',null,['class'=>'form-control','required' ,'v-model'=>'EF.old_price','required']) !!}
                            </div><!--End col-md-4-->
                            <div class="col-md-3">
                                <p> Discount (%): </p>
                                {!! Form::number('discount_percentage',null,['class'=>'form-control','required' ,'v-model'=>'EF.discount_percentage','required','min'=>'0','max'=>'100']) !!}
                            </div><!--End col-md-2-->
                            <div class="col-md-4">
                                <p> New Price (EGP): </p>
                                {!! Form::number('price',null,['class'=>'form-control','required' ,'v-model'=>'new_price','required','readonly','style'=>'color: black;']) !!}
                            </div><!--End col-md-4-->
                       </div><!--End row-->

                     </th>
                     <th>
                        <p> Quantity in Stock : </p>
                        {!! Form::number('quantity',null,['class'=>'form-control','required' ,'v-model'=>'EF.quantity','required']) !!}
                     </th>
                  </tr>

            </table>
            <hr>
            <!--IMAGES -->
            <h2> <center> <b> Add Images </b> </center>  </h2>

            <div class="container">


              <button type="button" class="btn btn-primary " v-on:click="addNewImage()" >
                  <i class="fa fa-plus mydir"></i> <span>Add Image (198px * 198px)</span>
              </button>
              <br>

              <table class="table mydir">
                <tbody is="transition-group" name="my-list" >
                    <tr v-for="(arr,index) in imagesArr" :key="arr.id">
                       <td width="30%">
                            <input type="file"  class="form-control" name="images[]" v-on:change='Preview_image($event)' data-from="addImage">
                            <input type="hidden" name="old_images_ids[]" v-if="arr.id" :value="arr.id">
                            <input type="hidden" name="images_no[]" :value="index" >
                       </td>
                       <td width="30%">
                           <img :src="arr.image" id="Preview_image_edit" class="Preview_image">
                       </td>
                       <td>
                           <br>
                           <button type="button" class="btn btn-danger btn-condensed" v-on:click="remove_Image(index)" v-if="index != 0"  >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                        </td>
                    </tr>
                </tbody>
              </table>

            </div><!--End container-->

            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner3 v-if="show_spinner"></spinner3>
            <!-- - - - - - -End spinner- - - - - - - -->

              <button type="submit" class="btn btn-success" style="width:100%" :disabled="btn_submit" > Update </button>

            {!! Form::close() !!}
       </div><!--End myVue-->

       <br><br>
    @endslot

    @slot('script')
        <script>
            var get_project = JSON.parse( escapeSpecialChars(`{!! ($Product) !!}`) ); console.log(get_project);
            var get_ProjectImages = JSON.parse(escapeSpecialChars(`{!! $ProductImages !!}`));
            var get_Categories_and_sub = JSON.parse(escapeSpecialChars(`{!! $Categories_and_sub !!}`));
        </script>
        <script src="{{asset('js_admin/Product/standard_expanded.js?ver=1.2')}}"> </script>
    @endslot

@endcomponent
