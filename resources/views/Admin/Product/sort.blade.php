

@component('components.panel_default_with_blank')
    @slot('active') Product @endslot
    {{-- @slot('page_title') Home Page  @endslot --}}
    @slot('panel_title') Products Sort @endslot
    @slot('the_permission') Product_main @endslot
    @slot('breadcrumb')
        <ul class="breadcrumb">
          <li><a href="{{url('Admin/Product')}}">Products</a></li>
          <li class="active">Sort</li>
       </ul>
     @endslot

    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">


          <div class="col-md-6 pull-right">
            {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent'=>'getResults()']) !!}
                {{-- <input type="text" name="search" class="form-control mydirection"   placeholder=" search"  > --}}
                <h3> Drag rows for rearrange</h3>
            {!! Form::close() !!}
          </div>
          <br> <br>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Name  </th>
                    <th> Price  </th>
                    <th> Visibility </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak  class="row_sortable">
                  <tr v-for="(list,index) in mainList" :key="list.id" :data-id="list.id">
                      <td> <p v-text="list.id"></p>  </td>
                      <td> <p v-text="list.name"></p>  </td>
                      <td> <p v-text="list.price"></p>  </td>
                      <td>
                         <span class="badge badge-success" v-if="list.status"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>
                      <td>
                           @permission('HomePage_status')
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.status ,'btn-danger':!list.status }"  v-on:click="showORhide_no_paginate(list.id)">
                               <i class="fa fa-eye" v-if="list.status"></i>
                               <i class="fa fa-eye-slash" v-else ></i>
                           </button>
                           @endpermission

                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->


       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/Product/delete`;
            var get_list =   `${admin_url}/Product/list_without_paginate`;
            var showORhide_api = `${admin_url}/Product/showORhide`;
            var updateRowsPosition_api = `${admin_url}/Product/updateRowsPosition`;
        </script>
        <script src="{{asset('js_admin/standard_sortable.js')}}"> </script>
    @endslot

@endcomponent
