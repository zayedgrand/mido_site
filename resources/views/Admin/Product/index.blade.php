

@component('components.panel_default_with_blank')
    @slot('active') Product @endslot
    {{-- @slot('page_title') Product  @endslot --}}
    @slot('panel_title') Product @endslot
    @slot('the_permission') Product_main @endslot

    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">
          @permission('Product_create')
          <a class="btn btn-primary btn-rounded" href="{{url('Admin/Product/create')}}">
                 <i class="fa fa-plus mydir"></i> Create
          </a>
          @endpermission
          <a class="btn btn-primary btn-rounded" href="{{url('Admin/Product/sort')}}">
                 <i class="fa fa-sort-amount-asc mydir"></i> Sort
          </a>
          <hr>

          {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent' => 'getResults()']) !!}
          <div class="row">
              <div class="col-md-3 pull-right">
                    <label> Search :</label>
                    <input type="text" name="search" class="form-control mydirection" placeholder=" search"  >
              </div>
              <div class="col-md-3">
                    <label> Category  : </label>
                    <v-select :options="Categories_and_sub" :name="'category_id'"  v-on:s_change="CategoryChanged()" :f_item="'All'" :s_id="'category_id'" :value="EF.category_id"   ></v-select>
                </div><!--End col-md-3-->
                <div class="col-md-3">
                    <label> Sub Category  : </label>
                    <select class="form-control" name="sub_category_id" id="Categoy_id" v-model="EF.sub_category_id" v-on:change="subCategoryChanged()"  >
                       <option value=""> choose </option>
                       <option v-for="(l) in subCategoires_list" v-text="l.label" :value="l.value" >   </option>
                    </select>
                </div><!--End col-md-3-->
                <div class="col-md-3">
                    <label> Brand  : </label>
                    <select class="form-control" name="brand_id" id="Brand_id"   v-model="EF.brand_id" v-on:change="getResults()" >
                       <option value=""> choose </option>
                       <option v-for="(l) in Brand_list" v-text="l.label" :value="l.value" >   </option>
                    </select>
                </div><!--End col-md-3-->
          </div><!--End row-->
          {!! Form::close() !!}
         <hr>

            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Image </th>
                    <th> Name  </th>
                    <th> Price  </th>
                    <th> Quantity  </th>
                    <th> Brand  </th>
                    {{-- <th> Description  </th> --}}
                    <th> Visibility </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      <td> <img :src="list.image" width="120px"> </td>
                      <td>
                        <p v-text="list.name_en"></p>
                        <p v-text="list.short_description_en"></p>
                      </td>
                      <td> <p> @{{list.price}} L.E </p>  </td>
                      <td> <p v-text="list.quantity"></p> </td>
                      <td>
                         <p v-text="list.category_name"></p>
                         <i class="fa fa-chevron-down"></i>
                         <i class="fa fa-chevron-down"></i>
                         <p v-text="list.sub_category_name"></p>
                         <i class="fa fa-chevron-down"></i>
                         <i class="fa fa-chevron-down"></i>
                         <p v-text="list.brand_name"></p>
                      </td>
                      {{-- <td width="30%">
                          <p style="text-align: justify;text-justify: inter-word;" v-html="list.description_en.replace(/\\r\\n/g, '<br />')"></p>
                      </td> --}}
                      <td>
                         <span class="badge badge-success"  v-if="list.status"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>
                      <td>
                           @permission('Product_status')
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.status ,'btn-danger':!list.status }"  v-on:click="showORhide(list.id)">
                               <i class="fa fa-eye" v-if="list.status"></i>
                               <i class="fa fa-eye-slash" v-else ></i>
                           </button>
                           @endpermission
                           @permission('Product_edit')
                           <br><br>
                           <a class="btn btn-primary btn-rounded" :href="'{{url('Admin/Product/edit')}}/'+list.id" >
                              <i class="fa fa-pencil"></i>
                           </a>
                           @endpermission
                           @permission('Product_delete')
                           <br><br>
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                           @endpermission
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->


       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/Product/delete`;
            var get_list =   `${admin_url}/Product/list`;
            var showORhide_api = `${admin_url}/Product/showORhide`;
            var get_Categories_and_sub = JSON.parse(`{!! $Categories_and_sub !!}`);
        </script>
        <script src="{{asset('js_admin/Product/standard.js?ver=1.1')}}"> </script>
    @endslot

@endcomponent
