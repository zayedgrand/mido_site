<table class="table">
  <thead>
    <tr>
      <th> <b>count</b> </th>
      <th> <b> DATABASE ID</b> </th>
      <th> <b>Name</b> </th>
      <th> <b>Email</b> </th>
      <th> <b>Phone</b> </th>
      <th> <b>Contacted</b> </th>
      <th> <b>message</b> </th>
      <th> <b>created date</b> </th>
    </tr>
  </thead>
  <tbody>
    @foreach ($ContactUs as $key => $Contact)
      <tr>
        <td> {{$key+1}} </td>
        <td> {{$Contact->id}} </td>
        <td> {{$Contact->name}} </td>
        <td> {{$Contact->email}} </td>
        <td> {{$Contact->phone}} </td>
        <td>
            @if ($Contact->is_contacted)
              <p> Yes </p>
            @else
              <p> No </p>
            @endif
        </td>
        <td> {{$Contact->message}} </td>
        <td> {{$Contact->created_at}} </td>
      </tr>
    @endforeach

  </tbody>
</table>
