<table class="table">
  <thead>
    <tr>
      <th> <b>count</b> </th>
      <th> <b> DATABASE ID</b> </th>
      <th> <b>Name</b> </th>
      <th> <b>Email</b> </th>
      <th> <b>Phone</b> </th>
      <th> <b>Contacted</b> </th>
      <th> <b>created date</b> </th>
    </tr>
  </thead>
  <tbody>
    @foreach ($Register as $key => $Reg)
      <tr>
        <td> {{$key}} </td>
        <td> {{$Reg->id}} </td>
        <td> {{$Reg->name}} </td>
        <td> {{$Reg->email}} </td>
        <td> {{$Reg->phone}} </td>
        <td>
            @if ($Reg->is_contacted)
              <p> Yes </p>
            @else
              <p> No </p>
            @endif
        </td>
        <td> {{$Reg->created_at}} </td>
      </tr>
    @endforeach

  </tbody>
</table>
