

@component('components.panel_default_with_blank')
    @slot('active') Brand @endslot
    {{-- @slot('page_title') City  @endslot --}}
    @slot('panel_title') Brand @endslot
    @slot('the_permission') Brand_main @endslot

    @slot('body')
        <style>
          [v-cloak] { display: none; }
          img.Preview_image{
            max-height:120px;
          }
        </style>
        <div id="myVue">

          {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent' => 'getResults()']) !!}
          <div class="row">
              <div class="col-md-6  ">
                    <label> Category</label>
                    <v-select :options="categoiry_subCategory" :name="'Category_id'"  v-on:s_change="CategoryChanged()" :s_id="'Category_id'" :f_item="'All'"></v-select>
              </div>
              <div class="col-md-6">
                  <label> Sub Category </label>
                  {{-- <v-select :options="subCategories" :name="'SubCategory_id'"  v-on:s_change="getResults()" :s_id="'SubCategory_id'" :f_item="'All'"></v-select> --}}
                  <select  name="SubCategory_id" v-on:change="getResults()" id="SubCategory_id" class="form-control" >
                      <option value=""> choose sub Category </option>
                      <option v-for="(sub,index) in subCategories" :value="sub.value"> @{{sub.label}} </option>
                  </select>
              </div>
          </div><!--End row-->
          {!! Form::close() !!}

          <div v-show="show_page">
            <hr>



          @permission('Brand_create')
          <button class="btn btn-primary btn-rounded" v-on:click="showCreateModel()">
                 <i class="fa fa-plus mydir"></i> Create
          </button>
          <br>
          @endpermission

          <div class="col-md-6 pull-right">
            {!! Form::open( [  'v-on:submit.prevent'=>'getResults()']) !!}
                <input type="text" name="search" class="form-control mydirection" id="inp_search" placeholder=" search"  >
            {!! Form::close() !!}
          </div>
          <br> <br>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Banner En </th>
                    <th> Banner Ar </th>
                    <th> Name En </th>
                    <th> Name Ar  </th>
                    <th> Visibility </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      <td> <img :src="list.banner_en" width="120px"> </td>
                      <td> <img :src="list.banner_ar" width="120px"> </td>
                      <td> <p v-text="list.name_en"></p>  </td>
                      <td> <p v-text="list.name_ar"></p>  </td>
                      <td>
                         <span class="badge badge-success"  v-if="list.status"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>
                      <td>
                          @permission('Brand_status')
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.status ,'btn-danger':!list.status }"  v-on:click="showORhide(list.id)">
                               <i class="fa fa-eye" v-if="list.status"></i>
                               <i class="fa fa-eye-slash" v-else ></i>
                           </button>
                           @endpermission
                           @permission('Brand_in_home_page')
                           <button  class="btn btn-primary btn-rounded"  v-on:click="switchinHomePage(list.id)">
                               <i class="fa fa-home"></i>
                           </button>
                           @endpermission
                           @permission('Brand_edit')
                           <button class="btn btn-primary btn-rounded" v-on:click="showEditModel(list)" >
                              <i class="fa fa-pencil"></i>
                           </button>
                           @endpermission
                           @permission('Brand_delete')
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                           @endpermission
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->

            {{-- ------------------- create ------------------------ --}}
                @permission('Brand_create')
                @component('components.modal_lg')
                    @slot('id')
                      create_model
                    @endslot
                    @slot('header')
                         Add New
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>"mydirection",'id'=>'create_form' ,'v-on:submit.prevent'=>'create()' ]) !!}
                    @endslot
                    @slot('body')
                      {!! Form::hidden('category_id',null,[ 'v-model'=>'Category_id']) !!}
                      {!! Form::hidden('sub_category_id',null,[ 'v-model'=>'SubCategory_id']) !!}

                      <div class="row">
                          <div class="col-md-6">

                            <div class="form-group">
                                {!! Form::label('banner_en',' Banner En: (1740px * 700px) ') !!}
                                {!! Form::file('banner_en',['class'=>'form-control','v-on:change'=>'Preview_image($event)'  ,'required'  ]) !!}
                                <img     class="Preview_image">
                            </div>

                            <div class="form-group">
                                {!! Form::label('banner_ar',' Banner Ar: (1740px * 700px) ') !!}
                                {!! Form::file('banner_ar',['class'=>'form-control','v-on:change'=>'Preview_image($event)'  ,'required'  ]) !!}
                                <img   class="Preview_image">
                            </div>

                          </div><!--End col-md-6-->
                          <div class="col-md-6">

                            <div class="form-group">
                                {!! Form::label('name_en','Name En') !!}
                                {!! Form::text('name_en',null,['class'=>'form-control','required'  ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('name_ar','Name Ar') !!}
                                {!! Form::text('name_ar',null,['class'=>'form-control','required'  ]) !!}
                            </div>
                            <hr>
                            <div class="form-group">
                                {!! Form::label('og_description_en','Tags Description En') !!}
                                {!! Form::textarea('og_description_en',null,['class'=>'form-control' ,'rows'=>'6' ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('og_description_ar','Tags Description Ar') !!}
                                {!! Form::textarea('og_description_ar',null,['class'=>'form-control' ,'rows'=>'6' ]) !!}
                            </div>

                          </div><!--End col-md-6-->
                      </div><!--End row-->

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" >  Add new </button>
                    @endslot
                @endcomponent
                @endpermission
            {{-- ------------------- edit ------------------------ --}}
                @permission('Brand_edit')
                @component('components.modal_lg')
                    @slot('id')
                      edit_model
                    @endslot
                    @slot('header')
                          Edit
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>" ",'id'=>'edit_form' ,'v-on:submit.prevent'=>'edit()'   ]) !!}
                    @endslot
                    @slot('body')

                      {!! Form::hidden('id',null,['id'=>'edit_id','v-model'=>'EF.id']) !!}
                      {!! Form::hidden('category_id',null,[ 'v-model'=>'Category_id']) !!}
                      {!! Form::hidden('sub_category_id',null,[ 'v-model'=>'SubCategory_id']) !!}
                      <div class="row">
                          <div class="col-md-6">

                            <div class="form-group">
                                {!! Form::label('banner_en',' Banner En: (1740px * 700px) ') !!}
                                {!! Form::file('banner_en',['class'=>'form-control','v-on:change'=>'Preview_image($event)' ]) !!}
                                <img :src="EF.banner_en"   class="Preview_image">
                            </div>

                            <div class="form-group">
                                {!! Form::label('banner_ar',' Banner Ar: (1740px * 700px) ') !!}
                                {!! Form::file('banner_ar',['class'=>'form-control','v-on:change'=>'Preview_image($event)' ]) !!}
                                <img :src="EF.banner_ar" class="Preview_image">
                            </div>

                          </div><!--End col-md-6-->
                          <div class="col-md-6">

                            <div class="form-group">
                                {!! Form::label('name_en','Name En') !!}
                                {!! Form::text('name_en',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_en']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('name_ar','Name Ar') !!}
                                {!! Form::text('name_ar',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_ar']) !!}
                            </div>
                            <hr>
                            <div class="form-group">
                                {!! Form::label('og_description_en','Tags Description En') !!}
                                {!! Form::textarea('og_description_en',null,['class'=>'form-control' ,'v-model'=>'EF.og_description_en','rows'=>'6' ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('og_description_ar','Tags Description Ar') !!}
                                {!! Form::textarea('og_description_ar',null,['class'=>'form-control' ,'v-model'=>'EF.og_description_ar','rows'=>'6' ]) !!}
                            </div>

                          </div><!--End col-md-6-->
                      </div><!--End row-->


                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" > Update </button>
                    @endslot
                @endcomponent
                @endpermission

              </div><!--End v-show=show_page-->

       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/Brand/delete`;
            var get_list =   `${admin_url}/Brand/list`;
            var showORhide_api = `${admin_url}/Brand/showORhide`;
            var create_api = `${admin_url}/Brand/create`;
            var update_api = `${admin_url}/Brand/update`;
            var switchinHomePage_api = `${admin_url}/Brand/switchinHomePage`;
            var get_categoiry_subCategory = JSON.parse(`{!!$categoiry_subCategory!!}`);
        </script>
        <script src="{{asset('js_admin/Brand/standard.js')}}"> </script>
    @endslot

@endcomponent
