

@component('components.panel_default_with_blank')
    @slot('active') AboutUs @endslot
    @slot('page_title') AboutUs  @endslot
    @slot('panel_title') AboutUs @endslot

    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">
          <button class="btn btn-primary btn-rounded" v-on:click="showCreateModel()">
                 <i class="fa fa-plus mydir"></i> Create
          </button>
          <br>

          <div class="col-md-6 pull-right">
            {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent'=>'getResults()']) !!}
                <input type="text" name="search" class="form-control mydirection"   placeholder=" search"  >
            {!! Form::close() !!}
          </div>
          <br> <br>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Image </th>
                    <th> Title  </th>
                    <th> Body  </th>
                    <th> Status </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      <td> <img :src="list.image" width="120px"> </td>
                      <td> <p v-text="list.title"></p>  </td>
                      <td width="30%"> <p v-text="list.body"></p>  </td>
                      <td>
                         <span class="badge badge-success"  v-if="list.status"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>
                      <td>
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.status ,'btn-danger':!list.status }"  v-on:click="showORhide(list.id)">
                               <i class="fa fa-eye" v-if="list.status"></i>
                               <i class="fa fa-eye-slash" v-else ></i>
                           </button>
                           <button class="btn btn-primary btn-rounded" v-on:click="showEditModel(list)" >
                              <i class="fa fa-pencil"></i>
                           </button>
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->

            {{-- ------------------- create ------------------------ --}}

                @component('components.modal_lg')
                    @slot('id')
                      create_model
                    @endslot
                    @slot('header')
                         Add new
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>"mydirection",'id'=>'create_form' ,'v-on:submit.prevent'=>'create()' ]) !!}
                    @endslot
                    @slot('body')

                      <div class="row">
                          <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('image',' Image: 710px*668px ') !!}
                                    {!! Form::file('image',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'create','id'=>'create_image','required'  ]) !!}
                                </div>
                                <img  id="Preview_image_create" class="Preview_image">
                                <br><br>
                                <div class="form-group">
                                    {!! Form::label('title','Title') !!}
                                    {!! Form::text('title',null,['class'=>'form-control','required'  ]) !!}
                                </div>
                          </div><!--End col-md-6-->
                          <div class="col-md-6">
                            {!! Form::label('body','Body') !!}
                            {!! Form::textarea('body',null,['class'=>'form-control','rows'=>'14','required' ]) !!}
                          </div><!--End col-md-6-->
                      </div><!--End row-->

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" >  Add new </button>
                    @endslot
                @endcomponent


            {{-- ------------------- edit ------------------------ --}}

                @component('components.modal_lg')
                    @slot('id')
                      edit_model
                    @endslot
                    @slot('header')
                          edit
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>" ",'id'=>'edit_form' ,'v-on:submit.prevent'=>'edit()'   ]) !!}
                    @endslot
                    @slot('body')

                      {!! Form::hidden('id',null,['id'=>'edit_id','v-model'=>'EF.id']) !!}
                      <div class="row">
                          <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('image',' Image: 710px*668px ') !!}
                                    {!! Form::file('image',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'edit','id'=>'edit_image' ]) !!}
                                </div>
                                <img :src="EF.image" id="Preview_image_edit" class="Preview_image">
                                <br><br>
                                <div class="form-group">
                                    {!! Form::label('title','Title') !!}
                                    {!! Form::text('title',null,['class'=>'form-control','required' ,'v-model'=>'EF.title']) !!}
                                </div>
                          </div><!--End col-md-6-->
                          <div class="col-md-6">
                            {!! Form::label('body','Body') !!}
                            {!! Form::textarea('body',null,['class'=>'form-control','rows'=>'14','v-model'=>'EF.body','required']) !!}
                          </div><!--End col-md-6-->
                      </div><!--End row-->

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" > Update </button>
                    @endslot
                @endcomponent


       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/AboutUs/delete`;
            var get_list =   `${admin_url}/AboutUs/list`;
            var showORhide_api = `${admin_url}/AboutUs/showORhide`;
            var create_api = `${admin_url}/AboutUs/create`;
            var update_api = `${admin_url}/AboutUs/update`;
        </script>
        <script src="{{asset('js_admin/standard.js')}}"> </script>
    @endslot

@endcomponent
