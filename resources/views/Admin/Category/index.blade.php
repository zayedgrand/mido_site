

@component('components.panel_default_with_blank')
    @slot('active') Category @endslot
    {{-- @slot('page_title') Brand  @endslot --}}
    @slot('panel_title') Category @endslot
    @slot('the_permission') Category_and_subCategory_main @endslot

    @slot('body')

        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">
          @permission('Category_and_subCategory_create')
          <a class="btn btn-primary btn-rounded" href="{{url('Admin/Category/create')}}">
                 <i class="fa fa-plus mydir"></i> Create
          </a>
          <br>
          @endpermission

          <div class="col-md-6 pull-right">
            {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent'=>'getResults()']) !!}
                <input type="text" name="search" class="form-control mydirection" placeholder=" search"  >
            {!! Form::close() !!}
          </div>
          <br> <br>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    {{-- <th> Logo </th> --}}
                    <th> Name En  </th>
                    <th> Name Ar  </th>
                    <th> Visibility </th>
                    {{-- <th> In home page </th> --}}
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      {{-- <td> <img :src="list.logo" width="120px"> </td> --}}
                      <td width="30%"> <p v-text="list.name_en"></p>  </td>
                      <td width="30%"> <p v-text="list.name_ar"></p>  </td>
                      <td>
                         <span class="badge badge-success"  v-if="list.status"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>
                      {{-- <td>
                         <span class="badge badge-success"  v-if="list.in_home_page"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td> --}}
                      <td>
                           @permission('Category_and_subCategory_status')
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.status ,'btn-danger':!list.status }"  v-on:click="showORhide(list.id)">
                               <i class="fa fa-eye" v-if="list.status"></i>
                               <i class="fa fa-eye-slash" v-else ></i>
                           </button>
                           @endpermission
                           {{-- @permission('Category_and_subCategory_in_home_page')
                           <button  class="btn btn-primary btn-rounded"  v-on:click="switchinHomePage(list.id)">
                               <i class="fa fa-home"></i>
                           </button>
                           @endpermission --}}
                           @permission('Category_and_subCategory_edit')
                           <a class="btn btn-primary btn-rounded" :href="'{{url('Admin/Category/edit')}}/'+list.id" >
                              <i class="fa fa-pencil"></i>
                           </a>
                           @endpermission
                           @permission('Category_and_subCategory_delete')
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                           @endpermission
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->


       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/Category/delete`;
            var get_list =   `${admin_url}/Category/list`;
            var showORhide_api = `${admin_url}/Category/showORhide`;
            var create_api = `${admin_url}/Category/create`;
            var update_api = `${admin_url}/Category/update`;
        </script>
        <script src="{{asset('js_admin/Category/index.js')}}"> </script>
    @endslot

@endcomponent
