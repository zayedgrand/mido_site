

@component('components.panel_default_with_blank')
    @slot('active') Category @endslot
    @slot('panel_title') Category Edit @endslot
    @slot('the_permission') Category_and_subCategory_edit @endslot
    @slot('breadcrumb')
        <ul class="breadcrumb">
          <li><a href="{{url('Admin/Category')}}">Category</a></li>
          <li class="active">Edit</li>
       </ul>
     @endslot


    @slot('body')
      <style>
          [v-cloak] { display: none; }

          .Categories_container
          {
              padding: 10px 40px;
          } 
      </style>
        <div id="myVue">

          <br>

          {!! Form::open([ 'method'=>'PATCH','url'=>['Admin/Category',$Category->id], 'class'=>" ",'id'=>'my_form' ,'v-on:submit'=>'do_submit()' ,'files'=>true  ]) !!}

            <table class="table mydir">
                  {{-- <tr>
                    <th> <p> logo: 415px*415px  </p> </th>
                    <td>
                          <div class="form-group">
                              {!! Form::file('logo',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'create','id'=>'edit_image' ]) !!}
                          </div>
                          <img :src="EF.logo" id="Preview_image_create" class="Preview_image">
                    </td>
                  </tr> --}}
                  <tr>
                    <th> <p> Name En: </p> </th>
                    <td>
                        {!! Form::text('name_en',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_en','required']) !!}
                    </td>
                  </tr>
                  <tr>
                    <th> <p> Name Ar: </p> </th>
                    <td>
                        {!! Form::text('name_ar',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_ar','required']) !!}
                    </td>
                  </tr>
                  {{-- <tr>
                    <th> <p> Banner Ar: 415px*415px </p> </th>
                    <td>
                          <div class="form-group">
                              {!! Form::file('banner_ar',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'addImage' ]) !!}
                          </div>
                          <img :src="EF.banner_ar" class="Preview_image">
                    </td>
                  </tr>
                  <tr>
                    <th> <p> Banner En: 415px*415px </p> </th>
                    <td>
                          <div class="form-group">
                              {!! Form::file('banner_en',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'addImage' ]) !!}
                          </div>
                          <img :src="EF.banner_en" class="Preview_image">
                    </td>
                  </tr> --}}
            </table>
            <hr>
            <!--IMAGES -->
            <h2> <center> <b> Add Sub Categories </b> </center>  </h2>

            <div class="Categories_container"> <!-- container -->


              <button type="button" class="btn btn-primary " v-on:click="addNewImage()"   >
                <i class="fa fa-plus mydir"></i> <span>Add Sub Category</span>
              </button>
              <br>

              <table class="table">
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(arr,index) in catsArr" :key="arr.cc"  class="cat">
                    <td>
                            <input type="hidden" name="old_cats_ids[]" v-if="arr.id" :value="arr.id">
                            <input type="hidden" name="cats_no[]" :value="index">

                          <div class="form-group">
                              <label> Category Name En </label>
                              <input type="text" name="subCat_name_en[]"  v-model="arr.name_en" class="form-control">
                          </div><!--End form-group-->
                          <div class="form-group">
                              <label for=""> Banner En (1740px * 700px)</label>
                              {!! Form::file('subCat_banner_en[]',['class'=>'form-control','v-on:change'=>'Preview_cat_image($event,arr,"en")','data-from'=>'text_image_en','id'=>'edit_image' ]) !!}
                              <img :src="arr.banner_en" id="Preview_image_create" class="Preview_image">
                          </div><!--End form-group-->
                    </td>
                    <td>
                        <div class="form-group">
                            <label> Category Name Ar </label>
                            <input type="text" name="subCat_name_ar[]"  v-model="arr.name_ar" class="form-control">
                        </div><!--End form-group-->
                        <div class="form-group">
                            <label for=""> Banner Ar (1740px * 700px)</label>
                            {!! Form::file('subCat_banner_ar[]',['class'=>'form-control','v-on:change'=>'Preview_cat_image($event,arr,"ar")','data-from'=>'text_image_ar','id'=>'edit_image' ]) !!}
                            <img :src="arr.banner_ar" id="Preview_image_create" class="Preview_image">
                        </div><!--End form-group-->
                    </td>
                    <td>
                      <br>
                      <div v-if="arr.id">
                        <button type="button" class="btn btn-primary btn-condensed" v-on:click="switchinHomePage(arr.id)">
                            <i class="fa fa-home"></i>
                        </button>
                        <br>
                      </div>
                      <br>
                      <button type="button" class="btn btn-danger btn-condensed" v-on:click="remove_categoiry(arr,index)"  >
                         <i class="glyphicon glyphicon-trash"></i>
                      </button>
                    </td>
                  </tr>
              </table>

                     {{-- <section is="transition-group" name="my-list" id="cats">
                         <div v-for="(arr,index) in catsArr" :key="arr.cc"  class="cat">

                               <input type="hidden" name="old_cats_ids[]" v-if="arr.id" :value="arr.id">
                               <input type="hidden" name="cats_no[]" :value="index">

                               <div class="headd">
                                   <p>
                                       <button v-if="arr.id" type="button" class="btn btn-primary btn-condensed" v-on:click="add_Cat_to_home_page(arr)"  >
                                          <i class="fa fa-home"></i>
                                       </button>
                                       <button type="button" class="btn btn-danger btn-condensed" v-on:click="remove_categoiry(arr,index)"  >
                                          <i class="glyphicon glyphicon-trash"></i>
                                       </button>
                                       <b> Category: @{{arr.name_en}} </b>
                                   </p>
                               </div><!--End headd-->

                                 <div class="bodyy">
                                   <div class="row">
                                         <div class="col-md-6">
                                               <div class="form-group">
                                                  <label for=""> Name En </label>
                                                  <input type="text" name="cat_name_en[]" v-model="arr.name_en" class="form-control" required>
                                               </div><!--End form-group-->
                                         </div><!--End col-md-6-->
                                         <div class="col-md-6">
                                               <div class="form-group">
                                                  <label for=""> Name Ar </label>
                                                  <input type="text" name="cat_name_ar[]" v-model="arr.name_ar" class="form-control" required>
                                               </div><!--End form-group-->
                                         </div><!--End col-md-6-->
                                       <!--................next row...............-->
                                         <div class="col-md-6">
                                           <div class="form-group">
                                                 <label for=""> Banner En </label>
                                                 {!! Form::file('cat_banner_en[]',['class'=>'form-control','v-on:change'=>'Preview_cat_image($event,arr,"en")','data-from'=>'text_image_en','id'=>'edit_image' ]) !!}
                                                 <img :src="arr.banner_en" id="Preview_image_create" class="Preview_image">
                                             </div><!--End form-group-->
                                         </div><!--End col-md-6-->
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                 <label for=""> Banner Ar </label>
                                                 {!! Form::file('cat_banner_ar[]',['class'=>'form-control','v-on:change'=>'Preview_cat_image($event,arr,"ar")','data-from'=>'text_image_ar','id'=>'edit_image' ]) !!}
                                                 <img :src="arr.banner_ar" id="Preview_image_create" class="Preview_image">
                                             </div><!--End form-group-->
                                         </div><!--End col-md-6-->

                                   </div><!--End row-->
                               </div><!--End bodyy-->

                         </div><!--End v-for , cat-->
                     </section> --}}

            </div><!--End container-->


            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner3 v-if="show_spinner"></spinner3>
            <!-- - - - - - -End spinner- - - - - - - -->

              <button type="submit" class="btn btn-success" style="width:100%" :disabled="btn_submit" > Update </button>

            {!! Form::close() !!}
       </div><!--End myVue-->

       <br><br>
    @endslot

    @slot('script')
        <script>
            var get_project = JSON.parse(`{!! $Category !!}`);
            var get_ProjectImages = JSON.parse(`{!! $SubCategory !!}`);
            var delete_cat_api = `${admin_url}/Category/subCategoiry/delete`;
            var subCat_in_HomePage_api = `${admin_url}/Category/subCategoiry/switchinHomePage`;
        </script>
        <script src="{{asset('js_admin/Category/standard_expanded.js')}}"> </script>
    @endslot

@endcomponent
