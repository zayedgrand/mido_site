

@component('components.panel_default_with_blank')
    @slot('active') Category @endslot
    {{-- @slot('page_title') Brand edit  @endslot --}}
    @slot('panel_title') Category Create @endslot
      @slot('the_permission') Category_and_subCategory_create @endslot
    @slot('breadcrumb')
        <ul class="breadcrumb">
          <li><a href="{{url('Admin/Category')}}">Category</a></li>
          <li class="active">create</li>
       </ul>
     @endslot


    @slot('body')
      <style>
          [v-cloak] { display: none; }

          .Categories_container
          {
              padding: 10px 40px;
          }

      </style>
        <div id="myVue">

          <br>

          {!! Form::open([ 'method'=>'post','url'=>'Admin/Category' , 'class'=>" ",'id'=>'my_form' ,'v-on:submit'=>'do_submit()' ,'files'=>true  ]) !!}

            <table class="table mydir">
                  {{-- <tr>
                    <th> <p> logo: 415px*415px </p> </th>
                    <td>
                          <div class="form-group">
                              {!! Form::file('logo',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'create','id'=>'edit_image' ,'required']) !!}
                          </div>
                          <img  id="Preview_image_create" class="Preview_image">
                    </td>
                  </tr> --}}
                  <tr>
                    <th> <p> Name Ar: </p> </th>
                    <td>
                        {!! Form::text('name_ar',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_ar','required']) !!}
                    </td>
                  </tr>
                  <tr>
                    <th> <p> Name En: </p> </th>
                    <td>
                        {!! Form::text('name_en',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_en','required']) !!}
                    </td>
                  </tr>
                  {{-- <tr>
                    <th> <p> Banner Ar: 415px*415px </p> </th>
                    <td>
                          <div class="form-group">
                              {!! Form::file('banner_ar',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'addImage' ]) !!}
                          </div>
                          <img  class="Preview_image">
                    </td>
                  </tr>
                  <tr>
                    <th> <p> Banner En: 415px*415px </p> </th>
                    <td>
                          <div class="form-group">
                              {!! Form::file('banner_en',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'addImage' ]) !!}
                          </div>
                          <img  class="Preview_image">
                    </td>
                  </tr> --}}
            </table>
            <hr>
            <!--IMAGES -->
            <h2> <center> <b> Add Sub Categories </b> </center>  </h2>

            <div class="Categories_container">  <!-- Categories_container -->


              <button type="button" class="btn btn-primary " v-on:click="addNewImage()" >
                <i class="fa fa-plus mydir"></i> <span>Add Sub Category</span>
              </button>
              <br>

                  <hr>

                  <table class="table">
                    {{-- <thead>
                        <th> Category Name En </th>
                        <th> Category Name Ar </th>
                        <th> Delete </th>
                    </thead> --}}
                    <tbody is="transition-group" name="my-list" v-cloak >
                      <tr v-for="(arr,index) in catsArr" :key="arr.cc"  class="cat">
                        <td>
                                <input type="hidden" name="old_cats_ids[]" v-if="arr.id" :value="arr.id">
                                <input type="hidden" name="cats_no[]" :value="index">

                                <div class="form-group">
                                    <label> Category Name En </label>
                                    <input type="text" name="subCat_name_en[]"  class="form-control">
                                </div><!--End form-group-->
                                <div class="form-group">
                                    <label for=""> Banner En (1740px * 700px)</label>
                                    {!! Form::file('subCat_banner_en[]',['class'=>'form-control','v-on:change'=>'Preview_cat_image($event,arr,"en")','data-from'=>'text_image_en','id'=>'edit_image' ]) !!}
                                    <img id="Preview_image_create" class="Preview_image">
                                </div><!--End form-group-->
                        </td>
                        <td>
                              <div class="form-group">
                                  <label> Category Name Ar </label>
                                  <input type="text" name="subCat_name_ar[]"  class="form-control">
                              </div><!--End form-group-->
                              <div class="form-group">
                                  <label for=""> Banner Ar (1740px * 700px)</label>
                                  {!! Form::file('subCat_banner_ar[]',['class'=>'form-control','v-on:change'=>'Preview_cat_image($event,arr,"ar")','data-from'=>'text_image_ar','id'=>'edit_image' ]) !!}
                                  <img id="Preview_image_create" class="Preview_image">
                              </div><!--End form-group-->
                        </td>
                        <td>
                          <button type="button" class="btn btn-danger btn-condensed" v-on:click="remove_categoiry(arr,index)"  >
                             <i class="glyphicon glyphicon-trash"></i>
                          </button>
                        </td>
                      </tr>
                  </table>

            </div><!--End container-->



            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner3 v-if="show_spinner"></spinner3>
            <!-- - - - - - -End spinner- - - - - - - -->

              <button type="submit" class="btn btn-success" style="width:100%" :disabled="btn_submit" > Create </button>

            {!! Form::close() !!}
       </div><!--End myVue-->

       <br><br>
    @endslot

    @slot('script')
        <script>
            var get_project = JSON.parse(`{}`);
            // var get_ProjectImages = JSON.parse(`[{  "name_en":"","name_ar":"","cc":"cc_"+${Math.random()} }]`);
            var get_ProjectImages = JSON.parse(`[ ]`);
            var delete_cat_api = `${admin_url}/Brand/categoiry/delete`;
        </script>
        <script src="{{asset('js_admin/Category/standard_expanded.js')}}"> </script>
    @endslot

@endcomponent
