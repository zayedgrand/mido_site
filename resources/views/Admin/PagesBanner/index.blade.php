

@component('components.panel_default_with_blank')
    @slot('active') PagesBanner @endslot
    @slot('page_title') Pages Banner  @endslot
    @slot('panel_title') Pages Banner @endslot

    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">
          <br>

          <div class="col-md-6 pull-right">
            {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent'=>'getResults()']) !!}
                <input type="text" name="search" class="form-control mydirection"   placeholder=" search"  >
            {!! Form::close() !!}
          </div>
          <br> <br>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Page </th>
                    <th> Image En</th>
                    <th> Image Ar</th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      <td> <p v-text="list.page"></p>  </td>
                      <td> <img :src="list.imageen" width="120px"> </td>
                      <td> <img :src="list.imagear" width="120px"> </td>
                      <td>
                           <button class="btn btn-primary btn-rounded" v-on:click="showEditModel(list)" >
                              <i class="fa fa-pencil"></i>
                           </button>
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->

            {{-- ------------------- edit ------------------------ --}}

                @component('components.modal_lg')
                    @slot('id')
                      edit_model
                    @endslot
                    @slot('header')
                          edit
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>" ",'id'=>'edit_form' ,'v-on:submit.prevent'=>'edit()'   ]) !!}
                    @endslot
                    @slot('body')

                      {!! Form::hidden('id',null,['id'=>'edit_id','v-model'=>'EF.id']) !!}

                      <div class="form-group">
                          {!! Form::label('imageen',' Image en ') !!}
                          {!! Form::file('imageen',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'edit','id'=>'edit_image' ]) !!}
                      </div>
                      <img :src="EF.imageen" id="Preview_image_edit" class="Preview_image">
                      <div class="form-group">
                          {!! Form::label('imagear',' Image ar ') !!}
                          {!! Form::file('imagear',['class'=>'form-control','v-on:change'=>'Preview_image($event)','data-from'=>'create','id'=>'create_image' ]) !!}
                      </div>
                      <img :src="EF.imagear" id="Preview_image_create" class="Preview_image">
 

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" > Update </button>
                    @endslot
                @endcomponent


       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/PagesBanner/delete`;
            var get_list =   `${admin_url}/PagesBanner/list`;
            var showORhide_api = `${admin_url}/PagesBanner/showORhide`;
            var create_api = `${admin_url}/PagesBanner/create`;
            var update_api = `${admin_url}/PagesBanner/update`;
        </script>
        <script src="{{asset('js_admin/more/standard_edit_reload.js')}}"> </script>
    @endslot

@endcomponent
