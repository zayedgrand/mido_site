@component('components.panel_default_with_blank')
    @slot('active') Setting @endslot
    {{-- @slot('page_title') Info & Content  @endslot --}}
    @slot('panel_title') Info & Content @endslot

    @slot('body')

        <div id="myVue">
          <br>
          {!! Form::open([ 'id'=>'create_form', 'v-on:submit.prevent'=>'save()','files'=>true]) !!}


              <h2> <center> Our Info  </center>  </h2>
              <div class="row">
                  <div class="col-md-6">

                      <label> Our Location En</label>
                      <textarea name="our_location_en" :value="list.our_location_en" class="form-control" rows="2" ></textarea>
                       <br>
                      <label> Our Location Ar</label>
                      <textarea name="our_location_ar" :value="list.our_location_ar" class="form-control" rows="2" ></textarea>
                      <hr>
                      <label> Our Phone 1 </label>
                      <input type="text" class="form-control" name="our_phone_1" :value="list.our_phone_1">
                      <br>
                      <label> Our Phone 2</label>
                      <input type="text" class="form-control" name="our_phone_2" :value="list.our_phone_2">

                  </div><!--End col-md-6 -->
                  <div class="col-md-6">
                    <label> Facebook </label>
                    <input type="text" class="form-control" name="facebook_link" :value="list.facebook_link">
                    <br>
                    <label>  Linkedin  </label>
                    <input rows="9" class="form-control" name="linkedin_link" :value="list.linkedin_link" >
                    <br>
                    <label>  Instagram </label>
                    <input rows="9" class="form-control" name="instagram_link" :value="list.instagram_link" >
                    <hr>
                    <label>  Our Email </label>
                    <input rows="9" class="form-control" name="our_email" :value="list.our_email" >

                  </div><!--End col-md-6 -->
              </div><!--End row-->
             <hr>
<br>

        <h2> <center> Pricing  </center>  </h2> <br>
        <div class="row">
            <div class="col-md-6">

                <label> Minimum basket amount (EGP)</label>
                <input type="text" class="form-control" name="minimum_basket_amount" :value="list.minimum_basket_amount">
                <br>
                <label> Minimum free Taste amount (EGP)</label>
                <input type="text" class="form-control" name="minimum_freeTast_amount" :value="list.minimum_freeTast_amount">

            </div><!--End col-md-6 -->
            <div class="col-md-6">
              <label> Tax percentage (%) </label>
              <input type="text" class="form-control" name="tax_price" :value="list.tax_price">
              <br>
              <label> shipping price  </label>
              <input type="text" class="form-control" name="shipping_price" :value="list.shipping_price">


            </div><!--End col-md-6 -->
        </div><!--End row-->

              {{-- <h2> <center> Our Content  </center>  </h2> <br>
              <div class="row">
                  <div class="col-md-6">
                      <label> aboutus page : paragraph of the description </label>
                      <textarea rows="9" class="form-control" name="aboutus_main_paragraph" cols="80"> @{{list.aboutus_main_paragraph}} </textarea>
                      <br>
                      <label> location page : paragraph of the description </label>
                      <textarea rows="9" class="form-control" name="location_description" cols="80"> @{{list.location_description}} </textarea>
                      <br>
                      <label> contact us page : paragraph of the description </label>
                      <textarea rows="9" class="form-control" name="contactus_description" cols="80"> @{{list.contactus_description}} </textarea>
                      <br>
                  </div><!--End col-md-6 -->
                  <div class="col-md-6">
                      <label> master plan page : paragraph of the description </label>
                      <textarea rows="9" class="form-control" name="master_plan_description" cols="80"> @{{list.master_plan_description}} </textarea>
                      <br>
                      <label> Construction page : paragraph of the description </label>
                      <textarea rows="9" class="form-control" name="Construction_description" cols="80"> @{{list.Construction_description}} </textarea>
                      <br>
                      <label> about us page : AboutDeveloper  </label>
                      <textarea rows="9" class="form-control" name="AboutDeveloper_description" cols="80"> @{{list.AboutDeveloper_description}} </textarea>
                      <br>
                  </div><!--End col-md-6 -->
              </div><!--End row--> --}}


              {{-- <h2> <center> صور الصفحة الرئسية بجانب  </center>  </h2>
              <div class="row">
                  <div class="col-md-6">
                      <label> في الاعلي </label> <br>
                      <img :src="'{{url('images/ads')}}/'+list.main_page_image1_beside_slider" id="Preview_main_page_image1_beside_slider" width="300px" >
                      <input type="file" name="main_page_image1_beside_slider" data-from="main_page_image1_beside_slider" v-on:change="Preview_image($event)" id="inp_main_page_image1_beside_slider" class="form-control" >
                  </div><!--End col-md-6 -->
                  <div class="col-md-6">
                      <label>  في الاسفل  </label><br>
                      <img :src="'{{url('images/ads')}}/'+list.main_page_image2_beside_slider" id="Preview_main_page_image2_beside_slider" width="300px" >
                      <input type="file" name="main_page_image2_beside_slider" data-from="main_page_image2_beside_slider" v-on:change="Preview_image($event)" id="inp_main_page_image2_beside_slider" class="form-control" >
                  </div><!--End col-md-6 -->
              </div><!--End row--> --}}


             <!-- - - - - - -START spinner- - - - - - - -->
             <spinner2 v-if="show_spinner"></spinner2>
             <!-- - - - - - -End spinner- - - - - - - -->
   <br><br>

             {!! Form::submit('Update',['class'=>'btn btn-success ','style'=>'width:100%',':disabled'=>'btn_submit']) !!}

             <br><br>
           {!! Form::close() !!}

       </div><!--End myVue-->



    @endslot

    @slot('script')
        <script>
            let list_api = '{{url('Admin/Setting/list')}}';
            let save_api = '{{url('Admin/Setting/save')}}';
        </script>
        <script src="{{asset('js_admin/Setting.js')}}"> </script>

        {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALsQCIjTvYTkx4BXABnxaW-J06RCJPWdM&callback=initMap" async defer></script> --}}

    @endslot

@endcomponent
