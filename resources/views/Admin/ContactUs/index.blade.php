

@component('components.panel_default_with_blank')
    @slot('active') ContactUs @endslot
    @slot('page_title') ContactUs  @endslot
    @slot('panel_title') ContactUs @endslot

    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">

          <a href="{{url('Admin/ContactUs/Export')}}" target="_blank" class="btn btn-info"> Export  </a>

          <div class="row">
            {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent'=>'getResults()']) !!}
              <div class="col-md-6 pull-right">
                    <label> search: </label>
                    <input type="text" name="search" class="form-control mydirection"   placeholder=" search"  >
              </div>
              <div class="col-md-6 pull-right">
                      <label> client connection: </label>
                      <select class="form-control" name="connection" v-on:change="getResults()">
                          <option value=""> All </option>
                          <option value="contacted"> contacted </option>
                          <option value="notContacted"> not contacted yet </option>
                      </select>
              </div>
              {!! Form::close() !!}
          </div><!--End row-->

          <br> <br>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Name </th>
                    <th> Phone  </th>
                    <th> Email  </th>
                    <th> Contacted </th>
                    <th> Created date </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                 <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      {{-- <td> <img :src="list.image" width="120px"> </td> --}}
                      <td> <p v-text="list.name"></p>  </td>
                      <td> <p v-text="list.phone"></p>  </td>
                      <td> <p v-text="list.email"></p>  </td>
                      <td>
                         <span class="badge badge-success"  v-if="list.is_contacted"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>
                      <td>
                        @{{list.created_at}} <br>
                        [@{{diffforhumans(list.created_at)}} ]
                      </td>
                      <td>
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.is_contacted ,'btn-danger':!list.is_contacted }" v-on:click="ContatctedOrNot(list.id)">
                               <i class="fa fa-check-square-o" v-if="list.is_contacted"></i>
                               <i class="fa fa-square-o" v-else ></i>
                           </button>
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->



       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/ContactUs/delete`;
            var get_list =   `${admin_url}/ContactUs/list`;
            var ContatctedOrNot_api = `${admin_url}/ContactUs/switch_contacted`;
        </script>
        <script src="{{asset('js_admin/standard.js')}}"> </script>
    @endslot

@endcomponent
