
@component('components.panel_default_with_blank')
    @slot('active') PromoCodes @endslot
    {{-- @slot('page_title') Promo Codes  @endslot --}}
    @slot('panel_title') Promo Code @endslot
    @slot('the_permission') PromoCodes_main @endslot

    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">
          @permission('PromoCodes_create')
          <button class="btn btn-primary btn-rounded" v-on:click="showCreateModel()">
                 <i class="fa fa-plus mydir"></i> Create
          </button>
          @endpermission
          <br>

          <div class="col-md-6 pull-right">
            {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent'=>'getResults()']) !!}
                <input type="text" name="search" class="form-control mydirection"   placeholder=" search"  >
            {!! Form::close() !!}
          </div>
          <br> <br>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Code  </th>
                    <th> Discount Percentage  </th>
                    <th> From Date  </th>
                    <th> To Date  </th>
                    <th> Visibility </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      <td> <p v-text="list.code"></p>  </td>
                      <td> <p> @{{list.discount_percentage}}% </p>  </td>
                      <td> <p v-text="list.from_date"></p>   <span> [@{{diffforhumans(list.from_date)}}] </span>   </td>
                      <td> <p v-text="list.to_date"></p> <span> [@{{diffforhumans(list.to_date)}}] </span>  </td>
                      <td>
                         <span class="badge badge-success"  v-if="list.status"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>
                      <td>
                           @permission('PromoCodes_status')
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.status ,'btn-danger':!list.status }"  >
                               <i class="fa fa-eye" v-if="list.status" v-on:click="showORhide(list.id)"  ></i> <!-- data-toggle="tooltip" title="cant be used"-->
                               <i class="fa fa-eye-slash" v-else v-on:click="showORhide(list.id)"  ></i>
                           </button>
                           @endpermission
                           @permission('PromoCodes_edit')
                           <button class="btn btn-primary btn-rounded" v-on:click="showEditModel(list)"  >
                              <i class="fa fa-pencil"></i>
                           </button>
                           @endpermission
                           @permission('PromoCodes_delete')
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                           @endpermission
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->

            {{-- ------------------- create ------------------------ --}}
                @permission('PromoCodes_create')
                @component('components.modal')
                    @slot('id')
                      create_model
                    @endslot
                    @slot('header')
                         Add new
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>"mydirection",'id'=>'create_form' ,'v-on:submit.prevent'=>'create()' ]) !!}
                    @endslot
                    @slot('body')

                        {!! Form::label('discount_percentage','Discount percentage') !!}
                        {!! Form::number('discount_percentage',null,['class'=>'form-control','required' ]) !!}

                        {!! Form::label('code','unique code') !!}
                        {!! Form::text('code',null,['class'=>'form-control','required' ]) !!}

                        {!! Form::label('from_date','From date') !!}
                        <input type="text" name="from_date"  class="form-control datepicker" required  >

                        {!! Form::label('to_date','To date') !!}
                        <input type="text" name="to_date"  class="form-control datepicker" required  >

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" >  Add new </button>
                    @endslot
                @endcomponent
                @endpermission
            {{-- ------------------- edit ------------------------ --}}
                @permission('PromoCodes_edit')
                @component('components.modal')
                    @slot('id')
                      edit_model
                    @endslot
                    @slot('header')
                          edit
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>" ",'id'=>'edit_form' ,'v-on:submit.prevent'=>'edit()'   ]) !!}
                    @endslot
                    @slot('body')

                      {!! Form::hidden('id',null,['id'=>'edit_id','v-model'=>'EF.id']) !!}

                            {!! Form::label('discount_percentage','Discount percentage') !!}
                            {!! Form::number('discount_percentage',null,['class'=>'form-control','required' ,'v-model'=>'EF.discount_percentage' ]) !!}

                            {!! Form::label('code','unique code') !!}
                            {!! Form::text('code',null,['class'=>'form-control','required' ,'v-model'=>'EF.code' ]) !!}

                            {!! Form::label('from_date','From date') !!}
                            <input type="text" name="from_date"  class="form-control datepicker"  v-model='EF.from_date' required  >

                            {!! Form::label('to_date','To date') !!}
                            <input type="text" name="to_date"  class="form-control datepicker" v-model='EF.to_date' required  >

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" > Update </button>
                    @endslot
                @endcomponent
                @endpermission

       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/PromoCodes/delete`;
            var get_list =   `${admin_url}/PromoCodes/list`;
            var showORhide_api = `${admin_url}/PromoCodes/showORhide`;
            var create_api = `${admin_url}/PromoCodes/create`;
            var update_api = `${admin_url}/PromoCodes/update`;

            $(".datepicker").datepicker({format: 'yyyy-mm-dd',startDate: date });

            $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
            });

        </script>
        <script src="{{asset('js_admin/more/standard_edit_reload.js')}}"> </script>
    @endslot

@endcomponent
