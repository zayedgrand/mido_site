

@extends('atlant.blank')

@section('content')
      @php($active='DashBoard')


      {{-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/> --}}

      <style media="screen">
          .widget-data
          {
              padding-right: 20px;
          }
      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12" id="myroot">


        <br>
    <!-- START WIDGETS -->
    <div class="row">
      <div class="col-md-3">

          <!-- START WIDGET MESSAGES -->
          <div class="widget widget-default widget-item-icon" onclick="location.href='{{url('Admin/Members')}}';" style="cursor: pointer">
                <div class="widget-item-left">
                    <span class="fa fa-users"></span>
                </div>
              <div class="widget-data">
                  <div class="widget-int num-count"> {{$Members_count}} </div>
                  <div class="widget-title">   Users  </div>
                  <div class="widget-subtitle"> Total Users </div>
              </div>
          </div>
          <!-- END WIDGET MESSAGES -->

      </div>
        <div class="col-md-3">

            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{url('Admin/Product')}}';" style="cursor: pointer">
                  <div class="widget-item-left">
                      <span class="fa fa-shopping-cart"></span>
                  </div>
                <div class="widget-data">
                    <div class="widget-int num-count"> {{$Products_count}} </div>
                    <div class="widget-title"> Products </div>
                    <div class="widget-subtitle"> Total Products </div>
                </div>
            </div>
            <!-- END WIDGET MESSAGES -->

        </div>
        <div class="col-md-3">

            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{url('Admin/Recipt')}}';" style="cursor: pointer">
                  <div class="widget-item-left">
                      <span class="fa fa-file-text-o"></span>
                  </div>
                <div class="widget-data">
                    <div class="widget-int num-count"> {{$Recipts_count}} </div>
                    <div class="widget-title">  Invoices  </div>
                    <div class="widget-subtitle"> Total  Invoices   </div>
                </div>
            </div>
            <!-- END WIDGET MESSAGES -->

        </div>
        <div class="col-md-3">

            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{url('Admin/Brand')}}';" style="cursor: pointer">
                  <div class="widget-item-left">
                      <span class="fa fa-briefcase"></span>
                  </div>
                <div class="widget-data">
                    <div class="widget-int num-count"> {{$Brands_count}} </div>
                    <div class="widget-title"> Brands </div>
                    <div class="widget-subtitle"> Total Brands  </div>
                </div>
            </div>
            <!-- END WIDGET MESSAGES -->

        </div>
    </div>

    {{-- ---------------------------Members charts-------------------------- --}}
    <div class="row">
          <div class="col-md-12">
              @component('components.panel_default')
                 @slot('panel_title')
                  <span class="fa fa-users"></span>   Users  ({{\Carbon\Carbon::now()->year}})
                 @endslot
                 @slot('body')
                       <div id="morris_Members"></div>
                 @endslot
              @endcomponent
    </div><!--End col-md-12-->
  </div><!--End row-->

  {{-- ---------------------------Recipts charts-------------------------- --}}
  <div class="row">
        <div class="col-md-8">
               @component('components.panel_default')
                  @slot('panel_title')
                      <span class="fa fa-file-text-o"></span>  Invoices  ({{\Carbon\Carbon::now()->year}})
                  @endslot
                  @slot('body')
                        <div id="morris_recipts"></div>
                  @endslot
               @endcomponent
        </div><!--End col-md-12-->
        <div class="col-md-4">

          @component('components.panel_default')
             @slot('panel_title')
                  <span class="fa fa-file-text-o"></span>  Invoices  ({{\Carbon\Carbon::now()->year}})
             @endslot
             @slot('body')
                 <div id="Donate_recipts" style="height:322px" ></div>
                 <table class="table mydirection"  >
                     {{-- <tr>
                        <th> processing </th>
                        <td> {{$Recipt_processing_count}} </td>
                     </tr>
                     <tr>
                        <th> shipping </th>
                        <td> {{$Recipt_shipping_count}}  </td>
                     </tr>
                     <tr>
                        <th> delivered </th>
                        <td> {{$Recipt_delivered_count}} </td>
                     </tr>
                     <tr>
                        <th> canceled </th>
                        <td> {{$Recipt_canceled_count}}  </td>
                     </tr> --}}
                 </table>
             @endslot
          @endcomponent

    </div><!--End col-md-4-->
  </div><!--End row-->
  {{-- ---------------------------End Items charts-------------------------- --}}





      </div><!--End col-md-12 root-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->



@endsection


@section('script')

  <script type="text/javascript" src="{{asset('atlant/js/plugins/morris/raphael-min.js')}}"></script>
  <script type="text/javascript" src="{{asset('atlant/js/plugins/morris/morris.min.js')}}"></script>

  {{-- <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
   <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.js"></script> --}}

  <script>

    morris_Members_MorrisArea();
    morris_recipts_MorrisArea();
    Donate_recipts();



   function morris_Members_MorrisArea()
   {
       var current_year = '{{\Carbon\Carbon::now()->year}}';
      {{-- @if (app()->getLocale() == 'ar')
           var months=["يناير", "فبراير", "مارس", "ابريل", "مايو", "يونيو", "يوليو", "اغستوس", "سبتمبر", "اكتوبر", "نوفمبير", "ديسمبر"];
       @else --}}
           var months=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      {{-- @endif --}}

       Morris.Area({
        element: 'morris_Members',
        data:
        [
               { month: current_year+'-1', count: 0 },
               { month: current_year+'-2', count: 0 },
               { month: current_year+'-3', count: 0 },
               { month: current_year+'-4', count: 0 },
               { month: current_year+'-5', count: 0 },
               { month: current_year+'-6', count: 0 },
               { month: current_year+'-7', count: 0 },
               { month: current_year+'-8', count: 0 },
               { month: current_year+'-9', count: 0 },
               { month: current_year+'-10', count: 0 },
               { month: current_year+'-11', count: 0 },
               { month: current_year+'-12', count: 0 },
                @foreach ($morris_members as $row)
                 {
                    month:current_year+'-{{$row->month}}',
                    count:'{{$row->count}}'
                 },
               @endforeach
        ],
       lineColors: ['#001a66', '#E0EEF9', '#ff758e'],
       xkey: 'month',
       ykeys: ['count'],
       labels: ["Users"],
       xLabelFormat:function(x) {
           var month=months[x.getMonth()];
           return month
       },
           dateFormat:function(x) {
               var month=months[new Date(x).getMonth()];
               return month
       },
       pointSize: 0,
       lineWidth: 0,
       resize: true,
       fillOpacity: 0.8,
       behaveLikeLine: true,
       gridLineColor: '#e0e0e0',
       hideHover: 'auto'
       });
   }

   function morris_recipts_MorrisArea()
   {
       var current_year = '{{\Carbon\Carbon::now()->year}}';
      {{-- @if (app()->getLocale() == 'ar')
           var months=["يناير", "فبراير", "مارس", "ابريل", "مايو", "يونيو", "يوليو", "اغستوس", "سبتمبر", "اكتوبر", "نوفمبير", "ديسمبر"];
       @else --}}
           var months=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      {{-- @endif --}}

       Morris.Area({
        element: 'morris_recipts',
        data:
        [
               { month: current_year+'-1', count: 0 },
               { month: current_year+'-2', count: 0 },
               { month: current_year+'-3', count: 0 },
               { month: current_year+'-4', count: 0 },
               { month: current_year+'-5', count: 0 },
               { month: current_year+'-6', count: 0 },
               { month: current_year+'-7', count: 0 },
               { month: current_year+'-8', count: 0 },
               { month: current_year+'-9', count: 0 },
               { month: current_year+'-10', count: 0 },
               { month: current_year+'-11', count: 0 },
               { month: current_year+'-12', count: 0 },
                @foreach ($morris_recipts as $row)
                 {
                    month:current_year+'-{{$row->month}}',
                    count:'{{$row->count}}'
                 },
               @endforeach
        ],
       lineColors: ['#006622', '#E0EEF9', '#ff758e'],
       xkey: 'month',
       ykeys: ['count'],
       labels: ["Invoices"],
       xLabelFormat:function(x) {
           var month=months[x.getMonth()];
           return month
       },
           dateFormat:function(x) {
               var month=months[new Date(x).getMonth()];
               return month
       },
       pointSize: 0,
       lineWidth: 0,
       resize: true,
       fillOpacity: 0.8,
       behaveLikeLine: true,
       gridLineColor: '#e0e0e0',
       hideHover: 'auto'
       });
   }


    function Donate_recipts()
    {
          Morris.Donut({
              element: 'Donate_recipts',
              data: [
                  {
                      label: '   processing ',
                      value: {{$Recipt_processing_count}}
                  },
                  {
                      label: '   shipping ',
                      value: {{$Recipt_shipping_count}}
                  },
                  {
                      label: ' delivered ',
                      value: {{$Recipt_delivered_count}}
                  },
                  {
                      label: '  canceled ',
                      value: {{$Recipt_canceled_count}}
                  }
              ],
              colors: [ '#ccc', '#006699','#b0dd91', '#560000' ],
              formatter: function(y) {
                  return y
              }
          });
    }



  </script>

@endsection
