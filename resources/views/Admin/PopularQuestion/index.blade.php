

@component('components.panel_default_with_blank')
    @slot('active') PopularQuestion @endslot
    {{-- @slot('page_title') PopularQuestion  @endslot --}}
    @slot('panel_title') Popular Questions @endslot
    @slot('the_permission') PopularQuestion_main @endslot

    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">
          @permission('PopularQuestion_create')
          <button class="btn btn-primary btn-rounded" v-on:click="showCreateModel()">
                 <i class="fa fa-plus mydir"></i> Create
          </button>
          <br>
          @endpermission

          <div class="col-md-6 pull-right">
            {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent'=>'getResults()']) !!}
                <input type="text" name="search" class="form-control mydirection" placeholder=" search"  >
            {!! Form::close() !!}
          </div>
          <br> <br>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Question En  </th>
                    <th> Question Ar  </th>
                    <th> Visibility </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      <td width="38%"> <p v-text="list.answer_en"></p>  </td>
                      <td width="38%"> <p v-text="list.answer_ar"></p>  </td>
                      <td>
                         <span class="badge badge-success"  v-if="list.status"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>
                      <td>
                          @permission('PopularQuestion_status')
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.status ,'btn-danger':!list.status }"  v-on:click="showORhide(list.id)">
                               <i class="fa fa-eye" v-if="list.status"></i>
                               <i class="fa fa-eye-slash" v-else ></i>
                           </button>
                           @endpermission
                           @permission('PopularQuestion_edit')
                           <br><br>
                           <button class="btn btn-primary btn-rounded" v-on:click="showEditModel(list)" >
                              <i class="fa fa-pencil"></i>
                           </button>
                           @endpermission
                           @permission('PopularQuestion_delete')
                           <br><br>
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                           @endpermission
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->

            {{-- ------------------- create ------------------------ --}}
                @permission('PopularQuestion_create')
                @component('components.modal_lg')
                    @slot('id')
                      create_model
                    @endslot
                    @slot('header')
                         Add new
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>"mydirection",'id'=>'create_form' ,'v-on:submit.prevent'=>'create()' ]) !!}
                    @endslot
                    @slot('body')
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('question_en','Question En') !!}
                                {!! Form::text('question_en',null,['class'=>'form-control','required' ]) !!}

                                {!! Form::label('answer_en','Answer En') !!}
                                {!! Form::textarea('answer_en',null,['class'=>'form-control','required' ]) !!}
                            </div><!--End col-md-6-->
                            <div class="col-md-6">
                              {!! Form::label('question_ar','Question Ar') !!}
                              {!! Form::text('question_ar',null,['class'=>'form-control','required' ]) !!}

                              {!! Form::label('answer_ar','Answer Ar') !!}
                              {!! Form::textarea('answer_ar',null,['class'=>'form-control','required' ]) !!}
                            </div><!--End col-md-6-->
                        </div><!--End row-->

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" >  Add new </button>
                    @endslot
                @endcomponent
                @endpermission
            {{-- ------------------- edit ------------------------ --}}
                @permission('PopularQuestion_edit')
                @component('components.modal_lg')
                    @slot('id')
                      edit_model
                    @endslot
                    @slot('header')
                          edit
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>" ",'id'=>'edit_form' ,'v-on:submit.prevent'=>'edit()'   ]) !!}
                    @endslot
                    @slot('body')

                      {!! Form::hidden('id',null,['id'=>'edit_id','v-model'=>'EF.id']) !!}

                            <div class="row">
                                <div class="col-md-6">
                                    {!! Form::label('question_en','Question En') !!}
                                    {!! Form::text('question_en',null,['class'=>'form-control','required','v-model'=>'EF.question_en' ]) !!}

                                    {!! Form::label('answer_en','Answer En') !!}
                                    {!! Form::textarea('answer_en',null,['class'=>'form-control','required','v-model'=>'EF.answer_en' ]) !!}
                                </div><!--End col-md-6-->
                                <div class="col-md-6">
                                  {!! Form::label('question_ar','Question Ar') !!}
                                  {!! Form::text('question_ar',null,['class'=>'form-control','required','v-model'=>'EF.question_ar' ]) !!}

                                  {!! Form::label('answer_ar','Answer Ar') !!}
                                  {!! Form::textarea('answer_ar',null,['class'=>'form-control','required','v-model'=>'EF.answer_ar' ]) !!}
                                </div><!--End col-md-6-->
                            </div><!--End row-->

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" > Update </button>
                    @endslot
                @endcomponent
                @endpermission

       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/PopularQuestion/delete`;
            var get_list =   `${admin_url}/PopularQuestion/list`;
            var showORhide_api = `${admin_url}/PopularQuestion/showORhide`;
            var create_api = `${admin_url}/PopularQuestion/create`;
            var update_api = `${admin_url}/PopularQuestion/update`;
        </script>
        <script src="{{asset('js_admin/standard.js')}}"> </script>
    @endslot

@endcomponent
