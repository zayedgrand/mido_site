
@extends('atlant.blank')

@section('content')
      @php($active='Role')

      <style>
        tr td:first-child
        {
            font-weight: bold;
        }
      </style>

      <!-- permission CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li class="active"> @lang('permission.permissions') </li>
            <li><a href="{{url('Admin\Role')}}"> @lang('permission.Role') </a></li>
        </ul>
        <!-- END BREADCRUMB -->
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('permission.edit Role') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
      {!! Form::model($role,['method'=>'PATCH','url'=>['Admin/Role',$role->id], 'id'=>'create_form' ]) !!}

      <div class="col-md-6">
          {!! Form::label('name',__('permission.name')) !!}
          {!! Form::text('name',null,['class'=>'form-control','required']) !!}

          {!! Form::label('comment',__('permission.comment')) !!}
          {!! Form::textarea('comment',null,['class'=>'form-control','rows'=>'3','required']) !!}
          <br>
      </div><!--End col-md-6-->

      <table class="table table-bordered mydirection">

          <thead>
              <th width="16%"> Authority </th>
              <th> @lang('permission.permissions')
                <span class="pull-atherWay">
                    <button type="button" class="btn btn-success btn-condensed" id="btn_all"> <i class="fa fa-check-square-o"></i> </button>
                </span>
              </th>
          </thead>
          <tbody>
            {{-- {{array_search('19', $selected_permissions)?'checked':''}} --}}

            <tr>  <!-- ============ DashBoard ===============  -->
              <td> @lang('permission.DashBoard') </td>
              <td>
                  {!! Form::label('DashBoard',__('permission.DashBoard')) !!} <br>
                  <label class="switch">
                      <input type="checkbox" class="switch" value="1" name="permissions[]" {{array_search('1', $selected_permissions)?'checked':''}}  />
                      <span></span>
                  </label>
              </td>
            </tr>
            <tr>  <!-- ============ Members ===============  -->
              <td> Members </td>
              <td>
                  {!! Form::label('DashBoard','Members') !!} <br>
                  <label class="switch">
                      <input type="checkbox" class="switch" value="2" name="permissions[]" {{array_search('2', $selected_permissions)?'checked':''}}  />
                      <span></span>
                  </label>
              </td>
            </tr>

            <tr>  <!-- ============ Brand_and_categoiry_main ===============  -->
                <td>  Category & subCategory   </td>
                <td>
                    <div class="row ">
                        <div class="col-md-2">
                            {!! Form::label('Category_and_subCategory_main',__('permission.main')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="3" name="permissions[]" {{array_search('3', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Category_and_subCategory_status',__('permission.status')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="4" name="permissions[]" {{array_search('4', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Category_and_subCategory_edit',__('permission.edit')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="6" name="permissions[]" {{array_search('6', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Category_and_subCategory_delete',__('permission.delete')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="7" name="permissions[]" {{array_search('7', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Category_and_subCategory_create',__('permission.create')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="8" name="permissions[]" {{array_search('8', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Category_and_subCategory_in_home_page','In home page') !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="5" name="permissions[]" {{array_search('5', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                    </div><!--End row-->
                </td>
            </tr>

            <tr>  <!-- ============ Brand_main ===============  -->
                <td>  Brands   </td>
                <td>
                    <div class="row ">
                        <div class="col-md-2">
                            {!! Form::label('Brand_main',__('permission.main')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="38" name="permissions[]" {{array_search('38', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Brand_status',__('permission.status')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="40" name="permissions[]" {{array_search('40', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Brand_edit',__('permission.edit')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="42" name="permissions[]" {{array_search('42', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Brand_delete',__('permission.delete')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="43" name="permissions[]" {{array_search('43', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Brand_create',__('permission.create')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="39" name="permissions[]" {{array_search('39', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Brand_in_home_page','In home page') !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="41" name="permissions[]" {{array_search('41', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                    </div><!--End row-->
                </td>
            </tr>

            <tr>  <!-- ============ City ===============  -->
                <td> District  </td>
                <td>
                    <div class="row ">
                        <div class="col-md-2">
                            {!! Form::label('City_main',__('permission.main')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="9" name="permissions[]" {{array_search('9', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('City_status',__('permission.status')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="11" name="permissions[]" {{array_search('11', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('City_edit',__('permission.edit')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="12" name="permissions[]" {{array_search('12', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('City_delete',__('permission.delete')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="13" name="permissions[]" {{array_search('13', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('City_create',__('permission.create')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="10" name="permissions[]"  {{array_search('10', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                    </div><!--End row-->
                </td>
            </tr>

            <tr>  <!-- ============ PromoCodes   ===============  -->
                <td> Promo Code  </td>
                <td>
                    <div class="row ">
                        <div class="col-md-2">
                            {!! Form::label('PromoCodes_main',__('permission.main')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="14" name="permissions[]" {{array_search('14', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('PromoCodes_status',__('permission.status')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="16" name="permissions[]" {{array_search('16', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('PromoCodes_edit',__('permission.edit')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="17" name="permissions[]" {{array_search('17', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('PromoCodes_delete',__('permission.delete')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="18" name="permissions[]" {{array_search('18', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('PromoCodes_create',__('permission.create')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="15" name="permissions[]" {{array_search('15', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                    </div><!--End row-->
                </td>
            </tr>

            <tr>  <!-- ============ HomeAdvertising   ===============  -->
                <td> Home Page  </td>
                <td>
                    <div class="row ">
                        <div class="col-md-2">
                            {!! Form::label('HomePage_main',__('permission.main')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="19" name="permissions[]" {{array_search('19', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('HomePage_status',__('permission.status')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="21" name="permissions[]" {{array_search('21', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {{-- {!! Form::label('HomeAdvertising_edit',__('permission.edit')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="22" name="permissions[]" {{array_search('22', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label> --}}
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('HomePage_delete',__('permission.delete')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="23" name="permissions[]" {{array_search('23', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {{-- {!! Form::label('HomeAdvertising_create',__('permission.create')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="20" name="permissions[]" {{array_search('20', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label> --}}
                        </div>
                    </div><!--End row-->
                </td>
            </tr>

            <tr>  <!-- ============ HomeAdvertising ===============  -->
                <td> Products  </td>
                <td>
                    <div class="row ">
                        <div class="col-md-2">
                            {!! Form::label('Product_main',__('permission.main')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="24" name="permissions[]" {{array_search('24', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Product_status',__('permission.status')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="26" name="permissions[]" {{array_search('26', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Product_edit',__('permission.edit')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="27" name="permissions[]" {{array_search('27', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Product_delete',__('permission.delete')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="28" name="permissions[]" {{array_search('28', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Product_create',__('permission.create')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="25" name="permissions[]"  {{array_search('25', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                    </div><!--End row-->
                </td>
            </tr>

            <tr>  <!-- ============ FreeTestList ===============  -->
                <td> Free Test   </td>
                <td>
                    <div class="row ">
                        <div class="col-md-2">
                            {!! Form::label('FreeTestList_main',__('permission.main')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="33" name="permissions[]" {{array_search('33', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('FreeTestList_status',__('permission.status')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="34" name="permissions[]" {{array_search('34', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('FreeTestList_edit',__('permission.edit')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="35" name="permissions[]" {{array_search('35', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('FreeTestList_delete',__('permission.delete')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="36" name="permissions[]" {{array_search('36', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('FreeTestList_create',__('permission.create')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="37" name="permissions[]"  {{array_search('37', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                    </div><!--End row-->
                </td>
            </tr>

            <tr>  <!-- ============ HomeAdvertising ===============  -->
                <td>  Invoices    </td>
                <td>
                    <div class="row ">
                        <div class="col-md-2">
                            {!! Form::label('Recipt_main',__('permission.main')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="29" name="permissions[]" {{array_search('29', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Recipt_make_piad',__('permission.make_piad')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="30" name="permissions[]" {{array_search('30', $selected_permissions)?'checked':''}} />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-2">
                            {!! Form::label('Recipt_delete',__('permission.delete')) !!} <br>
                            <label class="switch">
                                <input type="checkbox" class="switch" value="31" name="permissions[]" {{array_search('31', $selected_permissions)?'checked':''}}  />
                                <span></span>
                            </label>
                        </div>
                        <div class="col-md-2">

                        </div>
                    </div><!--End row-->
                </td>
            </tr>




        </tbody>
      </table>

      {!! Form::submit(__('permission.update'),['class'=>'btn btn-success ','style'=>'width:100%']) !!}

      <br><br>
    {!! Form::close() !!}

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->




@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
          $('#create_form').validate();
          var checked_status = 'not_checkedAll';

          $('#btn_all').click(function(event) {
              if (checked_status == 'not_checkedAll')
              {
                  $('input[type=checkbox]').prop('checked',true);
                  $(this).removeClass('btn-success');
                  $(this).addClass('btn-danger');
                  checked_status = 'checkedAll';
              }
              else if (checked_status == 'checkedAll')
              {
                  $('input[type=checkbox]').prop('checked',false);
                  $(this).removeClass('btn-danger');
                  $(this).addClass('btn-success');
                  checked_status = 'not_checkedAll';
              }
          });
    </script>
    {{-- <script src="{{asset('js/Country.js')}}"> </script> --}}


@endsection
