

@component('components.panel_default_with_blank')
    @slot('active') Recipt @endslot
    {{-- @slot('page_title') Invoices  @endslot --}}
    @slot('panel_title') Invoices @endslot
    @slot('the_permission') Recipt_main @endslot

    @slot('body')
        <style>
          [v-cloak] { display: none; }
          #delivery_status p
          {
              color: #ccc;
              cursor: pointer;
          }
          #delivery_status p.active
          {
            color: green;
            font-weight: bold;
          }
        </style>
        <div id="myVue">



          {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent' => 'getResults()']) !!}
          <div class="row">
              <div class="col-md-6 pull-right">
                    <label> Search</label>
                    <input type="text" name="search" class="form-control mydirection" placeholder=" search"  >
              </div>
              <div class="col-md-3 pull-right">
                  <label> District </label>
                  <v-select :options="set_searchArr_1" :name="'city_id'"  v-on:s_change="getResults()" :f_item="'All'"></v-select>
              </div>
              <div class="col-md-3 pull-right">
                  <label> Status </label>
                  <v-select :options="set_searchArr_2" :name="'status'"  v-on:s_change="getResults()" :f_item="'All'"></v-select>
              </div>
              {{-- <div class="col-md-3">
                  <label> gender</label>
                  <v-select :options="searchArr_1" :name="'gender'"  v-on:s_change="getResults()" :f_item="'All'"></v-select>
              </div> --}}
          </div><!--End row-->
          {!! Form::close() !!}
         <hr>


            <table class="table mydir">
                <thead>
                    <th> Invoice ID </th>
                    <th> Member  </th>
                    <th> District   </th>
                    <th> Total Price  </th>
                    <th> Discount  </th>
                    <th> Final Price  </th>
                    {{-- <th> Created date  </th> --}}
                    <th>  Issued Date  </th>
                    <th> Status </th>
                    <th> Piad </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      <td>
                        <p v-text="list.member_name"></p>
                        <p v-text="list.member_phone"></p>
                        <p v-text="list.member_email"></p>
                      </td>
                      <td> <p> @{{list.city_name}} </p>  </td>
                      <td> <p> @{{list.total_price}} L.E </p>  </td>
                      <td> <p> @{{list.discount_percentage}}% </p>  </td>
                      <td> <p> @{{list.final_price}} L.E </p>  </td>
                      <td> <p v-text="list.created_at"></p> <span> [@{{diffforhumans(list.created_at)}}] </span>  </td>
                      <td id="delivery_status">
                         <p :class="{'active':list.delivery_status =='processing'}" v-on:click="changeDeliveryStatus(list.id,'processing')">
                           <i :class="['fa' ,{'fa-square-o':list.delivery_status !='processing', 'fa-check-square-o': list.delivery_status == 'processing'} ]"></i> Processing
                         </p>
                         <p :class="{'active':list.delivery_status =='shipping'}" v-on:click="changeDeliveryStatus(list.id,'shipping')">
                           <i :class="['fa' ,{'fa-square-o':list.delivery_status !='shipping', 'fa-check-square-o': list.delivery_status == 'shipping'} ]"></i> Shipping
                         </p>
                         <p :class="{'active':list.delivery_status =='delivered'}" v-on:click="changeDeliveryStatus(list.id,'delivered')">
                           <i :class="['fa' ,{'fa-square-o':list.delivery_status !='delivered', 'fa-check-square-o': list.delivery_status == 'delivered'} ]"></i> Delivered
                         </p>
                         <p :class="{'active':list.delivery_status =='canceled'}" v-on:click="changeDeliveryStatus(list.id,'canceled')">
                           <i :class="['fa' ,{'fa-square-o':list.delivery_status !='canceled', 'fa-check-square-o': list.delivery_status == 'canceled'} ]"></i> Canceled
                         </p>
                      </td>
                      <td>
                         <span class="badge badge-success"  v-if="list.is_piad"> yes </span>
                         <span class="badge badge-danger" v-else> no  </span>
                      </td>

                      <td>
                           @permission('Recipt_make_piad')
                           <button :class="{'btn btn-rounded':true ,'btn-success':list.is_piad ,'btn-danger':!list.is_piad }"  v-on:click="switchPiad(list.id)">
                               <i class="fa fa-dollar"></i>
                           </button>
                           @endpermission
                           <br><br>
                           <a class="btn btn-primary btn-rounded" :href="'{{url('Admin/Recipt')}}/'+list.id" >
                                <i class="fa fa-search"></i>
                           </a>
                           <br><br>
                           @permission('Recipt_delete')
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                           @endpermission
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->


       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/Recipt/delete`;
            var get_list =   `${admin_url}/Recipt/list`;
            var switchPiad_api = `${admin_url}/Recipt/switchPiad`;
            var changeDeliveryStatus_api = `${admin_url}/Recipt/changeDeliveryStatus`;
            var set_searchArr_1 = JSON.parse(`{!!$cities!!}`) ;
            var set_searchArr_2 = [
              { label:'Processing' , value: 'processing' },
              { label:'Shipping' ,  value: 'shipping' },
              { label:'Delivered' , value: 'delivered' },
              { label:'Canceled' ,  value: 'canceled' },
              { label:'Paid' ,  value: 'paid' },
            ] ;
        </script>
        <script src="{{asset('js_admin/Recipt/standard.js')}}"> </script>
    @endslot

@endcomponent
