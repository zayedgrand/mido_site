
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
        <link rel="stylesheet" href="{{asset('css/app.css')}}"  media="all" >

        <style type="text/css">

          header img
          {
             float: left;
             margin: 10px;
             margin-left: 50px;
          }
          header
          {
            padding-top: 10px;
          }
          header .text-warper
          {
              padding-top: 23px;
          }
          header .text-warper p
          {
             margin: 3px;
             font-size: 12px;
             font-weight: bold;
          }

          #red_separator
          {
              position: relative  !important;
          }
          #red_separator .reded
          {
              background-color: #E20A16  !important;
              height: 16px  !important;
              width: 100%  !important;
          }
          #red_separator .text
          {
              font-size: 55px  !important;
              position: absolute  !important;
              top: -80px  !important;
              background-color: white  !important;
              right: 70px   !important;
              padding: 0px 20px  !important;
          }

          /* ----hear------ */

          #info_section .info-left_side
          {
            width: 48%;
            display: inline-block;
          }

          #info_section .info-right_side
          {
            width: 48%;
            display: inline-block;
          }

          #info_section .info-right_side .wraper
          {
            position: relative;
            top: -56px;
            right: -30px;
          }

          #info_section .info-left_side h4
          {
                 margin: 1px;
                font-size: 18px;
          }
          #info_section .info-left_side h3
          {
              margin: 1px;
               font-size: 21px;
          }
          #info_section .info-left_side p
          {
            margin: 5px;
          }

          #info_section .info-left_side p span:first-of-type{
            width: 63px;
            display: inline-block;
          }

         #info_section .info-right_side p
         {
            font-weight: bold;
            margin: 2px;
         }

         /* ----tabel----- */
         .table-invoice table
         {
            border: 1px solid #E7E9EE;
            padding: 0;
            width: 100%;
         }

         .table-invoice thead
         {
           background-color: #4D4D4D;
         }

         .table-invoice thead th
         {
            background-color: #4D4D4D;
            padding: 10px;
            color: white;
         }

         .table-invoice tbody tr:nth-of-type(2n)
         {
            background-color: #E0E0E0;
         }

         .table-invoice tbody td
         {
           text-align: center;
         }

         /* ---total */
         .total_price_container
         {
            display: flex;
            align-items: center;
         }
         .total_price_container .left-side
         {
            width: 50%;
            font-weight: bold;
            color: #363B5F;
            margin-left: 10px;
         }

         .total_price_container .right-side
         {
             width: 50%;
             background-color: #E20A16;
             color: white;
         }

         .total_price_container .right-side table
         {
           width: 85%;
           text-align: center;
         }

         footer p
         {
           color: #363B5F;
           margin: 0;
           font-size: 12px;
         }
       </style>

  </head>
  <body>

    <header>
        <img width="130" src="{{asset('images/assets/logo-for-admin.png')}}" style=" "/>
        <div class="text-warper">

            <p> MIDO International Trading S.A.E </p>
            <p> 8 fathy talaat St. Sheraton area, Heliopolis </p>
            <p> support@mido.com.eg </p>
            <p> ww.mido.com.eg </p>
        </div>
    </header>

    <div style="clear:both"> </div>

    <div id="red_separator">
        <p class="reded"></p>
        <p class="text">INVOICE</p>
    </div>
    <br>

    <div id="info_section">
          <div class="info-left_side">
              <h4> Invoice to: </h4>
              <h3> {{$Recipt->member_name}} </h3>
              <p> <span> Email: </span> <span> {{$Recipt->member_email}} </span> </p>
              <p> <span> Phone: </span> <span> {{$Recipt->member_phone}} </span> </p>
              <p> <span> Address: </span> <span> {{$Recipt->building_no}} {{$Recipt->member_address}} ,  apartment: {{$Recipt->apartment_no}} </span> </p>
          </div><!--End info-left_side-->

          <div class="info-right_side">
              <div class="wraper">
                <p> <span> Invoice: </span> <span> {{$Recipt->id}} </span> </p>
                <p> <span> Date: </span> <span> {{$Recipt->created_at}} </span> </p>
              </div>
          </div><!--End info-left_side-->
    </div>

    <div style="clear:both"> </div>


            {{-- <div class=" " style="position: absolute; top: 59px; right: 10px;">
                <button onclick="printtag('print_area')" class="btn btn-default" ><span class="fa fa-print"></span> Print</button>
            </div> --}}

            <br>

                 <div class="table-invoice">
                     <table class="   ">
                       <thead>
                         <th style="" > </th>
                         <th style="" >Item Description</th>
                         <th style="" class=""> Price</th>
                         <th style="" class="">Qty.</th>
                         <th style="" class="">Total</th>
                       </thead>
                       <tbody>
                           @foreach ($Products as $key => $Item)
                              <tr>
                                  <td>{{ $key+1 }}</td>
                                  <td width="45%">
                                      {{-- <img src="{{$Item->image}}" width="100px"> --}}
                                      <strong>{{$Item->product_name_en}}</strong>
                                      <p> {{$Item->bags_en}} </p>
                                  </td>
                                  <td class="">{{$Item->single_price}} EGP</td>
                                  <td class="">{{$Item->quantity}}</td>
                                  <td class="">{{$Item->total_price}} EGP</td>
                              </tr>
                           @endforeach
                       </tbody>
                     </table>
                 </div>
                 <br>
                 <div class="total_price_container">
                    <div class="left-side">
                        Thank you for shopping with us
                    </div>
                    <div class="right-side">
                      <table>
                        <tr>
                            <th> Sub Total: </th>
                            <td> {{$Recipt->total_price}} EGP </td>
                        </tr>
                        <tr>
                            <th> Tax: </th>
                            <td> {{$Recipt->tax_price}} EGP  </td>
                        </tr>
                        <tr>
                            <th> Shipping: </th>
                            <td> {{$Recipt->shipping_price}} EGP </td>
                        </tr>
                        <tr>
                            <th> Discount: </th>
                            <td> {{$Recipt->discount_percentage}} % </td>
                        </tr>
                        <tr>
                            <th> Total: </th>
                            <td> {{$Recipt->final_price}} EGP </td>
                        </tr>
                      </table>
                    </div>

                 </div><!--End total_price_container-->




                 @if ($FreeTest)
                   <h2> Free test </h2>
                   <table>
                     <tr>
                       <td> <img src="{{$FreeTest->image}}" width="120px"> </td>
                       <td> {{$FreeTest->name_en}} </td>
                     </tr>
                   </table>
                 @endif

<hr>

<footer>
    <p> Your opinion matter, please send us your feedback our service support@mido.com.eg </p>
    <p> All our frozen deliveries are done through -18 refrigerated trucks  </p>
    <p> Your customer support number is 01205455553 </p>
</footer>

             </div>
             <!-- END INVOICE -->



                  <script>

                        window.onload = function(){
                              window.print();
                        };


                     function printtag(tagid)
                     {
                          // var hashid = "#"+ tagid;
                          // var tagname =  $(hashid).prop("tagName").toLowerCase() ;
                          // var attributes = "";
                          // var attrs = document.getElementById(tagid).attributes;
                          //   $.each(attrs,function(i,elem){
                          //     attributes +=  " "+  elem.name+" ='"+elem.value+"' " ;
                          //   })
                          // var divToPrint= $(hashid).html() ;
                          // var head = "<html><head>"+ $("head").html() + "</head>" ;
                          // var allcontent = head + "<body  onload='window.print()' >"+ "<" + tagname + attributes + ">" +  divToPrint + "</" + tagname + ">" +  "</body></html>"  ;
                          // var newWin=window.open('','Print-Window');
                          // newWin.document.open();
                          // newWin.document.write(allcontent);
                          // newWin.document.close();
                          window.print();
                      }
                  </script>


  </body>
</html>
