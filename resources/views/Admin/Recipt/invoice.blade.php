

@component('components.panel_default_with_blank')
    @slot('active') Recipt @endslot
    @slot('panel_title') Invoice @endslot
    @slot('the_permission') Recipt_main @endslot
    @slot('breadcrumb')
        <ul class="breadcrumb">
          <li><a href="{{url('Admin/Recipt')}}">Invoice</a></li>
          <li class="active">view</li>
       </ul>
     @endslot

    @slot('body')
       <style type="text/css">
         @media print
         {
            #CompanySideMemberShip  {display:none;}
            div.page-content {margin-right: 20px; margin-left: 20px;}
            body:not(section#print) {display:none;}
         }
      </style>

        <div class="push-down-10 pull-right">
            <a href="{{url('Admin/Recipt/print/'.$Recipt->id)}}" target="_blank" class="btn btn-default"><span class="fa fa-print">

              </span> Print
            </a>
        </div>
        <section id="print">

         <h2>
            <img src="{{$Recipt->logo}}" width="150px"/>
         </h2>



         <!-- INVOICE -->
         <div class="invoice" id="print_area">

           <h2 style=" ">INVOICE <!--<strong>#Y14-152</strong>--></h2>

             <div class="row ">
                 <div class="col-md-4 col-ms-4 col-xs-2">

                     <div class="invoice-address">
                         <h5>From</h5>
                         <img width="120" src="{{asset('images/assets/logo-for-admin.png')}}" style="background-color: #BB262B;border-radius: 5px;"/>
                     </div>

                 </div>
                 <div class="col-md-4 col-ms-4 col-xs-5">

                     <div class="invoice-address">
                         <h5 >To</h5>
                         <table class="table table-striped table-bordered mydir">
                             <tr>
                                 <td><strong>Customer Id:</strong></td><td class="text-right">{{$Recipt->member_id}}</td>
                             </tr>
                             <tr>
                                 <td><strong>Customer Name:</strong></td><td class="text-right">{{$Recipt->member_name}}</td>
                             </tr>
                             <tr>
                                 <td><strong>Customer Phone:</strong></td><td class="text-right">{{$Recipt->member_phone}}</td>
                             </tr>
                             <tr>
                                 <td><strong>Customer Email:</strong></td><td class="text-right">{{$Recipt->member_email}}</td>
                             </tr>
                             <tr>
                                 <td><strong>  Address:</strong></td><td class="text-right">{{$Recipt->member_address}}</td>
                             </tr>
                             <tr>
                                 <td><strong>  Street:</strong></td><td class="text-right">{{$Recipt->street}}</td>
                             </tr>
                             <tr>
                                 <td><strong>  Building number:</strong></td><td class="text-right">{{$Recipt->building_no}}</td>
                             </tr>
                             <tr>
                                 <td><strong>  Apartment number:</strong></td><td class="text-right">{{$Recipt->apartment_no}}</td>
                             </tr>
                         </table>

                     </div>

                 </div>
                 <div class="col-md-4 col-ms-4 col-xs-5">

                     <div class="invoice-address">
                         <h5 >Invoice</h5>
                         <table class="table table-striped table-bordered mydir">
                             <tr>
                                 <td  >Invoice Number:</td><td class="text-right">{{$Recipt->id}}</td>
                             </tr>
                             <tr>
                                 <td>Invoice Date:</td><td class="text-right">{{$Recipt->created_at}}</td>
                             </tr>
                             <tr>
                                 <td><strong>Total:</strong></td><td class="text-right"><strong>{{$Recipt->total_price}} L.E </strong></td>
                             </tr>
                             <tr>
                                 <td><strong>Discount:</strong></td><td class="text-right"><strong>{{$Recipt->discount_percentage}} % </strong></td>
                             </tr>
                             <tr>
                                 <td><strong>Final price:</strong></td><td class="text-right"><strong>{{$Recipt->final_price}} L.E </strong></td>
                             </tr>
                         </table>

                     </div>

                 </div>
             </div>

             <div class="table-invoice">
                 <table class="table mydir">
                     <tr style=" background: #BB262B; ">
                         <th style="color: white;" >Item Description</th>
                         <th style="color: white;" class="text-center">Unit Price</th>
                         <th style="color: white;" class="text-center">Quantity</th>
                         <th style="color: white;" class="text-center">Total</th>
                     </tr>
                     @foreach ($Products as $Item)
                        <tr>
                            <td width="30%">
                                {{-- <img src="{{$Item->image}}" width="100px"> --}}
                                <strong>{{$Item->product_name_en}}</strong>
                                <p> {{$Item->bags_en}} </p>
                            </td>
                            <td class="text-center">{{$Item->single_price}} L.E</td>
                            <td class="text-center">{{$Item->quantity}}</td>
                            <td class="text-center">{{$Item->total_price}} L.E</td>
                        </tr>
                     @endforeach
                     <tr>
                       <td></td>
                       <td></td>
                       <td> </td>
                       <td>
                         <strong>Total:</strong>
                          <p style="color: #BB262B;"> <b> {{$Recipt->total_price}} L.E </b>  </p>
                       </td>
                     </tr>
                 </table>
             </div>
             <hr>

             @if ($FreeTest)
               <h1> Free test </h1>
               <table >
                 <tr>
                   <td> <img src="{{$FreeTest->image}}" width="220px"> </td>
                   <td> {{$FreeTest->name_en}} </td>
                 </tr>
               </table>
             @endif



         </div>
         <!-- END INVOICE -->
        </section><!--End print-->

     @endslot

    @slot('script')
      <script>
         function printtag(tagid)
         {
              var hashid = "#"+ tagid;
              var tagname =  $(hashid).prop("tagName").toLowerCase() ;
              var attributes = "";
              var attrs = document.getElementById(tagid).attributes;
                $.each(attrs,function(i,elem){
                  attributes +=  " "+  elem.name+" ='"+elem.value+"' " ;
                })
              var divToPrint= $(hashid).html() ;
              var head = "<html><head>"+ $("head").html() + "</head>" ;
              var allcontent = head + "<body  onload='window.print()' >"+ "<" + tagname + attributes + ">" +  divToPrint + "</" + tagname + ">" +  "</body></html>"  ;
              var newWin=window.open('','Print-Window');
              newWin.document.open();
              newWin.document.write(allcontent);
              newWin.document.close();
          }
      </script>
    @endslot

    @endcomponent
