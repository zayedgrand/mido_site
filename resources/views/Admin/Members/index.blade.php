

@component('components.panel_default_with_blank')
    @slot('active') Members @endslot
    {{-- @slot('page_title') Users  @endslot --}}
    @slot('panel_title') Users @endslot

    @slot('body')
        <style>   [v-cloak] { display: none; }   </style>
        <div id="myVue">
          {{-- <button class="btn btn-primary btn-rounded" v-on:click="showCreateModel()">
                 <i class="fa fa-plus mydir"></i> Create
          </button>
          <br> --}}

          {!! Form::open( [ 'id'=>'search_form' ,'v-on:submit.prevent' => 'getResults()']) !!}
          <div class="row">
              <div class="col-md-6 pull-right">
                    <label> Search</label>
                    <input type="text" name="search" class="form-control mydirection" placeholder=" search"  >
              </div>
              <div class="col-md-3">
                  <label> District </label>
                  <v-select :options="searchArr_2" :name="'city_id'"  v-on:s_change="getResults()" :f_item="'All'"></v-select>
              </div>
              <div class="col-md-3">
                  <label> Gender</label>
                  <v-select :options="searchArr_1" :name="'gender'"  v-on:s_change="getResults()" :f_item="'All'"></v-select>
              </div>
          </div><!--End row-->
          {!! Form::close() !!}
         <hr>


            <table class="table mydir">
                <thead>
                    <th> ID  </th>
                    <th> Name   </th>
                    <th> Email  </th>
                    <th> Gender  </th>
                    <th> Phone  </th>
                    <th> Birth Date  </th>
                    <th> Address  </th>
                    <th> District   </th>
                    <th> More </th>
                </thead>
                <tbody is="transition-group" name="my-list" v-cloak >
                  <tr v-for="(list,index) in mainList.data" :key="list.id" >
                      <td> <p v-text="list.id"></p>  </td>
                      <td> <p v-text="list.name"></p>  </td>
                      <td> <p v-text="list.email"></p>  </td>
                      <td> <p v-text="list.gender"></p>  </td>
                      <td> <p v-text="list.phone"></p>  </td>
                      <td> <p v-text="list.birthdate"></p>  </td>
                      <td width="20%">
                         <p v-text=""></p>
                         <ul v-if="list.all_address">
                           <li  v-for="addr in list.all_address.split(',')"> @{{addr}} </li>
                         </ul>
                      </td>
                      <td> <p v-text="list.city_name"></p>  </td>
                      <td>
                           <button type="button" class="btn btn-danger btn-rounded" v-on:click="DeleteMessage(list.id,index)" >
                              <i class="glyphicon glyphicon-trash"></i>
                           </button>
                      </td><!--end more-->
                  </tr>

                </tbody>
            </table>

            <!-- - - - - - -START paginate- - - - - - - -->
            <div class="row">
                  <div class="col-md-8 col-md-offset-5">
                        <pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
                            <span slot="prev-nav">&lt; prev </span>
                            <span slot="next-nav"> next &gt;</span>
                        </pagination>
                  </div>
            </div><!--End row-->
            <!-- - - - - - -End paginate- - - - - - - -->
            <br>
            <!-- - - - - - -START spinner- - - - - - - -->
            <spinner2 v-if="show_spinner"></spinner2>
            <!-- - - - - - -End spinner- - - - - - - -->

            {{-- ------------------- create ------------------------ --}}

                @component('components.modal')
                    @slot('id')
                      create_model
                    @endslot
                    @slot('header')
                         Add new
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>"mydirection",'id'=>'create_form' ,'v-on:submit.prevent'=>'create()' ]) !!}
                    @endslot
                    @slot('body')

                        {!! Form::label('name_en','Name en') !!}
                        {!! Form::text('name_en',null,['class'=>'form-control','required' ]) !!}

                        {!! Form::label('name_ar','Name ar') !!}
                        {!! Form::text('name_ar',null,['class'=>'form-control','required' ]) !!}

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" >  Add new </button>
                    @endslot
                @endcomponent

            {{-- ------------------- edit ------------------------ --}}

                @component('components.modal')
                    @slot('id')
                      edit_model
                    @endslot
                    @slot('header')
                          edit
                    @endslot
                    @slot('form_header')
                        {!! Form::open([ 'class'=>" ",'id'=>'edit_form' ,'v-on:submit.prevent'=>'edit()'   ]) !!}
                    @endslot
                    @slot('body')

                      {!! Form::hidden('id',null,['id'=>'edit_id','v-model'=>'EF.id']) !!}

                            <div class="form-group">
                                {!! Form::label('name_en','Name en') !!}
                                {!! Form::text('name_en',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_en']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('name_ar','Name ar') !!}
                                {!! Form::text('name_ar',null,['class'=>'form-control','required' ,'v-model'=>'EF.name_ar']) !!}
                            </div>

                      <!-- - - - - - -START spinner- - - - - - - -->
                      <spinner3 v-if="action_spinner"></spinner3>
                      <!-- - - - - - -End spinner- - - - - - - -->

                    @endslot
                    @slot('submit_input')
                      <button type="submit" class="btn btn-success" :disabled="btn_submit" > Update </button>
                    @endslot
                @endcomponent


       </div><!--End myVue-->


    @endslot

    @slot('script')
        <script>
            var delete_api =   `${admin_url}/Members/delete`;
            var get_list =   `${admin_url}/Members/list`;
            var showORhide_api = `${admin_url}/Members/showORhide`;
            var create_api = `${admin_url}/Members/create`;
            var update_api = `${admin_url}/Members/update`;
            var set_searchArr_1 = [{label:'Male',value:'male'},{label:'Female',value:'female'}] ;
            var set_searchArr_2 = JSON.parse(`{!!$cities!!}`) ;
        </script>
        <script src="{{asset('js_admin/standard.js')}}"> </script>
    @endslot

@endcomponent
