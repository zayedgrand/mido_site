@component('mail::message')


  Hello {{$Member->name}},
  We're sorry you're having trouble logging into your Mido account.
  We're happy to be of service, just click on the forget <br> my password link.
    reset password



@component('mail::button', ['url' => url('member/resite_password_form/'.$Member->forget_password)])
  click for reset password
@endcomponent

Thanks,<br>
Horreia
@endcomponent
