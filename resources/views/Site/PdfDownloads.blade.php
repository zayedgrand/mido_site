@extends('Site.layout.blank')
@section('content')
	<style>
		.grid-3 {grid-column-gap: 35px;grid-row-gap: 20px;}
		.files {margin:20px 0}
		.files > * {border: 1px solid #e6e6e6;}
		.files b {padding:10px}
		.files img {padding-left:5px}
	</style>
	<article id="download-page" class="main-page grid grid-1">
		<div class="container"> 
			<img style="width:100%;display:block" src="{{$banner}}"/>
			<section>
				<div class="grid grid-3 files">

					@foreach ($PdfDownloads as $key => $PdfDownload)
						<div class="flex flex-between flex-middle">
							<b>{{$PdfDownload->name}}</b>
							<div>
								<a href="{{$PdfDownload->pdf}}" download>
									<img style="width:50px;display:block" src="{{asset('site_assets/images/pdf.png')}}"/>
								</a>
							</div>
						</div><!--End flex flex-between flex-middle-->
					@endforeach

				</div>
			</section>
		</div>
	<article>
@endsection


@section('script')
<script>

</script>

@endsection
