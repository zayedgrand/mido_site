@extends('Site.layout.blank')
@section('content')
	<style>   [v-cloak] { display: none; }   </style>

  <div id="historyList">

		<article id="history-page" class="main-page grid grid-1">
			<div class="container">

				<form id="search_form" class="flex flex-between flex-middle wishlist-border flex-wrap">
						{{csrf_field()}}
					<h1 class="cap"> @lang('Design.orders_history')</h1>
					<div class="history-filter flex flex-wrap">

									<div class="dropdown-hover">
										<span class="dropdown-hover-head flex flex-between flex-middle main-btn">
												<span v-if="!status"> @lang('page.status') </span>
												<span v-else> @{{status | capitalize }} </span>
										</span>
										<div class="dropdown-hover-body">
											<ul class="dropdown-hover-body-content">
												<li class="active" v-on:click="statusChanged('')" >@lang('page.All')</li>
												<li v-on:click="statusChanged('processing')" > @lang('page.processing') </li>
												<li v-on:click="statusChanged('shipping')">    @lang('page.shipping')   </li>
												<li v-on:click="statusChanged('delivered')">   @lang('page.delivered')  </li>
												<li v-on:click="statusChanged('canceled')">    @lang('page.canceled')   </li>
											</ul>
										</div>
									</div><!--End dropdown-hover-->

									<div class="dropdown-hover">
										<span class="dropdown-hover-head flex flex-between flex-middle main-btn">
												<span v-if="!selectedDate"> @lang('page.All Dates') </span>
												<span v-else> @lang('page.past 3 months') </span>
										</span>
										<div class="dropdown-hover-body">
											<ul class="dropdown-hover-body-content">
												<li class="active" v-on:click="dateChanged('')" >@lang('page.All')</li>
												<li v-on:click="dateChanged('past_3')" >@lang('page.past 3 months')</li>
											</ul>
										</div>
									</div><!--End dropdown-hover-->

						 	 </div><!--End flex-->
				</form><!--End form flex flex-between wishlist-border-->


				<div v-for="(list,index) in mainList.data" v-cloak class="wishlist-product flex flex-between flex-wrap wishlist-border">
					<div>
						<div class="date">
							<span class="bold">@lang('Design.order_place_on'):</span>
							<span> @{{diffforhumans(list.created_at)}} </span>
						</div>
						<div class="order_id">
							<span class="bold">@lang('page.Order ID'):</span>
							<span> @{{list.id}} </span>
						</div>
						<div class="total_before">
							<span class="bold">@lang('page.Sub Total price'):</span>
							<span>@{{list.total_price}} @lang('page.EGP')</span>
						</div>
						<div class="discount">
							<span class="bold">@lang('page.Discount'):</span>
							<span>@{{list.discount_percentage}}%</span>
						</div>
						<div class="total_after">
							<span class="bold">@lang('page.Final price'):</span>
							<span> @{{list.final_price}} @lang('page.EGP')</span>
						</div>
						<div class="status">
							<span class="bold">@lang('page.status'):</span>
							 <span v-if="list.delivery_status=='processing'">     @lang('page.processing') </span>
							 <span v-else-if="list.delivery_status=='shipping'">  @lang('page.shipping')   </span>
							 <span v-else-if="list.delivery_status=='delivered'"> @lang('page.delivered')  </span>
							 <span v-else-if="list.delivery_status=='canceled'">  @lang('page.canceled')   </span>
						</div>
						<div class="status">
							<span class="bold">@lang('page.Is piad'):</span>
							<span v-if="list.is_piad"> @lang('page.yes') </span>
							<span v-else> @lang('page.no') </span>
						</div>
						<div class="address">
							<span class="bold">@lang('page.Address'):</span>
							<span> @{{list.address}} </span>
						</div>
					</div>
					<div>
						<a :href="'{{url('History/details')}}/'+list.id" class="flex flex-between main-btn cap"><span>@lang('page.Order Details')</span></a>
						<br>
						<button v-if="list.delivery_status=='processing'" type="button" class="flex flex-between main-btn cap" v-on:click="cancle_order(list)" > <span>@lang('page.cancele order')</span> </button>
						<button v-else-if="list.delivery_status=='canceled'" type="button" class="flex flex-between main-btn no"   > <span>@lang('page.order canceled')</span> </button>
						<button v-else type="button" class="flex flex-between main-btn cap no"  v-on:click="cant_cancle_order()"  > <span>@lang('page.cant cancele order')</span> </button>
					</div>
				</div><!--End wishlist-product flex flex-between flex-middle flex-wrap wishlist-border-->

				<!-- - - - - - -START paginate- - - - - - - -->
				<div class="row">
							<div class="col-md-8 col-md-offset-5">
										<pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
												<span slot="prev-nav">&lt; prev </span>
												<span slot="next-nav"> next &gt;</span>
										</pagination>
							</div>
				</div><!--End row-->
				<!-- - - - - - -End paginate- - - - - - - -->
				<br>
				<!-- - - - - - -START spinner- - - - - - - -->
				<spinner2 v-if="show_spinner"></spinner2>
				<!-- - - - - - -End spinner- - - - - - - -->

			</div><!--End row-->
		<article>
  </div><!--End container-->
@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	});
	//===========================================================================

	let historyList = new Vue({
		el: '#historyList',
		data:{
				mainList: {data:[]},
				show_spinner:true,
				status: null,
				selectedDate: null,
		},
		mounted()
		{
				this.getResults();
		},
		methods:{
				getResults(page = 1)
				{
						 this.mainList = {data:[]};
						 this.show_spinner = true;
						 var the_data = {
							 _token: csrf_token ,
							 status: this.status ,
							 selectedDate: this.selectedDate
						 };

						 $.post( `${site_url}/History/list?page=${page}`,the_data,(Response)=>{
								 historyList.mainList = Response;
								 historyList.show_spinner = false;
						 });
				},
				diffforhumans(data)
				{
						moment.locale(site_lang);
						if (data) {
							return  moment(data).fromNow();
						}
				},
				statusChanged(status)
				{
					  this.status = status;
						this.getResults();
				},
				dateChanged(date)
				{
						this.selectedDate = date;
						this.getResults();
				},
				cancle_order(list)
				{
							$.get( `${site_url}/History/cancle_order/${list.id}`,(Response)=>{
									list.delivery_status='canceled';
									new Noty({text: lacal('order cancled') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
							});
				},
				cant_cancle_order()
				{
							new Noty({text: lacal('order already canceled') ,layout: 'topRight', type: 'error',timeout: 2000 }).show();
				}
		},//End methods
		filters:
		{
			  capitalize: function (value)
				{
			    if (!value) return ''
			    value = value.toString()
			    return value.charAt(0).toUpperCase() + value.slice(1)
			  }
		}

	});//end vue
</script>

@endsection
