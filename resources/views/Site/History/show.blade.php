@extends('Site.layout.blank')
@section('content')

		<article id="history-inner-page" class="main-page grid grid-1">
			<div class="container">
				<div class="flex flex-between wishlist-border">
					<h1 class="cap">@lang('page.order') #{{$Recipt->id}} @lang('page.details')</h1>
				</div>
				<div class="wishlist-product flex flex-between flex-middle flex-wrap wishlist-border">
					<div>
						<div class="date">
							<span class="bold">@lang('page.Order Placed On'):</span>
							<span>{{$Recipt->created_at->diffForHumans()}}</span>
						</div>
						<div class="order_id">
							<span class="bold">@lang('page.Order ID'):</span>
							<span>{{$Recipt->id}}</span>
						</div>
						<div class="total_before">
							<span class="bold">@lang('page.Sub Total'):</span>
							<span> {{$Recipt->total_price}} @lang('page.EGP')</span>
						</div>
						<div class="discount">
							<span class="bold">@lang('page.Discount'):</span>
							<span>{{$Recipt->discount_percentage}}%</span>
						</div>
						<div class="total_after">
							<span class="bold">@lang('page.Total'):</span>
							<span>{{$Recipt->final_price}} @lang('page.EGP')</span>
						</div>
						<div class="status">
							<span class="bold">@lang('page.status'):</span>
							<span> @lang('page.'.$Recipt->delivery_status) </span>
						</div>
						<div class="status">
							<span class="bold">@lang('page.Is piad'):</span>
							<span>
									@if ($Recipt->is_piad) @lang('page.yes') @else @lang('page.no') 	@endif
							</span>
						</div>
						<div class="address">
							<span class="bold">@lang('page.Address'):</span>
							<span> {{$Recipt->address}} </span>
						</div>
					</div>
				</div>
				<div class="wishlist-product flex flex-between flex-middle flex-wrap wishlist-border">
					<div class="grid grid-2 w-100 history-grid">
						@foreach ($ReciptProducts as $key => $Product)
							<div class="grid grid-2">
								@if ($Product->id_or_hash =='#')
									<a href="#">
								@else
									<a href="{{$Product->path()}}">
								@endif

									@if ($Product->image)
										<img src="{{$Product->image}}" class="w-100 product-background" width="30" class="w-100 text-center" />
									@else
										<img src="{{asset('site_assets/images/dummy-product.png')}}" class="w-100 product-background" width="30" class="w-100 text-center" />
									@endif

								</a>
								<div class="flex flex-middle">
									<div>
										<div class="history-title">
											<span>{{$Product->product_name}}</span>
										</div>
										<div class="history-details">
											<span>{{$Product->short_description}}</span>
										</div>
										<div class="history-qty">
											<span class="bold">@lang('page.single price'):</span>
											<span>{{$Product->single_price}}</span>
										</div>
										<div class="history-qty">
											<span class="bold">@lang('page.Qty'):</span>
											<span>{{$Product->quantity}}</span>
										</div>
										<div class="history-price">
											<span class="bold">@lang('page.Total Price'):</span>
											<span>{{$Product->total_price}} @lang('page.EGP')</span>
										</div>
									</div>
								</div>
							</div><!--End grid grid-2-->
						@endforeach

					</div>
				</div>
			</div>

			 {{$ReciptProducts->links()}}

		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	})
</script>

@endsection
