@extends('Site.layout.blank')
@section('content')
	<style>
		[v-cloak] { display: none; }
		.main-btn {padding: 11px 19px;border:0}
	</style>

		<div id="root">


		<article id="checkout-page" class="main-page grid grid-1">
			<div class="container">
				<div class="checkout-row grid grid-7 grid-1-2-1-1-1-1 title-container">
					<span>1- @lang('Design.my_cart')</span>
					<span></span>
					<span>@lang('Design.price')</span>
					<span>@lang('Design.qty')</span>
					<span>@lang('page.total')</span>
					<span></span>
				</div><!-- End checkout-row grid grid-7 grid-1-2-1-1-1-1 title-container -->

				<!--products in cart-->
				<div v-for="(row,index) in mainList.data" :key="row.cc" class="checkout-row grid grid-7 grid-1-2-1-1-1-1 product-container">
					<div class="flex flex-middle">
						<a :href="datails_url(row)" class="product-background">
							<img width="75" :src="row.image"/>
						</a>
					</div>
					<div class="flex flex-middle">
						<div>
							{{-- <a :href="'{{url('productsOf/details')}}/'+row.id" class="product-name">@{{row.name}}</a> --}}
							<a :href="datails_url(row)" class="product-name">@{{row.name}}</a>
							<div class="product-size">@{{row.short_description}}</div>
						</div>
					</div>
					<span class="product-price flex flex-middle"><label class="responsive bold ">Price: </label>@{{row.price}} @lang('Design.pound') </span>
					<span class="product-qty flex flex-middle">


							<div v-if="row.quantity > 0" class="flex flex-middle" >

										<span class="qty-control flex flex-middle">
											<button class="minus flex flex-middle flex-center"  v-on:click="decreaseQuantity(row)"   >
												<i class="fas fa-minus"></i>
											</button>
											<input type="number"  v-model="row.in_card_quantity" class="text-center" min="0" disabled />
											<button class="plus flex flex-middle flex-center" v-on:click="increaseQuantity(row)">
												<i class="fas fa-plus"></i>
											</button>
										</span>

								</div>


					</span>
					<span class="product-total flex flex-middle"><label class="responsive bold ">Total: </label>@{{(parseInt(row.in_card_quantity) * parseFloat(row.price)).toFixed(2)}}  @lang('Design.pound')</span>
					<span class="product-delete flex flex-middle flex-center">
						<img v-on:click="delRow(row.id,index)" class="delete-icon" width="30" class="w-100 text-center" src="{{asset('site_assets/images/error22.svg')}}"/>
					</span>
				</div><!--End checkout-row grid grid-7 grid-1-2-1-1-1-1 product-container-->


				<!-- - - - - - -START paginate- - - - - - - -->
				<div class="row">
							<div class="col-md-8 col-md-offset-5">
										<pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
												<span slot="prev-nav">&lt; prev </span>
												<span slot="next-nav"> next &gt;</span>
										</pagination>
							</div>
				</div><!--End row-->
				<!-- - - - - - -End paginate- - - - - - - -->
				<br>
				<!-- - - - - - -START spinner- - - - - - - -->
				<spinner2 v-if="show_spinner"></spinner2>
				<!-- - - - - - -End spinner- - - - - - - -->


				<!--products in cart-->
				<form method="post" action="{{url('checkout')}}" id="main_form" v-on:submit.prevent="" >
					{{csrf_field()}}
					<div class="checkout-row payment-container">
						<span class="bold cap">2- @lang('Design.payment')</span>
						<div>
							<input type="radio" name="payment" value="cod" checked="checked" id="radio_cash_on_delivery"/>
							<label for="radio_cash_on_delivery">@lang('Design.cash_on_delivery')</label>
							<input type="radio" name="payment" value="creadit_card" id="radio_credit_card" disabled  />
							<label for="radio_credit_card">@lang('Design.credit_card')</label>
						</div>
					</div>
					<div class="checkout-row address-container">
						<span class="bold cap">3- @lang('page.address')</span>

						<!--...............START Address................-->
							<div v-for="(addr,indx) in address" >
								<input type="radio" name="address" class="inp_address" :value="addr.id" checked="checked"/>
								<label>@{{addr.address}}</label>
							</div>
						<!--...............END Address................-->

							<button v-on:click="show_form_of_add_new_address=true" type="button" class="main-btn"> @lang('page.Add new address')  </button>
							<div v-show="show_form_of_add_new_address">
								<br>
								{{-- <textarea col="50" class="w-100" id="inp_add_new_address" placeholder="@lang('page.please add Street name/Building number/Apartment number')" > </textarea> --}}

								<input type="text"  class="w-100" id="inp_add_new_address"  required placeholder="@lang('Design.address')"/>

								<div class="flex flex-between" style="margin-top:20px">
									<input type="text" placeholder="@lang('Design.Street name')"  class="w-30 street"   required  />
									<input type="text" placeholder="@lang('Design.Building no')"  class="w-30 building_no"  required  />
									<input type="text" placeholder="@lang('Design.Apartment no')" class="w-30 apartment_no"  required  />
								</div>

 								<button type="button" v-on:click="addNewAddress()" class="main-btn" style="margin-top:20px" >@lang('page.submit address')</button>

							</div><!--End show_form_of_add_new_address-->

					</div>

					<div class="checkout-row promo-container">
						<div class="title bold">4- @lang('Design.promo_code')</div>

							<input type="text" id="inp_code" name="code"></input>
							<button type="button" class="main-btn" v-on:click="addCode()" >@lang('Design.submit_promo_code')</button>

					</div>

					<div v-if="subTotal >= allowed_minimum_freeTast_amount" >

								<div class="note">
									<p class="flex flex-middle" style="line-height:10px"><div><span style="margin-bottom:10px">@lang('page.Congratulation')!</span></div> @lang('page.you_can_test_1')  @{{allowed_minimum_freeTast_amount}} @lang('page.you_can_test_2')  </p>
								</div>

								<div  class="grid grid-6 products-grid taste-grid">
									@foreach ($FreeTestList as $key => $FreeTest)
										<div class="flex flex-center product" v-on:click="choose_freeTast('{{$FreeTest->id}}')">
											<div class="w-100">
												<div href="#">
													<div class="flex flex-middle" style="padding:20px"><img src="{{$FreeTest->image}}"/></div>
													<div class="text-center details" style="margin:5px;margin-bottom:10px">{{$FreeTest->name}}</div>
													<div class="text-center details" style="margin:5px;margin-bottom:10px">
															<input type="radio" name="free_taste" id="radio_free_taste" value="{{$FreeTest->id}}" required />
													</div>
												</div>
											</div>
										</div><!--End flex flex-center product-->
									@endforeach
								</div><!--End grid grid-6 products-grid taste-grid-->

				 </div><!--End v-if="Total >= allowed_minimum_basket_amount"-->
				 <div v-else class="note">
					 	<p class="flex flex-middle">@lang('Design.maximize_message_1') @{{allowed_minimum_freeTast_amount}}@lang('Design.maximize_message_2')</p>
				 </div><!--End else-->


					<div class="flex flex-right total">
						<div class="total-table w-20">
							<div>
								<div class="flex flex-between">
									<span class="cap">@lang('Design.subtotal')</span>
									<span>@{{subTotal}} @lang('Design.pound')</span>
								</div>
								<div class="flex flex-between">
									<span class="cap">@lang('page.shipping')</span>
									{{-- <span>@{{shipping_price}} @lang('Design.pound')</span> --}}
									<span>@{{shipping_price}} @lang('Design.pound')</span>
								</div>
								<div class="flex flex-between">
									<span class="cap">@lang('page.tax')</span>
									<span>@{{tax_price}}  %</span>
								</div>
								<div class="flex flex-between">
									<span class="cap">@lang('Design.discount')</span>
									<span>@{{promo_discount_percentage}} %</span>
								</div>
							</div>
							<hr/>
							<div>
								<div class="flex flex-between">
									<span class="cap bold">@lang('Design.total')</span>
									<span class="bold">@{{Total}} @lang('Design.pound')</span>
								</div>
							</div>
							<button v-if="subTotal >= allowed_minimum_basket_amount" class="main-btn w-100 upper" type="submit" v-on:click="do_submit()" >checkout</button>
							<button v-else class="main-btn w-100 cap" type="submit" disabled > @lang('page.Minimum Amount') @{{allowed_minimum_basket_amount}} @lang('page.EGP')</button>
						</div>
					</div>
				</form>

				<div id="promoCodeArea"> </div>



			</div>
		<article>

 </div><!--End root-->

@endsection


@section('script')
<script>
	// $(function(){
	// 	$('.dropdown-click-head').on('click',function(){
	// 		if($(this).hasClass('active')){
	// 			$(this).next('.dropdown-click-body').slideUp();
	// 			$(this).removeClass('active');
	// 			//---------------------------------------
	// 			$(this).find('i').toggleClass('fa-minus fa-plus');
	// 		}else{
	// 			$('.dropdown-click-head').removeClass('active');
	// 			$('.dropdown-click-body').slideUp();
	// 			$(this).addClass('active');
	// 			$(this).next('.dropdown-click-body').slideDown();
	// 			//---------------------------------------
	// 			$('.dropdown-click-head').find('.fa-minus').toggleClass('fa-minus fa-plus');
	// 			$(this).find('i').toggleClass('fa-minus fa-plus');
	// 		}
	//
	// 		var faqs_height = $('#FAQs > div').innerHeight();
	// 	});
	// });

	//============================================================================

	let get_address = JSON.parse(escapeSpecialChars('{!!$address!!}'));
	let get_promo_discount_percentage = '{{$promo_discount_percentage}}';
	let get_shipping_price = '{{$site_settings['shipping_price']}}';
    let get_tax_price = '{{$site_settings['tax_price']}}';
    let code_page_url = '{{url('promo/add_promo')}}';
	let get_minimum_basket_amount = parseFloat('{{$site_settings['minimum_basket_amount']}}');
	let get_minimum_freeTast_amount = parseFloat('{{$site_settings['minimum_freeTast_amount']}}');
	let submit_promo_code_url = '{{url('promo/add')}}';

  let root = new Vue({
    el: '#root',
    data:{
      mainList: {data:[]},
      show_spinner: false,
      category_ids: [],
		  promo_discount_percentage: get_promo_discount_percentage,
	 	  shipping_price: get_shipping_price,
		  tax_price: get_tax_price,
			allowed_minimum_basket_amount: get_minimum_basket_amount,
			allowed_minimum_freeTast_amount: get_minimum_freeTast_amount,
			show_form_of_add_new_address: false,
			address: get_address,
			choosen_freeTast: null
    },
    mounted()
    {
          this.getResults();
    },
    methods:{
       getResults(page = 1)
       {
          this.mainList = {data:[]};
          this.show_spinner = true;

           $.get(`${site_url}/ShoppingCart/list?page=${page}` ,(Response)=>{console.log(Response);
               root.mainList = Response;
               root.show_spinner = false;
           });
        },
        increaseQuantity(list)
        {
            list.in_card_quantity++;
            var the_data = {
              product_id: list.product_id ,
              quantity: list.in_card_quantity,
              _token: csrf_token ,
            };
            $.post(`${site_url}/ShoppingCart/add`,the_data,(response)=>{
							if(response.status == 'success')
							{
									if(response.case == 'added')
									{
											if(list.in_card_quantity == 0) {  //if not in cart
													new Noty({text: lacal('Added to cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
											}
											else { //if in cart
													new Noty({text: lacal('prouduct increased in the cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
											}
											list.in_card = 1;
									}
									else // Max in Stock
									{
										 new Noty({text: lacal('Max in Stock') , layout: 'topRight', type: 'error',timeout: 2000 }).show();
									}
							}//End if
							else { //problem
								 new Noty({text: 'problem try agin', layout: 'topRight', type: 'error',timeout: 2000 }).show();
							}
							list.in_card_quantity = response.quantity;
            });
        },
        decreaseQuantity(list)
        {
            if(list.in_card_quantity > 1)
            {
                list.in_card_quantity--;
                var the_data = {
                  product_id: list.product_id ,
                  quantity: list.in_card_quantity,
                  _token: csrf_token ,
                }
								$.post(`${site_url}/ShoppingCart/add`,the_data,(response)=>{
									if(response.status == 'success')
									{
											if(response.case == 'added')
											{
													if(list.in_card_quantity == 0) {  //if not in cart
															new Noty({text: lacal('Added to cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
													}
													else { //if in cart
															new Noty({text: lacal('prouduct increased in the cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
													}
													list.in_card = 1;
											}
											else // Max in Stock
											{
												 new Noty({text: lacal('Max in Stock') , layout: 'topRight', type: 'error',timeout: 2000 }).show();
											}
									}//End if
									else { //problem
										 new Noty({text: 'problem try agin', layout: 'topRight', type: 'error',timeout: 2000 }).show();
									}
									list.in_card_quantity = response.quantity;
		            });
            }
        },

        delRow(id,index)
        {
            $.get(`${site_url}/ShoppingCart/remove/${id}` ,(Response)=>{
                if(Response.status == 'success')
                {
                    root.mainList.data.splice(index,1);
										navShoppingCart.getCountNumber();
                    new Noty({text: lacal('product has deleted from the cart'), layout: 'topRight', type: 'success',timeout: 2000  }).show();
                }
            });
        },
		addCode()
		{
				if($('#inp_code').val())
				{
					let promoCode_form_html = `
					        <form id="form_promoCode"  action="${submit_promo_code_url}" method="post">
											<input type="hidden" name="_token" value="${csrf_token}">
											<input type="hidden" name="code" value="${$('#inp_code').val()}">
									</form> `;
						$('#promoCodeArea').html('');
						$('#promoCodeArea').append(promoCode_form_html);
						$('button').prop('disabled', true);
						$('form#form_promoCode').submit();
				}
		},
		addNewAddress()
		{
			var get_new_address = $('#inp_add_new_address').val();
			var get_street = $('input.street').val();
			var get_building_no = $('input.building_no').val();
			var get_apartment_no = $('input.apartment_no').val();

			if( get_new_address && get_street && get_building_no && get_apartment_no)
			{
				var the_data = {
                  address: get_new_address,
                  street: get_street,
                  building_no: get_building_no,
                  apartment_no: get_apartment_no,
                  _token: csrf_token ,
                }
				$.post(`${site_url}/Auth/add_new_address`,the_data,(response)=>{

					$('#inp_add_new_address').val('');
					root.address = response.data;
					root.show_form_of_add_new_address = false;
					new Noty({text: lacal('new address is added') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
				});
			}
			else{
				new Noty({text: lacal('please fill the address input') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
			}

		},
		choose_freeTast(id)
		{
			this.choosen_freeTast = id;
		},
		do_submit()
		{
				//have free tast
				 if( this.Total >= this.allowed_minimum_freeTast_amount )
				 {
						 	if(this.choosen_freeTast)
							{
								  $('#main_form').submit();
							}
							else {
								new Noty({text: 'choose free tast product',layout: 'topRight', type: 'success',timeout: 2000 }).show();
							}
				 }
				 else {
				 			$('#main_form').submit();
				 }
		},
		datails_url(product)
		{
				var  link_url =  site_url +'/product/'+
							 product.id +'-' +
							 product.name.trim()
													 .replace(/\n/g, "").replace(/\r/g, "")
													 .replace(/\t/g, "").replace(/\f/g, "")
													 .replace(' ', "-").replace('/', "-")
													 .split(" ").join('-');
				return link_url;
		},

    },//End methods
    computed:{
			subTotal()
			{
					var total = 0;
					for (var i = 0; i < this.mainList.data.length; i++)
					{
						total += ( parseInt(this.mainList.data[i].in_card_quantity) * parseFloat(this.mainList.data[i].price) );
					}
					return total.toFixed(2);
			},
			Total()
			{
					var total = 0;
					for (var i = 0; i < this.mainList.data.length; i++)
					{
						total += ( parseInt(this.mainList.data[i].in_card_quantity) * parseFloat(this.mainList.data[i].price) );
					}
					//--promo code
					total = total - ( (total * this.promo_discount_percentage) / 100 );
					//--Shipping
					total = total + parseFloat(this.shipping_price);
					//--tax
					total = total + ( (total * parseFloat(this.tax_price)) / 100 );
					return total.toFixed(2);
			},
    }//End computed
  });

	$(document).on('click','.taste-grid .product',function(){
		$('.taste-grid .product').prop('checked','');
		$(this).find('input').prop('checked',true)
	});

</script>




@endsection
