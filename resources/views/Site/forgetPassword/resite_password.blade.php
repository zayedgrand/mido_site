@extends('Design.layout.blank')
@section('content')
		<style>
			header, footer {display:none}
			.main-page {padding-top:0}
			form p {    text-align: center;
    line-height: 27px;
    margin: 17px 0;}
		.back-to {margin-top:8px}
		</style>
		<article id="login-page" class="login-page grid grid-1">
			<div class="flex flex-center">
				<div>
					<a href="{{url('')}}">
						<img class="logo" width="250" class="w-100 text-center" src="{{asset('site_assets/images/mido_logo.png')}}"/>
					</a>
				</div>
			</div>
			<div class="form-container flex flex-center">
				<form id="ForgetPassword" method="post" action="{{url('member/reset_password')}}" >
					{{csrf_field()}}
					<h1 class="flex flex-center cap">reset password</h1>
					<div class="field-container w-100">
						<input type="password" placeholder="New Password" class="w-100" required minlength="6" id="fpassword" name="password" name="password"  />
					</div>
					<div class="field-container w-100">
						<input type="password" placeholder="Confirm New Password" class="w-100" required minlength="6" id="fpassword_again" name="password_again" />
						<input type="hidden" name="member_id" value="{{$Member->id}}">
						<input type="hidden" name="forget_password" value="{{$Member->forget_password}}">
					</div>
					<div><button type="submit" class="cap main-btn w-100">Send</button></div>
					<div class="flex flex-right"><a href="{{url('Auth/login')}}" class="back-to">Back to Sign In</a></div>
				</form>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	});

	//==========================================
	$('form#ForgetPassword').validate({
		rules: {
				password_again: {
					equalTo: "#fpassword"
				}}
	});

	@if ( \App::getLocale() =='ar' )
			jQuery.extend(jQuery.validator.messages, {
						required: "هذا الحقل إلزامي",
						remote: "يرجى تصحيح هذا الحقل للمتابعة",
						email: "رجاء إدخال عنوان بريد إلكتروني صحيح",
						url: "رجاء إدخال عنوان موقع إلكتروني صحيح",
						date: "رجاء إدخال تاريخ صحيح",
						dateISO: "رجاء إدخال تاريخ صحيح (ISO)",
						number: "رجاء إدخال عدد بطريقة صحيحة",
						digits: "رجاء إدخال أرقام فقط",
						creditcard: "رجاء إدخال رقم بطاقة ائتمان صحيح",
						equalTo: "رجاء إدخال نفس القيمة",
						accept: "رجاء إدخال ملف بامتداد موافق عليه",
						maxlength: jQuery.validator.format("الحد الأقصى لعدد الحروف هو {0}"),
						minlength: jQuery.validator.format("الحد الأدنى لعدد الحروف هو {0}"),
						rangelength: jQuery.validator.format("عدد الحروف يجب أن يكون بين {0} و {1}"),
						range: jQuery.validator.format("رجاء إدخال عدد قيمته بين {0} و {1}"),
						max: jQuery.validator.format("رجاء إدخال عدد أقل من أو يساوي (0}"),
						min: jQuery.validator.format("رجاء إدخال عدد أكبر من أو يساوي (0}")
		 });
	@endif


</script>

@endsection
