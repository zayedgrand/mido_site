@extends('Site.layout.blank')
@section('content')
	<style>
		#profile h1 { padding:20px 40px; }
	</style>
	<div id="profile">
		<article id="profile-page" class="main-page grid grid-1">
			<div class="container">
				<div class="">
						<h1 class="wishlist-border cap">@lang('Design.My Profile')</h1>
						{!! Form::open([ 'method'=>'PATCH','url'=>['member/update',$member->id],'class'=>"profile-info",'id'=>'profile','v-on:submit.prevent'=>'doSubmit()'  ]) !!}

						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>@lang('Design.name'):</label>
							</div>
							<input type="text" value="{{$member->name}}" name="name" required minlength="3" />
						</div>

						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>@lang('Design.email'):</label>
							</div>
							<input type="email" value="{{$member->email}}" name="email" required />
						</div>
						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>@lang('Design.birth_date'):</label>
							</div>
							<input type="text" value="{{$member->birthdate}}" name="birthdate" data-toggle="datepicker"  required />
						</div>
						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>@lang('Design.city'):</label>
							</div>
							<select class="w-100" name="city_id" required >
									@foreach ($Cities as $key => $Citiy)
											<option value="{{$Citiy->id}}" {{($Citiy->id==$member->city_id)?'selected':''}} >{{$Citiy->name}}</option>
									@endforeach
							</select>
						</div>
						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>@lang('Design.mobile'):</label>
							</div>
							<input type="text" value="{{$member->phone}}" name="phone" required minlength="6" />
						</div>

						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>@lang('Design.new_password'):</label>
							</div>
							<input type="password" value="" name="password"   minlength="6" id="password"/>
						</div><!--End grid grid-20 grid-1-20-1 field-->

						<div class="grid grid-20 grid-1-20-1 field">
							<div class="flex flex-middle">
								<label>@lang('Design.confirm_new_password'):</label>
							</div>
							<input type="password"    minlength="6" id="password_again" name="password_again" />
						</div><!--End grid grid-20 grid-1-20-1 field-->

						<div class="grid grid-20 grid-1-20-1 address-grid field">
							<div class="flex">
								<label>@lang('Design.address'):</label>
							</div>
							<ul class="address">
								<li v-for="(addr,index) in address" :key="addr.cc" class="field flex flex-middle flex-wrap">
									<img v-if="index!=0" v-on:click="removeAddress(addr,index)" class="delete" src="{{asset('site_assets/images/error22.svg')}}">
										<input type="hidden" name="count[]" :value="index">
										<input type="hidden" name="address_ids[]" :value="addr.id">
										<input type="text" name="address[]" v-model="addr.address" class="w-100 address"  required placeholder="@lang('Design.address')"/>

										<div class="flex flex-between" style="margin-top:20px">
											<input type="text" placeholder="@lang('Design.Street name')"  class="w-30 street" name="street[]" v-model="addr.street" required  />
											<input type="text" placeholder="@lang('Design.Building no')"  class="w-30 building_no" name="building_no[]" v-model="addr.building_no" required  />
											<input type="text" placeholder="@lang('Design.Apartment no')" class="w-30 apartment_no" name="apartment_no[]" v-model="addr.apartment_no" required  />
										</div>

								</li>
								<li><button v-on:click="addAddress()" type="button" class="add-adress second-btn">@lang('Design.add_new_address')</button></li>
 							</ul>
						</div>
						<div class="flex flex-between flex-wrap profile-router">
							<div>
								<a href="{{url('WishList')}}" class="main-btn cap">@lang('Design.my_wishlist')</a>
								<a class="main-btn cap">@lang('Design.my_order_history')</a>
							</div>
							<div>
								<button type="submit" class="main-btn cap">@lang('Design.save_profile')</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</article>

	</div><!--End profile-->


@endsection


@section('script')
	<script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>

<script>



	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	});

	//==========================================================================
	let get_address = JSON.parse(escapeSpecialChars(`{!!$address!!}`));

	let profile = new Vue({
		el: '#profile',
		data:{
				address: get_address,
		},
		mounted()
		{
				$('[data-toggle="datepicker"]').datepicker({ format: 'yyyy-mm-dd'});
		},
		methods:{
			removeAddress(row,index)
			{
					this.address.splice(index,1);
			},
			addAddress()
			{
					this.address.push({address:'',cc: 'cc_'+Math.random()});
			},
			doSubmit()
			{
				  let is_form_valid = true;
				  $("ul.address li").each(function(){
							 if( $(this).find("input.street").length  )
							 {
									 if( ! $(this).find("input.street").val() ){
											is_form_valid = false;
									 }
									 if( ! $(this).find("input.address").val() ){
											is_form_valid = false;
									 }
									 if( ! $(this).find("input.building_no").val() ){
											is_form_valid = false;
									 }
									 if( ! $(this).find("input.apartment_no").val() ){
											is_form_valid = false;
									 }
							 }
					});

					if(is_form_valid)
					{
								$('form#profile').submit();
					}
					else
					{
						 // new Noty({text: lacal('Added to cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
						 new Noty({text: 'please complete the address info' ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
					}
			}

		},//End methods

	});//end profile


	//...........login validation.......................
	$('form#profile').validate({
		rules: {
				password_again: {
					equalTo: "#password"
				}}
	});


	if(site_lang =='ar' )
	{
			jQuery.extend(jQuery.validator.messages, {
						required: "هذا الحقل إلزامي",
						remote: "يرجى تصحيح هذا الحقل للمتابعة",
						email: "رجاء إدخال عنوان بريد إلكتروني صحيح",
						url: "رجاء إدخال عنوان موقع إلكتروني صحيح",
						date: "رجاء إدخال تاريخ صحيح",
						dateISO: "رجاء إدخال تاريخ صحيح (ISO)",
						number: "رجاء إدخال عدد بطريقة صحيحة",
						digits: "رجاء إدخال أرقام فقط",
						creditcard: "رجاء إدخال رقم بطاقة ائتمان صحيح",
						equalTo: "رجاء إدخال نفس القيمة",
						accept: "رجاء إدخال ملف بامتداد موافق عليه",
						maxlength: jQuery.validator.format("الحد الأقصى لعدد الحروف هو {0}"),
						minlength: jQuery.validator.format("الحد الأدنى لعدد الحروف هو {0}"),
						rangelength: jQuery.validator.format("عدد الحروف يجب أن يكون بين {0} و {1}"),
						range: jQuery.validator.format("رجاء إدخال عدد قيمته بين {0} و {1}"),
						max: jQuery.validator.format("رجاء إدخال عدد أقل من أو يساوي (0}"),
						min: jQuery.validator.format("رجاء إدخال عدد أكبر من أو يساوي (0}")
				});
	}


</script>

@endsection
