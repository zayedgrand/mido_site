@extends('Site.layout.blank')
@section('content')
		<style>
			.dropdown-click 		{margin-bottom:30px}
			.dropdown-click-head 	{border:1px solid #ededed;padding:10px;min-height: 81px;}
			.dropdown-click-body {border-left:1px solid #ededed;border-right:1px solid #ededed;border-bottom:1px solid #ededed}
			.dropdown-click-head > span {font-weight:bold}
			.dropdown-click-body	{padding:10px}
			.dropdown-click-body p {font-size: 0.95em;color: #6f6f6f;}

			.dropdown-click-head * {text-transform:none;line-height: 24px;}

			.dropdown-click-body * {text-transform:none!important;line-height: 24px;}
			.fa-plus , .fa-minus 	{color:#de1222}
			.dropdown-click:nth-child(2n-1)	{padding-right:20px}


			.dropdown-click:nth-child(2n)	{padding-left:20px}
			#FAQs	{padding-bottom:0}
			
			@media(max-width:768px){
				.w-50 {width:100%}
				.dropdown-click:nth-child(2n)	{padding-left:0px}
				.dropdown-click:nth-child(2n-1) {padding-right:0}
			}
		</style>
		<article id="FAQ-page" class="main-page grid grid-1">
			<div class="container">
				<div class="banner-container">
					<img src="{{$banner}}"/>
				</div>
				<section id="FAQs">
					<div class="flex flex-wrap">
            @foreach ($PopularQuestions as $key => $Question)
              <div class="dropdown-click w-50">
                  <span class="dropdown-click-head flex flex-between flex-middle cap"><span>{{$Question->question}}</span><i class="fas fa-plus"></i></span>
                  <div class="dropdown-click-body">
                    <p>{{$Question->answer}} </p>
                  </div>
              </div>
            @endforeach
					</div><!--End flex flex-wrap flex-asian-->
				</section>
			</div>
		<article>

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			if($(this).hasClass('active')){
				$(this).next('.dropdown-click-body').slideUp();
				$(this).removeClass('active');
				//---------------------------------------
				$(this).find('i').toggleClass('fa-minus fa-plus');
			}else{
				$('.dropdown-click-head').removeClass('active');
				$('.dropdown-click-body').slideUp();
				$(this).addClass('active');
				$(this).next('.dropdown-click-body').slideDown();
				//---------------------------------------
				$('.dropdown-click-head').find('.fa-minus').toggleClass('fa-minus fa-plus');
				$(this).find('i').toggleClass('fa-minus fa-plus');
				//---------------------------------------
			}
		})
	})
</script>

@endsection
