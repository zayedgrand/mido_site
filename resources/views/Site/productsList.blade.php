@extends('Site.layout.blank')

@if($type == 'brand')
		@section('og')
		  	<meta property="og:description" content="{{$main->og_description}}"  >
				<meta property="og:image" content="{{asset('site_assets/images/mido_logo_main.png')}}"  >
				<meta property="og:image:secure_url" content="{{asset('site_assets/images/mido_logo_main.png')}}">
				<meta name="description" content="{{$main->og_description}}">
		@endsection
@endif

@section('content')

	  <style>   [v-cloak] { display: none; }   </style>

	<div id="productsList">
{{-- {{ dd('d') }} --}}

		<article id="brand-page" class="main-page grid grid-1">
			<section>
				<div class="container">
					<div class="banner-container">
						<img src="{{$main->banner}}"/>
					</div>
					<div class="grid grid-6 grid-1-rest brand-grid">

						<aside>
							<div class="dropdown-click">
									<span class="dropdown-click-head flex flex-between flex-middle">
											<h1 class="cap main-color bold" v-if="type=='subCategory'">@lang('Design.categories')</h1>
											<h1 class="cap main-color bold" v-if="type=='brand'">@lang('page.brands')</h1>
									</span>
									<div class="dropdown-click-body">
										<div class="dropdown-click-body-content">
											@if ($type == 'subCategory')
														@foreach ($site_categories as $key => $category)
															<div class="dropdown-click">
																	<span class="dropdown-click-head flex flex-between flex-middle">
																			<span class="upper">{{$category->name}}</span><i class="fas fa-chevron-down"></i>
																	</span>
																	<div class="dropdown-click-body">
																		<ul class="dropdown-click-body-content">
																			 @foreach ($category->SubCategories as $key => $SubCat)
																					<li class="{{($SubCat->id==$main->id)?'active':''}}">
																							<a href="{{$SubCat->path()}}"> {{$SubCat->name}} </a>
																					</li>
																			 @endforeach
																		</ul>
																	</div><!--End dropdown-click-body-->
															</div><!--End dropdown-click-->

																					<div class="product-border"></div>
														@endforeach
													@elseif($type == 'brand')
													 @foreach ($site_brands as $key => $brand)
														 <ul class="brands-body-content">
															 <li class="{{($brand->id==$main->id)?'active':''}}">
																	 <a href="{{$brand->path()}}"> {{$brand->name}} </a>
															 </li>
															<li class="product-border"></li>
														 </ul>
													 @endforeach
											@endif

										</div>
									</div><!--End dropdown-click-body-->
							</div><!--End dropdown-click-->

						</aside>


						<div class="products-grid-container">
							<div class="grid grid-4 products-grid">

								<div v-for="(item,listIndex) in mainList.data"  v-cloak class="flex flex-center product">
									<div>
										{{-- <a :href="'{{url('productsOf/details')}}/'+item.id" > --}}
										<a :href="datails_url(item)" >
											<div><img :src="item.image" /></div>
											<div class="text-center details bold">@{{item.name}} </div>
											<div class="text-center weight">@{{item.short_description}} </div>
											{{-- <div class="text-center price">@{{item.price}} @lang('Design.pound')</div> --}}

											<div class="text-center price discount" v-if="item.discount_percentage>0" > @{{item.discount_percentage}}% @lang('page.off')</div>
											<img  v-if="item.discount_percentage>0" src="{{asset('site_assets/images/discount.png')}}" alt="" class="discount-bg">
											<div v-if="item.discount_percentage>0" class="discount_prices flex flex-between"  >
													<div class="text-center price old" >@{{item.old_price}} @lang('page.EGP')</div>
													<div class="text-center price new">@{{item.price}} @lang('Design.pound')</div>
											</div>
											<div v-else class="text-center price">@{{item.price}} @lang('Design.pound')</div>

										</a>
										<span class="seperator"></span>
										<div class="product-footer">
											<div class="flex flex-center">
													<!-- if Out of stock -->
														<button v-if="item.quantity == 0" disabled  class="upper add-to-cart main-btn flex flex-between flexmiddle">
																<span>@lang('Design.Out of stock')</span> <i class="count"> </i>
														</button>
													<!-- if In cart -->
														<button v-else-if="item.in_card" v-on:click="addToCart(item)"  class="upper add-to-cart main-btn flex flex-between flexmiddle">
																<span>@lang('Design.in_cart')</span> <i class="count">@{{item.in_card_quantity}}</i>
														</button>
													<!-- if Not In cart -->
														<button v-else v-on:click="addToCart(item)" class="upper add-to-cart main-btn flex flex-between flexmiddle">
																<span>@lang('Design.add_to_cart')</span><i class="fas fa-shopping-cart"></i>
														</button>

											</div><!--End flex flex-center-->
										</div><!--End product-footer-->
									</div>
								</div><!--End  v-for ,  flex flex-center product-->

							</div><!--End grid grid-3 products-grid -->
						</div><!--End products-grid-container-->

					</div>
				</div>
			</section>
		<article>



			<!-- - - - - - -START paginate- - - - - - - -->
			<div class="row">
						<div class="col-md-8 col-md-offset-5">
									<pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
											<span slot="prev-nav">&lt; prev </span>
											<span slot="next-nav"> next &gt;</span>
									</pagination>
						</div>
			</div><!--End row-->
			<!-- - - - - - -End paginate- - - - - - - -->
			<br>
			<!-- - - - - - -START spinner- - - - - - - -->
			<spinner2 v-if="show_spinner"></spinner2>
			<!-- - - - - - -End spinner- - - - - - - -->


	 </div><!--end productsList-->

@endsection


@section('script')
<script>
	$('aside li.active').parents('.dropdown-click-body').show();
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	});

	//===========================================================================
	let selected_subCategory_id = "{{$main->id}}";
	let selected_type = "{{$type}}"; // subCategory  ,  brand

	let productsList = new Vue({
		el: '#productsList',
		data:{
				mainList: {data:[]},
				show_spinner:true,
				type: selected_type
		},
		mounted()
		{
				this.getResults();
		},
		methods:{
				getResults(page = 1)
				{
						 this.mainList = {data:[]};
						 this.show_spinner = true;

						 $.get( `${site_url}/productsOf/list/${selected_type}/${selected_subCategory_id}?page=${page}`,(Response)=>{
								 productsList.mainList = Response;
								 productsList.show_spinner = false;
						 });
				},
				addToCart(item)
				{
					if(member_id == 0)  //if not auth
					{
							new Noty({text: lacal('Must login first') , layout: 'topRight', type: 'success',timeout: 2000  }).show();
					}
					else //if auth
					{
								var the_data = {
										product_id: item.id ,
										_token: csrf_token
								}
								$.post(`${site_url}/ShoppingCart/add`,the_data,(response)=>{
										if(response.status == 'success')
										{
												if(response.case == 'added')
												{
														if(item.in_card_quantity == 0) {  //if not in cart
																new Noty({text: lacal('Added to cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
														}
														else { //if in cart
																new Noty({text: lacal('prouduct increased in the cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
														}
														item.in_card_quantity = response.quantity;
														item.in_card = 1;
														navShoppingCart.getCountNumber();
												}
												else // Max in Stock
												{
													 new Noty({text: lacal('Max in Stock') , layout: 'topRight', type: 'error',timeout: 2000 }).show();
												}
										}//End if
										else { //problem
											 new Noty({text: 'problem try agin', layout: 'topRight', type: 'error',timeout: 2000 }).show();
										}
								});
								navShoppingCart.getCountNumber();
					}//End if auth
				},//End addToCart()
        datails_url(product)
        {
	          var  link_url =  site_url +'/product/'+
	                 product.id +'-' +
	                 product.name.trim()
	                             .replace(/\n/g, "").replace(/\r/g, "")
	                             .replace(/\t/g, "").replace(/\f/g, "")
	                             .replace(' ', "-").replace('/', "-")
															 .split(" ").join('-');
	          return link_url;
        }
		},//End methods

	});//end vue
</script>

@endsection
