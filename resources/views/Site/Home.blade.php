
@extends('Site.layout.blank')

@section('og')
	<meta property="og:description" content="MIDO Company was established in 1977 with the prime objective to be the leading Food & Beverage distributor and solution provider to the Food Service and Away from Home market in Egypt."  >
	<meta property="og:image" content="{{asset('site_assets/images/mido_logo_main.png')}}"  >
	<meta property="og:image:secure_url" content="{{asset('site_assets/images/mido_logo_main.png')}}">
	<meta name="description" content="MIDO Company was established in 1977 with the prime objective to be the leading Food & Beverage distributor and solution provider to the Food Service and Away from Home market in Egypt.">
@endsection
@section('content')
  <style>   [v-cloak] { display: none; }   </style>
	<div id="homePage">
		<article id="home-page" class="main-page grid grid-1" >

			<section v-for="(list,listIndex) in mainList" v-cloak >
				<div class="container">
					<div class="banner-container">
						<img :src="list.item.banner"/>
					</div>
					<div class="products-grid-container">
						<div class="flex flex-right">
            <!--if subCategory-->
							<a v-if="list.type=='subCategory'" :href="SubCategory_url(list.item)" class="main-btn upper" >@lang('Design.view_all')</a>
            <!--if Brand-->
							<a v-else :href="Brand_url(list.item)" class="main-btn upper" >@lang('Design.view_all')</a>
						</div>



						<div class="grid grid-5 products-grid">

							<div v-for="(item,itemIndex) in list.items" class="flex flex-center product">
								<div>
									{{-- <a :href="'{{url('productsOf/details')}}/'+item.id"> --}}
									<a :href="datails_url(item)">
										<div><img :src="item.image"/></div>
										<h1 class="text-center details">@{{item.name}}</h1>
										<div class="text-center weight">@{{item.short_description}} </div>
					<div class="text-center price discount" v-if="item.discount_percentage>0" > @{{item.discount_percentage}}% @lang('page.off')</div>
					<img  v-if="item.discount_percentage>0" src="{{asset('site_assets/images/discount.png')}}" alt="" class="discount-bg">
                    <div v-if="item.discount_percentage>0" class="discount_prices flex flex-around flex-middle"  >
                        <div class="text-center price old" >@{{item.old_price}} @lang('page.EGP')</div>
                        <div class="text-center price new">@{{item.price}} @lang('Design.pound')</div>
                    </div>
                    <div v-else class="text-center price">@{{item.price}} @lang('Design.pound')</div>
									</a>
									<span class="seperator"></span>
									<div class="product-footer">
										<div class="flex flex-center">
                      <!-- if Out of stock -->
                        <button v-if="item.quantity == 0" disabled  class="upper add-to-cart main-btn flex flex-between flexmiddle">
                            <span>@lang('Design.Out of stock')</span> <i class="count"> </i>
                        </button>
											<!-- if In cart -->
												<button v-else-if="item.in_card" v-on:click="addToCart(item)"  class="upper add-to-cart main-btn flex flex-between flexmiddle">
														<span>@lang('Design.in_cart')</span> <i class="count">@{{item.in_card_quantity}}</i>
												</button>
											<!-- if Not In cart -->
												<button v-else v-on:click="addToCart(item)" class="upper add-to-cart main-btn flex flex-between flexmiddle">
														<span>@lang('Design.add_to_cart')</span><i class="fas fa-shopping-cart"></i>
												</button>
										</div><!--End flex flex-center-->
									</div><!--End product-footer-->
								</div>
							</div><!--End flex flex-center product-->


						</div><!--End grid grid-5 products-grid-->
					</div><!--End products-grid-container-->
				</div><!--End container-->
			</section>


		<article>
 </div><!--End homePage-->

@endsection


@section('script')
<!--script-->
	<script>
			let get_mainList = JSON.parse(escapeSpecialChars(`{!!$HomePage!!}`));

			let homePage = new Vue({
				el: '#homePage',
				data:{
						mainList: get_mainList,
				},
				mounted()
				{

				},
				methods:{
						addToCart(item)
						{
							if(member_id == 0)  //if not auth
							{
									new Noty({text: lacal('Must login first') , layout: 'topRight', type: 'success',timeout: 2000  }).show();
							}
							else //if auth
							{
										var the_data = {
												product_id: item.id ,
												_token: csrf_token
										}
										$.post(`${site_url}/ShoppingCart/add`,the_data,(response)=>{
												if(response.status == 'success')
												{
														if(response.case == 'added')
														{
															  if(item.in_card_quantity == 0) {  //if not in cart
																		new Noty({text: lacal('Added to cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
																}
																else { //if in cart
																  	new Noty({text: lacal('prouduct increased in the cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
																}
																item.in_card_quantity = response.quantity;
																item.in_card = 1;
                                navShoppingCart.getCountNumber();
														}
														else // Max in Stock
														{
															 new Noty({text: lacal('Max in Stock') , layout: 'topRight', type: 'error',timeout: 2000 }).show();
														}
												}//End if
												else { //problem
													 new Noty({text: 'problem try agin', layout: 'topRight', type: 'error',timeout: 2000 }).show();
												}
										});
                    navShoppingCart.getCountNumber();
							}//End if auth
						},//End addToCart()
						datails_url(product)
		        {
			          var  link_url =  site_url +'/product/'+
			                 product.id +'-' +
			                 product.name.trim()
			                             .replace(/\n/g, "").replace(/\r/g, "")
			                             .replace(/\t/g, "").replace(/\f/g, "")
			                             .replace(' ', "-").replace('/', "-")
																	 .split(" ").join('-');
			          return link_url;
		        },
						SubCategory_url(SubCategory)
		        {
			          var  link_url =  site_url +'/category/'+
			                 SubCategory.id +'-' +
			                 SubCategory.name.trim()
			                             .replace(/\n/g, "").replace(/\r/g, "")
			                             .replace(/\t/g, "").replace(/\f/g, "")
			                             .replace(' ', "-").replace('/', "-")
																	 .split(" ").join('-');
			          return link_url;
		        },
						Brand_url(brand)
		        {
			          var  link_url =  site_url +'/brand/'+
			                 brand.id +'-' +
			                 brand.name.trim()
			                             .replace(/\n/g, "").replace(/\r/g, "")
			                             .replace(/\t/g, "").replace(/\f/g, "")
			                             .replace(' ', "-").replace('/', "-")
																	 .split(" ").join('-');
			          return link_url;
		        }
				},//End methods

			});//end vue
	</script>

@endsection
