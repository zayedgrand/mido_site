@extends('Design.layout.blank')
@section('content')
		<style>
			header, footer {display:none}
			.main-page {padding-top:0}
		</style>
		<article id="login-page" class="login-page grid grid-1">
			<div class="flex flex-center">
				<div>
					<a href="{{url('')}}">
							<img class="logo" width="250" class="w-100 text-center" src="{{asset('site_assets/images/mido_logo.png')}}"/>
					</a>
				</div>
			</div>
			<div class="form-container flex flex-center">
				<form id="login" class="w-30" method="post" action="{{url('Auth/register')}}"  >
					{{csrf_field()}}
					<h1 class="flex flex-center cap">@lang('Design.login')</h1>
					<div class="field-container w-100">
						<input type="email" placeholder="@lang('Design.email')" class="w-100" name="email" required />
						<i class="fas fa-user-alt"></i>
					</div>
					<div class="field-container w-100">
						<input type="password" placeholder="@lang('Design.password')" class="w-100" name="password" required minlength="6" />
						<i class="fas fa-lock"></i>
					</div>

					<p id="wrongCredentials" style="display:none"> @lang('page.wrong email or password') </p>

					<div><button type="submit" class="cap main-btn w-100">@lang('Design.login')</button></div>
					<div class="flex flex-right"><a href="{{url('Auth/forgetPassword')}}" class="forget-password">@lang('Design.forget_password')</a></div>
					<div class="flex flex-center"><span class="cap new-customer">@lang('Design.new_customer')</span></div>

					<div><a href="{{url('Auth/register')}}" type="submit" class="cap second-btn w-100 text-center">@lang('Design.create_new_account')</a></div>
				</form>
			</div>
			<div class="flex flex-center flex-middle auth-footer">
				@lang('Design.copyright') <span>Vhorus</span>
			</div>
		<article>

@endsection


@section('script')
		<script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>

<script>

	 var csrf_token = '{{csrf_token()}}';
	 var site_url = '{{url('')}}';

	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	});



	    //...........login validation.......................
	    $('form#login').validate({
	      submitHandler: function(form) {
	          $('#wrongCredentials').hide();

	          var the_data = {
	              _token: csrf_token,
	              email: $(form).find('[name="email"]').val() ,
	              password: $(form).find('[name="password"]').val()
	          };
	          $.post(`${site_url}/Auth/login`,the_data,(response)=>{ console.log( response );
	                if(response.status == 'success') {
	                    window.location.replace(site_url);
	                }
	                else {
	                  $('#wrongCredentials').show();
	                }
	          });
	      }//end submitHandler
	    });//end $('form#login').validate

			@if ( \App::getLocale() =='ar' )
					jQuery.extend(jQuery.validator.messages, {
								required: "هذا الحقل إلزامي",
								remote: "يرجى تصحيح هذا الحقل للمتابعة",
								email: "رجاء إدخال عنوان بريد إلكتروني صحيح",
								url: "رجاء إدخال عنوان موقع إلكتروني صحيح",
								date: "رجاء إدخال تاريخ صحيح",
								dateISO: "رجاء إدخال تاريخ صحيح (ISO)",
								number: "رجاء إدخال عدد بطريقة صحيحة",
								digits: "رجاء إدخال أرقام فقط",
								creditcard: "رجاء إدخال رقم بطاقة ائتمان صحيح",
								equalTo: "رجاء إدخال نفس القيمة",
								accept: "رجاء إدخال ملف بامتداد موافق عليه",
								maxlength: jQuery.validator.format("الحد الأقصى لعدد الحروف هو {0}"),
								minlength: jQuery.validator.format("الحد الأدنى لعدد الحروف هو {0}"),
								rangelength: jQuery.validator.format("عدد الحروف يجب أن يكون بين {0} و {1}"),
								range: jQuery.validator.format("رجاء إدخال عدد قيمته بين {0} و {1}"),
								max: jQuery.validator.format("رجاء إدخال عدد أقل من أو يساوي (0}"),
								min: jQuery.validator.format("رجاء إدخال عدد أكبر من أو يساوي (0}")
				 });
			@endif

</script>

@endsection
