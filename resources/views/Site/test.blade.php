<script src="https://ap-gateway.mastercard.com/checkout/version/52/checkout.js"
{{-- <script src="https://ap-gateway.mastercard.com/api/nvp/version/52" --}}
                data-error="errorCallback"
                data-cancel="cancelCallback"
                 {{-- data-complete="{{url('confirm_is_paid/'.$Recipt->id.'/'.$Recipt->security_code)}}" --}}
                 data-afterRedirect="restorePageState"
                 return_url="{{url('gooo')}}"
              >
</script>

<script type="text/javascript">
function errorCallback(error) {
			console.log(JSON.stringify(error));
}
function cancelCallback() {
			console.log('Payment cancelled');
}

Checkout.configure({
		merchant: 'Test303030',
		order: {
				amount: function() {
						//Dynamic calculation of amount
						  return 20;
				},
				currency: 'EGP',
				description: 'Ordered goods',
			 id: Math.random()
		},
		interaction: {
				operation: 'PURCHASE', // set this field to 'PURCHASE' for Hosted Checkout to perform a Pay Operation. , AUTHORIZE
				merchant: {
						name: 'AAIB TEST',
						address: {
								line1: '200 Sample St',
								line2: '1234 Example Town'
						}
				} }
});

function restorePageState(data)
{
    window.location.replace("{{url('gooo')}}");
}

Checkout.showPaymentPage();

</script>
