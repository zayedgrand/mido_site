@extends('Site.layout.blank')
@section('og')
	<meta property="og:description" content="{{$product->short_description}}"  >
	<meta property="og:image" content="{{$Images[0]}}"  >
	<meta property="og:image:secure_url" content="{{$Images[0]}}">
	<meta name="description" content="{{$product->short_description}}">

@endsection
@section('content')

	<style>   [v-cloak] { display: none; }   </style>

	<div id="product">
		<article id="product-page" class="main-page grid grid-1">
			<section>
				<div class="container">
					<div class="banner-container">
						<img src="{{$banner}}"/>
					</div>
					<div class="grid grid-9 grid-1-5-5">
						<div class="thumbnails">
							@foreach ($Images as $key => $Image)
								<div class="thumbnail-container  product-background flex flex-center flex-middle {{($key==0)?'active':''}}">
									<img width="75" src="{{$Image}}"/>
								</div>
							@endforeach
						</div>
						<div class="product-img-container flex flex-center flex-middle product-background">

							@if ($product->discount_percentage > 0)
								<div class="discount"> {{$product->discount_percentage}}% @lang('page.off')</div>
							   <img   src="{{asset('site_assets/images/discount.png')}}" alt="" class="discount-bg">
							@endif
							<img width="400" src="{{$Images[0]}}"/>
						</div>
						<div class="flex flex-middle product-details">
							<div class="w-100 h-100">
								<div class="flex flex-wrap full-w-children h-100">
									<div>
										<h1><span>{{$product->name}}</span></h1>
										<div class="size">{{$product->short_description}}</div>
									</div>
									<div>

										@if ($product->discount_percentage > 0)
												<div class="discount_prices flex flex-middle">
													<div class="price upper old">{{$product->old_price}} @lang('Design.pound')</div>
													<div class="price upper new ">{{$product->price}} @lang('Design.pound')</div>
												</div>
										@else
												<div class="price upper">{{$product->price}} @lang('Design.pound')</div>
										@endif

										@if ($product->quantity > 0)
												<div class="status cap" >@lang('Design.in_stock')</div>
										@else
												<div class="status cap">@lang('Design.Out of stock')</div>
										@endif

									</div>
									<div class="flex flex-middle">
										<!-- if available quantity in stack > 15   -->
											<select v-if="selected_product.quantity>15" v-model="selected_product.in_card_quantity" >
													<option  v-for="i in 15" :value="i" v-text="i" > </option>
											</select>
										<!-- if available quantity in stack < 15   -->
											<select v-else v-model="selected_product.in_card_quantity" >
													<option  v-for="i in selected_product.quantity" :value="i" v-text="i" > </option>
											</select>
											<!-- if Out of stock -->
												<button v-if="selected_product.quantity == 0" disabled  class="add-to-cart cap main-btn flex flex-between upper">
														<span>@lang('Design.Out of stock')</span> <i class="count"> </i>
												</button>
											<!-- if In cart -->
												<button v-else-if="selected_product.in_card" v-on:click="addToCart_main()"  class="add-to-cart cap main-btn flex flex-between upper">
														<span>@lang('Design.add_to_cart')</span> <i class="count">@{{selected_product.in_card_quantity}}</i>
												</button>
											<!-- if Not In cart -->
												<button v-else v-on:click="addToCart_main()" class="add-to-cart cap main-btn flex flex-between upper">
														<span>@lang('Design.add_to_cart')</span><i class="fas fa-shopping-cart"></i>
												</button>

									</div>
									<!-- if In wishlist -->
									<div v-if="selected_product.in_wish_list" class="flex flex-middle">
										<i class="fa-heart fas" v-on:click="add_to_wishlist()"></i>
										<span class="cap">@lang('Design.in_wishlist')</span>
									</div>
									<!-- if Not In wishlist -->
									<div v-else class="flex flex-middle">
										<i class="fa-heart far" v-on:click="add_to_wishlist()"></i>
										<span class="cap">@lang('Design.add_to_wishlist')</span>
									</div>


									<div class="description">
										<span class="desc-title cap">@lang('Design.description')</span>
										{{-- <p> {!! stripslashes(str_replace('\\n', '<br/>', $product->description )) !!} </p> --}}
										<p> {!! stripslashes(str_replace('\\r', '', str_replace('\\n', '<br/>', $product->description ) )) !!} </p>
										{{-- <p> {!! $product->description !!} </p> --}}
									</div><!--End description-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>



			<div class="related-products">
				<div class="container">
					<h1 class="cap main-color">@lang('page.Related Products')</h1>
					<div class="grid grid-5 products-grid">

						<div v-for="(item,index) in relatedProducts" v-cloak class="flex flex-center product">
							<div>
								{{-- <a :href="'{{url('productsOf/details')}}/'+item.id" > --}}
								<a :href="datails_url(item)" >
									<div><img :src="item.image"/></div>
									<div class="text-center details">@{{item.name}}</div>
									<div class="text-center weight">@{{item.short_description}}</div>
									<div class="text-center price discount" v-if="item.discount_percentage>0" > @{{item.discount_percentage}}% @lang('page.off')</div>
									<img  v-if="item.discount_percentage>0" src="{{asset('site_assets/images/discount.png')}}" alt="" class="discount-bg">

									<div v-if="item.discount_percentage>0" class="discount_prices flex flex-around flex-middle"  >
											<div class="text-center price old" >@{{item.old_price}} @lang('page.EGP')</div>
											<div class="text-center price new">@{{item.price}} @lang('Design.pound')</div>
									</div>
									<div v-else class="text-center price">@{{item.price}} @lang('Design.pound')</div>
								</a>
								<span class="seperator"></span>
								<div class="product-footer">
									<div class="flex flex-center">
										<!-- if Out of stock -->
											<button v-if="item.quantity == 0" disabled  class="upper add-to-cart main-btn flex flex-between flexmiddle">
													<span>@lang('Design.Out of stock')</span> <i class="count"> </i>
											</button>
										<!-- if In cart -->
											<button v-else-if="item.in_card" v-on:click="addToCart(item)"  class="upper add-to-cart main-btn flex flex-between flexmiddle">
													<span>@lang('Design.in_cart')</span> <i class="count">@{{item.in_card_quantity}}</i>
											</button>
										<!-- if Not In cart -->
											<button v-else v-on:click="addToCart(item)" class="upper add-to-cart main-btn flex flex-between flexmiddle">
													<span>@lang('Design.add_to_cart')</span><i class="fas fa-shopping-cart"></i>
											</button>
									</div><!--End flex flex-center-->
								</div><!--End product-footer-->
							</div>
						</div><!--End flex flex-center product-->


					</div><!--End grid grid-5 products-grid-->
				</div><!--End container-->
			</div><!--End related-products-->
		<article>
	</div>
@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		});

		$('.thumbnails img').on('click',function(){
			$('.thumbnails img').parent('.thumbnail-container').removeClass('active');
			$(this).parent('.thumbnail-container').addClass('active');
			var img = $(this).attr('src');
			$('.product-img-container').children('img').attr('src',img)
		});
	});


	//-------------------------------------------------------------------
	let get_selected_product = JSON.parse(escapeSpecialChars(`{!!$product!!}`));
	let get_relatedProducts = JSON.parse(escapeSpecialChars(`{!!$relatedProducts!!}`));

	let product = new Vue({
		el: '#product',
		data:{
				selected_product: get_selected_product,
				relatedProducts: get_relatedProducts,
				show_spinner:true,
		},
		mounted()
		{

		},
		methods:{

				addToCart_main()
				{
					if(member_id == 0)  //if not auth
					{
							new Noty({text: lacal('Must login first') , layout: 'topRight', type: 'success',timeout: 2000  }).show();
					}
					else //if auth
					{
								var the_data = {
										product_id: this.selected_product.id ,
										quantity: this.selected_product.in_card_quantity?this.selected_product.in_card_quantity:1 ,
										_token: csrf_token
								}
										console.log(the_data);
								$.post(`${site_url}/ShoppingCart/add`,the_data,(response)=>{
										if(response.status == 'success')
										{
												if(response.case == 'added')
												{
														if(this.selected_product.in_card_quantity == 0) {  //if not in cart
																new Noty({text: lacal('Added to cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
														}
														else { //if in cart
																new Noty({text: lacal('prouduct increased in the cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
														}
														this.selected_product.in_card_quantity = response.quantity;
														this.selected_product.in_card = 1;
														navShoppingCart.getCountNumber();
												}
												else // Max in Stock
												{
													 new Noty({text: lacal('Max in Stock') , layout: 'topRight', type: 'error',timeout: 2000 }).show();
												}
										}//End if
										else { //problem
											 new Noty({text: 'problem try agin', layout: 'topRight', type: 'error',timeout: 2000 }).show();
										}
								});
								navShoppingCart.getCountNumber();
					}//End if auth
				},//End addToCart()
				addToCart(item)
				{
					if(member_id == 0)  //if not auth
					{
							new Noty({text: lacal('Must login first') , layout: 'topRight', type: 'success',timeout: 2000  }).show();
					}
					else //if auth
					{
								var the_data = {
										product_id: item.id ,
										_token: csrf_token
								}
								$.post(`${site_url}/ShoppingCart/add`,the_data,(response)=>{
										if(response.status == 'success')
										{
												if(response.case == 'added')
												{
														if(item.in_card_quantity == 0) {  //if not in cart
																new Noty({text: lacal('Added to cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
														}
														else { //if in cart
																new Noty({text: lacal('prouduct increased in the cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
														}
														item.in_card_quantity = response.quantity;
														item.in_card = 1;
														navShoppingCart.getCountNumber();
												}
												else // Max in Stock
												{
													 new Noty({text: lacal('Max in Stock') , layout: 'topRight', type: 'error',timeout: 2000 }).show();
												}
										}//End if
										else { //problem
											 new Noty({text: 'problem try agin', layout: 'topRight', type: 'error',timeout: 2000 }).show();
										}
								});
								navShoppingCart.getCountNumber();
					}//End if auth
				},//End addToCart()
				add_to_wishlist()
				{
					 if(member_id == 0)  //if not auth
					 {
							 new Noty({text: lacal('Must login first') , layout: 'topRight', type: 'success',timeout: 2000  }).show();
					 }
					 else
					 {
							 $.get(`${site_url}/WishList/addOrRemove/${this.selected_product.id}`,(response)=>{
										 if(response.status == 'success')
										 {
												 this.selected_product.in_wish_list = response.case;
										 }
							 });//End $.post
					 }
				},
				datails_url(product)
        {
	          var  link_url =  site_url +'/product/'+
	                 product.id +'-' +
	                 product.name.trim()
	                             .replace(/\n/g, "").replace(/\r/g, "")
	                             .replace(/\t/g, "").replace(/\f/g, "")
	                             .replace(' ', "-").replace('/', "-")
															 .split(" ").join('-');
	          return link_url;
        }
		},//End methods

	});//end vue

</script>

@endsection
