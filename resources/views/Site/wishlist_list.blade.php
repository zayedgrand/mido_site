@extends('Site.layout.blank')
@section('content')

	<style>   [v-cloak] { display: none; }   </style>

	<div id="productsList">

		<article id="wishlist-page" class="main-page grid grid-1">
			<div class="container">
				<h1 class="wishlist-border cap">@lang('Design.my_wishlist')</h1>
				{{-- :href="'{{url('productsOf/details')}}/'+item.id"   --}}
				<div v-for="(item,listIndex) in mainList.data" :key="item.id"  v-cloak href="" class="wishlist-product flex flex-between flex-middle flex-wrap wishlist-border">
					<img v-on:click="delRow(item.id,index)" class="delete-icon" width="30" class="w-100 text-center" src="{{asset('site_assets/images/error22.svg')}}"/>
					<div class="flex flex-middle">

							<a :href="datails_url(item)" class="img-wrapper product-background flex flex-center flex-middle">
								<img width="200" :src="item.image" >
							</a>
							<div>
								<a :href="datails_url(item)" class="title">@{{item.name}}</a>
								<div class="desc">@{{item.short_description}}</div>
								<div class="price upper">@{{item.price}} @lang('Design.pound')</div>
							</div>
					</div>
					<div>

						<!-- if In cart -->
							<button v-if="item.in_card" v-on:click="addToCart(item)"  class="flex flex-between main-btn add-to-cart cap">
									<span class="upper">@lang('Design.in_cart')</span> <i class="count">@{{item.in_card_quantity}}</i>
							</button>
						<!-- if Not In cart -->
							<button v-else v-on:click="addToCart(item)" class="flex flex-between main-btn add-to-cart upper">
									<span>@lang('Design.add_to_cart')</span><i class="fas fa-shopping-cart"></i>
							</button>


					</div>
				</div>


			</div>
		<article>

					<!-- - - - - - -START paginate- - - - - - - -->
					<div class="row">
								<div class="col-md-8 col-md-offset-5">
											<pagination :data="mainList" v-on:pagination-change-page="getResults" > <!-- the_mainList -->
													<span slot="prev-nav">&lt; prev </span>
													<span slot="next-nav"> next &gt;</span>
											</pagination>
								</div>
					</div><!--End row-->
					<!-- - - - - - -End paginate- - - - - - - -->
					<br>
					<!-- - - - - - -START spinner- - - - - - - -->
					<spinner2 v-if="show_spinner"></spinner2>
					<!-- - - - - - -End spinner- - - - - - - -->

 	</div><!--End productsList-->

@endsection


@section('script')
<script>
	$(function(){
		$('.dropdown-click-head').on('click',function(){
			$(this).next('.dropdown-click-body').slideToggle();
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
		})
	});
	//===========================================================================


	let productsList = new Vue({
		el: '#productsList',
		data:{
				mainList: {data:[]},
				show_spinner:true,
		},
		mounted()
		{
				this.getResults();
		},
		methods:{
				getResults(page = 1)
				{
						 this.mainList = {data:[]};
						 this.show_spinner = true;

						 $.get( `${site_url}/WishList/list?page=${page}`,(Response)=>{
								 productsList.mainList = Response;
								 productsList.show_spinner = false;
						 });
				},
				addToCart(item)
				{
					if(member_id == 0)  //if not auth
					{
							new Noty({text: lacal('Must login first') , layout: 'topRight', type: 'success',timeout: 2000  }).show();
					}
					else //if auth
					{
								var the_data = {
										product_id: item.id ,
										_token: csrf_token
								}
								$.post(`${site_url}/ShoppingCart/add`,the_data,(response)=>{
										if(response.status == 'success')
										{
												if(response.case == 'added')
												{
														if(item.in_card_quantity == 0) {  //if not in cart
																new Noty({text: lacal('Added to cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
														}
														else { //if in cart
																new Noty({text: lacal('prouduct increased in the cart') ,layout: 'topRight', type: 'success',timeout: 2000 }).show();
														}
														item.in_card_quantity = response.quantity;
														item.in_card = 1;
												}
												else // Max in Stock
												{
													 new Noty({text: lacal('Max in Stock') , layout: 'topRight', type: 'error',timeout: 2000 }).show();
												}
										}//End if
										else { //problem
											 new Noty({text: 'problem try agin', layout: 'topRight', type: 'error',timeout: 2000 }).show();
										}
								});
					}//End if auth
				},//End addToCart()
				delRow(id,index)
				{
							$.get(`${site_url}/WishList/addOrRemove/${id}`,(response)=>{
										if(response.status == 'success') {
												productsList.mainList.data.splice(index,1);
										}
							});//End $.post
					},
					datails_url(product)
					{
							var  link_url =  site_url +'/product/'+
										 product.id +'-' +
										 product.name.trim()
																 .replace(/\n/g, "").replace(/\r/g, "")
																 .replace(/\t/g, "").replace(/\f/g, "")
																 .replace(' ', "-").replace('/', "-")
																 .split(" ").join('-');
							return link_url;
					},
		},//End methods

	});//end vue
</script>

@endsection
