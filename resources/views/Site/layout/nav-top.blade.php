
<header>
  <div class="black-bg grey-color">
    <div class="container">
      <div class="flex flex-between flex-middle">
        <span class="filler"></span>
        <div id="search-form-container" class="flex flex-middle">
          <form method="post" action="{{url('search')}}" id="search-form">
            {{csrf_field()}}
            <input name="search" type="text" placeholder="@lang('Design.search')" value="{{$search??''}}" required/>
            <button type="submit"><i class="fas fa-search"></i></button>
          </form>
          @php( $lang = \App::getLocale() )
          @if($lang == 'ar')
              <a href="{{url('setLang/en')}}"> English </a>
          @else
            <a href="{{url('setLang/ar')}}">  العربية</a>
          @endif
          <span class="sep grey-bg"></span>
          <div class="dropdown-hover">
            @auth('Member')
              <span class="dropdown-hover-head">@lang('page.Hello'), {{auth('Member')->user()->name}} </head>
              <div class="dropdown-hover-body">
                <ul class="dropdown-hover-body-content">
                  <li class="b-list"> <a href="{{url('member/edit')}}">@lang('Design.my_profile')</a> </li>
                  <li class="b-list"> <a href="{{url('WishList')}}">@lang('Design.my_wishlist')</a> </li>
                  <li class="b-list"> <a href="{{url('History')}}">@lang('Design.orders_history')</a> </li>
                  <li class="b-list"> <a href="{{url('member/logout')}}">@lang('Design.logout')</a></li>
                </ul>
              </div>
            @else
              <span class=""> <a href="{{url('Auth/login')}}"> @lang('Design.login')  </a> </head> /
              <span class=""> <a href="{{url('Auth/register')}}"> @lang('Design.register') </a>  </head>
            @endauth
          </div><!--End dropdown-hover-->
        </div>
        <div>
          <ul class="flex social-list">
            <li> <a target="_blank" href="{{$site_settings['facebook_link']}}" class="flex flex-center flex-middle"> <i class="fab fa-facebook-f"></i>  </a> </li>
            <li> <a target="_blank" href="{{$site_settings['instagram_link']}}" class="flex flex-center flex-middle"> <i class="fab fa-instagram"></i>   </a> </li>
            <li> <a target="_blank" href="{{$site_settings['linkedin_link']}}" class="flex flex-center flex-middle"> <i class="fab fa-linkedin-in"></i> </a> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="grey-bg">
    <div class="flex flex-between black-color">
      <div class="container">
        <div class="flex flex-between">
          <div class="flex flex-middle">
            <div class="logos-container logo-space">
              <a href="{{url('home')}}">
                <img width="134" class="logo" class="w-100 text-center" src="{{asset('site_assets/images/mido_logo_main.png')}}"/>
              </a>
            </div>
            <ul class="flex bold">
									<li>
										<div class="dropdown-hover a-list">
											<span class="dropdown-hover-head cap">@lang('Design.categories')</span>
											<div class="dropdown-hover-body categories">
												<div class="container">
													<ul class="flex flex-wrap">
														<li class="logo-space"></li>
														@foreach ($site_categories as $key => $category)
														<li class="b-list">
															<div class="dropdown-hover">
																<a class="dropdown-hover-head cap" href="#">{{ucfirst($category->name)}}</a>
																<div class="dropdown-hover-body">
																		<div class="container">
																			<ul class="flex flex-wrap">
																				<li class="logo-space c-list"></li>
                                        @foreach ($category->SubCategories as $key => $SubCat)
                                            <li><a href="{{$SubCat->path()}}">{{$SubCat->name}}</a></li>
                                        @endforeach
																			</ul>
																		</div>
																</div>
															</div>
														</li>
														@endforeach
													</ul>
												</div>
											</div>
										</div>

									</li>
              <li>
                <div class="dropdown-hover a-list">
                  <span class="dropdown-hover-head cap" onclick="" style="">@lang('page.brands')</span>
                  <div class="dropdown-hover-body">
                    <div class="container">
                      <ul class="flex flex-wrap">
						<li class="logo-space"></li>
						@foreach ($site_brands as $key => $brand)
						<li class="b-list"><a href="{{$brand->path()}}"> {{($brand->name)}} </a></li>
						@endforeach
                      </ul>
                    </div>
                  </div>
                </div>

              </li>
            </ul>
          </div>
		  <div class="flex flex-middle">
          @if (auth('Member')->check())

            <a href="{{url('ShoppingCart')}}" class="flex flex-middle" id="navShoppingCart" >
              <span class="cart-icon-wrapper">
                {{-- <span class="count">{{$site_ShoppingCart_count}}</span> --}}
                <span class="count">@{{countNumber}}</span>
                <img class="cart-icon" width="30" class="w-100 text-center" src="{{asset('site_assets/images/cart.png')}}"/>
              </span>
              <span class="bold">@lang('Design.cart')</span>
            </a>
          @endif
			<div><i class="fas fa-bars"></i></div>
		  </div>
        </div>
      </div>
    </div>
  </div>
</header>
