<!DOCTYPE html>
<html>
	<head>
		<title>Mido - Food & Beverage Supplier Since 1977 </title>
		<link rel="icon" href="/main_site/images/favicon2.ico" type="image/x-icon">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<link rel="stylesheet" href="{{asset('site_assets/css/sm.css')}}"/>
		<link href="https://fonts.googleapis.com/css?family=Baloo" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Signika+Negative" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<meta name="keywords" content="mido,products,salmon,market,ecommece,market,illy,jacobs,maxwell,dilmah,dammann,pickwick,bonne mamman,anna,rich,delivery,seafood,evian,badoit,shopping">


		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!--Start Sharer -->
			<meta property="og:title" content="Mido"  >
			@yield('og')
		<!--End Sharer-->
		<style>
			@font-face {
			  font-family: AvenirNext;
			  src: url({{asset('site_assets/fonts/AvenirNextLTPro-Regular.ttf')}});
			}
			.main-btn.no {
				    background-color: #808080;
			}
			#history-page .wishlist-product .main-btn {
				width:100%
			}
		</style>
		<link rel="stylesheet" href="{{asset('site_assets/css/style.css')}}?ver=6.3"/>
	</head>
	<body class="{{($lang=='ar')?'direction-rtl':'direction-ltr'}}">


		@if ( $errors->any() )
				<ul class="alert alert-danger mydirection" >
					 @foreach ($errors->all() as $error)
						 <li class="mydirection text-center">{{$error}}</li>
					 @endforeach
				</ul>
			@endif

			@if (Session::has('flash_message') )

				<div id="msg-container" class="flex flex-center flex-middle">
					<div id="msg_wrapper">
						<img class="close" src="{{asset('site_assets/images/error22.svg')}}">
						<p>{{Session::get('flash_message')}} </p>
					</div>
				</div>
									<!--flash message popup-->
									{{-- <div class="flash-container flex flex-center flex-middle flash-message-popup">
										<div class="popup active">
											<p>{{Session::get('flash_message')}}</p>
										</div>
									</div> --}}
	 	@endif

		@include('Site.layout.nav-side')
		@include('Site.layout.nav-top')

				@yield('content')

		@include('Site.layout.footer')

			<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
			<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
		<script>
			$(function(){
					$('.fa-bars').on('click',function(){
						$('body').toggleClass('show-menu')
					});
					$('.menu-wrap .close-button').on('click',function(){
						$('body').removeClass('show-menu')

					})
			})
		</script>

			<!--,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,-->

			<script>
				 const csrf_token = '{{csrf_token()}}';
				 const site_url = '{{url('')}}';
				 const member_id = '{{auth('Member')->check() ? auth('Member')->id() : 0 }}';
				 const site_lang = '{{ \App::getLocale()??'en' }}';
			</script>


		<script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.js"> </script>
		<!--...........login register.......................-->
		<script>
			$(function(){
				$('#msg_wrapper .close').on('click',function(){
					$('#msg-container').fadeOut(500);
				})
			})
		</script>
		<script>

		function escapeSpecialChars(jsonString)
		{
				return jsonString.replace(/\n/g, "\\n")
								.replace(/\r/g, "\\r")
								.replace(/\t/g, "\\t")
								.replace(/\f/g, "\\f");
		}


		//,,,,,,,,,,,,,,,,nav top Shopping Cart -> set list,,,,,,,,,,,,,,,,,,
		let navShoppingCart = new Vue({
		    el:'#navShoppingCart',
		    data:{
		        countNumber: 0
		    },
		    mounted(){

		      if(member_id != 0){ //if member is auth
		          this.getCountNumber();
		      }

		    },//End mounted()
		    methods:{
		        getCountNumber()
						{
		             $.get(`${site_url}/ShoppingCart/getCount` ,(Response)=>{
		                 navShoppingCart.countNumber = Response;
		             });
		        },
		    },//End methods
		});

		</script>

		<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144489780-1"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'UA-144489780-1');
			</script>

			<!-- Global site tag (gtag.js) - Google Analytics -->
				<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144489780-1"></script>
				<script>
				  window.dataLayer = window.dataLayer || [];
				  function gtag(){dataLayer.push(arguments);}
				  gtag('js', new Date());

				  gtag('config', 'UA-144489780-1');



				</script>


				<!-- Facebook Pixel Code -->
			<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window,document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
			 fbq('init', '2341078892593330');
			fbq('track', 'PageView');
			</script>
			<noscript>
			 <img height="1" width="1"
			src="https://www.facebook.com/tr?id=2341078892593330&ev=PageView
			&noscript=1"/>
			</noscript>
			<!-- End Facebook Pixel Code -->


			<script type="text/javascript" src="{{asset('js_admin/lang_local.js?ver=1.3')}}"></script>

      @yield('script')


	</body>
</html>
