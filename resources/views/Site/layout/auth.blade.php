
<!--register popup-->
<div class="popup-container flex-center flex-middle register">
  <form id="register" class="popup active w-40" method="POST">
    <h1 class="cap flex flex-center">@lang('Design.register')</h1>
    <div class="flex flex-wrap">
      <label class="cap w-35 flex flex-middle">@lang('Design.name')*</label>
      <input class="w-65" type="text"/>
    </div>
    <div class="flex flex-wrap">
      <label class="cap w-35 flex flex-middle">@lang('Design.password')*</label>
      <input class="w-65" type="password"/>
    </div>
    <div class="flex flex-wrap">
      <label class="cap w-35 flex flex-middle">@lang('Design.confirm_password')*</label>
      <input class="w-65" type="password"/>
    </div>
    <div class="flex flex-wrap">
      <label class="cap w-35 flex flex-middle">@lang('Design.gender')</label>
      <select class="w-30">
        <option class="cap" value="male">@lang('Design.male')</option>
        <option class="cap" value="female">@lang('Design.female')</option>
      </select>
    </div>
    <div class="flex">
      <label class="cap w-35 flex flex-middle">@lang('Design.mobile')*</label>
      <input class="w-65" type="text"/>
    </div>
    <div class="flex">
      <label class="cap w-35 flex flex-middle">@lang('Design.email')*</label>
      <input class="w-65" type="text"/>
    </div>
    <div class="flex">
      <label class="cap w-35 flex flex-middle">@lang('Design.address')*</label>
      <input class="w-65" type="text"/>
    </div>
    <div class="flex">
      <label class="cap w-35 flex flex-middle">@lang('Design.city')*</label>
      <select class="w-30 cap">
		<option value="cairo">@lang('Design.city1')</option>
		<option value="alexandria">@lang('Design.city2')</option>
		<option value="mansoura">@lang('Design.city3')</option>
	  </select>
	</div>
    <div class="flex submit-container">
      <span class="w-35"></span>
      <input type="submit" value="@lang('Design.sign_up_button')" class="main-btn w-30 bold"/>
    </div>
  </form>
</div>
<!--login popup-->
<div class="popup-container flex-center flex-middle login">
  <form id="login" class="popup active w-35" method="POST">
    <h1 class="cap flex flex-center">@lang('Design.login')</h1>
    <div class="flex flex-wrap">
      <label class="cap w-35 flex flex-middle label-above">@lang('Design.email')</label>
      <input class="w-100" type="email"/>
    </div>
    <div class="flex flex-wrap">
      <label class="cap w-35 flex flex-middle label-above">@lang('Design.password')</label>
      <input class="w-100" type="password"/>
    </div>
    <div class="flex flex-center submit-container">
      <input type="submit" value="@lang('Design.login_button')" class="main-btn w-50 bold"/>
    </div>
    <div class="flex flex-center">
      <div class="flex flex-right w-50">
        <a id="forgot-password" href="#" class="cap">@lang('Design.forgot_password')</a>
      </div>
    </div>
  </form>
  <form id="forget-password-form" class="popup active w-35" method="POST">
    <h1 class="cap flex flex-center">reset password</h1>
    <div class="flex flex-wrap">
      <label class="cap w-35 flex flex-middle label-above">@lang('Design.email')</label>
      <input class="w-100" type="email"/>
    </div>
    <div class="flex flex-center submit-container">
      <input type="submit" value="submit" class="main-btn w-50 bold"/>
    </div>
  </form>
</div>
