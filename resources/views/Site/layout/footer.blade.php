<div class="pre-footer main-bg">
	<div class="container flex flex-between white-color">
		<div class="text-center w-100">
			<i class="fas fa-shipping-fast"></i>
			<span>@lang('Design.Free_delivery_in_Cairo')</span>
		</div>
		<span class="sep white-bg"></span>
		<div class="text-center w-100 white-color">
			<i class="fas fa-shopping-cart"></i>
			<span>@lang('Design.Minimum_basket') {{$site_settings['minimum_basket_amount']}} @lang('page.EGP')</span>
		</div>
	</div>
</div>
<footer>
  <div class="black-bg">
    <div class="container">
      <div class="flex">
        <div>
          <h1 class="white-color">@lang('Design.sitemap')</h1>
          <ul class="flex flex-asian flex-wrap">
            <li><a href="{{url('main_site/About')}}">@lang('Design.about_us')</a></li>
            <li><a href="{{url('main_site/News')}}">@lang('Design.news')</a></li>
            <li><a href="{{url('')}}">@lang('Design.shop')</a></li>
            <li><a href="{{url('main_site/University/index')}}">@lang('Design.our_courses')</a></li>
            <li><a href="{{url('main_site/Gallery')}}">@lang('Design.gallery')</a></li>
            <li><a href="{{url('main_site/awards')}}">@lang('Design.awards')</a></li>
            <li><a href="{{url('main_site/Careers')}}">@lang('Design.career')</a></li>
            <li><a href="{{url('uploads/mido-portfolio.pdf')}}">@lang('Design.portfolio')</a></li>
            <li><a href="{{url('FAQ')}}">@lang('Design.FAQs')</a></li>
          </ul>
        </div>
        <div>
          <h1 class="white-color">@lang('Design.contact_us')</h1>
          <ul class="flex flex-asian flex-wrap">
            <li><a href="tel:{{$site_settings['our_phone_1']}}" class="tel">{{$site_settings['our_phone_1']}}</a></li>
            <li><a href="tel:{{$site_settings['our_phone_2']}}" class="tel">{{$site_settings['our_phone_2']}}</a></li>
            <li><a class="lowercase" href="mailto:{{$site_settings['our_email']}}">{{$site_settings['our_email']}}</a></li>
            <li><a href="#" class="lowercase"> {!! nl2br(e($site_settings['our_location'])) !!} </a></li>
          </ul>
        </div>
        <div>
          <h1 class="white-color upper">@lang('Design.follow_us_on')</h1>
          <ul class="flex flex-wrap social-media">
            <li><a href="{{$site_settings['instagram_link']}}" class="flex flex-center flex-middle text-center" target="_blank"><i class="fab fa-instagram"></i></a></li>
            <li><a href="{{$site_settings['facebook_link']}}" class="flex flex-center flex-middle text-center" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="{{$site_settings['linkedin_link']}}" class="flex flex-center flex-middle text-center" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="text-center">
    <p>@lang('Design.copyright') <span class="cap">vhorus</span></p>
  </div>
</footer>
