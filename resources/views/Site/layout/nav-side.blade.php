<div class="menu-wrap">
  <button class="close-button" id="close-button">Close Menu</button>
  <nav class="menu">
    <div class="icon-list">
      <a href="./main_site"><span>Home</span></a>
      <a href="./main_site/About"><span>ABOUT US</span></a>
      <a href="./main_site/News"><span>NEWS</span></a>
      <a href="./"><span>ORDERS</span></a>
      <a href="./main_site/University/index"><span>UNIVERSITY OF COFFEE</span></a>
	  <a href="./main_site/Gallery"><span>GALLERY</span></a>
      <a href="./main_site/awards"><span>AWARDS</span></a><a href="/Careers/index"><span>CAREERS</span></a>
      <a href="./main_site/uploads/mido-portfolio.pdf"><span>Portfolio</span></a>
      <a href="{{url('PdfDownloads')}}"><span>Downloads</span></a>
      <div class="menu-info-block">
        <p>{{$site_settings['our_phone_1']}}</p>
        <p>{{$site_settings['our_phone_2']}}</p>
        <span>{{$site_settings['our_email']}}</span>
      </div>

      <div class="menu-info-block">
        <p>
        {!! stripslashes(str_replace('\\n', '<br/>', $site_settings['our_location'] )) !!}
        </p>
      </div>

      <div class="menu-info-block flex">
        <a href="{{$site_settings['facebook_link']}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <a href="{{$site_settings['instagram_link']}}" target="_blank"><i class="fab fa-instagram"></i></a>
        <a href="{{$site_settings['linkedin_link']}}" target="_blank"><i class="fab fa-linkedin-in"></i></a>
        <a href="#" target="_blank"><i class="fa fa-rss"></i></a>
      </div>
    </div>
  </nav>
</div>
