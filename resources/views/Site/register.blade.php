@extends('Design.layout.blank')
@section('content')
		<style>
			header, footer {display:none}
			.main-page {padding-top:0}
		</style>
		<article id="register-page" class="main-page grid grid-1">
			<div class="flex flex-center">
				<div>
					<a href="{{url('')}}">
						<img class="logo" class="w-100 text-center" src="{{asset('site_assets/images/mido_logo.png')}}"/>
					</a>

				</div>
			</div>


			@if ( $errors->any() )
					<ul class="alert alert-danger mydirection text-center" >
						 @foreach ($errors->all() as $error)
							 <li class="mydirection">{{$error}}</li>
						 @endforeach
					</ul>
				@endif

				@if (Session::has('flash_message') )
							<div class="alert alert-info mydirection">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<div class="mydirection">
											<i class="fa fa-thumbs-o-up white font-medium-5 mt-1"></i>
											{{Session::get('flash_message')}}
									</div>
							 </div>
				@endif

			<div class="form-container flex flex-center">
				<form id="register" class="w-30" method="post" action="{{url('Auth/register')}}" >
					{{csrf_field()}}
					<h1 class="flex flex-center cap">@lang('Design.register')</h1>
					<div class="field-container w-100">
						<input type="text" placeholder="@lang('Design.name')" class="w-100" name="name" required minlength="3" />
					</div>
					<div class="field-container w-100">
						<input type="password" placeholder="@lang('Design.password')" class="w-100" name="password" required minlength="6" id="password" />
					</div>
					<div class="field-container w-100">
						<input type="password" placeholder="@lang('Design.confirm_password')" class="w-100" required minlength="6" id="password_again" name="password_again" />
					</div>
					<div class="field-container w-100">
						<input type="text" placeholder="@lang('Design.birth_date')" class="w-100" name="birthdate" data-toggle="datepicker" />
					</div>
					<div class="field-container w-100 flex flex-middle gender-field">
						<label>@lang('Design.male')</label>
						<input type="radio" name="gender" value="male" name="gender" checked="checked" required />
						<label>@lang('Design.female')</label>
						<input type="radio" name="gender" value="male" name="gender" required />
					</div>
					<div class="field-container w-100">
						<input type="tel" placeholder="@lang('Design.mobile')" class="w-100" name="phone" required minlength="6" />
					</div>
					<div class="field-container w-100">
						<input type="email" placeholder="@lang('Design.email')" class="w-100" name="email" required    />
					</div>
					  <hr>
					<div class="field-container w-100">
						{{-- <textarea col="50" class="w-100" name="address" required minlength="6" placeholder="@lang('page.please add Street name/Building number/Apartment number')"></textarea> --}}
						<input type="text" placeholder="@lang('Design.address')" class="w-100" name="address" required  />

						<div class="flex flex-between" style="margin-top:20px">
							<input type="text" placeholder="@lang('Design.Street name')" class="w-30" name="street" required  />
							<input type="text" placeholder="@lang('Design.Building no')" class="w-30" name="building_no" required  />
							<input type="text" placeholder="@lang('Design.Apartment no')" class="w-30" name="apartment_no" required  />
						</div>

					</div>
						 
					<div class="field-container w-100">
						<select class="w-100" name="city_id" required  >
								<option value="">@lang('Design.choose_city')</option>
								@foreach ($Cities as $key => $Citiy)
										<option class="cap" value="{{$Citiy->id}}">{{$Citiy->name}}</option>
								@endforeach
						</select>
					</div>
					<div><button type="submit" class="cap main-btn w-100">@lang('Design.register')</button></div>
				</form>
			</div>
			<div class="flex flex-center flex-middle auth-footer">
				@lang('Design.copyright') <span>Vhorus</span>
			</div>
		<article>

@endsection


@section('script')
	<script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>
<script>
	$(function(){
			$('.dropdown-click-head').on('click',function(){
				$(this).next('.dropdown-click-body').slideToggle();
				$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
			})
	});


	//...........login validation.......................
	$('form#register').validate({
		rules: {
				password_again: {
					equalTo: "#password"
				}}
	});

	$('[data-toggle="datepicker"]').datepicker({ format: 'yyyy-mm-dd'});

	@if ( \App::getLocale() =='ar' )
			jQuery.extend(jQuery.validator.messages, {
		        required: "هذا الحقل إلزامي",
		        remote: "يرجى تصحيح هذا الحقل للمتابعة",
		        email: "رجاء إدخال عنوان بريد إلكتروني صحيح",
		        url: "رجاء إدخال عنوان موقع إلكتروني صحيح",
		        date: "رجاء إدخال تاريخ صحيح",
		        dateISO: "رجاء إدخال تاريخ صحيح (ISO)",
		        number: "رجاء إدخال عدد بطريقة صحيحة",
		        digits: "رجاء إدخال أرقام فقط",
		        creditcard: "رجاء إدخال رقم بطاقة ائتمان صحيح",
		        equalTo: "رجاء إدخال نفس القيمة",
		        accept: "رجاء إدخال ملف بامتداد موافق عليه",
		        maxlength: jQuery.validator.format("الحد الأقصى لعدد الحروف هو {0}"),
		        minlength: jQuery.validator.format("الحد الأدنى لعدد الحروف هو {0}"),
		        rangelength: jQuery.validator.format("عدد الحروف يجب أن يكون بين {0} و {1}"),
		        range: jQuery.validator.format("رجاء إدخال عدد قيمته بين {0} و {1}"),
		        max: jQuery.validator.format("رجاء إدخال عدد أقل من أو يساوي (0}"),
		        min: jQuery.validator.format("رجاء إدخال عدد أكبر من أو يساوي (0}")
	   });
	@endif


</script>

@endsection
