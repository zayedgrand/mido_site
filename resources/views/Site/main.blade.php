<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mido - Food & Beverage Supplier Since 1977 </title>
    <link rel="icon" href="/main_site/images/favicon2.ico" type="image/x-icon">
    <title>Mido</title>
    <link href="https://fonts.googleapis.com/css?family=Signika+Negative" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('main_page/sm.css') }}">
    <link rel="stylesheet" href="{{ asset('main_page/style2.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>
    <div class="flex flex-center">
        <div>
            <img width="134" class="logo" src="{{asset('site_assets/images/mido_logo_main.png')}}">
        </div>
    </div>
    <div class="flex flex-center">
        <div>
            <h1 class="text-cenetr">welcome to our world!</h1>
            <p class="about-paragraph">We are multi-discipline distribution company for premium food & beverage products with 40+ years of experience in food service in food service & retail channel.</p>
        </div>
    </div>
    <div class="flex flex-center">
        <div>
            <a href="{{ url('home') }}" class="btn"><img style="width: 17px;display:inline-block" src="{{ asset('main_page/cart-icon.png') }}" alt=""> <span>Online Shop</span></a>
            <a href="{{ url('main_site') }}" class="btn"><i class="fas fa-home"></i><span>Home</span></a>
        </div>
    </div>
    <div class="flex flex-center products-wrapper" style="background-image: url('{{ asset('main_page/products-bg.png') }}');background-size: contain">
        <div>
            <img class="products" width="1000" src="{{ asset('main_page/products.png') }}" alt="">
        </div>
    </div>
    <span class="hr"></span>
    <div class="flex flex-center">
        <div>
            <p class="copyright">All Rights Reserved MIDO &copy; 2019 Designed and Developed by <span>Vhorus</span></p>
        </div>
    </div>
</body>
</html>
