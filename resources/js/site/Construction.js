$('#Construction .gallory').hide(); //hide the slider

$('#Construction .main img').click(function(event) {

 var  images_string = $(this).attr('data-images_string');
 var  images_array  = images_string.split(',');
  //if there is images
 if(images_array[0])
 {
   let the_html = '';
   for (var i = 0; i < images_array.length; i++)
   {  console.log(images_array[i]);
     if( i ==0 ){
       the_html += `<div class="item" data-key="${i}" > `;
     }
     else {
       the_html += `<div class="item hidden" data-key="${i}" > `;
     }
      the_html += `   <img src="${images_array[i]}" alt="">
                   </div>`;
   }
   $('#Construction .gallory .slider').html(the_html);
   $('#Construction .gallory').show();
   $('#Construction .main').hide();
 }
 else { //if no images
   alert( 'no images' );
 }
});//End click

window.ConstructionGalloryArrowClicked = function(dir){
var selected_i = $('#Construction .gallory').find('.item[class!="item hidden"]').attr('data-key');  console.log('selected_i '+selected_i);

var len = $('#Construction .gallory .slider').find('.item').length ;

var goTo_i = 0;
if(dir == 'right')
{
   goTo_i =   parseInt(selected_i) + 1;
   if( len <= goTo_i ){
     goTo_i = 0;
   }
}
else if(dir == 'left')
{
   goTo_i =   parseInt(selected_i) - 1;
   if(  goTo_i < 0 ){
     goTo_i = parseInt(len) - 1;
   }
}
$('#Construction .gallory .slider').find('.item').addClass('hidden' , 1000);
$('#Construction .gallory .slider').find('.item[data-key='+goTo_i+']').removeClass('hidden' , 1000);
}

window.ConstructionGalloryClose = function(){
$('#Construction .gallory').hide();
$('#Construction .main').show();
}
