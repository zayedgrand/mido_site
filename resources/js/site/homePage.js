
  const homePage = new Vue({
      el: '#homePage',
      mounted(){
                              console.log('#homePage');
        this.initCarousel();
        this.initAbout_us();
      },
      methods:{
          initCarousel()
          {
              $('#homePage .carousel .item').not(':eq(0)').addClass('hidden');
              var len = $('#homePage .carousel').find('.item').length ;
              //----inset the dots---
              $('#homePage .carousel .dots').append(`<i class="fas fa-circle" onclick="homePage_carousel_change(0)" data-key='0' ></i>`);
              for (var i = 1; i < len; i++)
              {
                 $('#homePage .carousel .dots').append('<i class="far fa-circle" onclick="homePage_carousel_change('+i+')" data-key="'+i+'"></i>');
              }
          },
          homePage_carousel_change(i)
          {
              //if not the selected one
              if( ! $('#homePage .carousel .dots').find('i[data-key='+i+']').hasClass('fas') )
              {
                  var items = $('#homePage .carousel .item').find('h1');
                  items.addClass('go_down'); // h1 go down
                  setTimeout(function(){


                              $('#homePage .carousel .item').find('p').hide();
                              $('#homePage .carousel .item').find('h1').hide();
                              items.removeClass('go_down');

                              $('#homePage .carousel').find('.item[data-key!='+i+']').slideUp(800);
                              $('#homePage .carousel').find('.item[data-key='+i+']').slideDown(800,function(){

                                  $('#homePage .carousel').find('.item[data-key='+i+']').find('h1').show();

                                  $('#homePage .carousel').find('.item[data-key='+i+']').find('p').show();

                              });
                  }, 800);

                  //---change the dots----
                  $('#homePage .carousel .dots').find('i').removeClass('fas');
                  $('#homePage .carousel .dots').find('i[data-key!='+i+']').addClass('far');
                  $('#homePage .carousel .dots').find('i[data-key='+i+']').addClass('fas');
              }
          },
          initAbout_us()
          {
              $('#homePage .about_us .content p').not(':eq(0)').addClass('hidden');
              var len = $('#homePage .about_us .content').find('p').length ;
              //----inset the dots---
              $('#homePage .about_us .dots').append(`<i class="fas fa-circle" onclick="homePage_about_us_change(0)" data-key='0' ></i>`);
              for (var i = 1; i < len; i++)
              {
                 $('#homePage .about_us .dots').append('<i class="far fa-circle" onclick="homePage_about_us_change('+i+')" data-key="'+i+'"></i>');
              }
          },
          homePage_about_us_change(i)
          {
              if( ! $('#homePage .about_us .dots').find('i[data-key='+i+']').hasClass('fas') )
              {
                  $('#homePage .about_us').find('p').slideUp();
                  $('#homePage .about_us').find('p[data-key='+i+']').slideDown();
                  //---change the dots----
                  $('#homePage .about_us .dots').find('i').removeClass('fas');
                  $('#homePage .about_us .dots').find('i[data-key!='+i+']').addClass('far');
                  $('#homePage .about_us .dots').find('i[data-key='+i+']').addClass('fas');
              }
          },
          about_usArrowClicked(dir)
          {
              var selected_i = $('#homePage .about_us .dots i.fas').attr('data-key');
              var len = $('#homePage .about_us .content').find('p').length ;
              $('#homePage .about_us').find('p').slideUp(); // hidde all p
              $('#homePage .about_us .dots').find('i').removeClass('fas');
              var goTo_i = 0;
              if(dir == 'right')
              {
                  goTo_i =   parseInt(selected_i) + 1;
                  if( len <= goTo_i ){
                    goTo_i = 0;
                  }
              }
              else if(dir == 'left')
              {
                  goTo_i =   parseInt(selected_i) - 1;
                  if(  goTo_i < 0 ){
                    goTo_i = parseInt(len) - 1;
                  } console.log('left '+goTo_i);
              }
              //--replace content--
              $('#homePage .about_us .dots').find('i[data-key!='+goTo_i+']').addClass('far');
              $('#homePage .about_us .dots').find('i[data-key='+goTo_i+']').addClass('fas');
              $('#homePage .about_us').find('p[data-key='+goTo_i+']').slideDown();
          }
      }//End methods--
  });

//------------------------------------------------

  window.homePage_carousel_change=function(i){
      homePage.homePage_carousel_change(i);
  }
  window.homePage_about_us_change=function(i){
      homePage.homePage_about_us_change(i);
  }

  //-----------START projects hover------------
    $('#homePage .projects .inner > div').hover(function() {
        $(this).find(".insert_border").addClass('active');
        var link = $(this).find('img').attr('data-link');
        $(this).append('<a href="'+link+'"> Read More </a> ');
        $(this).find('img').css({'transform':'scale(1.2)'});
        $(this).find('p').hide();
    }, function() {
        $(this).find(".insert_border").removeClass('active');
        $(this).find('a').remove();
        $(this).find('img').css({'transform':'scale(1)'});
        $(this).find('p').show();
    });
 //-----------END projects hover------------

//----for make the slider move auto every 6 sec
 $(document).ready(function () {

     var imgs = $('#homePage .carousel .item');
     var z = 0;
     var previousImageId = "";

     $(imgs[0]).show();

     function loop()
     {
         var selected_i = $('#homePage .carousel .dots').find("i[class~='fas']").attr('data-key');
         var count = $('#homePage .carousel .dots').find("i").length ;
         selected_i = (selected_i == (count-1))? 0 : parseInt(selected_i)+1;
         homePage.homePage_carousel_change(selected_i);
         setTimeout(loop, 6000);
     }

     setTimeout(function(){  loop();  }, 6000);

 });

 //----------------
 // var items = $('#homePage .carousel .item').find('h1');
 // items.addClass('go_down'); // h1 go down
 // setTimeout(function(){
 //
 //             $('#homePage .carousel .item').find('p').hide();
 //
 //             $('#homePage .carousel').find('.item').hide();
 //
 //                 $('#homePage .carousel').find('.item[data-key!='+i+']').slideUp(800);
 //                 $('#homePage .carousel').find('.item[data-key='+i+']').slideDown(800,function(){
 //                     $('#homePage .carousel').find('.item[data-key='+i+']').find('h1').show();
 //                     $('#homePage .carousel').find('.item[data-key='+i+']').find('p').show();
 //
 //                 })
 //
 //             // $('#homePage .carousel').find('.item').slideUp(800,function(){
 //             //     // $('#homePage .carousel .item').find('h1').hide();
 //             //     items.removeClass('go_down');
 //             //
 //             //
 //             //     $('#homePage .carousel').find('.item[data-key='+i+']').slideDown(800,function(){
 //             //         $('#homePage .carousel').find('.item[data-key='+i+']').find('h1').show();
 //             //         $('#homePage .carousel').find('.item[data-key='+i+']').find('p').show();
 //             //
 //             //     });
 //             // });
 // }, 800);
 //
 // //---change the dots----
 // $('#homePage .carousel .dots').find('i').removeClass('fas');
 // $('#homePage .carousel .dots').find('i[data-key!='+i+']').addClass('far');
 // $('#homePage .carousel .dots').find('i[data-key='+i+']').addClass('fas');
