
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

try {
    // window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}



// window.Noty = require('noty');
//
// window.moment = require('moment');
//  moment().format(); moment.locale('en');


let token = document.head.querySelector('meta[name="csrf-token"]');

// if (token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//     console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }

window.Vue = require('vue');


// window.Noty = require('noty');

// window.owlCarousel = require('owl.carousel');

// window.moment = require('moment');
//  moment().format(); moment.locale('en');


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app'
// });


if( document.getElementById("homePage") ){
    require('./site/homePage');
}

if( document.getElementById("About_us") ){
    require('./site/About_us');
}

if( document.getElementById("Construction") ){
    require('./site/Construction');
}



//------for menu-----
$('#p_menu').click(function(event) {
   $(this).hide();
   $('#menu_content').show();
});

$('p.close_menu').click(function(event) {
   $('#menu_content').hide();
   $('#p_menu').show();
});

//-------------fix header on scroll
$(window).scroll(function() {
  if( $(window).width() > 800)
  {
      var $height = $(window).scrollTop();
      if($height > 600) {
    		$('header').addClass('fixed');
    	} else {
    		$('header').removeClass('fixed');
    	}
  }
});
