<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "site" middleware group. Now create something great!
|
*/

	function changeLang($lang)
	{
		\App::setLocale($lang);
	}
    Route::get('/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce',compact('lang'));
    });
    Route::get('/brand/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-brand',compact('lang'));
    });
    Route::get('/product/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-product',compact('lang'));
    });
	Route::get('/register/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-register',compact('lang'));
    });
    Route::get('/login/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-login',compact('lang'));
    });
    Route::get('/FAQ/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-FAQs',compact('lang'));
    });
    Route::get('/checkout/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-checkout',compact('lang'));
    });
    Route::get('/profile/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-profile',compact('lang'));
    });
    Route::get('/wishlist/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-wishlist',compact('lang'));
    });
    Route::get('/history/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-history',compact('lang'));
    });
    Route::get('/history-inner/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-history-inner',compact('lang'));
    });
    Route::get('/forget-password/{lang}',function($lang){
		changeLang($lang);
        return view('Design.ecommerce-forget-password',compact('lang'));
    });
    Route::get('/download/{lang}',function($lang){
		changeLang($lang);
        return view('Design.Download',compact('lang'));
    });
