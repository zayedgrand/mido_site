<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/



Auth::routes();
//----login----
Route::get('/login', 'Auth\LoginController@admin_login_form')->name('login');
Route::post('/login', 'Auth\LoginController@admin_login');


// Route::get('/', function () {
//     return redirect('Admin/login');
// });


Route::get('/', function () {
    if( auth::check() )
        return redirect('Admin/Members');
    else
        return redirect('Admin/login');
});





Route::get('/DashBoard', 'DashBoardController@index');


// Route::group(['prefix'=>'ContactUs' ],function(){
//       Route::get('/', 'ContactUsController@index');
//
//       //---apis---
//       Route::post('/list', 'ContactUsController@get_list');
//       Route::get('/switch_contacted/{id}', 'ContactUsController@switch_contacted');
//       Route::get('/delete/{id}', 'ContactUsController@destroy');
//       Route::post('/create', 'ContactUsController@store');
//       Route::post('/update', 'ContactUsController@update');
//       Route::get('/Export', 'ContactUsController@Export');
// });

// Route::group(['prefix'=>'AboutUs' ],function(){
//       Route::get('/', 'AboutUsController@index');
//
//       //---apis---
//       Route::post('/list', 'AboutUsController@get_list');
//       Route::get('/showORhide/{id}', 'AboutUsController@showORhide');
//       Route::get('/delete/{id}', 'AboutUsController@destroy');
//       Route::post('/create', 'AboutUsController@store');
//       Route::post('/update', 'AboutUsController@update');
// });

Route::group(['prefix'=>'Members' ],function(){
      Route::get('/', 'MemberController@index');

      //---apis---
      Route::post('/list', 'MemberController@get_list');
      Route::get('/showORhide/{id}', 'MemberController@showORhide');
      Route::get('/delete/{id}', 'MemberController@destroy');
      Route::post('/create', 'MemberController@store');
      Route::post('/update', 'MemberController@update');
});

Route::group(['prefix'=>'City' ],function(){
      Route::get('/', 'CityController@index');

      //---apis---
      Route::post('/list', 'CityController@get_list');
      Route::get('/showORhide/{id}', 'CityController@showORhide');
      Route::get('/delete/{id}', 'CityController@destroy');
      Route::post('/create', 'CityController@store');
      Route::post('/update', 'CityController@update');
});

Route::group(['prefix'=>'PopularQuestion' ],function(){
      Route::get('/', 'PopularQuestionController@index');

      //---apis---
      Route::post('/list', 'PopularQuestionController@get_list');
      Route::get('/showORhide/{id}', 'PopularQuestionController@showORhide');
      Route::get('/delete/{id}', 'PopularQuestionController@destroy');
      Route::post('/create', 'PopularQuestionController@store');
      Route::post('/update', 'PopularQuestionController@update');
});

Route::group(['prefix'=>'PdfDownloads' ],function(){
      Route::get('/', 'PdfDownloadsController@index');

      //---apis---
      Route::post('/list', 'PdfDownloadsController@get_list');
      Route::get('/showORhide/{id}', 'PdfDownloadsController@showORhide');
      Route::get('/delete/{id}', 'PdfDownloadsController@destroy');
      Route::post('/create', 'PdfDownloadsController@store');
      Route::post('/update', 'PdfDownloadsController@update');
});

Route::group(['prefix'=>'PromoCodes' ],function(){
      Route::get('/', 'PromoCodesController@index');

      //---apis---
      Route::post('/list', 'PromoCodesController@get_list');
      Route::get('/showORhide/{id}', 'PromoCodesController@showORhide');
      Route::get('/delete/{id}', 'PromoCodesController@destroy');
      Route::post('/create', 'PromoCodesController@store');
      Route::post('/update', 'PromoCodesController@update');
});

Route::group(['prefix'=>'HomePage' ],function(){
      Route::get('/', 'HomePageController@index');

      //---apis---
      Route::post('/list', 'HomePageController@get_list');
      Route::get('/showORhide/{id}', 'HomePageController@showORhide');
      Route::get('/delete/{id}', 'HomePageController@destroy');
      Route::post('/updateRowsPosition', 'HomePageController@updateRowsPosition');
});

Route::group(['prefix'=>'Recipt' ],function(){
      Route::get('/', 'ReciptController@index');
      Route::get('/{id}', 'ReciptController@show');
      Route::get('print/{id}', 'ReciptController@print_show');
      //---apis---
      Route::post('/list', 'ReciptController@get_list');
      Route::get('/switchPiad/{id}', 'ReciptController@switchPiad');
      Route::get('/changeDeliveryStatus/{id}/{status}', 'ReciptController@changeDeliveryStatus');
      Route::get('/delete/{id}', 'ReciptController@destroy');
});



Route::group(['prefix'=>'PagesBanner' ],function(){
      Route::get('/', 'PagesBannerController@index');

      //---apis---
      Route::post('/list', 'PagesBannerController@get_list');
      Route::post('/update', 'PagesBannerController@update');
});

Route::group(['prefix'=>'HomeSlider' ],function(){
      Route::get('/', 'HomeSliderController@index');

      //---apis---
      Route::post('/list', 'HomeSliderController@get_list');
      Route::get('/showORhide/{id}', 'HomeSliderController@showORhide');
      Route::get('/delete/{id}', 'HomeSliderController@destroy');
      Route::post('/create', 'HomeSliderController@store');
      Route::post('/update', 'HomeSliderController@update');
});

Route::group(['prefix'=>'Product' ],function(){
      Route::get('/', 'ProductController@index');
      Route::get('/create', 'ProductController@create');
      Route::get('/edit/{id}', 'ProductController@edit');
      Route::get('/sort', 'ProductController@sort_view');
      Route::post('/list_without_paginate', 'ProductController@list_without_paginate');
      Route::post('/updateRowsPosition', 'ProductController@updateRowsPosition');
      Route::patch('/{id}', 'ProductController@update');
      Route::post('/', 'ProductController@store');

      //---apis---
      Route::post('/list', 'ProductController@get_list');
      Route::get('/showORhide/{id}', 'ProductController@showORhide');
      Route::get('/delete/{id}', 'ProductController@destroy');
});

Route::group(['prefix'=>'Brand' ],function(){
      Route::get('/', 'BrandController@index');
      Route::post('/update', 'BrandController@update');
      Route::post('/create', 'BrandController@store');

      //---apis---
      Route::post('/list', 'BrandController@get_list');
      Route::get('/showORhide/{id}', 'BrandController@showORhide');
      Route::get('/switchinHomePage/{id}', 'BrandController@brand_switchinHomePage');
      Route::get('/delete/{id}', 'BrandController@destroy');
});

Route::group(['prefix'=>'Category' ],function(){
      Route::get('/', 'CategoryController@index');
      Route::get('/create', 'CategoryController@create');
      Route::get('/edit/{id}', 'CategoryController@edit');
      Route::patch('/{id}', 'CategoryController@update');
      Route::post('/', 'CategoryController@store');

      //---apis---
      Route::post('/list', 'CategoryController@get_list');
      Route::get('/showORhide/{id}', 'CategoryController@showORhide');
      Route::get('subCategoiry/switchinHomePage/{id}', 'CategoryController@SubCategory_switchinHomePage');
      Route::get('/delete/{id}', 'CategoryController@destroy');
      Route::get('/subCategoiry/delete/{id}', 'CategoryController@subCategoiryDestroy');
});


Route::group(['prefix'=>'FreeTestList' ],function(){
      Route::get('/', 'FreeTestListController@index');

      //---apis---
      Route::post('/list', 'FreeTestListController@get_list');
      Route::get('/showORhide/{id}', 'FreeTestListController@showORhide');
      Route::get('/delete/{id}', 'FreeTestListController@destroy');
      Route::post('/create', 'FreeTestListController@store');
      Route::post('/update', 'FreeTestListController@update');
});

//=================Setting==========================
Route::group(['prefix'=>'Setting' ],function(){
    Route::get('/list', 'SettingController@list');
    Route::get('/', 'SettingController@index');
    Route::post('/save', 'SettingController@store');
});


//=================Role==========================
Route::group(['prefix'=>'Role'],function(){
    Route::resource('/', 'RoleController');
    Route::get('/edit/{id}', 'RoleController@edit');
    Route::patch('/{id}', 'RoleController@update');
    Route::get('/search/{val}', 'RoleController@search');
});

Route::group(['prefix'=>'Admins'],function(){
    Route::resource('/', 'AdminController');
    Route::get('/edit/{id}', 'AdminController@edit');
    Route::patch('/{id}', 'AdminController@update');
    Route::get('/search/{val}', 'AdminController@search');
    Route::get('/delete/{id}', 'AdminController@destroy');
});
