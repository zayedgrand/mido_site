<?php

/*
|--------------------------------------------------------------------------
| site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "site" middleware group. Now create something great!
|
*/
Route::get('/c/c/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('route:clear');
    // return what you want
});

  Route::get('/', function(){  return view('Site.main');  });

  Route::get('/home', 'HomeController@index');

  // Route::get('/home', function(){  return redirect('');  });

Route::get('/shop', 'ShopListController@index');

Route::post('/checkout', 'ReciptController@checkout');
Route::get('/payment_page/{order_id}', 'ReciptController@payment_page');
Route::get('/confirm_is_paid/{order_id}/{security_code}', 'ReciptController@confirm_is_paid');

Route::get('/FAQ', 'PopularQuestionController@index');
Route::get('/PdfDownloads', 'PdfDownloadsController@index');


Route::get('/category/{SubCategory_id}-{slug}', 'ProductController@SubCategoryIndex');
Route::post('/search', 'ProductController@searchPage');
Route::get('brand/{Brand_id}-{slug}', 'ProductController@BrandIndex');
Route::get('/product/{product_id}', 'ProductController@show');


Route::group(['prefix'=>'brand' ],function(){
    Route::get('/list', 'BrandController@brandListPage');
    Route::get('/{id}', 'BrandController@index');
    Route::post('', 'BrandController@productsList');
});

// Route::group(['prefix'=>'product' ],function(){
//     Route::get('/{id}', 'ProductController@show');
// });




Route::group(['prefix'=>'productsOf' ],function(){
    //--api--
    Route::get('/list/{type}/{SubCategory_id}', 'ProductController@get_products_list');
    Route::get('/list_search', 'ProductController@get_products_list_search');
    Route::get('/details/{product_id}', 'ProductController@show');
});

Route::group(['prefix'=>'ShoppingCart' ],function(){
    Route::get('/', 'ShoppingCartController@index');
    //--api--
    Route::get('list', 'ShoppingCartController@list');
    Route::post('add', 'ShoppingCartController@add');
    Route::get('remove/{id}', 'ShoppingCartController@remove');
    Route::get('getCount', 'ShoppingCartController@get_Count_number');
});

Route::group(['prefix'=>'History' ],function(){
    Route::get('/', 'HistoryController@index');
    Route::get('/details/{id}', 'HistoryController@show');
    //--api--
    Route::post('/list', 'HistoryController@list');
    Route::get('/cancle_order/{Recipt_id}', 'HistoryController@cancle_order');
});

Route::group(['prefix'=>'WishList' ],function(){
    Route::get('/', 'WishListController@index');
    //--api--
    Route::get('/list', 'WishListController@list');
    Route::get('/addOrRemove/{product_id}', 'WishListController@addOrRemove');
});

Route::group(['prefix'=>'promo' ],function(){
    Route::post('/add', 'PromoCodesController@add_promo');
    Route::get('/get_code_percentage_value', 'PromoCodesController@get_code_percentage_value');

});

Route::get('/setLang/{lang}', function($lang){
    \Session::put('lang',$lang);
    return back();
});


//----auth----


Route::group(['prefix'=>'Auth' ],function(){
    Route::get('/login', 'AuthController@loginForm');
    Route::get('/register', 'AuthController@registerForm');
    Route::get('/forgetPassword', 'AuthController@forgetPassword_Form');
    Route::post('/register', 'AuthController@register');
    Route::post('/login', 'AuthController@login');
    Route::post('/updatePrpfile', 'AuthController@update');
    Route::post('/add_new_address', 'AuthController@add_new_address');
});


Route::group(['prefix'=>'member' ],function(){
    Route::get('/edit', 'AuthController@edit');
    Route::patch('/update/{id}', 'AuthController@update');
    Route::get('/resite_password_form/{forget_password_code}', 'AuthController@resite_password_form');
    Route::get('/logout', 'AuthController@logout');
    Route::post('/send_forget_password_email', 'AuthController@send_forget_password_email');
    Route::post('/reset_password', 'AuthController@reset_password');
});

Route::get('category_slider/{brand_id}', 'CategorySliderController@SliderPage');


              // //sub-brand page
              // Route::get('category/elite',function(){
              //     $lang = 'en';
              //     return view('Site.sub_brand.site-subBrand',compact('lang'));
              // });
              // Route::get('category/saula',function(){
              //     $lang = 'en';
              //     return view('Site.sub_brand.site-subBrand2',compact('lang'));
              // });
              // Route::get('category/farha',function(){
              //     $lang = 'en';
              //     return view('Site.sub_brand.site-subBrand3',compact('lang'));
              // });
              // Route::get('category/chicohend',function(){
              //     $lang = 'en';
              //     return view('Site.sub_brand.site-subBrand4',compact('lang'));
              // });

Route::group(['prefix'=>'Contact_us' ],function(){
    Route::get('/', 'ContactUsController@index');
    Route::post('/', 'ContactUsController@make_Contact');
});


Route::get('/About_us', 'AboutUsController@index');
Route::get('/location', 'LocationController@index');

Route::post('/make_register', 'HomeController@make_register');
Route::post('/make_Contact', 'ContactUsController@make_Contact');

Route::get('/make_old_price_eq_price', function(){
      $products = \App\Product::get();
      foreach ($products as $key => $product)
      {
          $product->update([
             'old_price' => $product->price ,
             'discount_percentage' => '0'
          ]);
      }
});

Route::get('/test', function(){
    return view('Site.test');
});

// Route::group(['prefix'=>'Course'],function(){
//     Route::get('/', 'CourseController@index');
//     Route::get('/calender', 'CourseController@calender');
//     Route::get('/{id}', 'CourseController@show');
// });
