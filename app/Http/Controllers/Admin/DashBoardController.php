<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;
use App\Product;
use App\Recipt;
use App\Brand;

class DashBoardController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        $Members_count = Member::count();
        $Products_count = Product::count();
        $Recipts_count = Recipt::count();
        $Brands_count = Brand::count();

        $Recipt_processing_count = Recipt::where('delivery_status','processing')->count();
        $Recipt_shipping_count = Recipt::where('delivery_status','shipping')->count();
        $Recipt_delivered_count = Recipt::where('delivery_status','delivered')->count();
        $Recipt_canceled_count = Recipt::where('delivery_status','canceled')->count();


        $morris_recipts = \DB::select("SELECT count(id) as count , month(`created_at`) as month FROM `recipts` WHERE YEAR(`created_at`) = ".date("Y")." group BY month(`created_at`) ORDER by `created_at` ASC") ;
        $morris_members = \DB::select("SELECT count(id) as count , month(`created_at`) as month FROM `members` WHERE YEAR(`created_at`) = ".date('Y')." group BY month(`created_at`) ORDER by `created_at` ASC") ;


        return view('Admin.DashBoard.DashBoard',compact('Members_count','Products_count','Recipts_count','Brands_count',
                                                        'Recipt_processing_count','Recipt_shipping_count',
                                                        'Recipt_delivered_count','Recipt_canceled_count','morris_recipts','morris_members' ));
    }
}
