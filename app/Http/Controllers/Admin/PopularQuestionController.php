<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PopularQuestion;

class PopularQuestionController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        return view('Admin.PopularQuestion.index');
    }

    //---api----
    public function get_list(Request $request)
    {
         $search = $request->search;
         return PopularQuestion::
          where(function($q)use($search){
              if ($search)
                $q->where('question_en','like','%'.$search.'%')->orWhere('question_ar','like','%'.$search.'%')
                ->orWhere('answer_en','like','%'.$search.'%')->orWhere('answer_ar','like','%'.$search.'%')
                ->orWhere('id',$search);
          })
          ->latest('id')->paginate();
    }

    public function store(Request $request)
    {
          $validator = \Validator::make($request->all(), [
              'question_en' => 'required',
              'question_ar' => 'required',
              'answer_ar' => 'required',
              'answer_en' => 'required',
          ]);
          if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }
          $item = PopularQuestion::create($request->except('_token'));
          $item->status = 1;
          return response()->json([
            'status' => 'success',
            'data' => $item
          ]);
    }

    //--api--
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
            'question_en' => 'required',
            'question_ar' => 'required',
            'answer_ar' => 'required',
            'answer_en' => 'required',
        ]);
        if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

        $item = PopularQuestion::findOrFail($request->id);
        $item->update($request->except('_token'));
        return response()->json([
          'status' => 'success',
          'data' => $item
        ]);
    }

    //--api--
    public function showORhide($id)
    {
         $item = PopularQuestion::findOrFail($id);
         if( $item->status )
         {
            $item->update(['status' => '0']);
            $case = 0;
         }
         else
         {
            $item->update(['status' => '1']);
            $case = 1;
         }

         return response()->json([
             'status' => 'success',
             'case' => $case
         ]);
    }

    //--api--
    public function destroy($id)
    {
         try {
           $deleted = PopularQuestion::destroy($id);
         } catch (\Exception $e) {
           return 'false';
         }
         return 'true';
    }

}
