<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Product;
use App\ProductImages;

use App\Brand;
use App\Category;
use App\SubCategory;

class ProductController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    { 
        $Categories_and_sub = Category::select('name_en as label','id as value')->get();

        foreach ($Categories_and_sub as $key => $Category)
        {
            $Category->subCategory = SubCategory::where('category_id',$Category->value)
                                                ->select('name_en as label','id as value')->get();
            foreach ($Category->subCategory as $key => $subCategory)
            {
                $subCategory->brand = Brand::select('name_en as label','id as value')
                            ->where('category_id',$Category->value)->where('sub_category_id',$subCategory->value)->get();
            }
        }
        return view('Admin.Product.index',compact('Categories_and_sub'));
    }

    public function list_without_paginate()
    {
        return Product::select('id','name_en as name','price','position','status')->orderBy('position')->get();
    }

    public function sort_view()
    {
        return view('Admin.Product.sort' );
    }

    public function create()
    {
        $Categories_and_sub = Category::select('name_en as label','id as value')->get();

        foreach ($Categories_and_sub as $key => $Category)
        {
            $Category->subCategory = SubCategory::where('category_id',$Category->value)
                                                ->select('name_en as label','id as value')->get();
            foreach ($Category->subCategory as $key => $subCategory)
            {
                $subCategory->brand = Brand::select('name_en as label','id as value')
                            ->where('category_id',$Category->value)->where('sub_category_id',$subCategory->value)->get();
            }
        }
        return view('Admin.Product.create',compact('Categories_and_sub'));
    }

    public function edit($id)
    {
        $Product = Product::findOrFail($id);
        $ProductImages = ProductImages::where('Product_id',$id)->get();
        $Categories_and_sub = Category::select('name_en as label','id as value')->get();

        foreach ($Categories_and_sub as $key => $Category)
        {
            $Category->subCategory = SubCategory::where('category_id',$Category->value)
                                                ->select('name_en as label','id as value')->get();
            foreach ($Category->subCategory as $key => $subCategory)
            {
                $subCategory->brand = Brand::select('name_en as label','id as value')
                            ->where('category_id',$Category->value)->where('sub_category_id',$subCategory->value)->get();
            }
        }

          $Product->description_en =  addcslashes( $Product->description_en, "\n" );
          $Product->description_en =  addcslashes( $Product->description_en, "\r" );
          $Product->description_ar =  addcslashes( $Product->description_ar, "\n" );
          $Product->description_ar =  addcslashes( $Product->description_ar, "\r" );

          // $Product->description_en =  '';
          // $Product->description_ar =  '';

              // return $Product;

        return view('Admin.Product.edit',compact( 'Product','ProductImages','Categories_and_sub' ));
    }

    //---api----
    public function get_list(Request $request)
    {
         $search = $request->search;
         $brand_id = $request->brand_id;
         $category_id = $request->category_id;
         $sub_category_id = $request->sub_category_id;
         // return Product::paginate();
         return Product::select('products.*',
              \DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image"),
              "brands.name_en as brand_name" , "categories.name_en as category_name","sub_categories.name_en as sub_category_name"
           )
          ->where(function($q)use($brand_id,$category_id,$sub_category_id){
              if($brand_id){
                  $q->where('products.brand_id',$brand_id);
              }
              if($category_id){
                  $q->where('products.category_id',$category_id);
              }
              if($sub_category_id){
                  $q->where('products.sub_category_id',$sub_category_id);
              }
          })
          ->where(function($q)use($search){
            if ($search)
              $q->where('products.name_en','like','%'.$search.'%')->orWhere('products.name_ar','like','%'.$search.'%')
                ->orWhere('short_description_en','like','%'.$search.'%')->orWhere('short_description_ar','like','%'.$search.'%')
                ->orWhere('description_en','like','%'.$search.'%')->orWhere('description_ar','like','%'.$search.'%')
                ->orWhere('price','like','%'.$search.'%')->orWhere('products.quantity','like','%'.$search.'%')
                ->orWhere('products.id',$search);
          })
          ->join('product_images','product_images.product_id','products.id')
          ->join('brands','brands.id','products.brand_id')
          ->join('categories','categories.id','products.category_id')
          ->join('sub_categories','sub_categories.id','products.sub_category_id')
          ->groupBy('products.id')
          ->orderBy('position')
          ->paginate();
    }

    public function store(Request $request)
    {
          $data = $request->validate([
              'category_id' => 'required',
              'sub_category_id' => 'required',
              'brand_id' => 'required',
              'name_en' => 'required',
              'name_ar' => 'required',
              'short_description_en' => 'required',
              'short_description_ar' => 'required',
              'description_en' => 'required',
              'description_ar' => 'required',
              'price' => 'required',
              'old_price' => 'required',
              'discount_percentage' => 'required',
              'quantity' => 'required',
              'images.*' => 'max:400'
          ]);

          $Product = Product::create($data);

            //---ProductImages---
          if($request->images_no)
          {
              for ($i=0; $i < count($request->images_no) ; $i++)
              {
                 if(isset($request->images[$i]))
                 {
                      ProductImages::create([
                         'product_id' => $Product->id ,
                         'image' => $request->images[$i]
                      ]);
                 }
              }
          }//End: if($request->images_no)

          \Session::flash('flash_message',' Product has been created ');
          return redirect('Admin/Product');
    }

    //--api--
    public function update(Request $request,$id)
    {
        $data = $request->validate([
          'category_id' => 'required',
          'sub_category_id' => 'required',
          'brand_id' => 'required',
          'name_en' => 'required',
          'name_ar' => 'required',
          'short_description_en' => 'required',
          'short_description_ar' => 'required',
          'description_en' => 'required',
          'description_ar' => 'required',
          'price' => 'required',
          'old_price' => 'required',
          'discount_percentage' => 'required',
          'quantity' => 'required',
          'images.*' => 'max:400'
        ]);

        $item = Product::findOrFail($id);

        $item->update($data);

        if($request->old_images_ids){
          ProductImages::where('product_id',$id)->whereNotIn('id',$request->old_images_ids)->delete();
        }
        else {
          ProductImages::where('product_id',$id)->delete();
        }

          //---ProductImages---
        if($request->images_no)
        {
            for ($i=0; $i < count($request->images_no) ; $i++)
            {
               if(isset($request->images[$i]))
               {
                   if(isset($request->old_images_ids[$i]))
                   {
                       $pImage = ProductImages::find($request->old_images_ids[$i]);
                       if($pImage)
                       {
                           $pImage->update([
                              'product_id' => $id ,
                              'image' => $request->images[$i]
                           ]);
                       }
                       else  {
                           ProductImages::create([
                              'product_id' => $id ,
                              'image' => $request->images[$i]
                           ]);
                       }
                   }
                   else {
                       ProductImages::create([
                          'product_id' => $id ,
                          'image' => $request->images[$i]
                       ]);
                   }
               }//End  if(isset($request->images[$i]))
            }//End  for ($i=0; $i < count($request->images_no) ; $i++)
        }//End: if($request->images_no)

        \Session::flash('flash_message',' Product has been updated ');
        return redirect('Admin/Product');
    }

    public function updateRowsPosition(Request $request)
    {
          $validator = \Validator::make($request->all(), [
              'postionArray' => 'required',
          ]);
          if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

          foreach($request->postionArray as $key => $value)
          {
              Product::find($value)->update([
                'position' => (1+$key)
              ]);
          }
          return response()->json([
            'status' => 'success',
          ]);
    }

    //--api--
    public function showORhide($id)
    {
         $item = Product::findOrFail($id);
         if( $item->status )
         {
            $item->update(['status' => '0']);
            $case = 0;
         }
         else
         {
            $item->update(['status' => '1']);
            $case = 1;
         }


         return response()->json([
             'status' => 'success',
             'case' => $case
         ]);
    }

    //--api--
    public function destroy($id)
    {
         try {
           $deleted = Product::destroy($id);
           \App\ShoppingCart::where('product_id',$id)->delete();
           \App\WishList::where('product_id',$id)->delete();
         } catch (\Exception $e) {
           return 'false';
         }
         return 'true';
    }

}
