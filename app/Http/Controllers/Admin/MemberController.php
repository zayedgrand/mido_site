<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Member;

class MemberController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        $cities = \App\City::select('name_en as label','id as value')->get(); //return $cities;
        return view('Admin.Members.index',compact('cities'));
    }

    //---api----
    public function get_list(Request $request)
    {
         $search = $request->search;
         $gender = $request->gender;
         $city_id = $request->city_id;
         return Member::select('members.*','cities.name_en as city_name', \DB::raw("GROUP_CONCAT(member_address.address) as all_address") )
          ->where(function($q)use($search,$gender,$city_id){
              if($gender){
                $q->where('gender',$gender);
              }
              if($city_id){
                $q->where('city_id',$city_id);
              }
              if ($search)
                $q->where('name','like','%'.$search.'%')->orWhere('phone','like','%'.$search.'%')
                  ->orWhere('email','like','%'.$search.'%')->orWhere('members.id',$search);
          })
          ->join('cities','cities.id','members.city_id')
          ->leftJoin('member_address','member_address.member_id','members.id')
          ->groupBy('members.id')
          ->latest('members.id')->paginate();
    }

    public function store(Request $request)
    {
          $validator = \Validator::make($request->all(), [
              'name_en' => 'required',
              'name_ar' => 'required',
          ]);
          if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }
          $item = Member::create($request->except('_token'));
          $item->status = 1;
          return response()->json([
            'status' => 'success',
            'data' => $item
          ]);
    }

    //--api--
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required',
        ]);
        if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

        $item = Member::findOrFail($request->id);
        $item->update($request->except('_token'));
        return response()->json([
          'status' => 'success',
          'data' => $item
        ]);
    }

    //--api--
    public function showORhide($id)
    {
         $item = Member::findOrFail($id);
         if( $item->status )
         {
            $item->update(['status' => '0']);
            $case = 0;
         }
         else
         {
            $item->update(['status' => '1']);
            $case = 1;
         }

         return response()->json([
             'status' => 'success',
             'case' => $case
         ]);
    }

    //--api--
    public function destroy($id)
    {
         try {
           $deleted = Member::destroy($id);
         } catch (\Exception $e) {
           return 'false';
         }
         return 'true';
    }

}
