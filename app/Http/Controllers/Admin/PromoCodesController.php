<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PromoCodes;

class PromoCodesController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        return view('Admin.PromoCodes.index');
    }

    //---api----
    public function get_list(Request $request)
    {
         $search = $request->search;
         return PromoCodes::
          where(function($q)use($search){
              if ($search)
                $q->where('code','like','%'.$search.'%')->orWhere('discount_percentage','like','%'.$search.'%')->orWhere('id',$search);
          })
          ->latest('id')->paginate();
    }

    public function store(Request $request)
    {
          $validator = \Validator::make($request->all(), [
              'discount_percentage' => 'required',
              'code' => 'required|unique:promo_codes',
              'from_date' => 'required',
              'to_date' => 'required',
          ]);
          if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }
          // $request->merge(['code'=> rand(11111,99999) ]);
          $item = PromoCodes::create($request->except('_token'));
          $item->status = 1;
          return response()->json([
            'status' => 'success',
            'data' => $item
          ]);
    }

    //--api--
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
            'discount_percentage' => 'required',
            'code' => 'required|unique:promo_codes,code,'.$request->id,
            'from_date' => 'required',
            'to_date' => 'required',
        ]);
        if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

        $item = PromoCodes::findOrFail($request->id);
        // $request->merge(['code'=> rand(11111,99999) ]);
        $item->update($request->except('_token'));
        return response()->json([
          'status' => 'success',
          'data' => $item
        ]);
    }

    //--api--
    public function showORhide($id)
    {
         $item = PromoCodes::findOrFail($id);
         if( $item->status )
         {
            $item->update(['status' => '0']);
            $case = 0;
         }
         else
         {
            $item->update(['status' => '1']);
            $case = 1;
         }

         return response()->json([
             'status' => 'success',
             'case' => $case
         ]);
    }

    //--api--
    public function destroy($id)
    {
         try {
           $deleted = PromoCodes::destroy($id);
         } catch (\Exception $e) {
           return 'false';
         }
         return 'true';
    }

}
