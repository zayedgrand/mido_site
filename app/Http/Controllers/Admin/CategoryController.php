<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;
use App\SubCategory;
use App\HomePage;

class CategoryController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        return view('Admin.Category.index');
    }

    public function create()
    {
        return view('Admin.Category.create');
    }

    public function edit($id)
    {
        $Category = Category::findOrFail($id);
        $SubCategory = SubCategory::select('*',\DB::raw("CONCAT('cc_',id) as cc"))->where('category_id',$id)->get();
        return view('Admin.Category.edit',compact('Category','SubCategory'));
    }

    //---api----
    public function get_list(Request $request)
    {
         $search = $request->search;
         return Category::
          where(function($q)use($search){
              if ($search)
                $q->where('name_en','like','%'.$search.'%')->orWhere('name_ar','like','%'.$search.'%')->orWhere('id',$search);
          })
          ->latest('id')->paginate();
    }

    public function store(Request $request)
    {
          $data = $request->validate([
            'logo' => '',
            'name_en' => 'required',
            'name_ar' => 'required',
            'subCat_banner_en.*' => 'max:600',
            'subCat_banner_ar.*' => 'max:600'
          ]);

          $Category = Category::create($data);

         //---BrandImages---
          if($request->cats_no)
          {
              for ($i=0; $i < count($request->cats_no) ; $i++)
              {
                if( $request->subCat_name_en[$i] && $request->subCat_name_ar[$i] ){
                    SubCategory::create([
                       'category_id' => $Category->id ,
                       'name_en' => $request->subCat_name_en[$i] ,
                       'name_ar' => $request->subCat_name_ar[$i] ,
                       'banner_en' => $request->subCat_banner_en[$i]??'',
                       'banner_ar' => $request->subCat_banner_ar[$i]??'',
                    ]);
                  }
              }
          }//End: if($request->images_no)

          \Session::flash('flash_message',' Category has been created ');
          return redirect('Admin/Category');
    }


    public function update(Request $request,$id)
    {
        $data = $request->validate([
            'logo' => '',
            'name_en' => 'required',
            'name_ar' => 'required',
            'subCat_banner_en.*' => 'max:600',
            'subCat_banner_ar.*' => 'max:600'
        ]);

        $Category = Category::findOrFail($id);

        $Category->update($data);

          //---BrandImages---
        if($request->cats_no)
        {
            for ($i=0; $i < count($request->cats_no) ; $i++)
            {
                if( $request->subCat_name_en[$i] && $request->subCat_name_ar[$i] )
                {
                      $update_array = [
                        'category_id' => $id ,
                        'name_en' => $request->subCat_name_en[$i] ,
                        'name_ar' => $request->subCat_name_ar[$i] ,
                      ];
                      if(isset($request->subCat_banner_en[$i]))
                          $update_array['banner_en'] = $request->subCat_banner_en[$i];

                      if(isset($request->subCat_banner_ar[$i]))
                         $update_array['banner_ar'] = $request->subCat_banner_ar[$i];

                      if(isset($request->old_cats_ids[$i])) {
                           SubCategory::find($request->old_cats_ids[$i])->update($update_array);
                      }
                      else {
                          SubCategory::create($update_array);
                      }
                }//End if
            }//End for
        }//End: if($request->images_no)

        \Session::flash('flash_message',' Category has been updated ');
        return redirect('Admin/Category');
    }

    //--api--
    public function showORhide($id)
    {
         $item = Category::findOrFail($id);
         if( $item->status )
         {
            $item->update(['status' => '0']);
            $case = 0;
         }
         else
         {
            $item->update(['status' => '1']);
            $case = 1;
         }

         return response()->json([
             'status' => 'success',
             'case' => $case
         ]);
    }

    public function SubCategory_switchinHomePage($id)
    {
         $item = SubCategory::findOrFail($id);
         $check = HomePage::where('type','subCategory')->where('relation_id',$id)->first();
         if(!$check)
         {
            HomePage::create([
              'type' => 'subCategory',
              'relation_id' => $id ,
              'position' =>  HomePage::max('position') + 1
            ]);
         }

         return response()->json([
             'status' => 'success',
             'case' => 1
         ]);
    }


    //--api--
    public function destroy($id)
    {
         try {
           $deleted = Category::destroy($id);
         } catch (\Exception $e) {
           return 'false';
         }
         return 'true';
    }


    public function subCategoiryDestroy($id)
    {
        try {
          $deleted = SubCategory::destroy($id);
        } catch (\Exception $e) {
          return 'false';
        }
        return 'true';
    }

}
