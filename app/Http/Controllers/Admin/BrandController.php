<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Brand;
use App\Category;
use App\SubCategory;
use App\HomePage;

class BrandController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        $categoiry_subCategory = Category::select('name_en as label','id as value')->get();
        foreach ($categoiry_subCategory as $key => $categoiry)
        {
           $categoiry->subCategoiry = SubCategory::select('name_en as label','id as value')
                                            ->where('category_id',$categoiry->value)->get();
        }

        return view('Admin.Brand.index',compact('categoiry_subCategory'));
    }

    public function create()
    {
        return view('Admin.Brand.create');
    }

    public function edit($id)
    {
        $Brand = Brand::findOrFail($id);
        $Category = Category::select('*',\DB::raw("'cc_".rand(11111,99999)."' as cc"))->where('brand_id',$id)->get();
        return view('Admin.Brand.edit',compact('Brand','Category'));
    }

    //---api----
    public function get_list(Request $request)
    {
         $search = $request->search;
         $Category_id = $request->Category_id;
         $SubCategory_id = $request->SubCategory_id;
         return Brand::
          where(function($q)use($search,$Category_id,$SubCategory_id){
              $q->where('category_id',$Category_id)->where('sub_category_id',$SubCategory_id);
              if ($search)
                $q->where('name_en','like','%'.$search.'%')->orWhere('name_ar','like','%'.$search.'%')->orWhere('id',$search);
          })
          ->latest('id')->paginate();
    }

    public function store(Request $request)
    {
          $validator = \Validator::make($request->all(), [
            'logo' => '',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'banner_en' => 'required',
            'banner_ar' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required',
            'og_description_en' => '',
            'og_description_ar' => '',
          ]);
          if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

          $Brand = Brand::create($request->except('_token'));
          $Brand->status = 1;

          return response()->json([
            'status' => 'success',
            'data' => $Brand
          ]);
    }


    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
          'id' => 'required',
          'logo' => '',
          'category_id' => 'required',
          'sub_category_id' => 'required',
          'banner_en' => '',
          'banner_ar' => '',
          'name_en' => 'required',
          'name_ar' => 'required',
          'og_description_en' => '',
          'og_description_ar' => '',
        ]);
        if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

        $Brand = Brand::findOrFail($request->id);

        $Brand->update($request->except('_token'));

        return response()->json([
          'status' => 'success',
          'data' => $Brand
        ]);
    }

    //--api--
    public function showORhide($id)
    {
         $item = Brand::findOrFail($id);
         if( $item->status )
         {
            $item->update(['status' => '0']);
            $case = 0;
         }
         else
         {
            $item->update(['status' => '1']);
            $case = 1;
         }

         return response()->json([
             'status' => 'success',
             'case' => $case
         ]);
    }

    public function brand_switchinHomePage($id)
    {
         $item = Brand::findOrFail($id);
         $check = HomePage::where('type','brand')->where('relation_id',$id)->first();
         if(!$check)
         {
            HomePage::create([
              'type' => 'brand',
              'relation_id' => $id ,
              'position' =>  HomePage::max('position') + 1
            ]);
         }

         return response()->json([
             'status' => 'success',
             'case' => 1
         ]);
    }


    //--api--
    public function destroy($id)
    {
         try {
           $deleted = Brand::destroy($id);
         } catch (\Exception $e) {
           return 'false';
         }
         return 'true';
    }


    public function deleteDestroy($id)
    {
        try {
          $deleted = Category::destroy($id);
        } catch (\Exception $e) {
          return 'false';
        }
        return 'true';
    }

}
