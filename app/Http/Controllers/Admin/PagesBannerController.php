<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PagesBanner;

class PagesBannerController extends Controller
{
      public function __construct()
      {
         $this->middleware('auth');
      }

      public function index()
      {
          return view('Admin.PagesBanner.index');
      }

      //---api----
      public function get_list(Request $request)
      {
           $search = $request->search;
           return PagesBanner::
            where(function($q)use($search){
                if ($search)
                  $q->where('page','like','%'.$search.'%')->orWhere('id',$search);
            })
            ->latest('id')->paginate();
      }

      //--api--
      public function update(Request $request)
      {
          $validator = \Validator::make($request->all(), [
              'id' => 'required',
              'imagear' => '',
              'imageen' => '',
              'body' => '',
          ]);
          if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

          $item = PagesBanner::findOrFail($request->id);
          $item->update($request->except('_token'));
          return response()->json([
            'status' => 'success',
            'data' => $item
          ]);
      }


}
