<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PromoCodes;
use App\MemberPromo;
use Carbon\Carbon;

class PromoCodesController extends Controller
{

      public function add_promo(Request $request)
      { 
          $validator = \Validator::make($request->all(), [
              'code' => 'required',
          ]);
          if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

         $checkPromoCodes = PromoCodes::where('code',$request->code)->where('status',1)
            ->where(function ($q) {
                 $q->where('from_date', '<=', Carbon::now());
                 $q->where('to_date', '>=', Carbon::now());
            })->first();

          if(!$checkPromoCodes)
          {
              \Session::flash('flash_message',' wrong code or outdated ');
              return redirect('ShoppingCart');
          }

          $chek_if_used_before = MemberPromo::where( 'member_id',auth('Member')->id() )->where('promo_id',$checkPromoCodes->id)->first();

          if($chek_if_used_before)
          {
              \Session::flash('flash_message',' you used this code before ');
              return redirect('ShoppingCart');
          }

         MemberPromo::create([
            'member_id' => auth('Member')->id() ,
            'promo_id' => $checkPromoCodes->id
         ]);
         \Session::flash('flash_message',' Promo code is added ');
         return redirect('ShoppingCart');
      }

      // public function get_code_percentage_value($code)
      // {
      //     $promo = PromoCodes::where('code',$code)->where('status',1)
      //        ->where(function ($q) {
      //             $q->where('from_date', '<=', Carbon::now());
      //             $q->where('to_date', '>=', Carbon::now());
      //        })->first();
      //
      //        $chek_if_used_before = MemberPromo::where( 'member_id',auth('Member')->id() )->where('promo_id',$promo->id)->first();
      //
      //        if($chek_if_used_before)
      //        {
      //            return response()->json([
      //              'status' => 'added_before',
      //            ]);
      //        }
      //
      //        if($promo)
      //        {
      //            return response()->json([
      //              'status' => 'success',
      //              'data' => $promo,
      //            ]);
      //        }
      //        else {
      //            return response()->json([
      //              'status' => 'failed',
      //            ]);
      //        }
      // }
      //

}
