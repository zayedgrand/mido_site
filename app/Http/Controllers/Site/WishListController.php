<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Product;
use App\WishList;

class WishListController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:Member');
    }

   public function index()
   {
       return view( 'Site.wishlist_list' );
   }

    public function list()
    {
          $lang = \App::getLocale();
          $AuthMember_id = ( auth('Member')->check() )? auth('Member')->id() : 0 ;
          $ShoppingCart = WishList::select('products.id as id','products.brand_id','products.category_id','products.price',
                            "products.name_$lang as name"  ,'products.id as product_id',
                            "products.quantity as quantity",
                             "description_$lang as description",
                            \DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image"),
                            \DB::raw("if( shopping_cart.product_id , 1,0 ) as in_card"),
                            \DB::raw("if( shopping_cart.quantity , shopping_cart.quantity,0 ) as in_card_quantity")  
                      )

                      ->join('products','products.id','wish_list.product_id')
                      ->leftJoin('shopping_cart',function($q)use($AuthMember_id){
                          $q->on('shopping_cart.product_id','products.id')->where('shopping_cart.member_id',$AuthMember_id);
                      })
                      ->join('product_images','product_images.product_id','products.id')

                      ->groupBy('wish_list.id')
                      ->where( 'wish_list.member_id',auth('Member')->id() )
                      ->paginate();
         return $ShoppingCart;
    }

    public function addOrRemove($product_id)
    {
          $WishList = WishList::where( 'member_id',auth('Member')->id() )->where('product_id',$product_id)->first();

          if($WishList)
          {
              $WishList->delete();
              return response()->json([
                'status' => 'success',
                'case' => 0,
              ]);
          }
          else
          {
              WishList::create([
                'member_id' => auth('Member')->id() ,
                'product_id' => $product_id
              ]);
              return response()->json([
                'status' => 'success',
                'case' => 1,
              ]);
          }
    }

}
