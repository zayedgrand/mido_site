<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\City;
use App\Member;
use App\MemberAddress;

class AuthController extends Controller
{

        public function __construct()
        {
           $this->middleware('guest:Member')->except('logout','edit','update','add_new_address');
           $this->middleware('auth:Member')->only('edit','update');
        }


      public function registerForm()
      {
          $lang = \App::getLocale();
          $Cities = \App\City::select('id',"name_$lang as name")->orderBy("name_$lang")->where('status',1)->get();
          return view('Site.register',compact('Cities'));
      }

      public function loginForm()
      {
          return view('Site.login');
      }

      public function register(Request $request)
      {
            $data = $request->validate([
                'name' => 'required',
                'email' => 'required|unique:members',
                'password' => 'required',
                'gender' => 'required',
                'phone' => 'required',
                'city_id' => 'required',
                'birthdate' => 'required|date',
                'address' => 'required',
                'street' => 'required',
                'building_no' => 'required',
                'apartment_no' => 'required',
            ]);
            $data['password'] = md5('moi'.$request->password);
            $Member = Member::create($data);
            MemberAddress::create([
                'member_id' => $Member->id ,
                'address' => self::remove_quotes($request->address) ,
                'street' => self::remove_quotes($request->street) ,
                'building_no' => self::remove_quotes($request->building_no) ,
                'apartment_no' => self::remove_quotes($request->apartment_no)
            ]);
            auth()->guard('Member')->loginUsingId($Member->id, true);
            return redirect('');
      }

      //--api--
      public function login(Request $request)
      {
          $data = $request->validate([
              'email' => 'required',
              'password' => 'required',
          ]);

          $Member = Member::where('email',$request->email)->where('password', md5('moi'.$request->password) )->first();

          if($Member)
          {
              auth()->guard('Member')->loginUsingId($Member->id, true);
              return response()->json([
                'status' => 'success',
              ]);
          }
          else
          {
              return response()->json([
                'status' => 'faild',
              ]);
          }
      }

      public function logout()
      {
          if(auth()->guard('Member')->check())
              auth()->guard('Member')->logout();
          return back();
      }

      public function edit()
      {
          $member = auth('Member')->user();
          $address = MemberAddress::select('*',\DB::raw("CONCAT('cc_',id) as cc"))->where('member_id',$member->id)->get();

          $lang = \App::getLocale();
          $Cities = \App\City::select('id',"name_$lang as name")->where('status',1)->get();

          return view('Site.profile',compact('member','address','Cities'));
      }

      public function update(Request $request,$id)
      {
          $data = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:members,email,'.$id,
            'password' => '',
            'address' => 'required',
            'phone' => 'required',
            'city_id' => 'required',
            'birthdate' => 'required|date',
        ]);
        $member_data = [
            'name' => $request->name ,
            'email' => $request->email ,
            'phone' => $request->phone ,
            'birthdate' => $request->birthdate ,
            'city_id' => $request->city_id ,
        ];
        if($data['password']){
          $member_data['password'] = md5('moi'.$request->password);
        }
          $Member = Member::findOrFail($id);
          $Member->update($member_data);

          MemberAddress::where('member_id',$Member->id)->whereNotIn('id',$request->address_ids)->delete();
          for ($i=0; $i < count($request->count) ; $i++)
          {
              if(isset($request->address[$i]))
              {
                  if(isset($request->address_ids[$i]))
                  {
                       MemberAddress::find($request->address_ids[$i])->update([
                          'address' => self::remove_quotes($request->address[$i]),
                          'street' => self::remove_quotes($request->street[$i]) ,
                          'building_no' => self::remove_quotes($request->building_no[$i]) ,
                          'apartment_no' => self::remove_quotes($request->apartment_no[$i])
                      ]);
                  }
                  else
                  {
                      MemberAddress::create([
                          'member_id' => $Member->id ,
                          'address' => self::remove_quotes($request->address[$i]),
                          'street' => self::remove_quotes($request->street[$i]) ,
                          'building_no' => self::remove_quotes($request->building_no[$i]) ,
                          'apartment_no' => self::remove_quotes($request->apartment_no[$i])
                      ]);
                  }
              }//end if(isset($request->address[$i]))
          }//end for
          \Session::flash('flash_message',__('page.profile has updated'));
          return redirect('home');
      }

      public function forgetPassword_Form()
      {
         return view('Site.forgetPassword.resite_email');
      }


    public function send_forget_password_email(Request $request)
    {
          $data = $request->validate([
              'email' => 'required',
          ]);

          $Member = Member::where('email',$request->email)->first();
          if($Member)
          {
             $Member->update(['forget_password' => str_random(80) ]);
             \Mail::to($Member->email)->send(new \App\Mail\ForgetPassword($Member));
          }

          \Session::flash('flash_message',__('page.Forget password Email has send to your mail'));
          return redirect('');
    }


    public function resite_password_form($ForgetPassword)
    {
        $Member = Member::where('forget_password',$ForgetPassword)->first();
        if($Member)
        {
            return view('Site.forgetPassword.resite_password',compact('Member'));
        }
        else {
          return 'expired';
        }
    }

    public function reset_password(Request $request)
    {
          $data = $request->validate([
              'password' => 'required',
              'member_id' => 'required',
              'forget_password' => 'required',
              'password_again' => '',
          ]);
          $Member = Member::where('forget_password',$request->forget_password)->findOrFail($request->member_id);
          $Member->update([
            'password' => md5('moi'.$request->password) ,
            'forget_password' => str_random(80)
          ]);
          auth()->guard('Member')->loginUsingId($Member->id, true);
          return redirect('');
    }

    public function add_new_address(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'address' => 'required',
            'street' => 'required',
            'building_no' => 'required',
            'apartment_no' => 'required',
        ]);
        if ($validator->fails()) { return response()->json([ 'state' => 'notValid' , 'data' => $validator->messages() ]);  }

        MemberAddress::create([
            'member_id' => auth('Member')->id() ,
            'address' => self::remove_quotes($request->address) ,
            'street' => self::remove_quotes($request->street) ,
            'building_no' => self::remove_quotes($request->building_no) ,
            'apartment_no' => self::remove_quotes($request->apartment_no)
        ]);

        return response()->json([
            'status' => 'success',
            'data' => MemberAddress::where('member_id', auth('Member')->id() )->get()
        ]);
    }



}
