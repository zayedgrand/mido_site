<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Recipt;
use App\ReciptProducts;

class HistoryController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth:Member');
    }

      public function index()
      {
          return view('Site.History.list' );
      }

      public function list(Request $request)
      {
          $status = $request->status;
          $selectedDate = $request->selectedDate;
          $lang = \App::getLocale();
          $AuthMember_id = ( auth('Member')->check() )? auth('Member')->id() : 0 ;
           return Recipt::where('member_id',$AuthMember_id)->orderBy('id','desc')
                          ->where(function($q)use($status,$selectedDate){
                              if($status){
                                 $q->where('delivery_status',$status);
                              }
                              if($selectedDate){
                                 $q->where('created_at','>=', date('Y-m-d', strtotime('-3 month')) );
                              }
                          })
                          ->latest()->paginate();
      }

      public function show($id)
      {
         $lang = \App::getLocale();
         $Recipt = Recipt::where('member_id',auth('Member')->id() )->findOrFail($id);  //return $Recipt;
         $ReciptProducts = ReciptProducts::select('recipt_products.*',"recipt_products.product_name_$lang as product_name",
                                  \DB::raw("IF(products.id,products.id,'#') as id_or_hash"),
                                  "products.short_description_$lang as short_description",
                                  \DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image")
                                )
                                ->leftJoin('products','products.id','recipt_products.product_id')
                                ->leftJoin('product_images','product_images.product_id','products.id')
                                ->groupBy('recipt_products.id')
                                ->where('recipt_id',$id)
                                ->paginate(30);

         return view('Site.History.show',compact('Recipt','ReciptProducts') );
      }

      public function cancle_order($Recipt_id)
      {
          $Recipt = Recipt::where('member_id',auth('Member')->id())->findOrFail($Recipt_id);
          // increse in the stock
          $ReciptProducts = ReciptProducts::where('recipt_id',$Recipt->id)->get();
          foreach ($ReciptProducts as $key => $RProduct)
          {
              $p = \App\Product::find($RProduct->product_id);
              if($p){
                  $p->increment('quantity');
              }
          }
          $Recipt->update(['delivery_status' => 'canceled' ]);

      }


}
