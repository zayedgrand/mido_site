<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Setting;
use App\ContactUs;
use App\PagesBanner;

class ContactUsController extends Controller
{
      public function index()
      {
          $contactus_description = Setting::where('title','contactus_description')->first();

          $Banner = PagesBanner::where('page','ContactUs')->first();

          return view('Site.Contact_us',compact('contactus_description','Banner'));
      }

      public function make_Contact(Request $request)
      {
            $data = $request->validate([
              'name' => 'required',
              'phone' => 'required',  // |unique:register,email
              'email' => 'required',
              'message' => 'required',
          ]);

          ContactUs::create($data);
          \Session::flash('flash_message',' Thank you for your interest in AGORA, one of our representative will contact you soon. ' );
          return back();
      }
}
