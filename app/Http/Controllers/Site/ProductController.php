<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Brand;
use App\Category;
use App\SubCategory;
use App\Product;
use App\ProductImages;
use DB;

class ProductController extends Controller
{

    public function SubCategoryIndex($id)
    {
        $lang = \App::getLocale();
        $main = SubCategory::select("name_$lang as name",
                                    DB::raw("CONCAT('".asset('images/Category/Banner').ucfirst($lang)."/',banner_$lang) as banner"),'id'
                                   )
                               ->where('status',1)->findOrFail($id);
        $type = 'subCategory';
        return view('Site.productsList',compact('main','type'));
    }

    public function searchPage(Request $request)
    {
        $data = $request->validate([
          'search' => 'required',
        ]);

        $lang = \App::getLocale();
        $type = 'subCategory';
        $search = $request->search;
        return view('Site.searchList',compact( 'type','search'));
    }

    public function BrandIndex($id)
    {
        $lang = \App::getLocale();
        $main = Brand::select("name_$lang as name","og_description_$lang as og_description",
                                   DB::raw("CONCAT('".asset('images/Brand/Banner').ucfirst($lang)."/',banner_$lang) as banner"),'id'
                                  )
                               ->where('status',1)->findOrFail($id);
        $type = 'brand';  
        return view('Site.productsList',compact('main','type'));
    }

    public function get_products_list($type,$id)
    {
        $lang = \App::getLocale();
        $AuthMember_id = ( auth('Member')->check() )? auth('Member')->id() : 0 ;
        $Products = \App\Product::select('products.id as id','products.brand_id','products.category_id',
                          "products.name_$lang as name" ,"products.short_description_$lang as short_description",'position',
                          "products.quantity as quantity", "description_$lang as description",
                          'products.price',"products.old_price as old_price","products.discount_percentage as discount_percentage",
                          \DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image"),
                          \DB::raw("GROUP_CONCAT('".asset('images/ProductImages')."/',product_images.image) as images"),
                          \DB::raw("if( shopping_cart.product_id , 1,0 ) as in_card"),
                          \DB::raw("if( shopping_cart.quantity , shopping_cart.quantity,0 ) as in_card_quantity")
                       )

                    ->join('product_images','product_images.product_id','products.id')->groupBy('products.id')
                    ->leftJoin('shopping_cart',function($q)use($AuthMember_id){
                        $q->on('shopping_cart.product_id','products.id')->where('shopping_cart.member_id',$AuthMember_id);
                    })
                    ->where('products.status',1)
                    ->where(function($q)use ($type,$id){
                        if($type == 'subCategory'){
                           $q->where('sub_category_id',$id);
                        }
                        else if($type == 'brand'){
                           $q->where('brand_id',$id);
                        }
                    })
                    ->orderBy('position')
                    ->paginate(20);
         return $Products ;
    }


    public function get_products_list_search(Request $request)
    {
        $data = $request->validate([
          'search' => 'required',
        ]);
        $lang = \App::getLocale();
        $search = $request->search;
        $AuthMember_id = ( auth('Member')->check() )? auth('Member')->id() : 0 ;
        $Products = \App\Product::select('products.id as id','products.brand_id','products.category_id',
                          "products.name_$lang as name" ,"products.short_description_$lang as short_description",
                          "products.quantity as quantity", "description_$lang as description",
                          'products.price',"products.old_price as old_price","products.discount_percentage as discount_percentage",
                          \DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image"),
                          \DB::raw("GROUP_CONCAT('".asset('images/ProductImages')."/',product_images.image) as images"),
                          \DB::raw("if( shopping_cart.product_id , 1,0 ) as in_card"),
                          \DB::raw("if( shopping_cart.quantity , shopping_cart.quantity,0 ) as in_card_quantity")
                       )

                    ->join('product_images','product_images.product_id','products.id')->groupBy('products.id')
                    ->leftJoin('shopping_cart',function($q)use($AuthMember_id){
                        $q->on('shopping_cart.product_id','products.id')->where('shopping_cart.member_id',$AuthMember_id);
                    })
                    ->where('products.name_en','like','%'.$search.'%')->orWhere('products.name_ar','like','%'.$search.'%')
                    ->orWhere('products.short_description_en','like','%'.$search.'%')
                    ->orWhere('products.short_description_ar','like','%'.$search.'%')
                    ->latest('products.id')
                    ->paginate(20);
         return $Products ;
    }


    public function show($id)
    {
          $lang = \App::getLocale($id);
          $AuthMember_id = ( auth('Member')->check() )? auth('Member')->id() : 0 ;
          $Images = ProductImages::where('product_id',$id)->pluck('image');

          $product = Product::select('products.id as id','products.brand_id','products.category_id',
                          "products.name_$lang as name","products.quantity as quantity","description_$lang as description",
                          "products.short_description_$lang as short_description",
                          'products.price',"products.old_price as old_price","products.discount_percentage as discount_percentage",
                          \DB::raw("if( shopping_cart.product_id , 1,0 ) as in_card"),
                          \DB::raw("if( shopping_cart.quantity , shopping_cart.quantity,0 ) as in_card_quantity"),
                          \DB::raw("if( wish_list.product_id , 1,0 ) as in_wish_list")
                       )
                  ->leftJoin('shopping_cart',function($q)use($AuthMember_id){
                      $q->on('shopping_cart.product_id','products.id')->where('shopping_cart.member_id',$AuthMember_id);
                  })
                  ->leftJoin('wish_list',function($q)use($AuthMember_id){
                      $q->on('wish_list.product_id','products.id')->where('wish_list.member_id',$AuthMember_id);
                  })
                  ->where('status',1)->where('products.id',$id)->first();
                  if(!$product){
                    return 'hidden Item';
                  }

                  $banner = Brand::findOrFail($product->brand_id)["banner_$lang"];

              $relatedProducts = \App\Product::select('products.id as id','products.brand_id','products.category_id' ,
                                "products.name_$lang as name","products.quantity as quantity","description_$lang as description",
                                'products.price',"products.old_price as old_price","products.discount_percentage as discount_percentage",
                                \DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image"),
                                \DB::raw("if( shopping_cart.product_id , 1,0 ) as in_card"),
                                \DB::raw("if( shopping_cart.quantity , shopping_cart.quantity,0 ) as in_card_quantity")
                             )

                          ->join('product_images','product_images.product_id','products.id')->groupBy('products.id')
                          ->leftJoin('shopping_cart',function($q)use($AuthMember_id){
                              $q->on('shopping_cart.product_id','products.id')->where('shopping_cart.member_id',$AuthMember_id);
                          })
                          ->orderByRaw('FIELD(products.sub_category_id , "'.$product->sub_category_id.'"  ) Desc')
                          ->orderByRaw('FIELD(products.category_id , "'.$product->category_id.'"  ) Desc')
                          ->orderByRaw('FIELD(products.brand_id , "'.$product->brand_id.'"  ) Desc')
                          ->where('products.id','!=',$product->id)
                          ->limit(5)->get();

          return view('Site.product_details',compact('product','Images','relatedProducts','banner'));
      }



}
