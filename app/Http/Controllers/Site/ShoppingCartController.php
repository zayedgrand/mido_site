<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Product;
use App\ShoppingCart;
use App\MemberPromo;
use App\PromoCodes;
use App\FreeTestList;
use DB;

class ShoppingCartController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:Member');
    }

   public function index()
   {
        $lang = \App::getLocale();
        $chekPromo = MemberPromo::where( 'member_id',auth('Member')->id() )->where('is_used',0)->first();
          
        if($chekPromo)
        {
          $promo = PromoCodes::find($chekPromo->promo_id);
          $promo_discount_percentage = $promo->discount_percentage??0;
        }
        else {
          $promo_discount_percentage = 0;
        }

        $FreeTestList = FreeTestList::select('id',"name_$lang as name",'image')->where('status',1)->get();
        $address = \App\MemberAddress::select('*',\DB::raw("CONCAT('cc_',id) as cc"))
                                     ->where( 'member_id',auth('Member')->id() )->get();

        return view('Site.ShoppingCart' ,compact('promo_discount_percentage','FreeTestList','address' ));
   }

    public function list()
    {
          $lang = \App::getLocale();
          $AuthMember_id = ( auth('Member')->check() )? auth('Member')->id() : 0 ;
          $ShoppingCart = ShoppingCart::select('products.id as id','products.brand_id','products.category_id','products.price',
                            "products.name_$lang as name" ,'products.id as product_id',
                            "products.short_description_$lang as short_description",
                            "products.quantity as quantity","description_$lang as description",
                            DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image"),
                            DB::raw("if( shopping_cart.product_id , 1,0 ) as in_card"),
                            DB::raw("if( shopping_cart.quantity , shopping_cart.quantity,0 ) as in_card_quantity"),
                            DB::raw("CONCAT('cc_',products.id) as cc")
                      )

                      ->join('products','products.id','shopping_cart.product_id')
                      ->join('product_images','product_images.product_id','products.id')

                      ->groupBy('shopping_cart.id')
                      ->where( 'member_id',auth('Member')->id() )
                      ->where( 'products.quantity','>','0' )
                      ->paginate();
         return $ShoppingCart;
    }

      public function add(Request $request)
      {
            $data = $request->validate([
              'product_id' => 'required',
              'quantity' => '',
            ]);
            // return ($request->all());

            $ShoppingCart = ShoppingCart::where( 'member_id',auth('Member')->id() )->where('product_id',$request->product_id)->first();
            $product = Product::findOrFail($request->product_id);
            if($ShoppingCart){
                if($request->quantity) //a spisifc quantity
                {
                    if($product->quantity >= $request->quantity)//there is available quantity
                    {
                      $ShoppingCart->update([ 'quantity' => (int)$request->quantity ]);
                      $ShoppingCart->quantity = (int)$request->quantity;
                      $ShoppingCart->save();
                    }
                    else //no available quantity
                    {
                        return response()->json([
                            'status' => 'success',
                            'case' => 'no available quantity',
                            'data' => $ShoppingCart,
                            'quantity' => $ShoppingCart->quantity
                        ]);
                    }
                }
                else
                {
                    if($product->quantity > $ShoppingCart->quantity){//there is available quantity
                      $ShoppingCart->increment('quantity');
                    }
                    else //no available quantity
                    {
                        return response()->json([
                          'status' => 'success',
                          'case' => 'no available quantity',
                          'data' => $ShoppingCart,
                          'quantity' => $ShoppingCart->quantity
                        ]);
                    }
                }
            }//End if($ShoppingCart)
            else {
               if($product->quantity > 0){//there is available quantity
                   $ShoppingCart = ShoppingCart::create([
                       'member_id' => auth('Member')->id(),
                       'product_id' => $request->product_id,
                       'quantity' => $request->quantity??1
                   ]);
                   // $ShoppingCart->quantity = 1;
               }
            }
            return response()->json([
              'status' => 'success',
              'case' => 'added',
              'data' => $ShoppingCart,
              'quantity' => $ShoppingCart->quantity
            ]);
      }

      public function remove($id)
      {
           ShoppingCart::where( 'member_id',auth('Member')->id() )->where('product_id',$id)->delete();
           return response()->json([
             'status' => 'success',
           ]);
      }

      public function get_Count_number()
      {
          return ShoppingCart::where( 'member_id',auth('Member')->id() )->count();
      }

}
