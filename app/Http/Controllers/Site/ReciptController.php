<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Product;
use App\ShoppingCart;
use App\Recipt;
use App\ReciptProducts;
use App\MemberPromo;
use App\PromoCodes;
use App\MemberAddress;

class ReciptController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth:Member');
    }

      public function get_totalPrice($ShoppingCart)
      {
          $total_price = 0;
          foreach ($ShoppingCart as $key => $Cart)
          {
               $price = Product::find($Cart->product_id);
               if(!$price){
                 dd($Cart->product_id);
               }
               $price = $price->price;
               // $price = Product::find($Cart->product_id)->price;
               $total_price += $price * $Cart->quantity;
          }
          return $total_price;
      }

      public function get_discount()
      {
          $chekPromo = MemberPromo::where( 'member_id',auth('Member')->id() )->where('is_used',0)->first();
          if($chekPromo)
          {
              $promo = PromoCodes::find($chekPromo->promo_id);  
              $promo_discount_percentage = $promo->discount_percentage??0;
          }
          else {
              $promo_discount_percentage = 0;
          }
          return $promo_discount_percentage;
      }


      public function checkout(Request $request)
      {
              $data = $request->validate([
                'address' => 'required',
              ]);

            $ShoppingCart = ShoppingCart::where( 'member_id',auth('Member')->id() )->get();

            //---if Shopping Cart is Empty---
            if($ShoppingCart->isEmpty()){
              \Session::flash('flash_message','Shopping Cart is Empty ');
              return back();
            }

            $setting = \App\Setting::where('title','shipping_price')->orWhere('title','tax_price')
                       ->orWhere('title','minimum_basket_amount')->orWhere('title','minimum_freeTast_amount')
                       ->pluck('value','title');

            $totalPrice = $this->get_totalPrice($ShoppingCart);
            $discount = $this->get_discount();

            //--promo code
            $final_price = $totalPrice - ( ($totalPrice * $discount) / 100 );
            //--Shipping
            // $final_price = $final_price + $setting['shipping_price'];
            //--tax
            $final_price = $final_price + ( ($totalPrice * $setting['tax_price']) / 100 );

            if( $final_price < (int)$setting['minimum_basket_amount'] ){
                \Session::flash('flash_message','minimum price is '.$setting['minimum_basket_amount'].' EGP');
                return back();
            }

            $addres = MemberAddress::where('member_id',auth('Member')->id() )->find($request->address);

            $recipt_array = [
                'member_id' => auth('Member')->id(),
                // 'address' => auth('Member')->user()->address,
                'address' => $addres->address,
                'street' => $addres->street,
                'building_no' => $addres->building_no,
                'apartment_no' => $addres->apartment_no,
                'total_price' => $totalPrice,
                'shipping_price' => $setting['shipping_price'] ,
                'tax_price' => $setting['tax_price'] ,
                'discount_percentage' => $discount,
                'final_price' => $final_price,
                'payment_method' => $request->payment,
                'security_code' => \Illuminate\Support\Str::random(40)
            ];

            if($final_price >= $setting['minimum_freeTast_amount'] ){
                  if(!$request->free_taste){
                    \Session::flash('flash_message',__('page.pleae choose a free test'));
                    return back();
                  }
                 $recipt_array['free_test_id'] =  $request->free_taste;
            }

            //--for create Recipt --
            $Recipt = Recipt::create($recipt_array);

            //--for create Recipt Products--
            foreach ($ShoppingCart as $key => $Cart)
            {
                $this_product = Product::find($Cart->product_id);
                if( $this_product->quantity > 0 )
                {
                    //check if selected quantity is not bigger than the stock quantity
                    $required_quantity = ($Cart->quantity < $this_product->quantity) ? $Cart->quantity : $this_product->quantity;
                    $produ = [
                        'recipt_id' => $Recipt->id,
                        'quantity' => $required_quantity ,
                        'product_name_en' => $this_product->name_en ,
                        'product_name_ar' => $this_product->name_ar,
                        'product_id' => $this_product->id ,
                        'code' => $this_product->code,
                    ];
                    $price = $this_product->price;
                    $produ['single_price'] = $price;
                    $produ['total_price'] = $price * $Cart->quantity;

                    ReciptProducts::create($produ);

                    //---decrease the quantity from the product---
                    $this_product->update([
                        'quantity' => $this_product->quantity - $required_quantity
                    ]);
                }//End if
            }//End foreach
            ShoppingCart::where( 'member_id',auth('Member')->id() )->delete(); //dd($discount);
            if($discount)
            {
               $myPromo = MemberPromo::where( 'member_id',auth('Member')->id() )->where('is_used',0)->first();
               $myPromo->update([
                 'is_used' => 1,
                 'used_date' => \Carbon\Carbon::now()
               ]);
            }

            \Session::flash('flash_message',__('page.Thank you for purchasing from us'));

            if($request->payment == 'creadit_card'){
                return redirect('/payment_page/'.$Recipt->id);
            }
            else {
              return redirect('/History');
            }
      }

      public function payment_page($Recipt_id)
      {
          $Recipt = Recipt::findOrFail($Recipt_id);
          return view('Site/payment',compact('Recipt'));
      }

      public function confirm_is_paid($Recipt_id,$security_code)
      {
           $Recipt = Recipt::whereId($Recipt_id)->where('security_code',$security_code)->first();
           if($Recipt)
           {
                $Recipt->update([
                    'is_piad' => 1
                ]);
                \Session::flash('flash_message',__('page.payment is happend'));
                  return redirect('/');
           }
      }

}
