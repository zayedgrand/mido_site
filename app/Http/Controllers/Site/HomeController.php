<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\HomePage;
use App\Brand;
use App\Category;
use App\SubCategory;
use App\Product;
use DB;


class HomeController extends Controller
{
      public function index()
      {
          $lang = \App::getLocale();
          $AuthMember_id = ( auth('Member')->check() )? auth('Member')->id() : 0 ;
          $HomePage = HomePage::select('type','relation_id')->where('status',1)->orderBy('position')->get();
          foreach ($HomePage as $key => $Page)
          {
              if($Page->type == 'brand')
              {
                    $Page->item = Brand::select('id',"name_$lang as name" ,'logo',DB::raw("CONCAT('".asset('images/Brand/Banner').ucfirst($lang)."/',banner_$lang) as banner"))
                                       ->find($Page->relation_id);


                    $Page->items = Product::select('products.id as id','products.brand_id','products.category_id',
                                      "products.name_$lang as name" ,"products.quantity as quantity",
                                      'products.price',"products.old_price as old_price","products.discount_percentage as discount_percentage",
                                       "description_$lang as description","products.short_description_$lang as short_description",
                                    \DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image"),
                                    \DB::raw("if( shopping_cart.product_id , 1,0 ) as in_card"),
                                    \DB::raw("if( shopping_cart.quantity , shopping_cart.quantity,0 ) as in_card_quantity")
                                 )

                            ->join('product_images','product_images.product_id','products.id')->groupBy('products.id')
                            ->leftJoin('shopping_cart',function($q)use($AuthMember_id){
                                $q->on('shopping_cart.product_id','products.id')->where('shopping_cart.member_id',$AuthMember_id);
                            })
                            // ->orderByRaw('FIELD(products.category_id , "'.$Page->category_id.'"  ) Desc')
                            ->orderByRaw('FIELD(products.brand_id , "'.$Page->relation_id.'"  ) Desc')

                            ->where('status',1)->where('products.brand_id',$Page->relation_id)
                            ->limit(5)->get();
                            // dd('f');
              }
              else if($Page->type == 'subCategory')
              {
                    $Page->item = SubCategory::select('id',"name_$lang as name",DB::raw("CONCAT('".asset('images/Category/Banner').ucfirst($lang)."/',banner_$lang) as banner") )
                                       ->find($Page->relation_id);

                    $Page->items = Product::select('products.id as id','products.brand_id','products.category_id',
                                      "products.name_$lang as name" ,"products.quantity as quantity",
                                      'products.price',"products.old_price as old_price","products.discount_percentage as discount_percentage",
                                       "products.short_description_$lang as short_description",
                                      \DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image"),
                                      \DB::raw("if( shopping_cart.product_id , 1,0 ) as in_card"),
                                      \DB::raw("if( shopping_cart.quantity , shopping_cart.quantity,0 ) as in_card_quantity")
                                   )

                          ->join('product_images','product_images.product_id','products.id')->groupBy('products.id')
                          ->leftJoin('shopping_cart',function($q)use($AuthMember_id){
                              $q->on('shopping_cart.product_id','products.id')->where('shopping_cart.member_id',$AuthMember_id);
                          })
                          ->orderByRaw('FIELD(products.sub_category_id , "'.$Page->relation_id.'"  ) Desc')
                          // ->orderByRaw('FIELD(products.brand_id , "'.$Page->relation_id.'"  ) Desc')

                          ->where('status',1)->where('products.sub_category_id',$Page->relation_id)
                          ->limit(5)->get();
              }//End else if($Page->type == 'category')
          }//End foreach ($HomePage as $key => $Page)

            // return $HomePage;
          return view('Site.Home',compact('HomePage'));
      }



      public function make_register(Request $request)
      {
            $data = $request->validate([
              'name' => 'required',
              'phone' => 'required',  // |unique:register,email
              'email' => 'required',
          ]);

          \App\Register::create($data);
          \Session::flash('flash_message','Thank you for your interest in AGORA, one of our representative will contact you soon. ');
          return back();
      }

}
