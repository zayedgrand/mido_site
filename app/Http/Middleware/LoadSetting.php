<?php

namespace App\Http\Middleware;

use Closure;

class LoadSetting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        \View::share('site_settings',  Setting::pluck('value','title')  );
        
        return $next($request);
    }
}
