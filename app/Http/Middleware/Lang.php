<?php

namespace App\Http\Middleware;

use Closure;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

         $lang = '';
          if( \Session::has('lang') )
          {
              \App::setLocale( \Session::get('lang') );
              $lang = \Session::get('lang');
          }
          else
          {
              \Session::put('lang','en');
              \App::setLocale(  'en' );
              $lang = 'en';
          }

          $setting = \App\Setting::pluck('value','title');
          $setting['our_location'] = $setting['our_location_'.$lang];
          
          $site_brands = \App\Brand::select( "name_$lang as name",'id'  )->where('status',1)->orderBy('id')->get();
          $site_categories = \App\Category::select( "name_$lang as name",'id' )->where('status',1)->orderBy('id')->get();
          foreach ($site_categories as $key => $Category)
          {
               $Category->SubCategories = \App\SubCategory::select( "name_$lang as name",'id' )
                                             ->where('category_id',$Category->id)->where('status',1)->orderBy('id')->get();
          }

          $site_ShoppingCart_count = \App\ShoppingCart::where( 'member_id',auth('Member')->id() )->count();


          // $site_ShoppingCart = \App\ShoppingCart::select('products.id as id','products.price',"products.quantity as quantity",
          //                   "products.name_$lang as name" ,"bags_$lang as bage","prev_description_$lang as prev_description",
          //                   'shopping_cart.quantity as in_cart_quantity',
          //                   \DB::raw("CONCAT('".asset('images/ProductImages')."/',product_images.image) as image"),
          //                   \DB::raw("SUM(products.price * shopping_cart.quantity)")
          //             )
          //             ->join('products','products.id','shopping_cart.product_id')
          //             ->join('product_images','product_images.product_id','products.id')
          //
          //             ->groupBy('shopping_cart.id')
          //             ->where( 'member_id',auth('Member')->id() )
          //             ->get();

          \View::share('lang', $lang );
          \View::share('site_settings', $setting );
          \View::share('site_brands', $site_brands );
          \View::share('site_categories', $site_categories );
          \View::share('site_ShoppingCart_count', $site_ShoppingCart_count );
          // \View::share('site_ShoppingCart', $site_ShoppingCart );

          return $next($request);
    }
}
