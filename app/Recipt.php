<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipt extends Model
{
    // protected $table = 'promo_codes';
    const UPDATED_AT = null;

    protected $fillable = [
        'member_id','address', 'street' ,'building_no','apartment_no',
         'total_price', 'shipping_price','tax_price','discount_percentage','final_price','is_piad',
        'payment_method','is_delivered','delivery_status','free_test_id','security_code'
    ];


}
