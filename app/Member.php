<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    use Notifiable;
    // public $timestamps = false;
    protected $fillable = [
        'name', 'email', 'password','phone','gender','birthdate','address','city_id','forget_password'
    ];


}
