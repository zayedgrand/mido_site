<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCodes extends Model
{
    protected $table = 'promo_codes';
    protected $fillable = [
        'code','discount_percentage', 'from_date','to_date', 'status', 
    ];


}
