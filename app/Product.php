<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    protected $fillable = [
        'brand_id','category_id','sub_category_id', 'name_en', 'short_description_en','short_description_ar',
        'description_en','description_ar','name_ar' ,
        'old_price','discount_percentage','price',
         'quantity','position','status'
    ];

    public function path()
    {
       return url("product/{$this->id}-". Str::slug($this->name??$this->name_en) );
    }


    public function setDescriptionArAttribute($value)
    {
        if($value && $value!= 'undefined')
          $value = addcslashes( $value, "\n");
          $value = str_replace('"',"",$value);
          $this->attributes['description_ar'] =  addcslashes( $value, "\r" );
    }

    public function setDescriptionEnAttribute($value)
    {
        if($value && $value!= 'undefined')
        $value = addcslashes( $value, "\n");
        $value = str_replace('"',"",$value);
        $this->attributes['description_en'] =  addcslashes( $value, "\r" );
    }




}
