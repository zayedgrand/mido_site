<?php

namespace App\Exports;

use App\Register;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RegisterExport implements FromView
{
     
    public function view(): View
    {
        // return Register::all();
          return view('Admin.Export.Register', [
            'Register' => Register::all()
          ]);
    }
}
