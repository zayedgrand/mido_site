<?php

namespace App\Exports;

use App\ContactUs;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ContactUsExport implements FromView
{

    public function view(): View
    {
        // return ContactUs::all();
          return view('Admin.Export.ContactUs', [
            'ContactUs' => ContactUs::all()
          ]);
    }
}
