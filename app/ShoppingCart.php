<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    protected $table = 'shopping_cart';
    const UPDATED_AT = null;

    protected $fillable = [
        'member_id' , 'product_id','quantity'
    ];


}
