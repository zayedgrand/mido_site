<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeTestList extends Model
{
    protected $table = 'free_test_list';
    protected $fillable = [
        'image','name_en', 'name_ar','status'
    ];

    public function getImageAttribute($value){
        return asset('images/FreeTestList/'.$value);
    }

     public function setImageAttribute($value)
     {
         if($value && $value!= 'undefined')
         {
             $fileName = 'FreeTestList_'.rand(11111,99999).'.'.$value->getClientOriginalExtension(); // renameing image
             $destinationPath = public_path('images/FreeTestList');
             $value->move($destinationPath, $fileName); // uploading file to given path
             $this->attributes['image'] = $fileName;
         }
     }

}
