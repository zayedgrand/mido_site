<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ReciptProducts extends Model
{
    protected $table = 'recipt_products';
    public $timestamps = false;

    public function path()
    {
       return url("product/{$this->id}-". Str::slug($this->name??$this->name_en) );
    }

    protected $fillable = [
        'recipt_id' , 'quantity' , 'single_price' , 'total_price','product_name_en','product_name_ar','product_id',

    ];


}
