<?php

namespace App\Providers;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
            Blade::if('permission', function ($val) {
              if (auth('Admin')->check())
              {
                    if ( auth()->user()->super_admin ) {
                        return true;
                    }

                    foreach (auth()->user()->get_Role->get_Permissions as  $value)
                    {
                        if ($value->title == $val)
                        {
                            return true;
                        }
                    }
                    return false;
               }
               else { //if not admin
                 return false;
               }
            });

            // Blade::directive('nl2brr', function ($val) {
            // 		return str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n"),"<br/>",$val)  ;
            // });

           

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
