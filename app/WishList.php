<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    protected $table = 'wish_list';
    const UPDATED_AT = null;

    protected $fillable = [
        'member_id' , 'product_id' 
    ];


}
