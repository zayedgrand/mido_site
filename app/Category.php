<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // protected $table = 'facilities';
    protected $fillable = [
        'banner_en','banner_ar' ,'name_en','name_ar', 'status'
    ];


        //
        // public function getBannerEnAttribute($value){
        //   if($value)
        //     return asset('images/Category/BannerEn/'.$value);
        // }
        //
        //  public function setBannerEnAttribute($value)
        //  {
        //      if($value && $value!= 'undefined')
        //      {
        //          $fileName = 'BannerEn_'.rand(11111,99999).'.'.$value->getClientOriginalExtension(); // renameing image
        //          $destinationPath = public_path('images/Category/BannerEn');
        //          $value->move($destinationPath, $fileName); // uploading file to given path
        //          $this->attributes['banner_en'] = $fileName;
        //      }
        //  }
        //
        // public function getBannerArAttribute($value){
        //   if($value)
        //     return asset('images/Category/BannerAr/'.$value);
        // }
        //
        //  public function setBannerArAttribute($value)
        //  {
        //      if($value && $value!= 'undefined')
        //      {
        //          $fileName = 'BannerAr_'.rand(11111,99999).'.'.$value->getClientOriginalExtension(); // renameing image
        //          $destinationPath = public_path('images/Category/BannerAr');
        //          $value->move($destinationPath, $fileName); // uploading file to given path
        //          $this->attributes['banner_ar'] = $fileName;
        //      }
        //  }


}
