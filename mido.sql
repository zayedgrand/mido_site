-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2019 at 03:52 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mido`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `lang` enum('en','ar') DEFAULT 'en',
  `remember_token` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `phone` varchar(220) DEFAULT NULL,
  `super_admin` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `password`, `lang`, `remember_token`, `email`, `phone`, `super_admin`, `role_id`) VALUES
(1, 'admin', 'admin', '$2y$10$N8jJmfNjxWK9SIw00v69SOS1GnN/NzlCplJ6Hffc0Q53dgHg/fd8K', 'en', '2GrpuEI6RrkXquh4hgyjUhav9RvJy7CRo4h8dwz7em4wnBroD5WaDazSu0Yh', 't54r.kl@gmail.com', '201094943793', 1, 1),
(3, 'ahmed', 'ahmed', '$2y$10$S/6cxXAQ8vWktsYSugnHi.QuletTehpOHelkk0fQxyXGqSD6krm8i', 'en', 'iRbEhENai5xsq6Do5MgpFB2egNRGpess5zIOr1yccPBabgaej5l8CXruWHqs', 'ahmed@m.com', '01010101010101', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `bad_rating_reasons_list`
--

CREATE TABLE `bad_rating_reasons_list` (
  `id` int(11) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `banner_en` varchar(225) NOT NULL,
  `banner_ar` varchar(225) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `logo` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `category_id`, `sub_category_id`, `banner_en`, `banner_ar`, `name_en`, `name_ar`, `logo`, `status`, `created_at`, `updated_at`) VALUES
(3, 11, 4, 'BannerEn_17961.jpg', 'BannerAr_52751.jpg', 'Mido Salmon', 'ميدو سالمون', '', 1, '2019-07-07 13:01:48', '2019-07-07 13:01:48'),
(4, 12, 6, 'BannerEn_84744.jpg', 'BannerAr_36908.png', 'illy', 'إيلي', '', 1, '2019-07-07 13:05:46', '2019-07-07 13:05:46'),
(5, 12, 6, 'BannerEn_25057.png', 'BannerAr_16395.png', 'Jacobs', 'جاكوبس', '', 1, '2019-07-07 13:06:45', '2019-07-07 13:06:45'),
(6, 12, 6, 'BannerEn_70225.png', 'BannerAr_82761.png', 'Maxwell House', 'ماكسويل هاوس', '', 1, '2019-07-07 13:07:26', '2019-07-07 13:07:26'),
(7, 12, 7, 'BannerEn_30625.png', 'BannerAr_17956.png', 'Dilmah', 'ديلما', '', 1, '2019-07-07 13:08:16', '2019-07-07 13:08:16'),
(8, 12, 7, 'BannerEn_98449.jpg', 'BannerAr_96720.jpg', 'Dammann', 'دامان', '', 1, '2019-07-07 13:09:38', '2019-07-07 13:09:38'),
(9, 12, 7, 'BannerEn_64654.jpg', 'BannerAr_95461.png', 'Pickwick', 'بيكويك', '', 1, '2019-07-07 13:10:54', '2019-07-07 13:10:54'),
(12, 14, 10, 'BannerEn_14922.png', 'BannerAr_52158.jpg', 'Bonne Maman', 'بون ماما', '', 1, '2019-07-07 13:16:41', '2019-07-17 10:50:18'),
(13, 14, 10, 'BannerEn_84560.png', 'BannerAr_39823.png', 'Anna', 'آنا', '', 1, '2019-07-07 13:18:19', '2019-07-07 13:18:19'),
(14, 14, 10, 'BannerEn_68950.jpg', 'BannerAr_38279.jpg', 'Rich', 'ريتش', '', 1, '2019-07-07 13:20:34', '2019-07-17 12:44:20'),
(15, 11, 5, 'BannerEn_56290.jpg', 'BannerAr_43576.jpg', 'Seafood', 'سي فود', '', 1, '2019-07-07 16:00:45', '2019-07-17 12:44:54'),
(16, 13, 9, 'BannerEn_48911.jpg', 'BannerAr_93281.jpg', 'Hag. Dazs', 'هاجن داز', '', 1, '2019-07-07 17:02:06', '2019-07-18 11:22:23'),
(17, 12, 8, 'BannerEn_61829.png', 'BannerAr_30858.png', 'Evian', 'إيفيان', '', 1, '2019-07-08 15:52:34', '2019-07-18 11:20:32'),
(18, 12, 8, 'BannerEn_23059.jpg', 'BannerAr_26225.jpg', 'Badoit', 'بادويت', '', 1, '2019-07-08 15:53:36', '2019-07-18 11:19:03');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `banner_en` varchar(225) DEFAULT NULL,
  `banner_ar` varchar(225) DEFAULT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `banner_en`, `banner_ar`, `name_en`, `name_ar`, `status`, `created_at`, `updated_at`) VALUES
(11, NULL, NULL, 'Protein', 'بروتين', 1, '2019-07-07 12:28:06', '2019-07-07 12:28:06'),
(12, NULL, NULL, 'Beverages', 'مشروبات', 1, '2019-07-07 12:41:02', '2019-07-07 12:41:02'),
(13, NULL, NULL, 'Desserts', 'الحلويات', 1, '2019-07-07 12:43:27', '2019-07-07 12:43:27'),
(14, NULL, NULL, 'Groceries', 'منتجات البقالة', 1, '2019-07-07 12:50:02', '2019-07-07 12:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name_en`, `name_ar`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Ain Shams', 'عين شمس', 1, '2019-01-28 00:00:00', '2019-07-17 12:46:58'),
(4, 'Abou Rawash', 'ابو رواش', 1, '2019-01-28 00:00:00', '2019-07-17 12:46:37'),
(5, 'Abbasiyya', 'العباسية', 1, '2019-01-28 00:00:00', '2019-07-17 12:46:09'),
(6, '15th of May city', '15 مايو', 1, '2019-01-28 00:00:00', '2019-07-17 12:45:40'),
(7, 'El Amiria', 'الاميرية', 1, '2019-07-17 12:47:14', '2019-07-17 12:47:14'),
(8, 'El Azhar', 'الازهر', 1, '2019-07-17 12:47:39', '2019-07-17 12:47:39'),
(9, 'El Basateen', 'البساتين', 1, '2019-07-17 12:48:00', '2019-07-17 12:48:00'),
(10, 'Shoruk City', 'الشروق', 1, '2019-07-17 12:49:55', '2019-07-17 13:03:57'),
(11, 'El Daher', 'الظاهر', 1, '2019-07-17 12:51:56', '2019-07-17 12:51:56'),
(12, 'Ghamra', 'غمرة', 1, '2019-07-17 12:52:21', '2019-07-17 12:52:21'),
(13, 'Helwan', 'حلوان', 1, '2019-07-17 12:52:37', '2019-07-17 12:52:37'),
(14, 'Madeinty', 'مدينتي', 1, '2019-07-17 12:52:52', '2019-07-17 12:52:52'),
(15, 'Matareya', 'المطرية', 1, '2019-07-17 12:53:11', '2019-07-17 12:53:11'),
(16, 'Nasr City', 'مدينة نصر', 1, '2019-07-17 12:53:35', '2019-07-17 12:53:35'),
(17, 'Obour City', 'العبور', 1, '2019-07-17 12:53:52', '2019-07-17 12:53:52'),
(18, 'Sayeda Zeinab', 'السيدة زينب', 1, '2019-07-17 12:54:06', '2019-07-17 12:54:06'),
(19, 'Shoubra El Khema', 'شبرا الخيمة', 1, '2019-07-17 12:54:24', '2019-07-17 12:54:24'),
(20, 'Abbasiya', 'العباسية', 1, '2019-07-17 12:54:45', '2019-07-17 12:54:45'),
(21, 'El Basateen', 'البساتين', 1, '2019-07-17 12:55:09', '2019-07-17 12:55:09'),
(22, 'DownTown', 'وسط البلد', 1, '2019-07-17 12:55:28', '2019-07-17 12:55:28'),
(23, 'Hadaiq Al-Qubba', 'حدائق القبة', 1, '2019-07-17 12:55:43', '2019-07-17 12:55:43'),
(24, 'Kasr El Eini', 'القصر العيني', 1, '2019-07-17 12:56:08', '2019-07-17 12:56:08'),
(25, 'Manyal', 'المنيل', 1, '2019-07-17 12:56:21', '2019-07-17 12:56:21'),
(26, 'Misr El-Gadidah', 'مصر الجديدة', 1, '2019-07-17 12:56:34', '2019-07-17 12:56:34'),
(27, 'New Cairo', 'القاهرة الجديدة', 1, '2019-07-17 12:56:49', '2019-07-17 12:56:49'),
(28, 'Ramsis', 'رمسيس', 1, '2019-07-17 12:57:01', '2019-07-17 12:57:01'),
(29, 'Sharabeya', 'الشرابية', 1, '2019-07-17 12:57:14', '2019-07-17 12:57:14'),
(30, 'Tahrir', 'التحرير', 1, '2019-07-17 12:57:32', '2019-07-17 12:57:32'),
(31, 'Ain Shams', 'عين شمس', 1, '2019-07-17 12:57:52', '2019-07-17 12:57:52'),
(32, 'Boulak Abo el Eila', 'بولاق ابو العلا', 1, '2019-07-17 13:00:51', '2019-07-17 13:00:51'),
(33, 'Garden City', 'جاردن سيتي', 1, '2019-07-17 13:01:07', '2019-07-17 13:01:07'),
(34, 'Heliopolis', 'هيليوبلس', 1, '2019-07-17 13:01:33', '2019-07-17 13:01:33'),
(35, 'Kattamia', 'القطامية', 1, '2019-07-17 13:01:46', '2019-07-17 13:01:46'),
(36, 'Marg', 'المرج', 1, '2019-07-17 13:01:58', '2019-07-17 13:01:58'),
(37, 'Misr El-Kadima', 'مصر القديمة', 1, '2019-07-17 13:02:22', '2019-07-17 13:02:22'),
(38, 'New Nozha', 'النزهة الجديدة', 1, '2019-07-17 13:02:45', '2019-07-17 13:02:45'),
(39, 'Rehab', 'الرحاب', 1, '2019-07-17 13:03:30', '2019-07-17 13:03:30'),
(40, 'Zamalek', 'الزمالك', 1, '2019-07-17 13:04:16', '2019-07-17 13:04:16'),
(41, 'Kalaa', 'القلعة', 1, '2019-07-17 13:04:37', '2019-07-17 13:04:37'),
(42, 'Gesr El Suez', 'جسر السويس', 1, '2019-07-17 13:05:09', '2019-07-17 13:05:09'),
(43, 'Helmeya', 'الحلمية', 1, '2019-07-17 13:05:47', '2019-07-17 13:05:47'),
(44, 'Maadi', 'المعادي', 1, '2019-07-17 13:06:05', '2019-07-17 13:06:05'),
(45, 'Masaken Sheraton', 'مساكن شيراتون', 1, '2019-07-17 13:06:22', '2019-07-17 13:06:22'),
(46, 'Mokattam', 'المقطم', 1, '2019-07-17 13:06:36', '2019-07-17 13:06:36'),
(47, 'Nozha', 'النزهة', 1, '2019-07-17 13:06:55', '2019-07-17 13:06:55'),
(48, 'Sayeda Naffisa', 'السيدة نفيسة', 1, '2019-07-17 13:07:07', '2019-07-17 13:07:07'),
(49, 'Shoubra st.', 'شبرا', 1, '2019-07-17 13:07:18', '2019-07-17 13:07:18'),
(50, 'Zaytoon', 'الزيتون', 1, '2019-07-17 13:07:34', '2019-07-17 13:07:34');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `company` varchar(225) DEFAULT NULL,
  `name` varchar(225) NOT NULL,
  `phone` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `message` text NOT NULL,
  `is_contacted` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `city_id`, `company`, `name`, `phone`, `email`, `message`, `is_contacted`, `created_at`, `updated_at`) VALUES
(1, 3, 'sdsds', 'ali', '010101010', 'm@m.xom', 'the current message', 1, '2018-12-30 13:19:36', '2019-02-17 14:34:19'),
(2, NULL, NULL, 'ali', '0101010101', 'm@m.com', 'ewelwkle', 0, '2018-12-30 14:19:34', '2018-12-30 14:19:34'),
(3, NULL, NULL, 'mona', '010101010010', 'm@m.xom', 'go thire', 1, '2019-01-02 14:34:30', '2019-02-17 14:44:11'),
(4, NULL, NULL, 'mona', '0101010', 'm@m.com', 'my message', 0, '2019-01-10 13:42:50', '2019-01-10 13:42:50'),
(5, NULL, NULL, 'test', '4576567567', 'test@test.com', 'test', 0, '2019-01-21 13:45:24', '2019-02-17 14:44:20'),
(8, 3, 'wdw', 'dwdwd', '01099494949', 'm@m.com', 'wdwd', 0, '2019-02-18 11:10:08', '2019-02-18 11:10:45');

-- --------------------------------------------------------

--
-- Table structure for table `free_test_list`
--

CREATE TABLE `free_test_list` (
  `id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `free_test_list`
--

INSERT INTO `free_test_list` (`id`, `image`, `name_en`, `name_ar`, `status`, `created_at`, `updated_at`) VALUES
(1, 'FreeTestList_15487.gif', 'Häagen-Dazs Strawberry Cheesecake Ice cream', 'هاجن داز -  بالفراولة و التشيز كيك', 1, '2018-08-14 08:45:37', '2019-07-17 10:57:53'),
(2, 'FreeTestList_18592.gif', 'Dilmah Moroccan Mint Green Leaf tea', 'ديلما شاي اخضر بالنعناع المغربي', 1, '2018-08-14 08:45:37', '2019-07-17 11:01:31'),
(3, 'FreeTestList_49060.gif', 'Frozen Smoked Salmon Slices 200g', 'سلمون مدخن شرائح 200 غم', 1, '2019-07-17 10:59:18', '2019-07-17 10:59:18'),
(4, 'FreeTestList_46314.gif', 'Salmon Fillet 320gm pack', 'سلمون فيليه', 1, '2019-07-17 11:00:00', '2019-07-17 11:00:00'),
(5, 'FreeTestList_72126.gif', 'illy Medium Roast Instant Coffee', 'قهوة سريعة التحضير تحميص وسط من ايلي', 1, '2019-07-22 13:02:13', '2019-07-22 13:02:13');

-- --------------------------------------------------------

--
-- Table structure for table `home_page`
--

CREATE TABLE `home_page` (
  `id` int(11) NOT NULL,
  `type` enum('brand','subCategory') NOT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page`
--

INSERT INTO `home_page` (`id`, `type`, `relation_id`, `position`, `status`, `created_at`, `updated_at`) VALUES
(4, 'brand', 4, 2, 1, '2019-07-07 17:55:07', '2019-07-10 15:53:35'),
(9, 'subCategory', 4, 1, 1, '2019-07-07 18:06:11', '2019-07-10 15:53:35'),
(10, 'brand', 7, 5, 1, '2019-07-15 17:05:20', '2019-07-17 12:43:18'),
(11, 'brand', 12, 6, 1, '2019-07-16 16:04:01', '2019-07-17 12:43:18'),
(12, 'brand', 17, 3, 1, '2019-07-17 10:23:03', '2019-07-17 12:43:16'),
(13, 'brand', 16, 4, 1, '2019-07-17 12:39:49', '2019-07-17 12:43:18');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `type` enum('full time','part time') NOT NULL,
  `title_en` varchar(225) NOT NULL,
  `title_ar` varchar(225) NOT NULL,
  `description_en` text NOT NULL,
  `description_ar` text NOT NULL,
  `skill_title_en` varchar(225) NOT NULL,
  `skill_title_ar` varchar(225) NOT NULL,
  `skills` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `type`, `title_en`, `title_ar`, `description_en`, `description_ar`, `skill_title_en`, `skill_title_ar`, `skills`, `status`, `created_at`, `updated_at`) VALUES
(1, 'full time', 'extrusion production manager job summary', 'extrusion production manager job summary', 'planning and managing processes to ensure their compliance with production schedules according to the required quality standads', 'planning and managing processes to ensure their compliance with production schedules according to the required quality standads', 'extrusion production manager professional knowledge &  job summary', 'extrusion production manager professional knowledge &  job summary', '', 1, '2019-03-21 00:00:00', '2019-03-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `job_applicants`
--

CREATE TABLE `job_applicants` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `birthday` date NOT NULL,
  `phone` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `university` varchar(225) NOT NULL,
  `major` varchar(225) NOT NULL,
  `graduation_year` varchar(225) NOT NULL,
  `experience_years` varchar(225) NOT NULL,
  `resume` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `email` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(225) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `remember_token` varchar(225) DEFAULT NULL,
  `forget_password` varchar(225) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `password`, `gender`, `email`, `address`, `phone`, `age`, `birthdate`, `city_id`, `remember_token`, `forget_password`, `created_at`, `updated_at`) VALUES
(10, 'Ehab Fares', 'd7ad7e659917788e92b9603bebb39735', 'male', 'efares@bsocial-eg.com', '8 Fathy Talaat, Sheraton, Heliopolis', '01222187584', NULL, '1975-07-23', 3, 'f3Kt9EUef3aAegra0pyDsRTtGsUCXwaUVWVJu1i9EE975mxrEj7YK5LNiz2B', NULL, '2019-07-15 11:06:59', '2019-07-15 11:06:59'),
(11, 'Hadil', '40e7d24ca8d974378686e2b0e48c57b6', 'male', 'hadilahmed.ny@gmail.com', 'Sheraton', '01123338838', NULL, '1995-11-03', 3, 'hgTe4vq397cqu29yK2m3hBpcCty1Wq9U8KYwp2EVEuj2zxCAeIBBp5926HQE', NULL, '2019-07-17 11:04:41', '2019-07-17 11:04:41'),
(12, 'Hayam', 'a44d386c4ef630ef7e0e95b1adc0f7a0', 'male', 'hayamelhawary@gmail.com', 'Abbas Alakad', '01024307057', NULL, '1991-05-08', 16, 'ilHHmRTapbjZK2iXQ25DfLBgvsq3wOBHrDFKJvhdYyutT7Ku8TSyZ1eIBKo8', NULL, '2019-07-17 13:57:49', '2019-07-17 13:57:49'),
(13, 'Amira el agaty', '67acffda2cbe221c7a34e5024c25139e', 'male', 'Amira.elagaty@gmail.com', 'City view. Cairo Alex road\r\nGate 3 building 7', '01221094055', NULL, '2019-10-01', 4, 'G5I31DJ9FY7mzbTFKKxDxh0DzyJKOo91xiVk4fAiHxvkXBV4LZ1xzvifT09X', NULL, '2019-07-17 22:56:23', '2019-07-17 22:56:23'),
(14, 'Hassan Azab', '29a0a2d9527fa2b2d78dabd2f7ac6725', 'male', 'hassanota@gmail.com', '#21 ELNAKHEEl Str.-Mohandessen_dokki Giza\r\n2nd floor/apt 5', '01275310002', NULL, '1948-09-12', 40, '76nc7D7Oww5LCLZwHCDWxsd1EHQZNWmm2yxKr8XcZSU29S6ZlrJWUPnhzH4q', NULL, '2019-07-19 21:45:36', '2019-07-19 21:45:36'),
(15, 'Hisham Nagy', '861e55545c7328914ffa548461287526', 'male', 'hismah@yahoo.com', 'El Rehab city, El Rehab 2 , Group 130 - Building 10 Flat 1 Cairo', '01222260276', NULL, '0000-00-00', 39, 'LMxeTP3XOXpQDt3YrVnn3M4nf3I3TmTyHVcBT3nqI0XnZhGgW2LMpOtlbEdj', NULL, '2019-07-20 11:45:33', '2019-07-20 11:45:33'),
(19, 'Youssef Wagieh', 'be950f35896ea348562f69b09eb15776', 'male', 'joeyoggie@gmail.com', '2 Ainshams University Teaching Staff Buildings, Ard El-Matba\'a', '01099824282', NULL, '1992-01-01', 20, 'TB1rSI6sLntpA7KxKG4rx3mD96jDDrcnxe357eyZVtXVCQyNVH1NUXjoa3Qe', NULL, '2019-07-22 18:15:12', '2019-07-22 18:15:12'),
(25, 'Alain Trad', '8b61637fd49f7b06442e564402e1720c', 'male', 'alain.trad@me.com', 'Taha Hussein street\r\nZamalek\r\n(President Hotel Building)', '01001232244', NULL, '0000-00-00', 40, 'HPnUoFEOSXzY7Bg2z542IBN34IvMHn1Apz3GfOOGFq74ERQOiSjv0uEuylzI', NULL, '2019-07-22 20:12:27', '2019-07-22 20:12:27'),
(26, 'Amany Gall', 'e3d2185627528735fe2a2f5be3c741ea', 'male', 'amany.galal@gmail.com', '306 Cornich Elnil Maadi , HSBC head office', '01000017341', NULL, '1984-03-28', 44, 'cm7UPKrgInTQoj9NbxXbgN2fL88dI6PZjeoiG6CDbJ9MSnrWXyF32a41PgXm', NULL, '2019-07-23 08:41:01', '2019-07-23 08:41:01'),
(27, 'Christine', 'ebd3130a55c01e2e44a71a6463b0443d', 'male', 'christine.ebeid@yahoo.com', 'Plot 77, el 90 street fifth settlement New Cairo beside el ahli bank', '01001210944', NULL, '2019-10-01', 27, 'gVTqcf0skbyV3qnLGO2fPlODuRozTODWfUiCYT5qOKuB7dOZmRlZqmBWlqmG', NULL, '2019-07-23 19:33:50', '2019-07-23 19:33:50'),
(30, 'nehal osama', '570d2e4f4710741edb72c8bccb095139', 'male', 'nehal_sm@hotmail.com', '9044 معراج السفلي معادي كارفور', '01069849421', NULL, '2019-07-11', 44, 'HDHA4edx6HJetCZ5hAoJ1HKfHB0I0vDv61mD0oynJc9g7uSNqPOR6WXMki7x', NULL, '2019-07-24 13:10:22', '2019-07-24 13:10:22'),
(32, 'Mohamed ibrahim', '4d1fcf9a7468395a647cb3d7d06a5986', 'male', 'hanin_1997@yahoo.com', 'floor 8', '01288884190', NULL, '1997-12-16', 16, 'LoLIqBaVL2WRFLdE2KQwVTKLPM5i2rMIEpXYNGAsorGayXmGrnkptxPCQLtw', NULL, '2019-07-25 16:17:39', '2019-07-25 16:17:39'),
(33, 'Ahmed Ruby', '59222ec138fd7bdbda5f3563097e04f4', 'male', 'ahmed_ruby@hotmail.com', '9 block 33', '01223924799', NULL, '1970-09-03', 16, 'xMXcPSwyqksA6c2VxwwH1MqNH6gct07JhA4WB7Njjoh1R6WAB18dIDJS9bRy', NULL, '2019-07-25 17:07:48', '2019-07-25 17:07:48'),
(34, 'Ibrahim Hebicka', '21edf3f91fb41d6da3dc62ea90b8817d', 'male', 'ihebicka@yahoo.com', '13 khalifa Elmaamoon', '01000005502', NULL, '1973-10-22', 34, 'YEzsAh3U1nezyd4atpUR8Ba5N1joL4u0Jl8Xj4NeV6HZ9cw8MpxrnkTpoieZ', NULL, '2019-07-25 17:28:22', '2019-07-25 17:28:22'),
(35, 'Magued Gabriel', '2e0e1fa502d9e3c3ab866848a1431a3a', 'male', 'mnigabriel@gmail.com', 'Lake view - 90 Road', '01223141842', NULL, '1966-11-05', 27, 'aLUQGgMvB4WHkVdF20uj20tfXeBm6l8Pn0J49qEpRp7h8sSkkWRyC7go2rSB', NULL, '2019-07-25 21:58:28', '2019-07-26 06:35:36'),
(36, 'Rania Kandil', '1a96df2616837e41ee129689c3e347c6', 'male', 'rania.kandil@icloud.con', 'Sheikh Zayed City, 7th district, 1st neighborhood,el Jouman 2 buildings', '01000980488', NULL, '1974-05-05', 4, 'V3fpNJHf3GZvbSkAMMp2kSwXRoimDOPmM2bVvqSgisV9mAZxdamO6yXFK6oq', NULL, '2019-07-25 22:30:24', '2019-07-25 22:30:24'),
(37, 'Maissoun', 'b0881ed1908ca301f1160868b07a7503', 'male', 'maissounk.t@icloud.com', '3 bank buildings maadi', '01033221330', NULL, '1989-07-03', 44, 'BgSAwltTzTdymIIFNGm25Wj5dva9mpYhHg3p1Y2FXFIh1kMyskspNaxAsQz5', NULL, '2019-07-26 19:02:26', '2019-07-26 19:02:26'),
(38, 'Merhan Magdy Mondy', '0cec261456af4ad293014412e807e229', 'male', 'merhan.mondy@vodafone.com', '1/2, Road 216, Degla, Maadi', '01001000342', NULL, '1977-02-10', 44, 'TubugJCsqJozQDJUYUZDY4K9X39U0tUlIbt1NE2vQgEBd7CJ8RMtj0kFEzbR', NULL, '2019-07-27 00:32:43', '2019-07-27 00:32:43'),
(39, 'Ebtesam naeim', '3e05da482564d806c4f93c8132e401e1', 'male', 'bs_naeim25@yahoo.com', '19elGehad street lebanon square mohandesene', '01090004507', NULL, '1958-11-25', 40, '6w7loZLEhPbir7DYBZIjv9E73Sh0MpmWb241VaPzjjmDdDgsEqMOfjNV2GS8', NULL, '2019-07-27 15:47:51', '2019-07-27 15:47:51'),
(40, 'Jilan', 'e1c041128ee921e140748cb4a91370f0', 'male', 'jilanhossam91@gmail.com', '60 شارع حسني احمد خلف مدينة نصر متفرع من شارع مصطفي النحاس امام سوبرماركت اولاد رجب بجوار صيدلية الدكتور جاد الدور الثاني شقه رقم ٤', '01091115047', NULL, '1991-12-24', 16, 'jnRY6J1vViaQq3qFcx0xdesWPZZ8GTDzR5NvqErdQRA3McYRY7N0fqJMizCY', NULL, '2019-07-27 17:54:06', '2019-07-27 17:54:06'),
(41, 'Mohamed hamdy', 'ec4bca3e3efa1e8bed46ce753b2d1f43', 'male', '1888@yahoo.com', '٢٣ عويس البحيري من شارع عبيد روض الفرج', '01094254680', NULL, '1991-08-15', 49, 'kn8BmsPlAbc88OVCwNSk37Vybqz3ZIOufSB4NJ0sdeABaG0zQEN5ZrdCfI8I', NULL, '2019-07-28 03:04:04', '2019-07-28 03:04:04'),
(43, 'Dina Mohamed Gamal', '848ea494c6b128d2f2106d3ebbe80f5f', 'male', 'salma.mg.87@gmail.com', 'Hadyek October', '01286466895', NULL, '1987-02-23', 30, 'PyHNCnVJZ1iHDay0JowY5qIrWkeXAdbODXVRP1JzfKNK72LYIqxKj5VMx2UL', NULL, '2019-07-28 18:25:51', '2019-07-28 18:25:51'),
(44, 'هيام سعيد', 'df0caee0eb112bad371390f059455236', 'male', 'Hayam.saeed@sta-egypt.com', 'فيلا ٦٦ ب، كومباوند حدائق الفداء ،بجوار مدرسة المنار هاوس، الشيخ زايد، الجيزة', '٠١٠٢٧٩٩٤٧١٤', NULL, '0000-00-00', 40, 'ZOj932dspAQOZLWvA0Wgqgg99rbZqwtMpJlMWtDTrhnxKedXTVw1gYpFlyjl', NULL, '2019-07-28 22:02:55', '2019-07-28 22:02:55'),
(45, 'Test2', 'ac2c147babb6052d69b463d811e67767', 'male', 'hadeer.elshamy@vhorus.com', '6 kaboul st nasrcity', '01111070745', NULL, '1990-09-26', 16, '1MaOwyl16iG4edzcpMWfHUBUVD8pGv4JdCqzQpJrAhaHQL8nIFMyLIAd1NXj', NULL, '2019-07-29 11:36:54', '2019-07-29 11:36:54'),
(46, 'test-dev', 'ac2c147babb6052d69b463d811e67767', 'male', 'test-dev@test-dev.com', 'sss', '01000478529', NULL, '1993-10-06', 30, '37SESJdJtbrpBvOdZnS6SbvJkBZIS5oogZq4Cgxh7uvhvNhOZkvyUy8Yp1Bc', NULL, '2019-07-29 14:53:52', '2019-07-29 14:53:52'),
(47, 'zayed', 'ac2c147babb6052d69b463d811e67767', 'male', 'm@m.com', 'my adrress', '01094943793', NULL, '2019-07-03', 16, 'CvmCjCUJQlS8F4YnoZZqOyfBqO4m76vN7nl4PsBRPBjgYHhyVZ22v937He0D', NULL, '2019-07-30 13:34:09', '2019-07-30 13:34:09');

-- --------------------------------------------------------

--
-- Table structure for table `member_address`
--

CREATE TABLE `member_address` (
  `id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `address` varchar(225) DEFAULT NULL,
  `street` varchar(225) DEFAULT NULL,
  `building_no` varchar(225) DEFAULT NULL,
  `apartment_no` varchar(225) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member_address`
--

INSERT INTO `member_address` (`id`, `member_id`, `address`, `street`, `building_no`, `apartment_no`, `created_at`, `updated_at`) VALUES
(6, 10, '8 Fathy Talaat, Sheraton, Heliopolis', NULL, NULL, NULL, '2019-07-15 11:06:59', '2019-07-15 11:06:59'),
(7, 11, 'Sheraton', NULL, NULL, NULL, '2019-07-17 11:04:41', '2019-07-17 11:04:41'),
(8, 12, 'Abbas Alakad', NULL, NULL, NULL, '2019-07-17 13:57:49', '2019-07-17 13:57:49'),
(9, 13, 'City view. Cairo Alex road\r\nGate 3 building 7', NULL, NULL, NULL, '2019-07-17 22:56:23', '2019-07-17 22:56:23'),
(10, 14, '#21 ELNAKHEEl Str.-Mohandessen_dokki Giza\r\n2nd floor/apt 5', NULL, NULL, NULL, '2019-07-19 21:45:36', '2019-07-19 21:45:36'),
(11, 15, 'El Rehab city, El Rehab 2 , Group 130 - Building 10 Flat 1 Cairo', NULL, NULL, NULL, '2019-07-20 11:45:33', '2019-07-20 11:45:33'),
(15, 19, '2 Ainshams University Teaching Staff Buildings, Ard El-Matba a', NULL, NULL, NULL, '2019-07-22 18:15:12', '2019-07-22 18:15:12'),
(21, 25, 'Taha Hussein street\r\nZamalek\r\n(President Hotel Building)', NULL, NULL, NULL, '2019-07-22 20:12:27', '2019-07-22 20:12:27'),
(22, 25, '6 El Gezira El Wosta\n13th floor\nApartment 50', NULL, NULL, NULL, '2019-07-22 20:13:59', '2019-07-22 20:13:59'),
(23, 26, '306 Cornich Elnil Maadi , HSBC head office', NULL, NULL, NULL, '2019-07-23 08:41:01', '2019-07-23 08:41:01'),
(24, 27, 'Plot 77, el 90 street fifth settlement New Cairo beside el ahli bank', NULL, NULL, NULL, '2019-07-23 19:33:50', '2019-07-23 19:33:50'),
(27, 30, '9044 معراج السفلي معادي كارفور', NULL, NULL, NULL, '2019-07-24 13:10:22', '2019-07-24 13:10:22'),
(34, 32, 'floor 8', 'Hosni ahmad khalaf', '66', '22', '2019-07-25 16:17:39', '2019-07-25 16:17:39'),
(35, 33, '9 block 33', 'Moustafa Elnahas, 9th District', '9', '1001', '2019-07-25 17:07:48', '2019-07-25 17:07:48'),
(36, 34, '13 khalifa Elmaamoon', 'Khalifa Elmaamoon', '13', '93', '2019-07-25 17:28:22', '2019-07-25 17:28:22'),
(37, 35, 'Lake view - 90 Road', '65/4', 'Villa', '65/3', '2019-07-25 21:58:28', '2019-07-25 21:58:28'),
(38, 36, 'Sheikh Zayed City, 7th district, 1st neighborhood,el Jouman 2 buildings', 'El mostakbal', 'A', '1', '2019-07-25 22:30:24', '2019-07-25 22:30:24'),
(39, 37, '3 bank buildings maadi', 'Zahraa str', 'Bank buildings', '202', '2019-07-26 19:02:26', '2019-07-26 19:02:26'),
(40, 38, '1/2, Road 216, Degla, Maadi', 'Road 216', '1/2', '16', '2019-07-27 00:32:43', '2019-07-27 00:32:43'),
(41, 39, '19elGehad street lebanon square mohandesene', 'Gehad', '19', '19', '2019-07-27 15:47:51', '2019-07-27 15:47:51'),
(42, 40, '60 شارع حسني احمد خلف مدينة نصر متفرع من شارع مصطفي النحاس امام سوبرماركت اولاد رجب بجوار صيدلية الدكتور جاد الدور الثاني شقه رقم ٤', 'Hosny ahmed khalaf', '60', '4', '2019-07-27 17:54:06', '2019-07-27 17:54:06'),
(43, 41, '٢٣ عويس البحيري من شارع عبيد روض الفرج', 'عويس البحيري', '٢٣', '٤٢', '2019-07-28 03:04:04', '2019-07-28 03:04:04'),
(45, 43, 'Hadyek October', 'Beta greens', '53', '251', '2019-07-28 18:25:51', '2019-07-28 18:25:51'),
(46, 44, 'فيلا ٦٦ ب، كومباوند حدائق الفداء ،بجوار مدرسة المنار هاوس، الشيخ زايد، الجيزة', 'كومباوند حدائق الفداء', '٦٦ ب', '١', '2019-07-28 22:02:55', '2019-07-28 22:02:55'),
(47, 45, '6 kaboul st nasrcity', 'Kaboul', '6', '13', '2019-07-29 11:36:54', '2019-07-29 11:36:54'),
(48, 46, 'sss', 'sss', 'sss', 'ss', '2019-07-29 14:53:52', '2019-07-29 14:53:52'),
(49, 47, 'my adrress', '20', '20', '20', '2019-07-30 13:34:09', '2019-07-30 13:34:09');

-- --------------------------------------------------------

--
-- Table structure for table `member_promo`
--

CREATE TABLE `member_promo` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `promo_id` int(11) NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT 0,
  `used_date` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member_promo`
--

INSERT INTO `member_promo` (`id`, `member_id`, `promo_id`, `is_used`, `used_date`, `created_at`) VALUES
(27, 36, 25, 0, NULL, '2019-07-25 22:37:39'),
(28, 38, 25, 0, NULL, '2019-07-27 00:47:06'),
(29, 44, 25, 0, NULL, '2019-07-28 22:14:06');

-- --------------------------------------------------------

--
-- Table structure for table `pages_banners`
--

CREATE TABLE `pages_banners` (
  `id` int(11) NOT NULL,
  `page` varchar(225) DEFAULT NULL,
  `imageen` varchar(225) DEFAULT NULL,
  `imagear` varchar(225) DEFAULT NULL,
  `body` text DEFAULT NULL,
  `title` varchar(225) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages_banners`
--

INSERT INTO `pages_banners` (`id`, `page`, `imageen`, `imagear`, `body`, `title`, `created_at`, `updated_at`) VALUES
(1, 'downloads', 'PagesBanner_60456.png', 'PagesBanner_75572.png', 'TSAR', '', NULL, NULL),
(2, 'FAQ', 'PagesBanner_33759.png', 'PagesBanner_89141.png', 'TSAR', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pdf_downloads`
--

CREATE TABLE `pdf_downloads` (
  `id` int(11) NOT NULL,
  `pdf` varchar(225) DEFAULT NULL,
  `name_en` varchar(225) DEFAULT NULL,
  `name_ar` varchar(225) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `related_to` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '2018-08-14 08:45:37'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `related_to`, `created_at`) VALUES
(1, 'DashBoard', 'DashBoard', '2018-08-14 08:45:37'),
(2, 'Members', 'Members', '2018-08-14 08:45:37'),
(3, 'Brand_and_categoiry_main', 'Brand_and_categoiry', '2018-08-14 08:45:37'),
(4, 'Brand_and_categoiry_status', 'Brand_and_categoiry', '2018-08-14 08:45:37'),
(5, 'Brand_and_categoiry_in_home_page', 'Brand_and_categoiry', '2018-08-14 08:45:37'),
(6, 'Brand_and_categoiry_edit', 'Brand_and_categoiry', '2018-08-14 08:45:37'),
(7, 'Brand_and_categoiry_delete', 'Brand_and_categoiry', '2018-08-14 08:45:37'),
(8, 'Brand_and_categoiry_create', 'Brand_and_categoiry', '2018-08-14 08:45:37'),
(9, 'City_main', 'City', '2018-08-14 08:45:37'),
(10, 'City_create', 'City', '2018-08-14 08:45:37'),
(11, 'City_status', 'City', '2018-08-14 08:45:37'),
(12, 'City_edit', 'City', '2018-08-14 08:45:37'),
(13, 'City_delete', 'City', '2018-08-14 08:45:37'),
(14, 'PromoCodes_main', 'PromoCodes', '2018-08-14 08:45:37'),
(15, 'PromoCodes_create', 'PromoCodes', '2018-08-14 08:45:37'),
(16, 'PromoCodes_status', 'PromoCodes', '2018-08-14 08:45:37'),
(17, 'PromoCodes_edit', 'PromoCodes', '2018-08-14 08:45:37'),
(18, 'PromoCodes_delete', 'PromoCodes', '2018-08-14 08:45:37'),
(19, 'HomePage_main', 'HomeAdvertising', '2018-08-14 08:45:37'),
(20, 'HomeAdvertising_create', 'HomeAdvertising', '2018-08-14 08:45:37'),
(21, 'HomeAdvertising_status', 'HomeAdvertising', '2018-08-14 08:45:37'),
(22, 'HomeAdvertising_edit', 'HomeAdvertising', '2018-08-14 08:45:37'),
(23, 'HomeAdvertising_delete', 'HomeAdvertising', '2018-08-14 08:45:37'),
(24, 'Product_main', 'Product', '2018-08-14 08:45:37'),
(25, 'Product_create', 'Product', '2018-08-14 08:45:37'),
(26, 'Product_status', 'Product', '2018-08-14 08:45:37'),
(27, 'Product_edit', 'Product', '2018-08-14 08:45:37'),
(28, 'Product_delete', 'Product', '2018-08-14 08:45:37'),
(29, 'Recipt_main', 'Recipt', '2018-08-14 08:45:37'),
(30, 'Recipt_make_piad', 'Recipt', '2018-08-14 08:45:37'),
(31, 'Recipt_delete', 'Recipt', '2018-08-14 08:45:37'),
(32, 'Setting_main', 'Setting', '2018-08-14 08:45:37');

-- --------------------------------------------------------

--
-- Table structure for table `popular_questions`
--

CREATE TABLE `popular_questions` (
  `id` int(11) NOT NULL,
  `question_en` text NOT NULL,
  `question_ar` text NOT NULL,
  `answer_en` text NOT NULL,
  `answer_ar` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `popular_questions`
--

INSERT INTO `popular_questions` (`id`, `question_en`, `question_ar`, `answer_en`, `answer_ar`, `status`, `created_at`, `updated_at`) VALUES
(2, '1. Do I need to create an account to shop at mido.com.eg? How do I register?', '1. Do I need to create an account to shop at mido.com.eg? How do I register?', 'Yes, you do. Creating an account is an easy and convenient way to save delivery and order information, view current order status, record past orders, and save favorites that you can share. You may also check out as a guest if you don’t wish to create an account at this time. Click here to create your account!', 'Yes, you do. Creating an account is an easy and convenient way to save delivery and order information, view current order status, record past orders, and save favorites that you can share. You may also check out as a guest if you don’t wish to create an account at this time. Click here to create your account!', 1, '2019-07-07 13:36:13', '2019-07-07 13:36:13'),
(3, '2. How do I find products at mido.com.eg?', '2. How do I find products at mido.com.eg?', '1. By Category – Think of our categories as shopping aisles. You can browse categories on the left navigation of the website. When you hover over a category or tap on it, it will open a list of more specific sub-categories. When you are on a Sub-category page, you can narrow your browsing further by selecting one of the filters.\r\n2. Product Search – Simply type the product you are looking for into the search bar at the top of the site and click Go! You can even search for brand names, categories and product attributes.', '1. By Category – Think of our categories as shopping aisles. You can browse categories on the left navigation of the website. When you hover over a category or tap on it, it will open a list of more specific sub-categories. When you are on a Sub-category page, you can narrow your browsing further by selecting one of the filters.\r\n2. Product Search – Simply type the product you are looking for into the search bar at the top of the site and click Go! You can even search for brand names, categories and product attributes.', 1, '2019-07-07 13:39:35', '2019-07-07 13:39:35'),
(4, '3. When can I get my shopping delivered?', '3. When can I get my shopping delivered?', 'We deliver to you five days a week, from Sunday to Thursday. Our order hours are from 8:30 am to 2:30 pm sent to orders@midoonlinestore.com.eg . Orders usually take from 48 – 7 hours.', 'We deliver to you five days a week, from Sunday to Thursday. Our order hours are from 8:30 am to 2:30 pm sent to orders@midoonlinestore.com.eg . Orders usually take from 48 – 7 hours.', 1, '2019-07-07 13:40:03', '2019-07-07 13:40:03'),
(5, '4. What if MIDO does not sell a product I would like to buy?', '4. What if MIDO does not sell a product I would like to buy?', 'We’d love to hear your suggestions on what you would like to buy! Please suggest a product to us, so that we can look into stocking it for your future purchases.', 'We’d love to hear your suggestions on what you would like to buy! Please suggest a product to us, so that we can look into stocking it for your future purchases.', 1, '2019-07-07 13:40:45', '2019-07-07 13:40:45'),
(6, '5. How are my food items stored and delivered?', '5. How are my food items stored and delivered?', 'When it comes to your food, our top priority is ensuring your health and safety, and we work hard to ensure that you get the freshest food in the safest manner possible. We store and transport products in accordance with market standards and regulations. We carefully control the conditions in which your products are stored and transported, from the moment they are delivered to us to the time you receive them at your doorstep. Fresh items stay chilled and frozen items remain frozen.', 'When it comes to your food, our top priority is ensuring your health and safety, and we work hard to ensure that you get the freshest food in the safest manner possible. We store and transport products in accordance with market standards and regulations. We carefully control the conditions in which your products are stored and transported, from the moment they are delivered to us to the time you receive them at your doorstep. Fresh items stay chilled and frozen items remain frozen.', 1, '2019-07-07 13:41:20', '2019-07-07 13:41:20'),
(7, '6. I have a problem with the products in my order. What can I do next?', '6. I have a problem with the products in my order. What can I do next?', 'We ask that you please check your order upon delivery. If you’re not completely happy with your products, please reach out to us via support@mido.com.eg and we’ll assist you from there.', 'We ask that you please check your order upon delivery. If you’re not completely happy with your products, please reach out to us via support@mido.com.eg and we’ll assist you from there.', 1, '2019-07-07 13:42:10', '2019-07-07 13:42:10'),
(8, '7. How do I enter a promo code?', '7. How do I enter a promo code?', 'You’ll have the opportunity to enter a promo code during the payment stage of the checkout process. Simply enter the code (e.g. \'mido101\') followed by the \'Enter\' button on the right. Once the code has been successfully redeemed, the total amount will be updated. If there are any issues, you will see a small red box explaining the issue.', 'You’ll have the opportunity to enter a promo code during the payment stage of the checkout process. Simply enter the code (e.g. \'mido101\') followed by the \'Enter\' button on the right. Once the code has been successfully redeemed, the total amount will be updated. If there are any issues, you will see a small red box explaining the issue.', 1, '2019-07-07 13:42:41', '2019-07-07 13:42:41'),
(9, '8. Is MIDO online store secure?', '8. Is MIDO online store secure?', 'Yes. We take every precaution to protect your privacy and to prevent misuse of the private information you provide us.', 'Yes. We take every precaution to protect your privacy and to prevent misuse of the private information you provide us.', 1, '2019-07-07 13:43:16', '2019-07-07 13:43:16'),
(10, '9. There’s a problem with one of the items I ordered. Can I get a refund?', '9. There’s a problem with one of the items I ordered. Can I get a refund?', 'If an item is missing from your order or you have received a damaged item, please refund the order upon delivery or contact us for support via support@mido.com.eg', 'If an item is missing from your order or you have received a damaged item, please refund the order upon delivery or contact us for support via support@mido.com.eg', 1, '2019-07-07 13:43:42', '2019-07-07 13:43:42'),
(11, '10. When will my order arrive once it’s ordered?', '10. When will my order arrive once it’s ordered?', 'The delivery for your order usually take from 48-72 hours.', 'The delivery for your order usually take from 48-72 hours.', 1, '2019-07-07 13:44:25', '2019-07-07 13:44:25');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `short_description_en` varchar(225) NOT NULL,
  `short_description_ar` varchar(225) DEFAULT NULL,
  `description_en` text NOT NULL,
  `description_ar` text NOT NULL,
  `old_price` float DEFAULT NULL,
  `discount_percentage` float DEFAULT NULL,
  `price` float NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `position` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `brand_id`, `category_id`, `sub_category_id`, `name_en`, `name_ar`, `short_description_en`, `short_description_ar`, `description_en`, `description_ar`, `old_price`, `discount_percentage`, `price`, `quantity`, `position`, `status`, `created_at`, `updated_at`) VALUES
(2, 3, 11, 4, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '100g', '100غ', 'Raised in the clear fast flowing waters .Each Salmon has been specifically selected exclusively as per Mido’s Specs guaranteeing a consistently high quality product.\\r\\n\\r\\nThis Smoked salmon has been salted by hand and smoked in the old fashioned manner using 100% natural smoke.', 'المنتج : سلمون مدخن\\r\\n\\r\\nطريقة التدخين : تدخين على البارد\\r\\n\\r\\nالوزن عند التعبئة : 100 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 شهور', NULL, NULL, 74, 30, 126, 1, '2019-07-07 13:44:03', '2019-07-27 15:55:59'),
(4, 3, 11, 4, 'Fresh Smoked Salmon Slices', 'سلمون مدخن شرائح فريش', '100g', '100غ', 'Raised in the clear fast flowing waters .Each Salmon has been specifically selected exclusively as per Mido’s Specs guaranteeing a consistently high quality product.\\r\\n\\r\\nThis Smoked salmon has been salted by hand and smoked in the old fashioned manner using 100% natural smoke.', 'المنتج : سلمون مدخن\\r\\n\\r\\nطريقة التدخين : تدخين على البارد\\r\\n\\r\\nالوزن عند التعبئة : 100 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 أيام', NULL, NULL, 74, 50, 127, 1, '2019-07-07 14:44:50', '2019-07-18 11:26:29'),
(5, 3, 11, 4, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '200g', '200غ', 'Raised in the clear fast flowing waters of Norway. Each Salmon has been specifically selected exclusively as per Mido’s Specs guaranteeing a consistently high-quality product.\\r\\n\\r\\nThis Smoked salmon has been salted by hand and smoked in the old fashioned manner using 100% natural smoke.', 'المنتج : سلمون مدخن\\r\\n\\r\\nطريقة التدخين : تدخين على البارد\\r\\n\\r\\nالوزن عند التعبئة : 100 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 شهور', NULL, NULL, 147, 49, 128, 1, '2019-07-07 14:50:38', '2019-07-29 17:14:20'),
(8, 3, 11, 4, 'Salmon Fillet', 'سلمون فيليه', '2 Pieces - 320g', 'قطعتين - 320 غ', 'Enjoy super-premium salmon fillet 320 gm (2 portions). Hand-filleted, bones removed, individually packaged and flash-frozen to maintain quality, flavor, and nutrients.', 'المنتج : سلمون فيليه قطع\\r\\n\\r\\nطريقة التعبئة : vacuum packed\\r\\n\\r\\nالوزن عند التعبئة : 320 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 شهور', NULL, NULL, 113, 27, 131, 1, '2019-07-07 15:02:31', '2019-07-29 17:23:48'),
(10, 6, 12, 6, 'Maxwell House Mild Blend Sticks', 'قهوة قورية', 'Pack of 1000 Sticks', '1000 ستيك', 'With a mild and refreshing taste, the Maxwell House instant coffee gives a smooth, satisfying coffee drink. Simply add hot water, to enjoy the mild coffee aroma. The 1.5g sticks are ideal for catering, meeting rooms and breakout areas. Supplied in packs containing 1000 sticks.', 'نوع المنتج: ستيك قهوة قورية\\r\\nالكرتونة: 1 × 1000 ستيك\\r\\nالعلامة التجارية: ماكسويل هاوس', NULL, NULL, 1562.5, 20, 138, 1, '2019-07-07 15:21:38', '2019-07-17 12:38:06'),
(12, 6, 12, 6, 'Maxwell House Instant Decaf Sticks', 'قهوة فورية  بدون كافيين', 'Pack of 1000 Sticks', '1000 ستيك', 'With a mild and refreshing taste, the Maxwell House instant decaffeinated coffee gives a smooth, satisfying coffee drink. Simply add hot water, to enjoy the mild coffee aroma. The 1.5g sticks are ideal for catering, meeting rooms and breakout areas. Supplied in packs containing 1000 sticks.', 'Product Type:  Decaffeinated Sticks\\r\\nIn The Box:  1 x Pack of 1000 Sticks\\r\\nBrand: Maxwell House', NULL, NULL, 1562.5, 20, 139, 1, '2019-07-07 15:28:27', '2019-07-17 12:38:06'),
(103, 3, 11, 4, 'Fresh Smoked Salmon Slices', 'سلمون مدخن شرائح فريش', '200g', '200غ', 'Raised in the clear fast flowing waters of Norway. Each Salmon has been specifically selected exclusively as per Mido’s Specs guaranteeing a consistently high-quality product.\\r\\n\\r\\nThis Smoked salmon has been salted by hand and smoked in the old fashioned manner using 100% natural smoke.', 'المنتج : سلمون مدخن\\r\\n\\r\\nطريقة التدخين : تدخين على البارد\\r\\n\\r\\nالوزن عند التعبئة : 100 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 أيام', NULL, NULL, 147, 43, 129, 1, '2019-07-09 11:55:57', '2019-07-28 16:55:11'),
(105, 3, 11, 4, 'Salmon Steak', 'سلمون ستيك', '2 Pieces - 320g', 'قطعتين - 320 غ', 'Raised in the clear fast flowing waters .Each Salmon has been specifically selected exclusively as per Mido’s Specs guaranteeing a consistently high quality product.', 'المنتج : سلمون ستيك قطع\\r\\n\\r\\nطريقة التعبئة : vacuum packed\\r\\n\\r\\nالوزن عند التعبئة : 320 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 شهور', NULL, NULL, 113, 19, 132, 1, '2019-07-09 12:20:35', '2019-07-29 17:23:48'),
(106, 3, 11, 4, 'Smoked Salmon Stripes', 'سلمون مدخن ستريبس', '250g', '250غ', 'Raised in the clear fast flowing waters .Each Salmon has been specifically selected exclusively as per Mido’s Specs guaranteeing a consistently high quality product.\\r\\n\\r\\nThis Smoked salmon has been salted by hand and smoked in the old fashioned manner using 100% natural smoke.', 'المنتج : سلمون مدخن ستريبس\\r\\n\\r\\nطريقة التدخين : تدخين على البارد\\r\\n\\r\\nالوزن عند التعبئة : 250 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 شهور', NULL, NULL, 154, 29, 130, 1, '2019-07-09 12:31:42', '2019-07-29 17:23:48'),
(107, 3, 11, 4, 'Smoked Salmon Slices Skin On', 'سلمون مدخن شرائح بجلد', '1050g', '1050غ', 'Our hand sliced smoked and hand salted salmon. It offers the pure pleasure of smoked salmon with no other flavors to distract from the mouth-watering taste.', 'المنتج : سلمون مدخن\\r\\n\\r\\nطريقة التدخين : تدخين على البارد\\r\\n\\r\\nالوزن عند التعبئة : 1050 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 شهور', NULL, NULL, 502, 16, 136, 1, '2019-07-09 12:41:39', '2019-07-29 17:14:20'),
(108, 3, 11, 4, 'Smoked Salmon Slices Skin Off', 'سلمون مدخن شرائح بدون جلد', '950g', '950غ', 'Our hand sliced smoked and hand salted salmon. It offers the pure pleasure of smoked salmon with no other flavors to distract from the mouth-watering taste.', 'المنتج : سلمون مدخن\\r\\n\\r\\nطريقة التدخين : تدخين على البارد\\r\\n\\r\\nالوزن عند التعبئة : 950 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 شهور', NULL, NULL, 521.5, 20, 135, 1, '2019-07-09 12:49:46', '2019-07-30 13:38:42'),
(109, 3, 11, 4, 'Whole Salmon Fillet Side', 'جنب سلمون فيليه كامل', '1300g to 1700g', '1300 غ إلى 1700 غ', 'This is an ideal dinner party meal, full of delicious flavors and is quick and easy to prepare and cook.  Serve the whole salmon fillet with mashed potatoes and steamed seasonal vegetables or salad.\\r\\nThe salmon is a good source of omega-3 fatty acids that can lower your cholesterol and may increase blood, preventing different heart diseases. It also provides all the essential amino acids, as well as A and D vitamins that protect the nervous system and plays an important role in healthy bone development and absorption of calcium. Additionally, it is a good source of B vitamin that increases the nutrient use efficiency, helps to create red cells and provides with minerals as iron, selenium and phosphorus.', 'المنتج : جنب كامل يتراوح وزنه ما بين 1300 جرام إلى 1700 جرام\\r\\n\\r\\nالوزن عند التعبئة : 1300 جرام إلى 1700 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 شهور \\r\\n\\r\\nسعر الكيلو : 325 جنيه', NULL, NULL, 325, 10, 137, 1, '2019-07-09 13:17:53', '2019-07-29 17:14:20'),
(110, 3, 11, 4, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '500g', '500غ', 'Raised in the clear fast flowing waters .Each Salmon has been specifically selected exclusively as per Mido’s Specs guaranteeing a consistently high quality product.\\r\\n\\r\\nThis Smoked salmon has been salted by hand and smoked in the old fashioned manner using 100% natural smoke.', 'المنتج : سلمون مدخن تريمنج\\r\\n\\r\\nطريقة التدخين : تدخين على البارد\\r\\n\\r\\nالوزن عند التعبئة : 500 جرام (+/-2%)\\r\\n\\r\\nطريقة الحفظ : يحفظ مجمد -18 مئوية في عبوات مفرغة الهواء\\r\\n\\r\\nمدة الصلاحية : 6 شهور', NULL, NULL, 70, 12, 134, 1, '2019-07-09 13:47:30', '2019-07-30 13:38:42'),
(111, 3, 11, 4, 'Salmon loin / Sashimi cut', 'سلمون ساشيمي', '400g', '400غ', 'The finest part of the salmon cut into sashimi-like slices, or sear it briskly on the outside to retain the melt-in-your-mouth inner texture. The perfect gourmet meal in 15 seconds!\\r\\nYou don\'t have to go to the sushi bar to enjoy the finest raw fish. We\'re proud to say that our balik salmon attracts many of sushi lovers.', 'المنتج : سلمون ساشيمي\\r\\n\\r\\nطريقة التقديم : تقدم  نيئة مقطعة على شرائح رقيقة تقدم عادة مع صلصة مثل صلصة الصويا مع معجون الواسابي أو الزنجبيل \\r\\n\\r\\nالوزن عند التعبئة : 100 جرام (+/-2%)', NULL, NULL, 216, 33, 133, 1, '2019-07-09 14:20:51', '2019-07-29 17:23:48'),
(112, 4, 12, 6, 'Ground Drip Classico Coffee - Medium Roast', 'ايلي (اسبريسو) تحميص متوسط', '250g', '250غ', 'Classico, classic roast coffee has a lingering sweetness and delicate notes of caramel, orange blossom and jasmine.', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.\\r\\nتحميص متوسط medium roast\\r\\n\\r\\nالمميزات:\\r\\n\\r\\n100% بن أرابيكا\\r\\n    عبوة معبأة ومختومة للحفاظ على الرائحة والطعم\\r\\n\\r\\nالعبوة:\\r\\n250 جرام', NULL, NULL, 198, 19, 69, 1, '2019-07-09 14:48:25', '2019-07-24 19:19:07'),
(113, 4, 12, 6, 'Ground Drip Decaffeinated Coffee', 'ايلي (اسبريسو) بدون كافيين', '250g', '250غ', 'Rich and balanced with notes of caramel with no higher than 0.1% caffeine.', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.\\r\\n\\r\\nالمميزات:\\r\\n\\r\\n100% بن أرابيكا\\r\\n    عبوة معبأة ومختومة للحفاظ على الرائحة والطعم\\r\\n\\r\\nالعبوة:\\r\\n250 جرام', NULL, NULL, 198, 0, 70, 1, '2019-07-09 15:02:03', '2019-07-28 10:27:38'),
(114, 4, 12, 6, 'Ground Drip Classico Coffee - Medium Roast', 'ايلي (اسبريسو) تحميص متوسط', '125g', '125غ', 'Classico, classic roast coffee has a lingering sweetness and delicate notes of caramel, orange blossom and jasmine.', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.\\r\\n\\r\\nالمميزات:\\r\\n100% بن أرابيكا\\r\\nعبوة معبأة ومختومة للحفاظ على الرائحة والطعم\\r\\n\\r\\nالعبوة:\\r\\n125 جرام', NULL, NULL, 104.5, 0, 66, 1, '2019-07-09 16:16:34', '2019-07-25 12:18:41'),
(115, 4, 12, 6, 'Filter Coffee Dark Roast', 'قهوة  (فلتر كوفي) illy مطحونة', '250g', '250غ', 'Intense, robust and full flavored with notes of deep cocoa', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.\\r\\n\\r\\nالمميزات:\\r\\n100% بن أرابيكا\\r\\nعبوة معبأة ومختومة للحفاظ على الرائحة والطعم\\r\\n\\r\\nالعبوة:\\r\\n250 جرام', NULL, NULL, 198, 19, 68, 1, '2019-07-09 16:25:58', '2019-07-24 19:19:07'),
(116, 4, 12, 6, 'iperEspresso Capsules Classico - Medium Roast', 'ايلي كبسولات  تحميص متوسط', '21 capsules', '21 كبسولة', 'Classico, classic roast coffee has a lingering sweetness and delicate notes of caramel, orange blossom and jasmine.', '• العلامة التجارية: ايلي \\r\\n• شكل القهوة: كبسولات\\r\\n• نوع الكافيين: تحتوي على الكافيين\\r\\n• نوع القهوة: اسبرسو\\r\\n• نوع التحميص: متوسط\\r\\n• تعبئة و تغلفة القهوة: عبوة', NULL, NULL, 297, 5, 72, 1, '2019-07-09 17:06:27', '2019-07-29 14:54:40'),
(117, 4, 12, 6, 'iperEspresso Capsules Intenso Capsules - Dark Roast', 'ايلي كبسولات  تحميص داكن', '21 capsules', '21 كبسولة', 'Classico, classic roast coffee has a lingering sweetness and delicate notes of caramel, orange blossom and jasmine.', '• العلامة التجارية: ايلي\\r\\n• شكل القهوة: كبسولات\\r\\n• نوع الكافيين: تحتوي على الكافيين\\r\\n• نوع القهوة: اسبرسو\\r\\n• نوع التحميص: داكن\\r\\n• تعبئة و تغلفة القهوة: عبوة', NULL, NULL, 297, 0, 73, 1, '2019-07-10 14:10:59', '2019-07-21 10:49:00'),
(118, 4, 12, 6, 'Arabica Selection iperEspresso Etiopia', 'ايلي كبسولات من البن الأثيوبي - مونو ارابيكا', '21 capsules', '21 كبسولة', 'Etiopia- Delicate Intensity\\r\\nDelicate and aromatic with gentle notes of jasmine.', '• العلامة التجارية: ايلي\\r\\n• شكل القهوة: كبسولات\\r\\n• نوع الكافيين: تحتوي على الكافيين\\r\\n• نوع القهوة: اسبرسو\\r\\n• نوع التحميص: متوسط\\r\\n• تعبئة و تغلفة القهوة: عبوة', NULL, NULL, 297, 0, 75, 1, '2019-07-10 14:19:45', '2019-07-21 10:49:21'),
(119, 4, 12, 6, 'Arabica Selection iperEspresso Brasile', 'ايلي كبسولات من البن البرازيلي', '21 capsules', '21 كبسولة', 'Brasile - Intense Taste\\r\\nIntense and full flavored, with notes of caramel', '• العلامة التجارية: ايلي \\r\\n• شكل القهوة: كبسولات\\r\\n• نوع الكافيين: تحتوي على الكافيين\\r\\n• نوع القهوة: اسبرسو\\r\\n• نوع التحميص: غامقة\\r\\n• تعبئة و تغلفة القهوة: عبوة', NULL, NULL, 297, 20, 77, 1, '2019-07-10 14:59:33', '2019-07-17 12:38:06'),
(120, 4, 12, 6, 'Ground Drip Decaffeinated Coffee', 'ايلي (اسبريسو) بدون كافيين', '125g', '125غ', 'Rich and balanced with notes of caramel with no higher than 0.1% caffeine.', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.\\r\\n\\r\\nالمميزات:\\r\\n\\r\\n100% بن أرابيكا\\r\\nعبوة معبأة ومختومة للحفاظ على الرائحة والطعم\\r\\n\\r\\nالعبوة:\\r\\n125 جرام', NULL, NULL, 104.5, 0, 67, 1, '2019-07-10 15:11:25', '2019-07-28 15:16:53'),
(121, 4, 12, 6, 'Instant Coffee Smooth Taste', 'قهوة سريعة التحضير تحميص وسط', '95g', '95غ', 'Medium Roast\\r\\nRich and balanced with notes of caramel and chocolate.', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.\\r\\nتحميص متوسط medium roast\\r\\n\\r\\nالمميزات:\\r\\n100% بن أرابيكا\\r\\nعبوة معبأة ومختومة للحفاظ على الرائحة والطعم\\r\\n\\r\\nالعبوة:\\r\\n95 جرام', NULL, NULL, 173, 10, 64, 1, '2019-07-10 15:25:32', '2019-07-17 12:38:06'),
(122, 4, 12, 6, 'E.S.E. Pods Medium Roast', 'كبسولات قهوة اسبريسو فردية', '18 pods', '18 كبسولة', 'Medium Roast\\r\\nRich and balanced with notes of caramel and chocolate.', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.\\r\\nتحميص متوسط medium roast\\r\\n\\r\\nالمميزات:\\r\\n100% بن أرابيكا\\r\\n\\r\\nالعبوة:\\r\\n18 كبسولة', NULL, NULL, 137, 10, 65, 1, '2019-07-11 10:23:11', '2019-07-17 12:38:06'),
(123, 7, 12, 7, 'Moroccan Mint Green Tea', 'شاي أخضر مغربي بالنعناع', '25 tea bags', '25 كيس شاي', 'An enlivening and wonderfully refreshing green tea combining natural mint leaves with green tea in a traditional and wholly natural  fusion of flavours. The marriage produces a sparkling tea that adds a minty fragrance and taste to a clean, and bright green tea.', 'نوع الشاي : شاي أخضر', NULL, NULL, 50, 10, 87, 1, '2019-07-11 10:42:03', '2019-07-17 12:38:06'),
(124, 7, 12, 7, 'Nuwara Eliya Pekoe Black Tea', 'نوارا إليا بيكوي شاي أسود', '100g', '100غ', 'Tea from Nuwara Eliya, the highest elevation tea growing region in Sri Lanka, also called \'Little England\', combines sophisticated flavour, with a delicate, golden infusion. This tea country ‘capital’ is at the centre of the four premier tea growing regions of Ceylon. Her teas are relished by connoisseurs for their brightness and distinctive flavour. This seasonal Nuwara Eliya tea is distinctive amongst Ceylon Teas, recognizable by the delicate and gentle character, the golden glow of the tea, and its soft aroma.', 'نوع الشاي : شاي أسود', NULL, NULL, 150, 0, 110, 1, '2019-07-11 10:50:40', '2019-07-21 14:36:14'),
(126, 7, 12, 7, 'Ceylon Green Tea', 'شاي سيلان أخضر', '50 tea bags', '50 كيس شاي', 'Gently steamed Ceylon Green tea with a mildly astringent taste, characteristic of the finest green tea. The pale yellow infusion is tinged with olive highlights. A pleasant, delicate tea with a lightly sweet finish.', 'نوع الشاي : شاي أخضر', NULL, NULL, 212.5, 20, 91, 1, '2019-07-11 11:01:01', '2019-07-29 14:54:40'),
(127, 7, 12, 7, 'Sencha Green Extra Special', 'سنشا الأخضر سبيشال', '95g', '95غ', 'Sencha is a steamed green tea, the most popular tea in Japan where its delicate flavour, and mild finish are especially appreciated. Sencha offers a gentle cup, and a smooth, herbal finish with a touch of sweetness. This is a pleasing and refreshing green tea and can be enjoyed throughout the day. It is an ideal accompaniment to lunch or dinner, aiding digestion and refreshing the palate.', 'نوع الشاي : شاي أخضر', NULL, NULL, 142.5, 0, 109, 1, '2019-07-11 11:04:29', '2019-07-21 14:33:06'),
(128, 7, 12, 7, 'The Original Earl Grey', 'ايرل جراي الأصلي', '50 tea bags', '50 كيس شاي', 'A legendary tea, named after Charles, 2nd Earl of Grey and former Prime Minister of England. This tea is a combination of a rich and full bodied Ceylon Single Origin Tea with the flavour of Bergamot, a citrus fruit that is native to Calabria in Italy. The strong and distinctive Ceylon Tea partners harmoniously with the Bergamot flavour to offer an authentic Earl Grey Tea. Full bodied yet aromatic, with hints of citrus and a lingering and mildly sweet floral note, this is a majestic tea.', 'نوع الشاي : شاي أسود', NULL, NULL, 212.5, 10, 97, 1, '2019-07-11 11:35:16', '2019-07-17 12:38:06'),
(129, 7, 12, 7, 'Elegant Earl Grey', 'ايرل جراي', '50 tea bags', '50 كيس شاي', 'A bold and bright single region Ceylon Tea, grown at 5500ft altitude, gently fused with Bergamot flavour. The result is a balanced, medium strength tea with the citrus note that is known as Earl Grey. This floral and fruity flavour balances the strength of the tea, to offer a refreshing and delightful drink.', 'نوع الشاي : شاي أسود بنكهة', NULL, NULL, 212.5, 10, 89, 1, '2019-07-11 11:42:39', '2019-07-17 12:38:06'),
(130, 7, 12, 7, 'Brilliant Breakfast', 'بريكفاست', '125g', '125غ', 'A bright, brisk and bold tea. Intense and majestic, this tea offers body, strength, colour and pungency representing the essence of a fine Ceylon tea. Grown in the Dimbula Valley, the Broken Orange Pekoe leaf yields a burgundy coloured brew which is robust and strong yet bright with an energetic personality. The fine BOP grade of tea with its small particle size produces the strength in this tea.', 'نوع الشاي : شاي اسود', NULL, NULL, 187, 10, 111, 1, '2019-07-11 12:10:53', '2019-07-17 12:38:06'),
(131, 7, 12, 7, 'Ceylon Cinnamon Spice Tea', 'شاي سيلان بالقرفة', '100g', '100غ', 'The famed Island of Ceylon was sought by ancient explorers for its spices and, since the late 1800s, also for its tea. This tea is a combination of Ceylon’s finest produce – a Single Region Tea, grown at 4,000 feet elevation in the Dimbula valley, and Ceylon Cinnamon, known as ‘true Cinnamon’. When infused, the brew appears golden brown and offers brightness, medium body and a sweet piquancy. The woody note in Cinnamon, with its pervasive fragrance, is enlivening, and complements the tea perfectly with a spicy finish', 'نوع الشاي : شاي أسود بنكهة', NULL, NULL, 150, 20, 112, 1, '2019-07-11 12:16:41', '2019-07-17 12:38:06'),
(132, 7, 12, 7, 'English Breakfast', 'انجلش بريكفاست', '50 tea bags', '50 كيس شاي', 'A bright morning tea, with body, strength, colour and pungency representing the essence of fine Ceylon tea. The beautifully even Broken Orange Pekoe leaf yields a burgundy coloured brew which is robust and strong yet bright with a bold personality', 'نوع الشاي : شاي اسود', NULL, NULL, 212.5, 20, 93, 1, '2019-07-11 12:23:33', '2019-07-17 12:38:06'),
(133, 7, 12, 7, 'Fragrant Jasmine Green Tea', 'شاي أخضر بالياسمين', '50 tea bags', '50 كيس شاي', 'An inspiring green tea with bold leaf appearance, combined with petals of natural Jasmine flowers. The brew produced by this combination is mild and gentle; the special fragrance and a touch of sweetness comes from the night blooming Jasmine flower. A soothing and meditative tea.', 'نوع الشاي : شاي أخضر', NULL, NULL, 212.5, 20, 92, 1, '2019-07-11 12:34:36', '2019-07-17 12:38:06'),
(134, 4, 12, 6, 'Espresso Coffee beans', 'قهوة إيلي (إسبريسو)  حبوب', '1.5Kg', '1.5كغ', 'Classico (Medium Roast)\\r\\nClassico, classic roast coffee has a lingering sweetness and delicate notes of caramel, orange blossom and jasmine.', 'اسطوانة 1.5 كيلو للمقاهي فقط', NULL, NULL, 1028, 0, 125, 1, '2019-07-14 09:16:32', '2019-07-21 10:48:22'),
(135, 5, 12, 6, 'Jacobs Bankett Filter Coffee', 'Jacobs Bankett Filter Coffee', '1Kg', '1كغ', 'The magic aroma has made Jacobs famous for delivering a wonderful cup of coffee since 1895. Jacobs Bankett Medium is a round, balanced and harmonious Arabica and Robusta blend, with a strong flavor profile and medium acidity. It has a long-lasting, full aromatic body.', 'The magic aroma has made Jacobs famous for delivering a wonderful cup of coffee since 1895. Jacobs Bankett Medium is a round, balanced and harmonious Arabica and Robusta blend, with a strong flavor profile and medium acidity. It has a long-lasting, full aromatic body.', NULL, NULL, 420, 1, 120, 1, '2019-07-14 09:33:49', '2019-07-17 12:38:06'),
(136, 5, 12, 6, 'Jacobs Kronung Filter Coffee', 'Jacobs Kronung Filter Coffee', '250g', '250غ', 'Filter Coffee in vacuum pack\\r\\n\\r\\nNet weight: 250g', 'Filter Coffee in vacuum pack\\r\\n\\r\\nNet weight: 250g', NULL, NULL, 78, 10, 122, 1, '2019-07-14 09:40:44', '2019-07-17 12:38:06'),
(137, 5, 12, 6, 'Jacobs Espresso Intenso Coffee Capsules Compatible for Nespresso Machines', 'كبسولات قهوة جاكوبس متوافقة مع ماكينات نسبريسو 10 Intenso', '10 Capsules', '10 كبسولة', 'Jacobs Espresso Intenso Coffee Capsules Compatible for Nespresso Machines', 'من كبسولات نسبريسو من شركة جاكوبس JACOBS العريقة كبسولات ألمنيوم بجودة عالية ونكهة مميزة', NULL, NULL, 91, 10, 113, 1, '2019-07-14 09:44:57', '2019-07-17 12:38:06'),
(138, 5, 12, 6, 'Jacobs Espresso Lungo Coffee Capsules Compatible for Nespresso Machines', 'كبسولات قهوة جاكوبس متوافقة مع ماكينات نسبريسو 6 لونجو', '10 Capsules', '10 كبسولة', 'Jacobs Espresso Lungo Coffee Capsules Compatible for Nespresso Machines.', 'من كبسولات نسبريسو من شركة جاكوبس JACOBS العريقة كبسولات ألمنيوم بجودة عالية ونكهة مميزة', NULL, NULL, 91, 9, 114, 1, '2019-07-14 09:49:46', '2019-07-25 17:38:07'),
(139, 5, 12, 6, 'Jacobs Caffè Crema Espresso Beans', 'Jacobs Caffè Crema Espresso Beans', '1Kg', '1كغ', 'Jacobs Crema Espresso is a selection of the finest beans are harmoniously blended and expertly roasted to form this full-bodied and balanced coffee. Jacobs roasting masters ensure a special coffee experience with the magical pampering aroma through the optimal medium brown roasting of the coffee beans in the gentle drum roaster.', 'Jacobs Crema Espresso is a selection of the finest beans are harmoniously blended and expertly roasted to form this full-bodied and balanced coffee. Jacobs roasting masters ensure a special coffee experience with the magical pampering aroma through the optimal medium brown roasting of the coffee beans in the gentle drum roaster.', NULL, NULL, 355, 10, 121, 1, '2019-07-14 10:03:24', '2019-07-29 11:15:17'),
(140, 5, 12, 6, 'Jacobs Espresso 7 Classico Coffee Capsules Nespresso Compatible Coffee', 'كبسولات قهوة جاكوبس متوافقة مع ماكينات نسبريسو 7 كلاسيكو', '10 Capsules', '10 كبسولة', 'Jacobs Espresso Intenso Coffee Capsules Compatible for Nespresso Machines', 'Jacobs Espresso Intenso Coffee Capsules Compatible for Nespresso Machines', NULL, NULL, 91, 10, 115, 1, '2019-07-14 10:07:57', '2019-07-17 12:38:06'),
(141, 5, 12, 6, 'Jacob\'s Coffee Jacobs 2 in 1', 'قهوة سريعة التحضير 2 في 1', '20 Sticks', '20 ستيك', 'Coffee with sugar, in one easy pack for the perfect cup of Jacobs instant coffee.', 'Coffee with sugar, in one easy pack for the perfect cup of Jacobs instant coffee.', NULL, NULL, 100, 0, 124, 1, '2019-07-14 10:17:21', '2019-07-17 12:38:06'),
(142, 5, 12, 6, 'Jacobs Export Beans', 'Jacobs Export Beans', '1Kg', '1كغ', 'The magic aroma has made Jacobs famous for delivering a wonderful cup of coffee since 1895. Jacobs Export is a round, balanced and harmonious Arabica and Robusta blend, with a strong flavor profile and medium acidity. It has a long-lasting, full aromatic body.', 'The magic aroma has made Jacobs famous for delivering a wonderful cup of coffee since 1895. Jacobs Export is a round, balanced and harmonious Arabica and Robusta blend, with a strong flavor profile and medium acidity. It has a long-lasting, full aromatic body.', NULL, NULL, 350, 10, 118, 1, '2019-07-14 10:33:53', '2019-07-17 12:38:06'),
(144, 5, 12, 6, 'Jacobs Export Filter Coffee', 'Jacobs Export Filter Coffee', '1Kg', '1كغ', 'The magic aroma has made Jacobs famous for delivering a wonderful cup of coffee since 1895. Jacobs Export Filter Coffee is a round, balanced and harmonious Arabica and Robusta blend, with a strong flavor profile and medium acidity. It has a long-lasting, full aromatic body.', 'The magic aroma has made Jacobs famous for delivering a wonderful cup of coffee since 1895. Jacobs Export Filter Coffee is a round, balanced and harmonious Arabica and Robusta blend, with a strong flavor profile and medium acidity. It has a long-lasting, full aromatic body.', NULL, NULL, 340, 0, 117, 1, '2019-07-14 10:39:01', '2019-07-17 12:38:06'),
(145, 5, 12, 6, 'Jacobs Cronat Gold Instant Coffee', 'Jacobs Cronat Gold Instant Coffee', '200g', '200غ', 'Jacob\'s Cronat Gold is 100% finest quality instant coffee from Jacob\'s. The harmonious blend has been carefully selected by Jacob\'s experts for the demanding connaisseurs that appreciate its fine aromatic taste.', 'Jacob\'s Cronat Gold is 100% finest quality instant coffee from Jacob\'s. The harmonious blend has been carefully selected by Jacob\'s experts for the demanding connaisseurs that appreciate its fine aromatic taste.', NULL, NULL, 200, 0, 116, 1, '2019-07-14 10:50:13', '2019-07-24 09:16:35'),
(146, 5, 12, 6, 'Jacobs Original 3 in 1', 'Jacobs Original 3 in 1', '20 Sticks', '20 ستيك', 'Coffee with sugar, in one easy pack for the perfect cup of Jacobs instant coffee.', 'Coffee with sugar, in one easy pack for the perfect cup of Jacobs instant coffee.', NULL, NULL, 100, 0, 123, 1, '2019-07-14 10:52:22', '2019-07-17 12:38:06'),
(147, 5, 12, 6, 'Jacobs Bankett Espresso Beans', 'Jacobs Bankett Espresso Beans', '1Kg', '1كغ', 'The magic aroma has made Jacobs famous for delivering a wonderful cup of coffee since 1895. Jacobs Bankett Medium is a round, balanced and harmonious Arabica and Robusta blend, with a strong flavor profile and medium acidity. It has a long-lasting, full aromatic body.', 'The magic aroma has made Jacobs famous for delivering a wonderful cup of coffee since 1895. Jacobs Bankett Medium is a round, balanced and harmonious Arabica and Robusta blend, with a strong flavor profile and medium acidity. It has a long-lasting, full aromatic body.', NULL, NULL, 355, 0, 119, 1, '2019-07-14 10:56:06', '2019-07-17 12:38:06'),
(148, 7, 12, 7, 'Natural Rosehip with Hibiscus Flowers', 'ثمر الورد الطبيعي مع أزهار الكركديه', '90g', '90غ', 'The English have enjoyed rosehips as a herbal beverage for centuries. Its characteristic tart flavour is softened here with Hibiscus flowers. Reputedly a rich source of natural antioxidants, Rosehips and Hibiscus Flowers produce a red infusion with a strong fruity aroma. The beverage is rich in Vitamin C and is best taken without sugar. Try it with a touch of honey to keep the infusion all natural.', 'The English have enjoyed rosehips as a herbal beverage for centuries. Its characteristic tart flavour is softened here with Hibiscus flowers. Reputedly a rich source of natural antioxidants, Rosehips and Hibiscus Flowers produce a red infusion with a strong fruity aroma. The beverage is rich in Vitamin C and is best taken without sugar. Try it with a touch of honey to keep the infusion all natural.', NULL, NULL, 134, 20, 102, 1, '2019-07-14 11:47:23', '2019-07-17 12:38:06'),
(149, 7, 12, 7, 'Pure Green', 'شاي اخضر طبيعي', '25 tea bags', '25 كيس شاي', 'Pale infusion yielding a light, slightly smoky brew. Pure green tea from China with hints of herb, a touch of spice and a pleasingly mild finish.', 'نوع الشاي : شاي أخضر', NULL, NULL, 50, 0, 81, 1, '2019-07-14 11:49:15', '2019-07-21 14:31:40'),
(150, 7, 12, 7, 'English Breakfast', 'انجلش بريكفاست', '25 tea bags', '25 كيس شاي', 'Hand-picked and artisanally made in the Dimbula region of Sri Lanka which is renowned for strong and full-bodied tea.', 'نوع الشاي : شاي أسود', NULL, NULL, 50, 0, 82, 1, '2019-07-14 11:50:57', '2019-07-21 14:30:40'),
(151, 7, 12, 7, 'Italian Almond Black Tea', 'شاي اسود باللوز الإيطالي', '100g', '100غ', 'A delicious union – a medium strength Ceylon Single Region Tea from the Nawalapitiya region, fused with the bittersweet flavor of Italian Almond. Fragrant and rich, the almond and the mellow, malty note that is typical of teas from this region, combine in indulgent harmony, to produce a deep amber infusion. The tea is enveloped by a deliciously piquant aroma of almond, producing an engaging brew with a nutty, sweet edge and mellow but prominent character.', 'نوع الشاي : شاي أسود', NULL, NULL, 150, 20, 103, 1, '2019-07-14 11:56:29', '2019-07-17 12:38:06'),
(152, 7, 12, 7, 'Peach', 'خوخ', '25 tea bags', '25 كيس شاي', 'A gentle fruity aroma with the softly sweet fragrance yielding an elegant and bright tea with a touch of peach. The high grown Ceylon Pekoe from Dilmah Tea Gardens in the Dimbula Valley, produces a medium bodied tea that deliciously complements the soft flavour of peach.', 'نوع الشاي : شاي أسود', NULL, NULL, 50, 0, 83, 1, '2019-07-14 12:06:16', '2019-07-21 14:32:06'),
(153, 7, 12, 7, 'Chamomile', 'بابونج', '25 tea bags', '25 كيس شاي', 'Camomile is a gentle and relaxing herb enjoyed by many as a tonic for centuries. Camomile has a serene character and wonderful aroma. The daisy-like Camomile flower, where the subtle flavour is concentrated, has an uplifting aroma reminiscent of apples. It is often enjoyed as a gentle, naturally caffeine free infusion in the evening.', 'نوع الشاي : بالأعشاب', NULL, NULL, 50, 0, 84, 1, '2019-07-14 12:20:38', '2019-07-21 14:32:27'),
(154, 4, 12, 6, 'IperEspresso Single Flowpack Classico', 'Iperespresso Single Flowpack Classico', '100 Capsules', '100 كبسولة', '1 carton = 100 pcs\\r\\n\\r\\nEspresso coffee capsules, in pressure sealed packaging in order for its organoleptic characteristics to remain unaltered.\\r\\n\\r\\nMedium roast (red cap): mild, sweet, balanced flavor, with subtle caramel, chocolate, and light floral aromas.\\r\\n\\r\\nIlly Iperespresso capsules contain a unique authentic blend of 9 different varieties of high quality Arabic coffee.', '-', NULL, NULL, 1175, 99, 78, 1, '2019-07-14 12:38:08', '2019-07-23 08:46:45'),
(155, 7, 12, 7, 'Single Estate Darjeeling', 'دارجيلنغ شاي اسود', '100g', '100غ', 'A distinctive and refined experience in tea, fine Darjeeling comes from India’s most famous Darjeeling. This is a Single Estate Darjeeling Tea, grown at 7,000 feet above sea level and within sight of Mount Everest. It offers the characteristically subtle, delicate and sophisticated taste of the best of Darjeeling. The leaf is dark brown with hints of olive, developing to a golden infusion when brewed. The light brew is slightly floral and has a prominent Muscatel note in its finish. The brew maintains a slight piquancy and character whilst overall being soft and gentle', 'نوع الشاي : شاي أسود', NULL, NULL, 150, 0, 104, 1, '2019-07-14 13:04:32', '2019-07-17 12:38:06'),
(156, 7, 12, 7, 'Moroccan Mint Green Tea', 'شاي اخضر مغربي بالنعناع', '50 tea bags', '50 كيس شاي', 'Gently uplifting, this combination of green tea with Peppermint leaves produces a delicious and all natural tea experience. The olive green Hyson leaf is tightly curled but opens dramatically when infused, yielding a medium strength amber brew of tea embraced by Peppermint. An aromatic and zestful tea, our Moroccan Mint combines the elegance of Ceylon Young Hyson green tea with an energetic and cleansing Mint to offer a perfect after dinner tea.', 'نوع الشاي : شاي أخضر', NULL, NULL, 212.5, 10, 94, 1, '2019-07-15 09:27:46', '2019-07-17 12:38:06'),
(157, 7, 12, 7, 'Pure Chamomile Flowers', 'أزهار البابونج النقي', '42g', '42غ', 'Chamomile is a gentle and relaxing herb enjoyed by Europeans as a tonic for centuries. The daisy-like Chamomile flower has an uplifting aroma reminiscent of apples. Its subtle flavour is concentrated in the flower and this infusion therefore offers the infusion of the Chamomile flower. Chamomile has a serene character and wonderful aroma. It is often enjoyed as a gentle, naturally caffeine free infusion in the evening.', 'نوع الشاي : بالاغشاب', NULL, NULL, 63, 20, 105, 1, '2019-07-15 09:36:40', '2019-07-17 12:38:06'),
(158, 7, 12, 7, 'Natural Rosehip with Hibiscus Flowers', 'الورد الطبيعي مع أزهار الكركديه', '30 tea bags', '30 كيس شاي', 'The English have enjoyed rosehips as a herbal beverage for centuries. Its characteristic tart flavour is softened here with Hibiscus flowers. Reputedly a rich source of natural antioxidants, Rosehips and Hibiscus Flowers produce a red infusion with a strong fruity aroma. The beverage is rich in Vitamin C and is best taken without sugar. Try it with a touch of honey to keep the infusion all natural.', 'نوع الشاي : بالاعشاب', NULL, NULL, 212.5, 10, 95, 1, '2019-07-15 09:43:02', '2019-07-17 12:38:06'),
(159, 7, 12, 7, 'Jasmine Petals Green Tea', 'شاي أخضر بالياسمين', '25 tea bags', '25 كيس شاي', 'A mild green tea with the traditional, natural Jasmine flavour. The perfume of Jasmine is added using petals of Jasmine, a process several thousand years old. Uplifting & aromatic.', 'نوع الشاي : شاي أخضر', NULL, NULL, 50, 10, 86, 1, '2019-07-15 10:06:18', '2019-07-17 12:38:06'),
(160, 7, 12, 7, 'Rose with French Vanilla', 'روز مع الفانيلا الفرنسية', '100g', '100غ', 'A seductive tea, combining a fine Single Region Pekoe from the Nuwara Eliya region with the sensuous and almost mystical fragrance of rose petals. Celebrated throughout history for its romance, the rose is one of the most pleasing of flavours. The gentle tea is encircled by the soft and slightly sweet aroma of red Rose to offer an alluring and gentle brew. The Rose fragrance is tinged with the flavour of French Vanilla.', 'نوع الشاي : بالاعشاب', NULL, NULL, 150, 10, 106, 1, '2019-07-15 10:15:19', '2019-07-17 12:38:06'),
(161, 7, 12, 7, 'The Original Earl Grey', 'ايرل جراي الأصلي', '100g', '100غ', 'A legendary tea, named after Charles, 2nd Earl of Grey and former Prime Minister of England. This tea is a combination of a rich and full bodied Ceylon Single Origin Tea with the flavour of Bergamot, a citrus fruit that is native to Calabria in Italy. The strong and distinctive Ceylon Tea partners harmoniously with the Bergamot flavour to offer an authentic Earl Grey Tea. Full bodied yet aromatic, with hints of citrus and a lingering and mildly sweet floral note, this is a majestic tea.', 'نوع الشاي : شاي أسود', NULL, NULL, 150, 16, 107, 1, '2019-07-15 10:24:23', '2019-07-17 12:38:06'),
(162, 7, 12, 7, 'Chamomile', 'بابونج', '50 tea bags', '50 كيس شاي', 'Camomile is a gentle and relaxing herb enjoyed by many as a tonic for centuries. Camomile has a serene character and wonderful aroma. The daisy-like Camomile flower, where the subtle flavour is concentrated, has an uplifting aroma reminiscent of apples. It is often enjoyed as a gentle, naturally caffeine free infusion in the evening.', 'نوع الشاي : أعشاب', NULL, NULL, 212.5, 10, 88, 1, '2019-07-15 12:16:51', '2019-07-17 12:38:06'),
(163, 7, 12, 7, 'Black Tea with Mint', 'شاي أخضر بالنعناع', '25 tea bags', '25 كيس شاي', 'A bright morning tea, with body, strength, colour and pungency representing the essence of fine Ceylon tea.', 'نوع الشاي : شاي أسود', NULL, NULL, 50, 0, 85, 1, '2019-07-15 12:39:08', '2019-07-21 14:31:09'),
(164, 7, 12, 7, 'Rose with French Vanilla', 'روز مع الفانيلا الفرنسية', '50 tea bags', '50 كيس شاي', 'Inspiringly aromatic, with a medium bodied floral note, the flavour of Rose with a hint of French Vanilla combines with Ceylon tea in a perfect embrace. Elegant and sophisticated, a perfect afternoon or after dinner tea with a touch of romance.', 'نوع الشاي : شاي أسود ينكهة', NULL, NULL, 212.5, 10, 98, 1, '2019-07-15 12:47:29', '2019-07-17 12:38:06'),
(165, 7, 12, 7, 'Moroccan Mint Green Tea', 'شاي أخضر مغربي بالنعناع', '80g', '80غ', 'Gently uplifting, this combination of green tea with Peppermint leaves produces a delicious and all natural tea experience. The olive green Hyson leaf is tightly curled but opens dramatically when infused, yielding a medium strength amber brew of tea embraced by Peppermint. An aromatic and zestful tea, our Moroccan Mint combines the elegance of Ceylon Young Hyson green tea with an energetic and cleansing Mint to offer a perfect after dinner tea.', 'نوع الشاي : شاي أخضر', NULL, NULL, 120, 10, 108, 1, '2019-07-15 12:54:56', '2019-07-17 12:38:06'),
(166, 7, 12, 7, 'Nuwara Eliya Pekoe Black Tea', 'نوارا إليا بيكوي الشاي الأسود', '50 tea bags', '50 كيس شاي', 'Tea from Nuwara Eliya, the highest elevation tea growing region in Sri Lanka, also called ‘Little England’, combines sophisticated flavour, with a delicate, golden infusion. This tea country ‘capital’ is at the centre of the four premier tea growing regions of Ceylon. Her teas are relished by connoisseurs for their brightness and distinctive flavour. This seasonal Nuwara Eliya tea is distinctive amongst Ceylon Teas, recognizable by the delicate and gentle character, the golden glow of the tea, and its soft aroma.', 'نوع الشاي : اسود', NULL, NULL, 212.5, 20, 96, 1, '2019-07-15 12:58:01', '2019-07-21 14:34:26'),
(167, 7, 12, 7, 'Italian Almond Black Tea', 'شاي اسود باللوز الإيطالي', '50 tea bags', '50 كيس شاي', 'A delicious union – a medium strength Ceylon Single Region Tea from the Nawalapitiya region, fused with the bittersweet flavor of Italian Almond. Fragrant and rich, the almond and the mellow, malty note that is typical of teas from this region, combine in indulgent harmony, to produce a deep amber infusion. The tea is enveloped by a deliciously piquant aroma of almond, producing an engaging brew with a nutty, sweet edge and mellow but prominent character.', 'نوع الشاي : شاي أسود', NULL, NULL, 212.5, 10, 101, 1, '2019-07-15 13:00:34', '2019-07-17 12:38:06'),
(168, 7, 12, 7, 'Ceylon Spice Chai', 'سيلان سبايس تشاي', '50 tea bags', '50 كيس شاي', 'Authentic Chai combining Ceylon’s famous Spices - Clove, Ginger and Cinnamon – with our Dombagastalawa BOP Special Single Estate Tea. The richly aromatic Clove and the woody sweetness of Cinnamon, lightly enhanced by a hint of Gingery warmth, offers a piquant and spirited Real Chai.', 'نوع الشاي : شاي أسود', NULL, NULL, 212.5, 20, 90, 1, '2019-07-15 13:14:14', '2019-07-17 12:38:06'),
(169, 7, 12, 7, 'Ceylon Cinnamon Spice Tea', 'شاي سيلان بالقرفة و البهارات', '50 tea bags', '50 كيس شاي', 'The famed Island of Ceylon was sought by ancient explorers for its spices and, since the late 1800s, also for its tea. This tea is a combination of Ceylon’s finest produce – a Single Region Tea, grown at 4,000 feet elevation in the Dimbula valley, and Ceylon Cinnamon, known as ‘true Cinnamon’. When infused, the brew appears golden brown and offers brightness, medium body and a sweet piquancy. The woody note in Cinnamon, with its pervasive fragrance, is enlivening, and complements the tea perfectly with a spicy finish.', 'نوع الشاي : شاي اسود بنكهة', NULL, NULL, 212.5, 10, 99, 1, '2019-07-15 15:14:18', '2019-07-17 12:38:06'),
(170, 7, 12, 7, 'Ceylon Silver tips White Tea', 'سيلان الفضة شاي أبيض', '30 tea bags', '30 كيس شاي', 'A rare handmade white tea from the Nuwara Eliya region of Ceylon, grown at 6,000 feet elevation. The tender buds of the tea plant are handpicked at dawn, carefully carried in a silk pouch and entirely handmade. The buds are sun dried and carefully hand rolled, producing the characteristic silver, needle-like buds, with a velvety texture. Their pale brew is surpassingly light and delicate, elusive and mild on the palate with a touch of a ‘greenish’ seasonal high grown character in the finish. The beauty of the infused buds adds to the pleasure of this unique brew', 'نوع الشاي : شاي ابيض', NULL, NULL, 212.5, 2, 100, 1, '2019-07-15 15:47:59', '2019-07-17 12:38:06'),
(171, 4, 12, 6, 'Arabica Selection iperEspresso Guatemala', 'ايلي كبسولات من البن جواتيمالا - مونو ارابيكا', '21 Capsules', '21 كبسولة', 'Guatemala - Bold Intensity\\r\\nComplex and balanced, with notes of chocolate.', '• العلامة التجارية: ايلي\\r\\n• شكل القهوة: كبسولات\\r\\n• نوع الكافيين: تحتوي على الكافيين\\r\\n• نوع القهوة: اسبرسو\\r\\n• نوع التحميص: متوسط\\r\\n• تعبئة و تغلفة القهوة: عبوة', NULL, NULL, 297, 0, 76, 1, '2019-07-16 09:20:00', '2019-07-17 12:38:06'),
(172, 4, 12, 6, 'IperEspresso Single Flowpack Decaf', 'كبسولات قهوة اسبريسو فردية', '100 Capsules', '100 كبسولة', '1 carton = 100 pcs\\r\\n\\r\\nEspresso coffee capsules, in pressure sealed packaging in order for its organoleptic characteristics to remain unaltered.\\r\\n\\r\\nDecaffeinated (Blue) \\r\\n\\r\\nIlly Iperespresso capsules contain a unique authentic blend of 9 different varieties of high quality Arabic coffee.', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.', NULL, NULL, 1175, 10, 80, 1, '2019-07-16 09:31:51', '2019-07-18 10:34:07'),
(173, 4, 12, 6, 'IperEspresso Single Flowpack Intenso', 'كبسولات قهوة اسبريسو فردية دارك روست', '100 Capsules', '100 كبسولة', '1 carton = 100 pcs\\r\\n\\r\\nEspresso coffee capsules, in pressure sealed packaging in order for its organoleptic characteristics to remain unaltered.\\r\\n\\r\\nINTENSO – BOLD ROAST\\r\\nChocolate and dried fruit with intense, full sweet aftertaste.\\r\\n\\r\\nIlly Iperespresso capsules contain a unique authentic blend of 9 different varieties of high quality Arabic coffee.', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.', NULL, NULL, 1175, 10, 79, 1, '2019-07-16 11:02:56', '2019-07-18 10:33:48'),
(174, 4, 12, 6, 'Ground Drip Intenso Coffee', 'ايلي (اسبريسو) تحميص متوسط', '250g', '250غ', 'Intenso, bold roast coffee has a pleasantly robust finish, with warm notes of cocoa and dried fruit.', 'قهوة غنية ورائحة عطرة مميزة. طعم ثابت يوفر تجربة القهوة الإيطالية الأصلية. تنتج إيلي هذا المزيج العظيم ويتكون من 100٪ قهوة أرابيكا. يتم إختيار القهوة بعناية من أفضل المزارع في البرازيل وأمريكا الوسطى والهند وأفريقيا ثم يخضعن الى 114 خطوة مراقبة جودة قبل البدء في بيعها.\\r\\nتحميص داكن\\r\\n\\r\\nالمميزات:\\r\\n100% بن أرابيكا\\r\\nعبوة معبأة ومختومة للحفاظ على الرائحة والطعم\\r\\n\\r\\nالعبوة:\\r\\n250 جرام', NULL, NULL, 198, 0, 71, 1, '2019-07-16 11:15:05', '2019-07-17 12:38:06'),
(175, 4, 12, 6, 'iperEspresso Capsules Decaffeinated Classico - Medium Roast', 'ايلي كبسولات بدون كافيين', '21 Capsules', '21 كبسولة', 'Classico, classic roast coffee has a lingering sweetness and delicate notes of caramel, orange blossom and jasmine.', '• العلامة التجارية: ايلي\\r\\n• شكل القهوة: كبسولات\\r\\n• نوع الكافيين: بدون كافيين\\r\\n• نوع القهوة: اسبرسو\\r\\n• نوع التحميص: داكن\\r\\n• تعبئة و تغلفة القهوة: عبوة', NULL, NULL, 297, 0, 74, 1, '2019-07-16 12:02:17', '2019-07-17 12:38:06'),
(176, 8, 12, 7, 'Darjeeling Black Tea', 'شاي دارجيلنج الأسود', '100g', '100غ', 'Black tea from Darjeeling harvested on the Himalayan foothills, delivering an amber-coloured liquor,with light notes of ripe fruits. To drink at any time of the day.\\r\\nConditioned in a metal box.', 'نوع الشاي : شاي أسود', NULL, NULL, 187, 10, 52, 1, '2019-07-16 12:12:59', '2019-07-17 12:38:06'),
(177, 8, 12, 7, 'Miss Dammann Green Tea', 'ميس دامان شاي أخضر', '100g', '100غ', 'Spirited and spicy like a Parisian mademoiselle, Miss Dammann combines green tea and ginger with the fruity, tangy flavours of lemon and passion fruit. A fresh and lively blend. Packed in rose gold metal box.', 'نوع الشاي : شاي أخضر', NULL, NULL, 187, 10, 53, 1, '2019-07-16 12:15:54', '2019-07-17 12:38:06'),
(178, 8, 12, 7, 'Yunnan Vert Green Tea', 'يونان فيرت شاي أخضر', '24 tea bags', '24 كيس شاي', 'The Yunnan province produces almost exclusively black, dark teas. However, delicate green teas can also be found there, sich as this example which gives a bright yellow liquor. The infusion exhibits green and fruity notes with an enduring finish.', 'نوع الشاي : شاي أخضر', NULL, NULL, 148, 10, 59, 1, '2019-07-16 12:19:38', '2019-07-17 12:38:06'),
(179, 8, 12, 7, 'Chamomile', 'بابونج', '24 tea bags', '24 كيس شاي', 'A hardy plant grown widely in Eastern Europe and harvested between May and July. Its bright yellow infusion reveals sweet, fruity flavours with notes of pineapple', 'نوع الشاي : بالأعشاب', NULL, NULL, 199, 10, 62, 1, '2019-07-16 12:22:18', '2019-07-17 12:38:06'),
(180, 8, 12, 7, 'Earl Grey', 'ايرل جراي', '24 tea bags', '24 كيس شاي', 'A selection of fine black teas enriched with white tips and petals, and flavoured with Calabrian bergamot. The perfect balance of this superior Earl Grey will satisfy the most refined taste buds.\\r\\nPacked in metal tin.', 'نوع الشاي : شاي أسود', NULL, NULL, 137, 10, 54, 1, '2019-07-16 12:24:33', '2019-07-17 12:38:06'),
(181, 8, 12, 7, 'Darjeeling Black Tea', 'شاي دارجيلنج الأسود', '24 tea bags', '24 كيس شاي', 'Black tea from Darjeeling harvested on the Himalayan foothills, delivering an amber-coloured liquor,with light notes of ripe fruits. To drink at any time of the day.\\r\\nConditioned in a metal box.', 'نوع الشاي : شاي أسود', NULL, NULL, 137, 10, 55, 1, '2019-07-16 12:27:39', '2019-07-17 12:38:06'),
(182, 8, 12, 7, 'Breakfast', 'بريكفست', '100g', '100غ', 'A blend of Ceylon, Darjeeling and Assam teas. Enjoyed with a splash of milk, this is the perfect tonic tea to begin the day. Conditioned in a metal box.', 'نوع الشاي : شاي أسود', NULL, NULL, 187, 0, 50, 1, '2019-07-16 12:29:58', '2019-07-21 14:36:42'),
(183, 8, 12, 7, 'Jasmin Green Tea', 'شاي أخضر بالياسمين', '24 tea bags', '24 كيس شاي', 'Among China\'s most celebrated flower tea compositions, this jasmine tea provides a beautiful balance between a full-bodied green tea and the white flower\'s sweet and delicate fragrance. This is the ideal tea to drink with Chinese food.', 'نوع الشاي : شاي أخضر', NULL, NULL, 148, 10, 58, 1, '2019-07-16 12:50:04', '2019-07-17 12:38:06'),
(184, 8, 12, 7, 'Jaune Lemon Green Tea', 'شاي أخضر بالليمون', '24 tea bags', '24 كيس شاي', 'A unique and novel combination of green tea and lemongrass, harmoniously scented with lime and sweet lemon essential oils and ginger. A resolutely fresh and invigorating tea.', 'نوع الشاي : شاي اخضر', NULL, NULL, 199, 0, 61, 1, '2019-07-16 12:56:22', '2019-07-17 12:38:06'),
(185, 8, 12, 7, 'Mint Green Tea', 'شاي أخضر بالنعناع', '24 tea bags', '24 كيس شاي', 'A Gunpowder green tea, mint leaves and a natural mint aroma give this very aromatic blend incredible power and freshness. An iconic welcoming tea for the Berber people, it is customary to drink three cups of this tea in a row as a sign of gratitude and politeness. In keeping with tradition, it is served very hot and very sweet.', 'نوع الشاي : شاي اخضر', NULL, NULL, 199, 10, 63, 1, '2019-07-16 13:00:11', '2019-07-17 12:38:06'),
(186, 8, 12, 7, 'Soleil Vert Green Tea', 'شاي سوليل فير الأخضر', '24 tea bags', '24 كيس شاي', 'Green tea flavoured with blood orange essential oil. A fresh and vegetal infusion with pleasantly tangy fruity notes.', 'نوع الشاي : شاي أخضر', NULL, NULL, 148, 10, 60, 1, '2019-07-16 13:02:37', '2019-07-17 12:38:06'),
(187, 8, 12, 7, 'Earl Grey Black Tea', 'ايرل جراي', '100g', '100غ', 'Timeless and just as delicious as ever, the flavour of Bergamot is combined with black tea along with distinguished silky buds and a sprinkling of flower petals to make it a feast for the eyes. A classic Earl Grey to drink to your heart\'s content!', 'نوع الشاي : شاي أسود', NULL, NULL, 187, 5, 47, 1, '2019-07-16 13:05:38', '2019-07-17 12:38:06');
INSERT INTO `products` (`id`, `brand_id`, `category_id`, `sub_category_id`, `name_en`, `name_ar`, `short_description_en`, `short_description_ar`, `description_en`, `description_ar`, `old_price`, `discount_percentage`, `price`, `quantity`, `position`, `status`, `created_at`, `updated_at`) VALUES
(188, 8, 12, 7, 'Breakfast', 'بريكفاست', '24 tea bags', '24 كيس شاي', 'Association of small-sized leaves from thes two famous tea regions. A strong blend offering long tasting notes without any bitterness. The ideal cup for breakfast.', 'نوع الشاي : شاي أسود', NULL, NULL, 137, 10, 56, 1, '2019-07-16 13:07:36', '2019-07-17 12:38:06'),
(189, 8, 12, 7, 'Assam Black Tea', 'شاي آسام الاسود', '24 tea bags', '24 كيس شاي', 'One of the most famous plantations of Assam located on the north bank of Brahmaputra river, 120 kms from Guwahati, capital of the province. The selected offers a tea with many golden tips. Slightly tobacco nose giving an aromatic liquor with malty and slightly caramelized notes with creamy finish. A ‘grand cru’of Assam !', 'نوع الشاي : شاي أسود', NULL, NULL, 187, 10, 48, 1, '2019-07-16 13:09:22', '2019-07-17 12:38:06'),
(190, 8, 12, 7, 'Ceylan Black Tea', 'شاي سيلان الاسود', '24 tea bags', '24 كيس شاي', 'A beautifully regular leaf creating a subtly scented infusion with beautiful woody notes. Perfect tea to be enjoyed in the afternoon.', 'نوع الشاي : شاي اسود', NULL, NULL, 137, 10, 57, 1, '2019-07-16 13:11:12', '2019-07-17 12:38:06'),
(191, 8, 12, 7, 'Rooibos Cederberg Herbal Tea', 'رويبوس سيدربيرج شاي بالاعشاب', '100g', '100غ', 'The rooibos small tree, grows in wild conditions in Cederberg region, western region of South Africa. The rooibos gives an infusion with low tannin level, caffeine free. Its round and sweet taste makes it an ideal drink for children to consume served hot or iced.', 'نوع الشاي : شاي بالاعشاب', NULL, NULL, 187, 10, 51, 1, '2019-07-16 13:13:51', '2019-07-17 12:38:06'),
(192, 8, 12, 7, 'Jasmin Chung Hao Green tea', 'شاي أخضر بالياسمين', '100g', '100غ', 'Of all the jasmine teas produced in China, Jasmin Chung Hao is one of the most delicate and fragrant. Made with a superb green tea, it produces a fresh and delicate brew. A real pleasure every time, it also goes perfectly with Chinese food.', 'نوع الشاي : شاي أخضر', NULL, NULL, 187, 10, 49, 1, '2019-07-16 13:16:53', '2019-07-17 12:38:06'),
(193, 8, 12, 7, 'Carcadet Fantasia Fruit Infusion', 'شاي الكركديه', '100g', '100غ', 'This 100g box of Fantasia flavoured Carcadet offers you the the pleasure of refreshing fruit infusion combining hibiscus flowers with apple pieces, orange and rosehip peels, all deliciously scented with the aromas of blood orange.', 'نوع الشاي : بالاعشاب', NULL, NULL, 187, 5, 46, 1, '2019-07-16 13:19:47', '2019-07-17 12:38:06'),
(194, 12, 14, 10, 'Cherry Jam (Pack of 15)', 'مربى الكرز (15 قطعة)', '30g', '30غ', 'Whether you spread it on toast, slather it over waffles, or sneak spoonfuls straight from the jar, you’ll love our Cherry Preserves. With chunks of cooked cherries throughout, these preserves are as close as you’ll get to homemade without doing the picking, cooking and canning yourself.\\r\\n\\r\\nINGREDIENTS\\r\\n\\r\\nCherries, sugar, cane sugar, concentrated lemon juice, fruit pectin. An occasional pit may be present.\\r\\n\\r\\n50 Calories Per 1 tbsp (20g)', 'المكونات : كرز 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 95, 10, 43, 1, '2019-07-16 13:42:48', '2019-07-17 12:38:06'),
(195, 12, 14, 10, 'Fruits of the Forest Jam', 'مربى ثمار الغابة', '370g', '370غ', 'Each jar features chunks of real cherries, strawberries, redcurrants and raspberries. It’s the perfect mixture to spread on bread, muffins or with classic savory pairings like cheese, peanut butter or lightly salted nuts. It also combines wonderfully with many recipes and can be used as a substitute anytime a well-rounded fruit flavor is needed.\\r\\n\\r\\nINGREDIENTS\\r\\nCherries, Strawberries, Redcurrants, Raspberries, Sugar, Cane Sugar, Concentrated Lemon Juice, Fruit Pectin. An occasional pit may be present.\\r\\n\\r\\n50 Calories Per 1 tbsp (20g)', 'المكونات :  50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات.\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 91, 10, 33, 1, '2019-07-16 13:49:32', '2019-07-17 12:38:06'),
(196, 12, 14, 10, 'Four Fruits Jam', 'مربى الاربع فواكه', '370g', '370غ', 'Combining four lusciously tasty fruits, our Four Fruits Preserves’ flavor and texture are made with natural ingredients. Each jar features chunks of real cherries, strawberries, redcurrants and raspberries. It’s the perfect mixture to spread on bread, muffins or with classic savory pairings like cheese, peanut butter or lightly salted nuts. It also combines wonderfully with many recipes and can be used as a substitute anytime a well-rounded fruit flavor is needed.\\r\\n\\r\\nINGREDIENTS\\r\\nCherries, Strawberries, Redcurrants, Raspberries, Sugar, Cane Sugar, Concentrated Lemon Juice, Fruit Pectin. An occasional pit may be present.\\r\\n\\r\\n50 Calories Per 1 tbsp (20g)', 'المكونات : 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات.\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 91, 10, 34, 1, '2019-07-16 13:53:58', '2019-07-17 12:38:06'),
(197, 12, 14, 10, 'Orange Jam (Pack of 15)', 'مربى البرتقال (15 قطعة)', '30g', '30غ', 'Heady aromas of an orange grove paired with natural, fresh taste in each bite make our Orange Marmalade an anytime favorite.\\r\\n\\r\\nINGREDIENTS\\r\\n\\r\\nSugar, bitter oranges, cane sugar, concentrated lemon juice, fruit pectin\\r\\n\\r\\n50 Calories Per 1 tbsp (20g)', 'المكونات : البرتقال 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 95, 10, 44, 1, '2019-07-16 14:12:31', '2019-07-17 12:38:06'),
(198, 12, 14, 10, 'Orange Marmalade Jam', 'مربى البرتقال', '370g', '370غ', 'Heady aromas of an orange grove paired with natural, fresh taste in each bite make our Orange Marmalade an anytime favorite.\\r\\n\\r\\nINGREDIENTS\\r\\n\\r\\nSugar, bitter oranges, cane sugar, concentrated lemon juice, fruit pectin\\r\\n\\r\\n50 Calories Per 1 tbsp (20g)', 'المكونات : البرتقال 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 91, 10, 35, 1, '2019-07-16 14:18:06', '2019-07-17 12:38:06'),
(199, 12, 14, 10, 'Strawberry Jam', 'مربى الفراولة', '370g', '370غ', 'Each jar of our Strawberry Preserves features an abundance of our trademark pieces of fruit. The sweet richness of cooked strawberries creates a perfect texture and brilliant flavor. Like all Bonne Maman flavors, it is made with natural ingredients including the highest quality strawberries available.\\r\\n\\r\\nINGREDIENTS\\r\\n\\r\\nStrawberries, sugar, cane sugar, concentrated lemon juice, fruit pectin', 'المكونات : فراولة 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 91, 10, 36, 1, '2019-07-16 14:21:56', '2019-07-17 12:38:06'),
(200, 12, 14, 10, 'Cherry Jam', 'مربى الكرز', '370g', '370غ', 'Whether you spread it on toast, slather it over waffles, or sneak spoonfuls straight from the jar, you’ll love our Cherry Preserves. With chunks of cooked cherries throughout, these preserves are as close as you’ll get to homemade without doing the picking, cooking and canning yourself.\\r\\n\\r\\nINGREDIENTS\\r\\n\\r\\nCherries, sugar, cane sugar, concentrated lemon juice, fruit pectin. An occasional pit may be present.\\r\\n\\r\\n50 Calories Per 1 tbsp (20g)', 'المكونات : كرز 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 91, 10, 37, 1, '2019-07-16 14:41:00', '2019-07-17 12:38:06'),
(201, 12, 14, 10, 'Wild Blueberry Jam', 'مربى التوت الاسود', '370g', '370غ', 'Bonne Maman Wild Blueberries Preserves offers a full flavor, robust blueberry experience with every spoonful. The natural sweetness, brilliant blue color and fresh taste make our Wild Blueberry Preserves ideal for savoring on its own or as an ingredient.\\r\\n\\r\\nINGREDIENTS\\r\\n\\r\\nWild blueberries, sugar, cane sugar, concentrated lemon juice, fruit pectin.', 'المكونات : توت بري 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 91, 10, 38, 1, '2019-07-16 15:06:25', '2019-07-17 12:38:06'),
(202, 12, 14, 10, 'Caramel Spread', 'كراميل', '380g', '380غ', 'A perfect mix between milk and natural sugars that brings back memories of childhood. It can be savoured on bread, pancakes, ice cream or simply with a spoon.\\r\\n\\r\\n70 Calories Per 1 tbsp (20g)', 'مكونات : حليب مكثف محلى ، شراب الجلوكوز ، سكر ، سترات الصوديوم ، ملح ، بكتين الفاكهة\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 70 كالوري', NULL, NULL, 91, 10, 41, 1, '2019-07-16 15:43:57', '2019-07-17 12:38:06'),
(203, 12, 14, 10, 'Raspberry Jam', 'مربى التوت', '370g', '370غ', 'Rich and delicious because they are made with only the highest quality raspberries and other natural ingredients, our Raspberry Preserves are perfect for spreading on toast and pastries! Or use them as an ingredient in favorite recipes.\\r\\n\\r\\nINGREDIENTS\\r\\n\\r\\nRaspberries, sugar, cane sugar, concentrated lemon juice, fruit pectin.', 'المكونات : توت العليق الأحمر 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 91, 0, 42, 1, '2019-07-16 15:49:04', '2019-07-21 14:30:18'),
(204, 12, 14, 10, 'Apricot Jam (Pack of 15)', 'مربى المشمش (15 قطعة)', '30g', '30غ', 'Bonne Maman Apricot Preserves glow with the orange-yellow luster of fresh, ripe apricots. Each jar is filled with large pieces of apricots making every bite a delicious treat. Try it on toast, muffins, in a recipe or as part of a sauce.\\r\\n\\r\\nINGREDIENTS\\r\\nApricots, Sugar, Brown sugar, Concentrated Lemon Juice, Fruit Pectin. An occasional pit may be present.', 'المكونات : المشمش 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات', NULL, NULL, 95, 10, 45, 1, '2019-07-16 15:56:12', '2019-07-17 12:38:06'),
(205, 12, 14, 10, 'Apricot Jam', 'مربى المشمش', '370g', '370غ', 'Bonne Maman Apricot Preserves glow with the orange-yellow luster of fresh, ripe apricots. Each jar is filled with large pieces of apricots making every bite a delicious treat. Try it on toast, muffins, in a recipe or as part of a sauce.\\r\\n\\r\\nINGREDIENTS\\r\\nApricots, Sugar, Brown sugar, Concentrated Lemon Juice, Fruit Pectin. An occasional pit may be present. \\r\\n\\r\\n50 Calories Per 1 tbsp (20g)', 'المكونات : المشمش 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات \\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 91, 10, 40, 1, '2019-07-16 16:00:21', '2019-07-17 12:38:06'),
(206, 12, 14, 10, 'Fig Jam', 'مربى التين', '370g', '370غ', 'Our Fig Preserves are made with ripened, tender figs that feature smooth, sweet flavors that are perfect for savory flavor combinations. The figs’ natural flavors combine beautifully with bread, cheese, ice cream and more!\\r\\n\\r\\nINGREDIENTS\\r\\n\\r\\nWhite figs, sugar, cane sugar, concentrated lemon juice, fruit pectin\\r\\n\\r\\n50 Calories Per 1 tbsp (20g)', 'المكونات : تين 50 % ، سكر ، سكر أسمر من قصب السكر ، عصير الحامض المركَّز ، بكتين الحمضيات\\r\\n\\r\\nلكل حصة من 20 غ (معلقة كبيرة) - 50 كالوري', NULL, NULL, 91, 10, 39, 1, '2019-07-16 16:03:07', '2019-07-17 12:38:06'),
(207, 17, 12, 8, 'Evian Mineral Water,  24 x 0.5 Litre', 'إيفيان – مياه معدنية طبيعية 500 مل × 24', '0.5L', '0.5لتر', 'Evian Natural drinking water from the French Alps. Evian drinking water springs out as nature intended naturally pure and uniquely balanced in minerals. Evian natural mineral water is bottled right at the spring outlet and remains untouched by man during the whole bottling process. Natural and constant minerally-balanced Evian provides pure daily hydration and sustains a healthy lifestyle. Evian\'s low sodium content and unique balanced mineral composition make it perfectly suitable for babies. Available in 500 ml bottle. \\r\\n\\r\\nIngredients:\\r\\nevian - Calcium Ca++ 80, Magnesium Mg++ 26, Sodium Na+ 6.5, Potassium K+ 1, Silica Si02 15, Bicarbonates HC03- 360, Sulphates S04-- 14, Chlorides Cl- 10, Nitrates N03- 3.8', 'من قلب جبال الألب الفرنسية تكتسب مياه إيفيان توازناً معدنياً فريداً ، منخفضة في نسبة الصوديوم وتحتوي على مكونات معنية متوازنة لتناسب الجميع ، تمتع بالمذاق الطبيعي المنعش والنقي في أي مكان وفي كل وقت.\\r\\n\\r\\nالمواصفات :\\r\\n\\r\\n الماركة : إيفيان\\r\\n\\r\\n الحجم : 24 × 0.50 لتر\\r\\n\\r\\n المصدر : من قلب جبال الألب\\r\\n\\r\\nمعبأة في فرنسا', NULL, NULL, 486, 10, 24, 1, '2019-07-16 16:29:05', '2019-07-17 17:06:44'),
(208, 17, 12, 8, 'Evian Mineral Water, 12 x 0.75 Litre', 'إيفيان – مياه معدنية طبيعية 750 مل × 12', '0.75L', '0.75لتر', 'Evian Natural drinking water from the French Alps. Evian drinking water springs out as nature intended naturally pure and uniquely balanced in minerals. Evian natural mineral water is bottled right at the spring outlet and remains untouched by man during the whole bottling process. Natural and constant minerally-balanced Evian provides pure daily hydration and sustains a healthy lifestyle. Evian\'s low sodium content and unique balanced mineral composition make it perfectly suitable for babies. Available in 750 ml bottle.\\r\\n\\r\\nIngredients:\\r\\nevian - Calcium Ca++ 80, Magnesium Mg++ 26, Sodium Na+ 6.5, Potassium K+ 1, Silica Si02 15, Bicarbonates HC03- 360, Sulphates S04-- 14, Chlorides Cl- 10, Nitrates N03- 3.8', 'من قلب جبال الألب الفرنسية تكتسب مياه إيفيان توازناً معدنياً فريداً ، منخفضة في نسبة الصوديوم وتحتوي على مكونات معنية متوازنة لتناسب الجميع ، تمتع بالمذاق الطبيعي المنعش والنقي في أي مكان وفي كل وقت.\\r\\n\\r\\nالمواصفات :\\r\\n\\r\\n الماركة : إيفيان\\r\\n\\r\\n الحجم : 12 × 0.75 لتر\\r\\n\\r\\n المصدر : من قلب جبال الألب\\r\\n\\r\\nمعبأة في فرنسا', NULL, NULL, 380, 10, 25, 1, '2019-07-16 17:14:34', '2019-07-17 17:07:19'),
(209, 17, 12, 8, 'Evian Mineral Water, 12 x 1 Litre', 'إيفيان – مياه معدنية طبيعية 1 لتر × 12', '1L', '1 لتر', 'Evian Natural drinking water from the French Alps. Evian drinking water springs out as nature intended naturally pure and uniquely balanced in minerals. Evian natural mineral water is bottled right at the spring outlet and remains untouched by man during the whole bottling process. Natural and constant minerally-balanced Evian provides pure daily hydration and sustains a healthy lifestyle. Evian\'s low sodium content and unique balanced mineral composition make it perfectly suitable for babies. Available in 1 Litre bottle.\\r\\n\\r\\nIngredients:\\r\\nevian - Calcium Ca++ 80, Magnesium Mg++ 26, Sodium Na+ 6.5, Potassium K+ 1, Silica Si02 15, Bicarbonates HC03- 360, Sulphates S04-- 14, Chlorides Cl- 10, Nitrates N03- 3.8', 'من قلب جبال الألب الفرنسية تكتسب مياه إيفيان توازناً معدنياً فريداً ، منخفضة في نسبة الصوديوم وتحتوي على مكونات معنية متوازنة لتناسب الجميع ، تمتع بالمذاق الطبيعي المنعش والنقي في أي مكان وفي كل وقت.\\r\\n\\r\\nالمواصفات :\\r\\n\\r\\n الماركة : إيفيان\\r\\n\\r\\n الحجم : 12 × 1 لتر\\r\\n\\r\\n المصدر : من قلب جبال الألب\\r\\n\\r\\nمعبأة في فرنسا', NULL, NULL, 445, 10, 26, 1, '2019-07-17 10:03:59', '2019-07-29 14:54:40'),
(210, 17, 12, 8, 'Evian Mineral Water, 24 x 0.33 Litre', 'إيفيان – مياه معدنية طبيعية 330 مل × 24', '0.33L', '0.33L', 'Evian Natural drinking water from the French Alps. Evian drinking water springs out as nature intended naturally pure and uniquely balanced in minerals. Evian natural mineral water is bottled right at the spring outlet and remains untouched by man during the whole bottling process. Natural and constant minerally-balanced Evian provides pure daily hydration and sustains a healthy lifestyle. Evian\'s low sodium content and unique balanced mineral composition make it perfectly suitable for babies. Available in 330 ml bottle.\\r\\n\\r\\nIngredients:\\r\\nevian - Calcium Ca++ 80, Magnesium Mg++ 26, Sodium Na+ 6.5, Potassium K+ 1, Silica Si02 15, Bicarbonates HC03- 360, Sulphates S04-- 14, Chlorides Cl- 10, Nitrates N03- 3.8', 'من قلب جبال الألب الفرنسية تكتسب مياه إيفيان توازناً معدنياً فريداً ، منخفضة في نسبة الصوديوم وتحتوي على مكونات معنية متوازنة لتناسب الجميع ، تمتع بالمذاق الطبيعي المنعش والنقي في أي مكان وفي كل وقت.\\r\\n\\r\\nالمواصفات :\\r\\n\\r\\n الماركة : إيفيان\\r\\n\\r\\n الحجم : 24 × 0.33 لتر\\r\\n\\r\\n المصدر : من قلب جبال الألب\\r\\n\\r\\nمعبأة في فرنسا', NULL, NULL, 399, 0, 23, 1, '2019-07-17 10:08:29', '2019-07-29 12:19:14'),
(211, 17, 12, 8, 'Evian Natural Mineral Water Glass, 20 x 330ml', 'إيفيان – مياه معدنية طبيعية زجاج 330 مل × 20', '0.33L', '0.33 لتر', 'Evian Natural drinking water from the French Alps. Evian drinking water springs out as nature intended naturally pure and uniquely balanced in minerals. Evian natural mineral water is bottled right at the spring outlet and remains untouched by man during the whole bottling process. Natural and constant minerally-balanced Evian provides pure daily hydration and sustains a healthy lifestyle. Evian\'s low sodium content and unique balanced mineral composition make it perfectly suitable for babies. Available in 330 ml bottle.\\r\\n\\r\\nIngredients:\\r\\nevian - Calcium Ca++ 80, Magnesium Mg++ 26, Sodium Na+ 6.5, Potassium K+ 1, Silica Si02 15, Bicarbonates HC03- 360, Sulphates S04-- 14, Chlorides Cl- 10, Nitrates N03- 3.8', 'من قلب جبال الألب الفرنسية تكتسب مياه إيفيان توازناً معدنياً فريداً ، منخفضة في نسبة الصوديوم وتحتوي على مكونات معنية متوازنة لتناسب الجميع ، تمتع بالمذاق الطبيعي المنعش والنقي في أي مكان وفي كل وقت.\\r\\n\\r\\nالمواصفات :\\r\\n\\r\\n الماركة : إيفيان\\r\\n\\r\\n الحجم : 20 × 0.50 لتر\\r\\n\\r\\n المصدر : من قلب جبال الألب\\r\\n\\r\\nمعبأة في فرنسا', NULL, NULL, 660, 9, 22, 1, '2019-07-17 10:15:23', '2019-07-24 18:25:09'),
(212, 9, 12, 7, 'Green Tea', 'شاي أخضر', '25 tea bags', '25 كيس شاي', 'Pickwick Green tea has a distinctive, original, light color. A touch of lemon gives a nice fresh taste. Pickwick Green tea is refreshing and fits into a healthy diet.', 'نوع الشاي : شاي أخضر', NULL, NULL, 72, 10, 27, 1, '2019-07-17 10:28:15', '2019-07-17 12:38:06'),
(213, 9, 12, 7, 'Camomile', 'كاموميل', '25 tea bags', '25 كيس شاي', 'A delicate and refreshing drink known for its mellow flavor and soothing qualities.', 'نوع الشاي : شاي بالأعشاب', NULL, NULL, 72, 10, 28, 1, '2019-07-17 10:29:58', '2019-07-17 12:38:06'),
(214, 9, 12, 7, 'English Breakfast', 'انجلش بريكفاست', '25 tea bags', '25 كيس شاي', 'Since 1753, Pickwick has consistently delivered a delightful selection of quality teas with enticing aromas and exquisite flavor profiles. Now a strong international brand, Pickwick offers a medley of pure and natural teas sure to please the most discriminating taste. Enjoy your favorite Pickwick tea any time.', 'نوع الشاي : شاي أسود', NULL, NULL, 72, 0, 29, 1, '2019-07-17 10:32:21', '2019-07-21 14:59:43'),
(215, 9, 12, 7, 'Cinnamon', 'قرفة', '25 envelopes', '25 كيس شاي', 'Pickwick Cinnamon has a fine tea melange. Real cinnamon is added resulting in a delicious, refreshing tea with cinnamon flavor. Enjoy your cup of Pickwick Cinnamon at any moment of the day.', 'نوع الشاي : شاي بالقرفة', NULL, NULL, 72, 10, 30, 1, '2019-07-17 10:42:23', '2019-07-17 12:38:06'),
(216, 9, 12, 7, 'Minty Moroccon Tea', 'شاي مغربي بالنعناع', '25 tea bags', '25 كيس شاي', 'Pickwick Minty Marocco is a fresh and sweet Maroccan inspired herbal infusion. In this herbal infusion mint is combined with the delicate herbals cinnamon and liquorice.\\r\\n\\r\\nFlavoured herbal tea.\\r\\n\\r\\nIngredients: Rosehip, cinnamon (23%), liquorice (18%), chicory, spearmint (8,5%) orange peels, flavouring.', 'نوع الشاي : شاي مغربي بالنعناع', NULL, NULL, 72, 10, 31, 1, '2019-07-17 10:45:43', '2019-07-17 12:38:06'),
(217, 9, 12, 7, 'Earl Grey', 'ايرل جراي', '25 tea bags', '25 كيس شاي', 'Since 1753, Pickwick has consistently delivered a delightful selection of quality teas with enticing aromas and exquisite flavor profiles. Now a strong international brand, Pickwick offers a medley of pure and natural teas sure to please the most discriminating taste. Enjoy your favorite Pickwick tea any time.', 'نوع الشاي : شاي اسود', NULL, NULL, 72, 0, 32, 1, '2019-07-17 10:47:37', '2019-07-21 15:01:12'),
(218, 16, 13, 9, 'Häagen-Dazs Cookies & Cream Ice cream', 'هاجن داز - ايس كريم  بالكوكيز و الكريم', '100g', '100غ', 'Pieces of rich, chocolaty cookies are dunked in delicious, creamy vanilla ice cream to satisfy the milk-and-cookies kid in all of us.', 'النكهة : كوكيز و كريم\\r\\n\\r\\nالحجم : 100 جرام', NULL, NULL, 40, 10, 1, 1, '2019-07-17 11:20:05', '2019-07-17 12:38:05'),
(219, 16, 13, 9, 'Häagen-Dazs Rasperry & Mango Ice cream', 'هاجن داز - ايس كريم  بالمانجو وتوت', '100g', '100غ', 'One exotic spoonful. Our fusion of mango ice cream and sweet raspberry swirls will take you to new heights.', 'النكهة : المانجو و التوت\\r\\n\\r\\nالحجم : 100 جرام', NULL, NULL, 40, 0, 2, 1, '2019-07-17 11:25:00', '2019-07-17 12:38:05'),
(220, 16, 13, 9, 'Häagen-Dazs Vanilla Ice cream', 'هاجن داز - ايس كريم  بالفانيليا', '500g', '500غ', 'Vanilla is the essence of elegance and sophistication. This marriage of pure, sweet cream and Madagascar vanilla creates the sweet scent of exotic spice and a distinctive taste that lingers on your tongue.', 'النكهة : فانيليا\\r\\n\\r\\nالحجم : 500 جرام', NULL, NULL, 128.5, 0, 12, 1, '2019-07-17 11:27:08', '2019-07-22 16:40:25'),
(221, 16, 13, 9, 'Häagen-Dazs Strawberry Ice cream', 'هاجن داز - ايس كريم بالفراولة', '100g', '100غ', 'We introduce sweet summer strawberries to pure cream and other natural ingredients. Because it\'s brimming with real fruit, the true flavor of our strawberries comes shining through.', 'النكهة : فراولة\\r\\n\\r\\nالحجم : 100 جرام', NULL, NULL, 40, 10, 3, 1, '2019-07-17 11:29:23', '2019-07-17 12:38:05'),
(222, 16, 13, 9, 'Häagen-Dazs Caramel Biscuit & Cream Ice cream', 'هاجن داز - ايس كريم  بسكويت بالكراميل و الكريم', '500g', '500غ', 'Discover this indulgent pint of Caramel Biscuit & Cream Ice Cream from Häagen-Dazs, a delight of taste and texture, made with premium Belgian biscuits and deliciously rich caramel. The ultimate indulgence.', 'النكهة : بسكويت بالكراميل و الكريم\\r\\n\\r\\nالحجم : 500 جرام', NULL, NULL, 128.5, 10, 13, 1, '2019-07-17 11:46:08', '2019-07-17 12:38:33'),
(223, 16, 13, 9, 'Häagen-Dazs Salted Caramel Ice Cream Bars', 'هاجن داز - ايس كريم بالكراميل المملح', '80ml', '80مل', 'Häagen-Dazs Salted Caramel ice cream is a flavourful combination of creamy caramel, swirled with salted caramel ribbons, and coated in rich milk chocolate. Made with 100% Canadian dairy, this delicious treat satisfies both sweet and salty senses.', 'النكهة : كراميل مملح\\r\\n\\r\\nالحجم :80 مل', NULL, NULL, 42, 9, 20, 1, '2019-07-17 11:53:21', '2019-07-25 22:58:27'),
(224, 16, 13, 9, 'Häagen-Dazs Vanilla Ice cream', 'هاجن داز - ايس كريم  بالفانيليا', '100g', '100غ', 'Vanilla is the essence of elegance and sophistication. This marriage of pure, sweet cream and Madagascar vanilla creates the sweet scent of exotic spice and a distinctive taste that lingers on your tongue.', 'النكهة : فانيليا\\r\\n\\r\\nالحجم : 100 جرام', NULL, NULL, 40, 10, 5, 1, '2019-07-17 11:56:33', '2019-07-17 12:38:33'),
(225, 16, 13, 9, 'Häagen-Dazs Macadamia Nut Brittle Ice cream', 'هاجن داز - بالكاديميا', '100g', '100غ', 'Try a spoonful of paradise. Delight in our classic vanilla ice cream with a little added bite.', 'النكهة :مكاديميا\\r\\n\\r\\nالحجم : 100 جرام', NULL, NULL, 40, 10, 6, 1, '2019-07-17 11:59:20', '2019-07-17 12:38:33'),
(226, 16, 13, 9, 'Häagen-Dazs Dulce de leche Ice Cream', 'هاجن داز - دولسي دي ليتشي', '100g', '100غ', 'Inspired by Latin America\'s treasured dessert, our dulce de leche ice cream is a delicious combination of caramel and sweet cream, swirled with ribbons of golden caramel.', 'النكهة :دولسي دي ليتشي\\r\\n\\r\\nالحجم : 100 جرام', NULL, NULL, 40, 10, 11, 1, '2019-07-17 12:01:15', '2019-07-17 12:38:33'),
(227, 16, 13, 9, 'Häagen-Dazs Cookies & Cream Ice cream', 'هاجن داز - ايس كريم  بالكوكيز و الكريم', '500g', '500غ', 'Pieces of rich, chocolaty cookies are dunked in delicious, creamy vanilla ice cream to satisfy the milk-and-cookies kid in all of us.', 'النكهة : كوكيز و كريم\\r\\n\\r\\nالحجم : 500 جرام', NULL, NULL, 40, 10, 8, 1, '2019-07-17 12:03:38', '2019-07-17 12:38:33'),
(228, 16, 13, 9, 'Häagen-Dazs Macadamia Nut Brittle Stick Ice cream', 'هاجن داز - مكاديميا ستيك', '80ml', '80مل', 'Discover our smooth Macadamia Nut Ice-cream enrobed in Belgian Chocolate for the ultimate experience.', 'النكهة : كراميل مملح\\r\\n\\r\\nالحجم :80 مل', NULL, NULL, 42, 10, 21, 1, '2019-07-17 12:10:38', '2019-07-17 12:38:51'),
(229, 16, 13, 9, 'Häagen-Dazs Belgian Chocolate Ice cream', 'هاجن داز - ايس كريم الشوكولاتة البلجيكية', '500g', '500غ', 'Our Belgian chocolate combines rich, velvety chocolate ice cream with finely shaved Belgian chocolate for a uniquely textured experience.', 'النكهة : شوكولاته بلجيكية\\r\\n\\r\\nالحجم :500 جرام', NULL, NULL, 128.5, 9, 14, 1, '2019-07-17 12:13:12', '2019-07-25 22:58:27'),
(230, 16, 13, 9, 'Häagen-Dazs Strawberry Cheesecake Ice cream', 'هاجن داز -  بالفراولة و التشيز كيك', '500g', '500غ', 'Wowzer, this pint is packing a whole lot of flavour. Cheesecake ice cream meets crunchy biscuit pieces and saucy strawberry swirls in a taste overload.', 'النكهة : فراولة بالتشيز كيك\\r\\n\\r\\nالحجم : 500 جرام', NULL, NULL, 128.5, 10, 15, 1, '2019-07-17 12:18:17', '2019-07-17 12:38:51'),
(232, 16, 13, 9, 'Häagen-Dazs Belgian Chocolate Ice cream', 'هاجن داز - ايس كريم الشوكولاتة البلجيكية', '100g', '100غ', 'Our Belgian chocolate combines rich, velvety chocolate ice cream with finely shaved Belgian chocolate for a uniquely textured experience.', 'النكهة : شوكولاته بلجيكية\\r\\n\\r\\nالحجم :100 جرام', NULL, NULL, 40, 10, 9, 1, '2019-07-17 12:21:43', '2019-07-17 12:38:33'),
(233, 16, 13, 9, 'Häagen-Dazs Strawberry Ice cream', 'هاجن داز - ايس كريم بالفراولة', '500g', '500غ', 'We introduce sweet summer strawberries to pure cream and other natural ingredients. Because it\'s brimming with real fruit, the true flavor of our strawberries comes shining through.', 'النكهة : فراولة\\r\\n\\r\\nالحجم : 500 جرام', NULL, NULL, 128.5, 10, 16, 1, '2019-07-17 12:22:53', '2019-07-17 12:38:51'),
(234, 16, 13, 9, 'Häagen-Dazs Macadamia Nut Brittle Ice cream', 'هاجن داز - بالكاديميا', '500g', '500غ', 'Try a spoonful of paradise. Delight in our classic vanilla ice cream with a little added bite.', 'النكهة : مكاديميا\\r\\n\\r\\nالحجم : 500 جرام', NULL, NULL, 128.5, 10, 17, 1, '2019-07-17 12:24:33', '2019-07-17 12:38:51'),
(235, 16, 13, 9, 'Häagen-Dazs Rasperry & Mango Ice cream', 'هاجن داز - ايس كريم  بالمانجو وتوت', '500g', '500غ', 'One exotic spoonful. Our fusion of mango ice cream and sweet raspberry swirls will take you to new heights.', 'النكهة : المانجو و التوت\\r\\n\\r\\nالحجم : 500 جرام', NULL, NULL, 128.5, 10, 18, 1, '2019-07-17 12:27:21', '2019-07-17 12:38:51'),
(236, 16, 13, 9, 'Häagen-Dazs Caramel Biscuit & Cream Ice cream', 'هاجن داز - ايس كريم بسكويت بالكراميل و الكريم', '100g', '100غ', 'Discover this indulgent pint of Caramel Biscuit & Cream Ice Cream from Häagen-Dazs, a delight of taste and texture, made with premium Belgian biscuits and deliciously rich caramel. The ultimate indulgence.', 'النكهة : بسكويت بالكراميل و الكريم\\r\\n\\r\\nالحجم : 100 جرام', NULL, NULL, 40, 10, 7, 1, '2019-07-17 12:29:09', '2019-07-17 12:38:33'),
(237, 16, 13, 9, 'Häagen-Dazs Dulce de leche Ice Cream', 'هاجن داز - دولسي دي ليتشي', '500g', '500غ', 'Inspired by Latin America\'s treasured dessert, our dulce de leche ice cream is a delicious combination of caramel and sweet cream, swirled with ribbons of golden caramel.', 'النكهة :دولسي دي ليتشي\\r\\n\\r\\nالحجم : 500 جرام', NULL, NULL, 128.5, 10, 19, 1, '2019-07-17 12:33:25', '2019-07-17 12:38:51'),
(238, 16, 13, 9, 'Häagen-Dazs Strawberry Cheesecake Ice cream', 'هاجن داز -  بالفراولة و التشيز كيك', '100g', '100غ', 'Wowzer, this icecream is packing a whole lot of flavour. Cheesecake ice cream meets crunchy biscuit pieces and saucy strawberry swirls in a taste overload.', 'النكهة : فراولة بالتشيز كيك\\r\\n\\r\\nالحجم : 100 جرام', NULL, NULL, 40, 10, 4, 1, '2019-07-17 12:35:46', '2019-07-17 12:38:33'),
(239, 13, 14, 10, 'Anna Peeled Tomatoes', 'طماطم مقشرة آنا', '2.55Kg', '2.55كغ', 'Anna Peeled Tomatoes add depth and texture to your favorite family meals.  Many cooks choose peeled tomatoes for pasta sauces, hearty stews, and slow-cooked roasts.\\r\\n\\r\\nINGREDIENTS : Peeled Tomatoes, Tomato Juice, Acidity Regulator: Citric Acid\\r\\n\\r\\n2550 GRAMS / ITALY', '.الطماطم كاملة مقشرة مثالية للوصفات الإيطالية، محضرة من الطماطم الناضجة والمزروعة في إيطاليا', NULL, NULL, 91, 10, NULL, 1, '2019-07-17 15:48:43', '2019-07-17 15:48:43'),
(240, 14, 14, 10, 'Rich\'s Versatie Cream', 'كريمة ريتش للطبخ و الحلويات', '1L', '1لتر', 'Ideal for both sweet and savoury applications, used hot or cold. Versatie® is the preferred alternative to fresh dairy cream and is the ideal solution for all your cooking, pouring and whipping needs.\\r\\n\\r\\n\\r\\nIngredients : \\r\\n\\r\\nWater, Fully Hydrogenated Vegetable Oil (Palm Kernel), Fully Hydrogenated Vegetable Oil (Coconut), Milk Solids (Buttermilk powder), Stabilisers (E1442, E412, E410, E407, E415), Sugar, Emulsifiers (E471, E435, E322 (Soyabean), Acidity regulator (E339(ii)), Colourant (Beta Carotene, E160(a).\\r\\n\\r\\nAllergens: Cow\'s milk, Soya', 'مثالي لكل من الأطباق الحلوة واللذيذة ، الساخنة أو الباردة. تعد كريمة ريتش البديل المفضل لكريم الألبان الطازج وهو الحل المثالي لجميع احتياجاتك في الطهي والخفق.', NULL, NULL, 62.5, 20, NULL, 1, '2019-07-17 16:23:17', '2019-07-17 16:23:17'),
(241, 18, 12, 8, 'Badoit Sparkling Water, 30 x 330ml', 'مياة طبيعية فوارة من بادويت، 330مل', '0.33L', '0.33 لتر', 'A gift of Nature : Badoit emerges naturally sparkling at its spring. Badoit sources its water beneath the small town of Saint - Galmier, located in the Loire Region of Southern France, at the foot of Mount Forez. Badoit Natural Mineral water begins its long journey 153 metres below ground through granite soil that naturally endows it with native carbon dioxide gas.\\r\\n\\r\\nAs it journeys through various mineralogical strata, it acquires its sparkle, finesse and unique mineral properties. It springs forth at a constant temperature of 16°C to give us the hydrating properties and trace elements that make it so rich : Magnesium (80 mg/L), Calcium (153 mg/L), Flouried and , above all, valuable bicarbonates (1,250 mg/L)', '-', 688, 2, 688, 9, NULL, 1, '2019-07-17 17:05:46', '2019-07-30 14:33:51'),
(242, 18, 12, 8, 'Badoit Sparkling Water, 6 x 1 Litre', 'مياة طبيعية فوارة من بادويت، 1لتر', '1L', '1 لتر', 'A gift of Nature : Badoit emerges naturally sparkling at its spring. Badoit sources its water beneath the small town of Saint - Galmier, located in the Loire Region of Southern France, at the foot of Mount Forez. Badoit Natural Mineral water begins its long journey 153 metres below ground through granite soil that naturally endows it with native carbon dioxide gas.\\r\\n\\r\\nAs it journeys through various mineralogical strata, it acquires its sparkle, finesse and unique mineral properties. It springs forth at a constant temperature of 16°C to give us the hydrating properties and trace elements that make it so rich : Magnesium (80 mg/L), Calcium (153 mg/L), Flouried and , above all, valuable bicarbonates (1,250 mg/L)', '-', NULL, NULL, 202, 10, NULL, 1, '2019-07-18 09:36:09', '2019-07-18 09:41:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`) VALUES
(1, 1, 'ProductImages_75917.PNG'),
(2, 1, 'ProductImages_57766.PNG'),
(3, 2, 'ProductImages_13534.gif'),
(4, 3, 'ProductImages_75345.png'),
(5, 4, 'ProductImages_55727.gif'),
(6, 5, 'ProductImages_98161.png'),
(7, 6, 'ProductImages_34829.png'),
(8, 7, 'ProductImages_68039.jpg'),
(9, 8, 'ProductImages_91683.gif'),
(10, 9, 'ProductImages_77011.jpg'),
(11, 10, 'ProductImages_75189.png'),
(12, 11, 'ProductImages_73585.png'),
(13, 12, 'ProductImages_96555.gif'),
(14, 13, 'ProductImages_12691.png'),
(15, 14, 'ProductImages_61039.gif'),
(16, 15, 'ProductImages_15022.gif'),
(17, 16, 'ProductImages_97835.gif'),
(18, 17, 'ProductImages_55705.gif'),
(19, 18, 'ProductImages_27236.png'),
(20, 19, 'ProductImages_87296.gif'),
(21, 20, 'ProductImages_71993.gif'),
(22, 21, 'ProductImages_16103.gif'),
(23, 22, 'ProductImages_29817.png'),
(24, 23, 'ProductImages_26905.gif'),
(25, 24, 'ProductImages_33918.gif'),
(26, 25, 'ProductImages_39629.gif'),
(27, 26, 'ProductImages_71845.gif'),
(28, 27, 'ProductImages_58413.gif'),
(29, 28, 'ProductImages_46122.gif'),
(30, 29, 'ProductImages_60985.gif'),
(31, 30, 'ProductImages_51812.png'),
(32, 31, 'ProductImages_59109.gif'),
(33, 32, 'ProductImages_71084.png'),
(34, 33, 'ProductImages_45790.gif'),
(35, 34, 'ProductImages_59224.gif'),
(36, 35, 'ProductImages_90328.png'),
(37, 36, 'ProductImages_20352.gif'),
(38, 37, 'ProductImages_22434.png'),
(39, 38, 'ProductImages_77160.gif'),
(40, 39, 'ProductImages_74175.gif'),
(41, 40, 'ProductImages_74779.gif'),
(42, 41, 'ProductImages_51167.png'),
(43, 42, 'ProductImages_75420.jpeg'),
(44, 43, 'ProductImages_87141.gif'),
(45, 44, 'ProductImages_76902.gif'),
(46, 45, 'ProductImages_70263.png'),
(47, 46, 'ProductImages_43830.png'),
(48, 47, 'ProductImages_47500.gif'),
(49, 48, 'ProductImages_61603.png'),
(50, 49, 'ProductImages_50824.png'),
(51, 50, 'ProductImages_74740.gif'),
(52, 51, 'ProductImages_64487.png'),
(53, 52, 'ProductImages_66557.gif'),
(54, 53, 'ProductImages_99066.gif'),
(55, 54, 'ProductImages_46789.png'),
(56, 55, 'ProductImages_68016.gif'),
(57, 56, 'ProductImages_47926.gif'),
(58, 57, 'ProductImages_64716.gif'),
(59, 58, 'ProductImages_41914.gif'),
(60, 59, 'ProductImages_23206.gif'),
(61, 60, 'ProductImages_35760.png'),
(62, 61, 'ProductImages_89462.gif'),
(63, 62, 'ProductImages_66781.png'),
(64, 63, 'ProductImages_93824.png'),
(65, 64, 'ProductImages_27319.gif'),
(66, 65, 'ProductImages_86223.gif'),
(67, 66, 'ProductImages_91090.gif'),
(68, 67, 'ProductImages_55716.gif'),
(69, 68, 'ProductImages_98366.jpg'),
(70, 69, 'ProductImages_41777.gif'),
(71, 70, 'ProductImages_94286.png'),
(72, 71, 'ProductImages_56088.gif'),
(73, 72, 'ProductImages_51482.gif'),
(74, 73, 'ProductImages_62650.gif'),
(75, 74, 'ProductImages_71857.png'),
(76, 75, 'ProductImages_63098.gif'),
(77, 76, 'ProductImages_63597.png'),
(78, 77, 'ProductImages_39767.gif'),
(79, 78, 'ProductImages_64764.gif'),
(80, 79, 'ProductImages_67866.gif'),
(81, 80, 'ProductImages_52198.gif'),
(82, 81, 'ProductImages_63108.gif'),
(83, 82, 'ProductImages_57032.gif'),
(84, 83, 'ProductImages_91171.png'),
(85, 84, 'ProductImages_74401.png'),
(86, 85, 'ProductImages_33753.gif'),
(87, 86, 'ProductImages_53344.gif'),
(88, 87, 'ProductImages_77292.jpg'),
(89, 88, 'ProductImages_38512.gif'),
(90, 89, 'ProductImages_11117.gif'),
(91, 90, 'ProductImages_32062.gif'),
(92, 91, 'ProductImages_59280.gif'),
(93, 92, 'ProductImages_52078.gif'),
(94, 93, 'ProductImages_59107.gif'),
(95, 94, 'ProductImages_73228.gif'),
(96, 95, 'ProductImages_88425.gif'),
(97, 96, 'ProductImages_46073.gif'),
(98, 97, 'ProductImages_37803.gif'),
(99, 98, 'ProductImages_92316.jpg'),
(100, 99, 'ProductImages_15631.png'),
(101, 100, 'ProductImages_45498.png'),
(102, 101, 'ProductImages_62086.png'),
(103, 102, 'ProductImages_92590.png'),
(104, 103, 'ProductImages_52417.gif'),
(105, 104, 'ProductImages_64153.gif'),
(106, 105, 'ProductImages_71355.gif'),
(107, 106, 'ProductImages_57578.gif'),
(108, 107, 'ProductImages_67404.gif'),
(109, 108, 'ProductImages_17334.gif'),
(110, 109, 'ProductImages_58467.gif'),
(111, 110, 'ProductImages_44796.gif'),
(112, 111, 'ProductImages_14168.gif'),
(113, 112, 'ProductImages_23364.gif'),
(114, 113, 'ProductImages_97355.gif'),
(115, 114, 'ProductImages_99296.gif'),
(116, 115, 'ProductImages_46967.gif'),
(117, 116, 'ProductImages_16294.gif'),
(118, 117, 'ProductImages_24929.gif'),
(119, 118, 'ProductImages_45994.gif'),
(120, 119, 'ProductImages_98451.gif'),
(121, 120, 'ProductImages_52285.gif'),
(122, 121, 'ProductImages_85745.gif'),
(123, 122, 'ProductImages_46587.gif'),
(124, 123, 'ProductImages_78068.gif'),
(125, 124, 'ProductImages_79401.gif'),
(126, 125, 'ProductImages_18715.gif'),
(127, 126, 'ProductImages_92121.gif'),
(128, 127, 'ProductImages_30358.gif'),
(129, 128, 'ProductImages_68975.gif'),
(130, 129, 'ProductImages_59975.gif'),
(131, 130, 'ProductImages_25564.gif'),
(132, 131, 'ProductImages_16068.gif'),
(133, 132, 'ProductImages_23420.gif'),
(134, 133, 'ProductImages_84996.gif'),
(135, 134, 'ProductImages_21118.gif'),
(136, 135, 'ProductImages_80371.gif'),
(137, 136, 'ProductImages_97627.gif'),
(138, 137, 'ProductImages_60182.gif'),
(139, 138, 'ProductImages_20145.gif'),
(140, 139, 'ProductImages_29219.gif'),
(141, 140, 'ProductImages_59973.gif'),
(142, 141, 'ProductImages_94272.gif'),
(143, 142, 'ProductImages_41167.gif'),
(144, 143, 'ProductImages_85072.gif'),
(145, 144, 'ProductImages_54879.gif'),
(146, 145, 'ProductImages_24845.gif'),
(147, 146, 'ProductImages_22960.gif'),
(148, 147, 'ProductImages_92043.gif'),
(149, 148, 'ProductImages_24859.gif'),
(150, 149, 'ProductImages_79429.gif'),
(151, 150, 'ProductImages_14451.gif'),
(152, 151, 'ProductImages_77234.gif'),
(153, 152, 'ProductImages_69544.gif'),
(154, 153, 'ProductImages_55108.gif'),
(155, 154, 'ProductImages_19016.gif'),
(156, 155, 'ProductImages_24349.gif'),
(157, 156, 'ProductImages_20716.gif'),
(158, 157, 'ProductImages_54897.gif'),
(159, 158, 'ProductImages_40592.gif'),
(160, 159, 'ProductImages_11929.gif'),
(161, 160, 'ProductImages_86309.gif'),
(162, 161, 'ProductImages_29215.gif'),
(163, 162, 'ProductImages_81604.gif'),
(164, 163, 'ProductImages_41259.gif'),
(165, 164, 'ProductImages_18453.gif'),
(166, 165, 'ProductImages_93469.gif'),
(167, 166, 'ProductImages_86148.gif'),
(168, 167, 'ProductImages_44158.gif'),
(169, 168, 'ProductImages_58577.gif'),
(170, 169, 'ProductImages_66065.gif'),
(171, 170, 'ProductImages_80749.gif'),
(172, 171, 'ProductImages_47469.gif'),
(173, 172, 'ProductImages_38838.gif'),
(174, 173, 'ProductImages_42723.gif'),
(175, 174, 'ProductImages_88884.gif'),
(176, 175, 'ProductImages_32900.gif'),
(177, 176, 'ProductImages_45589.gif'),
(178, 177, 'ProductImages_15871.gif'),
(179, 178, 'ProductImages_61872.gif'),
(180, 179, 'ProductImages_59238.gif'),
(181, 180, 'ProductImages_50710.gif'),
(182, 181, 'ProductImages_12033.gif'),
(183, 182, 'ProductImages_33613.gif'),
(184, 183, 'ProductImages_73113.gif'),
(185, 184, 'ProductImages_82650.gif'),
(186, 185, 'ProductImages_77412.gif'),
(187, 186, 'ProductImages_14345.gif'),
(188, 187, 'ProductImages_43186.gif'),
(189, 188, 'ProductImages_30490.gif'),
(190, 189, 'ProductImages_90479.gif'),
(191, 190, 'ProductImages_81034.gif'),
(192, 191, 'ProductImages_91725.gif'),
(193, 192, 'ProductImages_99687.gif'),
(194, 193, 'ProductImages_23159.gif'),
(195, 194, 'ProductImages_83202.gif'),
(196, 195, 'ProductImages_38215.gif'),
(197, 196, 'ProductImages_34421.gif'),
(198, 197, 'ProductImages_92985.gif'),
(199, 198, 'ProductImages_28446.gif'),
(200, 199, 'ProductImages_76025.gif'),
(201, 200, 'ProductImages_79597.gif'),
(202, 201, 'ProductImages_91646.gif'),
(203, 202, 'ProductImages_51528.gif'),
(204, 203, 'ProductImages_86786.gif'),
(205, 204, 'ProductImages_50088.gif'),
(206, 205, 'ProductImages_57139.gif'),
(207, 206, 'ProductImages_70511.png'),
(208, 207, 'ProductImages_86132.gif'),
(209, 208, 'ProductImages_53148.gif'),
(210, 209, 'ProductImages_11117.gif'),
(211, 210, 'ProductImages_95993.gif'),
(212, 211, 'ProductImages_89861.gif'),
(213, 212, 'ProductImages_59808.gif'),
(214, 213, 'ProductImages_81572.gif'),
(215, 214, 'ProductImages_50904.gif'),
(216, 215, 'ProductImages_16621.gif'),
(217, 216, 'ProductImages_65330.gif'),
(218, 217, 'ProductImages_51269.gif'),
(219, 218, 'ProductImages_47794.gif'),
(220, 219, 'ProductImages_23428.gif'),
(221, 220, 'ProductImages_38536.gif'),
(222, 221, 'ProductImages_69667.gif'),
(223, 222, 'ProductImages_83323.gif'),
(224, 223, 'ProductImages_59447.gif'),
(225, 224, 'ProductImages_19169.gif'),
(226, 225, 'ProductImages_86649.gif'),
(227, 226, 'ProductImages_90413.gif'),
(228, 227, 'ProductImages_73702.gif'),
(229, 228, 'ProductImages_49946.gif'),
(230, 229, 'ProductImages_66159.gif'),
(231, 230, 'ProductImages_82049.gif'),
(232, 231, 'ProductImages_34987.gif'),
(233, 232, 'ProductImages_73946.gif'),
(234, 233, 'ProductImages_75151.gif'),
(235, 234, 'ProductImages_34170.gif'),
(236, 235, 'ProductImages_27823.gif'),
(237, 236, 'ProductImages_33513.gif'),
(238, 237, 'ProductImages_65035.gif'),
(239, 238, 'ProductImages_57961.gif'),
(240, 239, 'ProductImages_44396.gif'),
(241, 240, 'ProductImages_88461.gif'),
(242, 241, 'ProductImages_64651.gif'),
(243, 242, 'ProductImages_11531.gif');

-- --------------------------------------------------------

--
-- Table structure for table `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` int(11) NOT NULL,
  `code` varchar(225) NOT NULL,
  `discount_percentage` float NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promo_codes`
--

INSERT INTO `promo_codes` (`id`, `code`, `discount_percentage`, `from_date`, `to_date`, `status`, `created_at`, `updated_at`) VALUES
(25, 'midostore15', 15, '2019-07-24', '2019-08-24', 1, '2019-07-24 12:36:17', '2019-07-24 12:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `recipts`
--

CREATE TABLE `recipts` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `street` varchar(225) DEFAULT NULL,
  `building_no` varchar(225) DEFAULT NULL,
  `apartment_no` varchar(225) DEFAULT NULL,
  `total_price` float NOT NULL,
  `shipping_price` float NOT NULL DEFAULT 0,
  `tax_price` float NOT NULL DEFAULT 0,
  `discount_percentage` float NOT NULL,
  `final_price` float NOT NULL,
  `is_delivered` tinyint(1) NOT NULL DEFAULT 0,
  `is_piad` tinyint(1) NOT NULL DEFAULT 0,
  `delivery_status` enum('processing','shipping','delivered','canceled') NOT NULL,
  `payment_method` enum('cod','creadit_card') DEFAULT NULL,
  `free_test_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `security_code` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recipts`
--

INSERT INTO `recipts` (`id`, `member_id`, `address`, `street`, `building_no`, `apartment_no`, `total_price`, `shipping_price`, `tax_price`, `discount_percentage`, `final_price`, `is_delivered`, `is_piad`, `delivery_status`, `payment_method`, `free_test_id`, `created_at`, `security_code`) VALUES
(25, 14, '#21 ELNAKHEEl Str.-Mohandessen_dokki Giza\r\n2nd floor/apt 5', NULL, NULL, NULL, 732, 10, 2, 0, 746.64, 0, 0, 'delivered', 'cod', NULL, '2019-07-19 22:14:37', 'hqGE8baHiz2FhzLZnYjFuVIBNqPRj80v9gERAQhf'),
(32, 25, '6 El Gezira El Wosta\n13th floor\nApartment 50', NULL, NULL, NULL, 660, 0, 0, 0, 660, 0, 0, 'processing', 'creadit_card', NULL, '2019-07-22 20:14:29', '3jgO0CyeHqF9FziW8l6z8Pn942yLM4Y7WTG0Gbza'),
(33, 26, '306 Cornich Elnil Maadi , HSBC head office', NULL, NULL, NULL, 1175, 0, 0, 0, 1175, 0, 0, 'shipping', 'creadit_card', 3, '2019-07-23 08:46:45', '3MBRWQ0pDduwWLAhiW2hWMCKDwZVLqguCsdzklDD'),
(42, 34, '13 khalifa Elmaamoon', 'Khalifa Elmaamoon', '13', '93', 1228, 0, 0, 0, 1228, 0, 0, 'processing', 'cod', 3, '2019-07-25 17:38:07', '7u7BgtRl4LaT0S5YcbpikybLVb8PQTjYip6af4R5'),
(43, 36, 'Sheikh Zayed City, 7th district, 1st neighborhood,el Jouman 2 buildings', 'El mostakbal', 'A', '1', 625, 0, 0, 0, 625, 0, 0, 'processing', 'cod', NULL, '2019-07-25 22:58:27', 'lvqZeDsm2zR1yrBMAJzRkg0DC11fpc1CKq64LGHt'),
(44, 33, '9 block 33', 'Moustafa Elnahas, 9th District', '9', '1001', 1323, 0, 0, 0, 1323, 0, 0, 'processing', 'cod', 3, '2019-07-26 01:24:59', '4DVdAVey2rJS2Ks3bS2XqidJpkIiLdsX2GZp0aMH'),
(45, 35, 'Lake view - 90 Road', '65/4', 'Villa', '65/3', 500, 0, 0, 0, 500, 0, 0, 'processing', 'cod', NULL, '2019-07-26 06:34:45', 'vd4ny3yI9ior28tKuTQQTAoDJt7JEHXAkeeUt3ww'),
(46, 39, '19elGehad street lebanon square mohandesene', 'Gehad', '19', '19', 561, 0, 0, 0, 561, 0, 0, 'processing', 'cod', NULL, '2019-07-27 15:55:59', 'eWi8H6L0HwYkk2N9GVXTpfbEC51Bq1SGO6s8dSRw'),
(47, 41, '٢٣ عويس البحيري من شارع عبيد روض الفرج', 'عويس البحيري', '٢٣', '٤٢', 560, 0, 0, 0, 560, 0, 0, 'processing', 'cod', NULL, '2019-07-28 11:50:55', '7w9NBxkgEGFKZnF34pbfMMVdsXTcHQE19do9RZNu'),
(54, 47, 'my adrress', '20', '20', '20', 591.5, 0, 0, 0, 591.5, 0, 0, 'canceled', 'cod', NULL, '2019-07-30 13:38:39', 'yXYkLnosO8ZWdYStlvGcN9htjCdTwa7vSyTGJBBj');

-- --------------------------------------------------------

--
-- Table structure for table `recipt_products`
--

CREATE TABLE `recipt_products` (
  `id` int(11) NOT NULL,
  `recipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name_en` varchar(225) NOT NULL,
  `product_name_ar` varchar(225) NOT NULL,
  `bags_en` varchar(225) NOT NULL,
  `bags_ar` varchar(225) NOT NULL,
  `quantity` int(11) NOT NULL,
  `single_price` float NOT NULL,
  `total_price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recipt_products`
--

INSERT INTO `recipt_products` (`id`, `recipt_id`, `product_id`, `product_name_en`, `product_name_ar`, `bags_en`, `bags_ar`, `quantity`, `single_price`, `total_price`) VALUES
(35, 15, 40, 'chicohend wafers', 'سولا ويفر', '30 bags x 25 pieces', '30 شنطة X 25 قطعة', 4, 30, 120),
(36, 16, 40, 'chicohend wafers', 'سولا ويفر', '30 bags x 25 pieces', '30 شنطة X 25 قطعة', 9, 30, 2340),
(37, 16, 32, 'saula wafers', 'سولا ويفر', '30 bags x 40 pieces', '30 شنطة X 40 قطعة', 5, 40, 200),
(38, 17, 40, 'chicohend wafers', 'سولا ويفر', '30 bags x 25 pieces', '30 شنطة X 25 قطعة', 10, 30, 960),
(40, 19, 40, 'chicohend wafers', 'سولا ويفر', '30 bags x 25 pieces', '30 شنطة X 25 قطعة', 1, 30, 30),
(41, 20, 32, 'saula wafers', 'سولا ويفر', '30 bags x 40 pieces', '30 شنطة X 40 قطعة', 1, 40, 40),
(42, 21, 4, 'Fresh Smoked Salmon Slices', 'سلمون مدخن شرائح فريش', '', '', 1, 74, 74),
(43, 21, 110, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '', '', 2, 138, 276),
(44, 21, 106, 'Smoked Salmon Stripes', 'سلمون مدخن ستريبس', '', '', 1, 154, 154),
(45, 22, 2, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '', '', 15, 74, 1110),
(46, 23, 109, 'Whole Salmon Fillet Side', 'جنب سلمون فيليه كامل', '', '', 4, 325, 1300),
(47, 24, 116, 'iperEspresso Capsules Classico - Medium Roast', 'ايلي كبسولات  تحميص متوسط', '', '', 4, 297, 1188),
(48, 25, 8, 'Salmon Fillet', 'سلمون فيليه', '', '', 4, 113, 452),
(49, 25, 110, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '', '', 2, 140, 280),
(50, 26, 111, 'Salmon loin / Sashimi cut', 'سلمون ساشيمي', '', '', 15, 216, 3240),
(51, 27, 114, 'Ground Drip Classico Coffee - Medium Roast', 'ايلي (اسبريسو) تحميص متوسط', '', '', 7, 104.5, 731.5),
(52, 28, 2, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '', '', 15, 74, 1110),
(53, 29, 108, 'Smoked Salmon Slices Skin Off', 'سلمون مدخن شرائح بدون جلد', '', '', 4, 521.5, 2086),
(54, 30, 220, 'Häagen-Dazs Vanilla Ice cream', 'هاجن داز - ايس كريم  بالفانيليا', '', '', 10, 128.5, 1285),
(55, 31, 111, 'Salmon loin / Sashimi cut', 'سلمون ساشيمي', '', '', 1, 216, 216),
(56, 31, 5, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '', '', 3, 147, 441),
(57, 32, 211, 'Evian Natural Mineral Water Glass, 20 x 330ml', 'إيفيان – مياه معدنية طبيعية زجاج 330 مل × 20', '', '', 1, 660, 660),
(58, 33, 154, 'IperEspresso Single Flowpack Classico', 'Iperespresso Single Flowpack Classico', '', '', 1, 1175, 1175),
(59, 34, 108, 'Smoked Salmon Slices Skin Off', 'سلمون مدخن شرائح بدون جلد', '', '', 1, 521.5, 521.5),
(60, 35, 103, 'Fresh Smoked Salmon Slices', 'سلمون مدخن شرائح فريش', '', '', 4, 147, 588),
(61, 36, 8, 'Salmon Fillet', 'سلمون فيليه', '', '', 6, 113, 678),
(62, 37, 109, 'Whole Salmon Fillet Side', 'جنب سلمون فيليه كامل', '', '', 1, 325, 325),
(63, 37, 105, 'Salmon Steak', 'سلمون ستيك', '', '', 1, 113, 113),
(64, 37, 107, 'Smoked Salmon Slices Skin On', 'سلمون مدخن شرائح بجلد', '', '', 1, 502, 502),
(65, 38, 109, 'Whole Salmon Fillet Side', 'جنب سلمون فيليه كامل', '', '', 1, 325, 325),
(66, 38, 2, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '', '', 1, 74, 74),
(67, 38, 107, 'Smoked Salmon Slices Skin On', 'سلمون مدخن شرائح بجلد', '', '', 2, 502, 1004),
(68, 39, 110, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '', '', 1, 140, 140),
(69, 39, 115, 'Filter Coffee Dark Roast', 'قهوة  (فلتر كوفي) illy مطحونة', '', '', 1, 198, 198),
(70, 39, 116, 'iperEspresso Capsules Classico - Medium Roast', 'ايلي كبسولات  تحميص متوسط', '', '', 1, 297, 297),
(71, 39, 112, 'Ground Drip Classico Coffee - Medium Roast', 'ايلي (اسبريسو) تحميص متوسط', '', '', 1, 198, 198),
(72, 40, 109, 'Whole Salmon Fillet Side', 'جنب سلمون فيليه كامل', '', '', 1, 325, 325),
(73, 40, 107, 'Smoked Salmon Slices Skin On', 'سلمون مدخن شرائح بجلد', '', '', 1, 502, 502),
(74, 41, 8, 'Salmon Fillet', 'سلمون فيليه', '', '', 3, 113, 339),
(75, 41, 108, 'Smoked Salmon Slices Skin Off', 'سلمون مدخن شرائح بدون جلد', '', '', 2, 521.5, 1043),
(76, 41, 114, 'Ground Drip Classico Coffee - Medium Roast', 'ايلي (اسبريسو) تحميص متوسط', '', '', 4, 104.5, 418),
(77, 42, 8, 'Salmon Fillet', 'سلمون فيليه', '', '', 5, 113, 565),
(78, 42, 110, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '', '', 2, 70, 140),
(79, 42, 111, 'Salmon loin / Sashimi cut', 'سلمون ساشيمي', '', '', 2, 216, 432),
(80, 42, 138, 'Jacobs Espresso Lungo Coffee Capsules Compatible for Nespresso Machines', 'كبسولات قهوة جاكوبس متوافقة مع ماكينات نسبريسو 6 لونجو', '', '', 1, 91, 91),
(81, 43, 110, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '', '', 5, 70, 350),
(82, 43, 120, 'Ground Drip Decaffeinated Coffee', 'ايلي (اسبريسو) بدون كافيين', '', '', 1, 104.5, 104.5),
(83, 43, 229, 'Häagen-Dazs Belgian Chocolate Ice cream', 'هاجن داز - ايس كريم الشوكولاتة البلجيكية', '', '', 1, 128.5, 128.5),
(84, 43, 223, 'Häagen-Dazs Salted Caramel Ice Cream Bars', 'هاجن داز - ايس كريم بالكراميل المملح', '', '', 1, 42, 42),
(85, 44, 108, 'Smoked Salmon Slices Skin Off', 'سلمون مدخن شرائح بدون جلد', '', '', 2, 521.5, 1043),
(86, 44, 110, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '', '', 4, 70, 280),
(87, 45, 110, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '', '', 3, 70, 210),
(88, 45, 111, 'Salmon loin / Sashimi cut', 'سلمون ساشيمي', '', '', 1, 216, 216),
(89, 45, 2, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '', '', 1, 74, 74),
(90, 46, 8, 'Salmon Fillet', 'سلمون فيليه', '', '', 3, 113, 339),
(91, 46, 2, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '', '', 3, 74, 222),
(92, 47, 110, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '', '', 8, 70, 560),
(93, 48, 109, 'Whole Salmon Fillet Side', 'جنب سلمون فيليه كامل', '', '', 1, 325, 325),
(94, 48, 5, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '', '', 1, 147, 147),
(95, 48, 111, 'Salmon loin / Sashimi cut', 'سلمون ساشيمي', '', '', 1, 216, 216),
(96, 49, 241, 'Badoit Sparkling Water, 30 x 330ml', 'مياة طبيعية فوارة من بادويت، 330مل', '', '', 1, 688, 688),
(97, 50, 103, 'Fresh Smoked Salmon Slices', 'سلمون مدخن شرائح فريش', '', '', 4, 147, 588),
(98, 51, 116, 'iperEspresso Capsules Classico - Medium Roast', 'ايلي كبسولات  تحميص متوسط', '', '', 1, 297, 297),
(99, 51, 209, 'Evian Mineral Water, 12 x 1 Litre', 'إيفيان – مياه معدنية طبيعية 1 لتر × 12', '', '', 1, 445, 445),
(100, 51, 126, 'Ceylon Green Tea', 'شاي سيلان أخضر', '', '', 1, 212.5, 212.5),
(101, 52, 8, 'Salmon Fillet', 'سلمون فيليه', '', '', 1, 113, 113),
(102, 52, 106, 'Smoked Salmon Stripes', 'سلمون مدخن ستريبس', '', '', 1, 154, 154),
(103, 52, 105, 'Salmon Steak', 'سلمون ستيك', '', '', 1, 113, 113),
(104, 52, 111, 'Salmon loin / Sashimi cut', 'سلمون ساشيمي', '', '', 1, 216, 216),
(105, 53, 5, 'Frozen Smoked Salmon Slices', 'سلمون مدخن شرائح', '', '', 2, 147, 294),
(106, 53, 109, 'Whole Salmon Fillet Side', 'جنب سلمون فيليه كامل', '', '', 7, 325, 2275),
(107, 53, 107, 'Smoked Salmon Slices Skin On', 'سلمون مدخن شرائح بجلد', '', '', 1, 502, 502),
(108, 54, 108, 'Smoked Salmon Slices Skin Off', 'سلمون مدخن شرائح بدون جلد', '', '', 1, 521.5, 521.5),
(109, 54, 110, 'Smoked Salmon Trimmings', 'سلمون مدخن تريمنج', '', '', 1, 70, 70);

-- --------------------------------------------------------

--
-- Table structure for table `rejection_reasons`
--

CREATE TABLE `rejection_reasons` (
  `id` int(11) NOT NULL,
  `text_ar` varchar(225) CHARACTER SET utf8 DEFAULT NULL,
  `text_en` varchar(225) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `rejection_reasons`
--

INSERT INTO `rejection_reasons` (`id`, `text_ar`, `text_en`, `created_at`, `updated_at`) VALUES
(1, 'نطاق المبلغ', 'amount range', '2019-04-11 13:07:04', '2019-04-11 13:07:06'),
(2, 'خارج نطاق المحفظة', 'out of wallet range', '2019-04-11 13:08:26', '2019-04-11 13:08:29'),
(3, 'العنوان إلى بعيد', 'address to far', '2019-04-11 13:08:56', '2019-04-11 13:08:59'),
(4, 'vvvvccccc', 'vvv', '2019-04-23 09:37:55', '2019-04-23 09:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `comment` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `comment`, `created_at`, `updated_at`) VALUES
(1, 'الادارة', 'المخولين بالدخول', '2018-09-03 12:26:09', '2018-10-30 06:02:15'),
(2, 'الادارة 2', 'الإدارة 2', '2018-10-30 06:04:11', '2018-10-30 06:04:11'),
(3, 'my 1', 'my 1 go', '2019-02-03 15:36:18', '2019-02-03 15:36:18'),
(4, 'Products only', 'Products only', '2019-02-03 15:37:17', '2019-02-03 15:58:35');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`id`, `role_id`, `permission_id`) VALUES
(1, 4, 24),
(2, 4, 26),
(3, 4, 27),
(4, 4, 28),
(7, 4, 25),
(34, 4, 29),
(35, 4, 30),
(36, 4, 31);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `value`) VALUES
(24, 'our_location_en', '8 Fathy Talaat St., Sheraton Buildings, Heliopolis, Cairo, Egypt'),
(25, 'our_phone', ''),
(26, 'facebook_link', 'https://www.facebook.com/midodistributor/'),
(27, 'twitter_link', 'https://www.twitter.com/'),
(28, 'instagram_link', 'https://www.instagram.com/midodistributor/'),
(40, 'about_us_text_ar', 'بواسطة الإمام فندي. تعتبر عائلة فندي اللاعبين الرئيسيين للمنتجات الحلوة والحلويات في مصر. Fourrée ، هارد كاندي (كور بيزنس) ، كانوي سويتيز (Éclairs ، توفي) ، دراجيس ، شوكولا (مقولب ، قضبان / عدد الكيلومترات ، فردي) ، البسكويت (كلاسيك-فيلد) والويفر. Horreia هي الرائدة في السوق في السوق المحلية الموقف في صناعة الحلوى الصلبة. ما جعل Horreia أبديًا ومتزايدًا طوال هذه العقود ، هو إخلاص الأجيال الأولى والثانية من Fendi إلى الحب المستمر للابتكارات في المنتجات التي تقدم أوسع نطاق بين جميع المسابقات تقديم الحلوى الصلبة من جميع الأنواع والابتكارات ، سواء كانت كبيرة وصغيرة ومليئة بالمركز الطبيعي أو الاقتصاد كل نوع من الوصفات ، بالإضافة إلى التوفي ، مجموعة من النكهات المختلفة من الرقاقات المليئة بالشوكولاته ، كريمات الفانيلا وغيرها ، وتطويرها للأسواق المحلية والدولية ، وعناصر Govrit التي نطورها للسوق المحلية ، والفول السوداني المغلفة بالشوكولاته ، والحلوى وأكثر في قسم من dragee. وقد نجح الجميع أيضًا في الوصول إلى التصدير من جميع الشركات المحيطة في الشرق الأوسط وأفريقيا ، وصولًا إلى الولايات المتحدة الأمريكية وكندا.'),
(35, 'youtube_link', 'https://www.youtube.com/'),
(36, 'shipping_price', '0'),
(37, 'tax_price', '0'),
(38, 'linkedin_link', 'https://www.linkedin.com/company/midodistributor'),
(39, 'about_us_text_en', 'by Imam Fendi. Fendi’s Family is the key players of sweet producers and confectionary in Egypt Horreia’s product portfolio covers; Fourrée, Hard Candy (Core Business), Chewy Candies (Éclairs, Toffee), Dragees, Chocolate (Moulded, Bars/Count-lines, Single), Biscuits (Classic-Filled) and wafers. Horreia is Market-Leader in Domestic Market Position in hard candy industry. What made Horreia everlasting and growing throughout these decades, the devotion of Fendi’s 1st & 2nd generations into constant love of product innovations offering the widest range among all competition Offering hard candy of all types and innovations, be it large, small, center filled, natural or economy all sort of recipes, plus toffee, range of different flavors of Wafers filled with chocolate, Vanilla creams etc. and develop it for the domestic and international markets, and Govrit items we develop it for the domestic market, chocolate coated peanuts, candy and more into the section of dragee. That all succeeded also into the export reaching from all surrounding companies in middle east & Africa, all the way to USA & Canada.'),
(41, 'our_phone_1', '01205455553'),
(42, 'our_phone_2', '+202 2266 8412'),
(43, 'our_email', 'support@mido.com.eg'),
(44, 'minimum_basket_amount', '499'),
(45, 'minimum_freeTast_amount', '999'),
(46, 'our_location_ar', '8 ش فتحى طلعت ، مساكن شيراتون ، مصر الجديدة\r\nالقاهرة، مصر');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cart`
--

CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shopping_cart`
--

INSERT INTO `shopping_cart` (`id`, `member_id`, `product_id`, `quantity`, `created_at`) VALUES
(7, 5, 2, 6, '2019-07-11 11:12:33'),
(8, 5, 4, 1, '2019-07-11 11:12:45'),
(9, 5, 103, 1, '2019-07-11 11:12:49'),
(10, 5, 111, 1, '2019-07-11 11:12:52'),
(11, 5, 8, 1, '2019-07-11 11:12:54'),
(12, 5, 108, 1, '2019-07-11 11:16:16'),
(13, 8, 105, 3, '2019-07-12 16:18:01'),
(14, 8, 109, 1, '2019-07-12 16:19:18'),
(23, 12, 115, 1, '2019-07-17 13:58:04'),
(25, 13, 237, 2, '2019-07-17 22:59:07'),
(26, 13, 108, 1, '2019-07-17 22:59:41'),
(40, 16, 8, 1, '2019-07-22 16:59:46'),
(41, 16, 110, 1, '2019-07-22 16:59:48'),
(42, 18, 5, 5, '2019-07-22 17:05:32'),
(43, 18, 140, 1, '2019-07-22 17:05:41'),
(44, 19, 4, 1, '2019-07-22 18:15:25'),
(46, 22, 105, 1, '2019-07-22 18:37:01'),
(52, 27, 137, 2, '2019-07-23 19:34:55'),
(53, 27, 140, 1, '2019-07-23 19:36:17'),
(54, 27, 126, 1, '2019-07-23 19:38:00'),
(59, 24, 4, 1, '2019-07-24 11:48:49'),
(60, 24, 108, 1, '2019-07-24 11:48:50'),
(61, 29, 109, 4, '2019-07-24 12:03:59'),
(62, 30, 112, 2, '2019-07-24 13:11:30'),
(63, 30, 103, 1, '2019-07-24 13:13:03'),
(76, 32, 110, 2, '2019-07-25 16:17:53'),
(92, 37, 110, 1, '2019-07-26 19:03:10'),
(93, 38, 8, 2, '2019-07-27 00:35:55'),
(94, 38, 105, 1, '2019-07-27 00:36:13'),
(95, 38, 113, 1, '2019-07-27 00:36:54'),
(100, 40, 4, 2, '2019-07-27 17:56:29'),
(101, 33, 110, 1, '2019-07-27 22:31:46'),
(108, 43, 110, 4, '2019-07-28 18:26:33'),
(112, 44, 230, 1, '2019-07-28 22:07:50'),
(113, 44, 225, 1, '2019-07-28 22:08:16'),
(115, 44, 218, 1, '2019-07-28 22:10:45'),
(116, 44, 221, 1, '2019-07-28 22:11:00'),
(117, 44, 224, 1, '2019-07-28 22:11:33'),
(118, 44, 103, 1, '2019-07-28 22:13:29'),
(121, 44, 236, 1, '2019-07-28 22:17:00'),
(122, 44, 110, 1, '2019-07-28 22:18:53'),
(124, 44, 2, 1, '2019-07-28 22:21:48'),
(138, 47, 5, 1, '2019-07-30 15:41:47');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `banner_ar` varchar(225) DEFAULT NULL,
  `banner_en` varchar(225) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `name_en`, `name_ar`, `banner_ar`, `banner_en`, `created_at`, `updated_at`, `status`) VALUES
(4, 11, 'Salmon', 'سالمون', 'BannerAr_59530.jpg', 'BannerEn_37599.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(5, 11, 'Seafood', 'مأكولات بحرية', 'BannerAr_20163.jpg', 'BannerEn_99112.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(6, 12, 'Coffee', 'قهوة', 'BannerAr_11422.jpg', 'BannerEn_26228.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(7, 12, 'Tea', 'شاى', 'BannerAr_27135.jpg', 'BannerEn_31157.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(8, 12, 'Water', 'مياه', 'BannerAr_43962.jpg', 'BannerEn_19783.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(9, 13, 'Icecream', 'مثلجات', 'BannerAr_61909.png', 'BannerEn_80537.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(10, 14, 'Preserves', 'معلبات', 'BannerAr_95605.png', 'BannerEn_40846.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wish_list`
--

CREATE TABLE `wish_list` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wish_list`
--

INSERT INTO `wish_list` (`id`, `member_id`, `product_id`, `created_at`) VALUES
(1, 5, 108, '2019-07-11 11:16:07'),
(3, 17, 111, '2019-07-21 17:48:03'),
(4, 30, 112, '2019-07-24 13:11:51'),
(5, 30, 103, '2019-07-24 13:13:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `bad_rating_reasons_list`
--
ALTER TABLE `bad_rating_reasons_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_category_id` (`sub_category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `free_test_list`
--
ALTER TABLE `free_test_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page`
--
ALTER TABLE `home_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_applicants`
--
ALTER TABLE `job_applicants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_id` (`job_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `member_address`
--
ALTER TABLE `member_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `member_promo`
--
ALTER TABLE `member_promo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `promo_id` (`promo_id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `pages_banners`
--
ALTER TABLE `pages_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pdf_downloads`
--
ALTER TABLE `pdf_downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popular_questions`
--
ALTER TABLE `popular_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brand_id` (`brand_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `sub_category_id` (`sub_category_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipts`
--
ALTER TABLE `recipts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `free_test_id` (`free_test_id`);

--
-- Indexes for table `recipt_products`
--
ALTER TABLE `recipt_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipt_id` (`recipt_id`);

--
-- Indexes for table `rejection_reasons`
--
ALTER TABLE `rejection_reasons`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shopping_cart`
--
ALTER TABLE `shopping_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_ibfk_2` (`category_id`);

--
-- Indexes for table `wish_list`
--
ALTER TABLE `wish_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bad_rating_reasons_list`
--
ALTER TABLE `bad_rating_reasons_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `free_test_list`
--
ALTER TABLE `free_test_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `home_page`
--
ALTER TABLE `home_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `job_applicants`
--
ALTER TABLE `job_applicants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `member_address`
--
ALTER TABLE `member_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `member_promo`
--
ALTER TABLE `member_promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `pages_banners`
--
ALTER TABLE `pages_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pdf_downloads`
--
ALTER TABLE `pdf_downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `popular_questions`
--
ALTER TABLE `popular_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=244;

--
-- AUTO_INCREMENT for table `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `recipts`
--
ALTER TABLE `recipts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `recipt_products`
--
ALTER TABLE `recipt_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `rejection_reasons`
--
ALTER TABLE `rejection_reasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `shopping_cart`
--
ALTER TABLE `shopping_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wish_list`
--
ALTER TABLE `wish_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `brands_ibfk_2` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`);

--
-- Constraints for table `member_address`
--
ALTER TABLE `member_address`
  ADD CONSTRAINT `member_address_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `member_promo`
--
ALTER TABLE `member_promo`
  ADD CONSTRAINT `member_promo_ibfk_1` FOREIGN KEY (`promo_id`) REFERENCES `promo_codes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `products_ibfk_3` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`);

--
-- Constraints for table `recipts`
--
ALTER TABLE `recipts`
  ADD CONSTRAINT `recipts_ibfk_1` FOREIGN KEY (`free_test_id`) REFERENCES `free_test_list` (`id`);

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
